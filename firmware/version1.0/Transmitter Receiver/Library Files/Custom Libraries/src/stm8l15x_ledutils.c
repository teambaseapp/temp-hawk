/**
  ******************************************************************************
  * @file    stm8l15x_ledutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with LEDs connected 
  *          to the STM8L micro controller.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_ledutils.h"

#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
/**
  * @brief  Initialises GPIO pins connected to Green and Yellow LED
  * @param  None
  * @retval None
  */
void GPIOLEDInit()
{
  GPIO_Init( GREEN_LED_PORT, GREEN_LED, GPIO_Mode_Out_PP_Low_Fast );
  GPIO_Init( YELLOW_LED_PORT, YELLOW_LED, GPIO_Mode_Out_PP_Low_Fast );
}

/**
  * @brief  Switches on and off a specified LED.
  * @param  LED_Colour: It can be any one of the two
  *             @arg LED_YELLOW
  *             @arg LED_GREEN
  * @param  LED_Status: It can be any one of the two
  *             @arg LED_OFF
  *             @arg LED_ON
  * @retval none
  */
void OnOffLED( LED_COLOUR LED_Colour, LED_STATUS LED_Status )
{
  if( LED_Colour == LED_YELLOW )
  {
    if( LED_Status == LED_ON )
    {
      YELLOW_LED_PORT->ODR |= YELLOW_LED;                                       // GREEN LED ON
    }
    else
    {
      YELLOW_LED_PORT->ODR &= (uint8_t)(~YELLOW_LED);                           // YELLOW LED OFF      
    }
  }
  else
  {
    if( LED_Status == LED_ON )
    {
      GREEN_LED_PORT->ODR |= GREEN_LED;                                         // GREEN LED ON
    }
    else
    {
      GREEN_LED_PORT->ODR &= (uint8_t)(~GREEN_LED);                             // GREEN LED OFF
    }
  }
}

/**
  * @brief  Switches on and off a both LEDs (ideally to indicate a a faulty 
  *         state of the micro controller).
  * @param  none
  * @retval none
  */
void LEDFaultyBlink()
{
  uint8_t loop;
  // disable the watchdog timer
  IWDG->KR = 0;
  YELLOW_LED_PORT->ODR |= YELLOW_LED;                                           // YELLOW LED ON
  for( loop=0; loop<10; loop++ )
  {
    YELLOW_LED_PORT->ODR ^= YELLOW_LED;                                         // YELLOW LED TOGGLE
    GREEN_LED_PORT->ODR ^= GREEN_LED;                                           // GREEN LED TOGGLE
    
    RTC_SetWakeUpCounter( 1 );
    RTC_WakeUpCmd( ENABLE );
    halt();
    RTC_WakeUpCmd( DISABLE );
    CLK->CKDIVR = (uint8_t) CLK_SYSCLKDiv_1;                                    // clock divider initialized back to 1
  }
  YELLOW_LED_PORT->ODR &= (uint8_t)(~YELLOW_LED);                               // YELLOW LED OFF
  GREEN_LED_PORT->ODR &= (uint8_t)(~GREEN_LED);                                 // GREEN LED OFF
}

/**
  * @brief  Switches on and off a both LEDs (ideally to indicate a power on).
  * @param  none
  * @retval none
  */
void StartUpBlinkLED()
{
  uint8_t loop;
  YELLOW_LED_PORT->ODR |= YELLOW_LED;                                           // YELLOW LED ON
  for( loop=0; loop<4; loop++ )
  {
    YELLOW_LED_PORT->ODR ^= YELLOW_LED;                                         // YELLOW LED TOGGLE
    GREEN_LED_PORT->ODR ^= GREEN_LED;                                           // GREEN LED TOGGLE
    delay_ms( 100 );
  }
  YELLOW_LED_PORT->ODR &= (uint8_t)(~YELLOW_LED);                               // YELLOW LED OFF
  GREEN_LED_PORT->ODR &= (uint8_t)(~GREEN_LED);                                 // GREEN LED OFF
}


/**
  * @brief  Blinks Yellow LED to notify (typically to indicate an event).
  * @param  none
  * @retval none
  */
void NotifyYellowLED()
{
  for( uint8_t loop=0; loop<5; loop++ )
  {
    YELLOW_LED_PORT->ODR ^= YELLOW_LED;                                         // LED TOGGLE
    delay_ms( 100 );
  }
  YELLOW_LED_PORT->ODR &= (uint8_t)(~YELLOW_LED);                               // LED OFF
}

/**
  * @brief  Blinks Green LED to notify (typically to indicate an event).
  * @param  none
  * @retval none
  */
void NotifyGreenLED()
{
  for( uint8_t loop=0; loop<5; loop++ )
  {
    GREEN_LED_PORT->ODR ^= GREEN_LED;                                           // LED TOGGLE
    delay_ms( 100 );
  }
  GREEN_LED_PORT->ODR &= (uint8_t)(~GREEN_LED);                                 // LED OFF
}

#else
void StartUpBlinkLED()
{
  // dummy function to solve compilation error for GATEWAY
}

void LEDFaultyBlink()
{
  // dummy function to solve compilation error for GATEWAY
}
#endif
