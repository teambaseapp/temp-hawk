/**
  ******************************************************************************
  * @file    stm8l15x_clockutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with STM8L clock 
  *          and powerdown configuration.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_clockutils.h"

/**
  * @brief  Initialises the STM8L clock
  * @param  None
  * @retval None
  */
void ClockConf()
{
  /* Clock configration */
  CLK_DeInit();
  /*
  CLK->ECKCR = CLK_ECKCR_RESET_VALUE;
  CLK->CRTCR = CLK_CRTCR_RESET_VALUE;
  CLK->CBEEPR = CLK_CBEEPR_RESET_VALUE;
  CLK->SWCR = CLK_SWCR_RESET_VALUE;
  CLK->PCKENR1 = CLK_PCKENR1_RESET_VALUE;
  CLK->PCKENR2 = CLK_PCKENR2_RESET_VALUE;
  CLK->PCKENR3 = CLK_PCKENR3_RESET_VALUE;
  CLK->CSSR  = CLK_CSSR_RESET_VALUE;
  CLK->CCOR = CLK_CCOR_RESET_VALUE;
  CLK->HSITRIMR = CLK_HSITRIMR_RESET_VALUE;
  CLK->HSICALR = CLK_HSICALR_RESET_VALUE;
  CLK->HSIUNLCKR = CLK_HSIUNLCKR_RESET_VALUE;
  CLK->REGCSR = CLK_REGCSR_RESET_VALUE;
  */
  /*
  CLK_HSICmd( ENABLE );
  CLK_LSICmd( ENABLE );
  CLK_SYSCLKSourceConfig( CLK_SYSCLKSource_HSI );
  CLK_SYSCLKDivConfig( CLK_SYSCLKDiv_1 );
  */
  // HSI & LSI Enable
  CLK->ICKCR = (uint8_t) ~( CLK_ICKCR_HSION | CLK_ICKCR_LSION );
  // System Clock Source is HSI
  CLK->SWR = (uint8_t) CLK_SYSCLKSource_HSI;
  // System clock division is 1
  CLK->CKDIVR = (uint8_t) CLK_SYSCLKDiv_1;
}

/**
  * @brief  Sets up RTC peripheral for wakeup from active-halt timer.
  * @param  None
  * @retval None
  */
void SetUpRTC()
{
  /* Sets up RTC with the minimal required configuration */
  RTC_DeInit();

  /* Select LSE (38 KHz) as RTC clock source */
  // CLK_RTCClockConfig( CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_1 );
  CLK->CRTCR = (uint8_t)( (uint8_t) CLK_RTCCLKSource_LSI | (uint8_t) CLK_RTCCLKDiv_1 );
  CLK_PeripheralClockConfig( CLK_Peripheral_RTC, ENABLE );
  
  // initializes the RTC wakeup clock interrupt
  RTC_WakeUpClockConfig( RTC_WakeUpClock_CK_SPRE_16bits );
  RTC_ITConfig( RTC_IT_WUT , ENABLE );
}

/**
  * @brief  Configures the STM8L for low power (active-halt) mode
  * @note:  This function is to be called before going to the active-halt mode.
  * @param  None
  * @retval None
  */
void LowPowerConf()
{
#ifdef CODE_AS_SENSOR
  //I2CSoftReset( I2C_HTU20D_ADDRESS, HTU20D_SOFTRESET_REG );                     // ideally we should wait for 15ms for reset
  //HTU20DPowerDown();
  FLASH_DeInit();
  ADC_VRef_Disable();
#endif
  RFM69PowerDown();
  
  //ADC_VrefintCmd( DISABLE );
  ADC1->TRIGR[0] &= (uint8_t)(~ADC_TRIGR1_VREFINTON);
  //ADC_SchmittTriggerConfig( ADC1, ADC_Channel_Vrefint, DISABLE );
  ADC1->TRIGR[0] &= (uint8_t)(~(uint8_t) ADC_Channel_Vrefint);  
  
  //FLASH_PowerWaitModeConfig( FLASH_Power_IDDQ );
  FLASH->CR1 |= (uint8_t)FLASH_CR1_WAITM;
  
  //PWR_FastWakeUpCmd( ENABLE ); and PWR_UltraLowPowerCmd( ENABLE );
  PWR->CSR2 |= ( PWR_CSR2_FWU | PWR_CSR2_ULP );
  
  GPIO_DeInit( GPIOA );
  GPIO_DeInit( GPIOB );
  GPIO_DeInit( GPIOC );
  GPIO_DeInit( GPIOD );
  EXTI_DeInit();
  GPIO_Init( GPIOA, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow );
#ifdef CODE_AS_SENSOR
  GPIO_Init( GPIOB, GPIO_Pin_All ^ ( SENSOR1 | SENSOR2 | RFM69_POWER ) , GPIO_Mode_Out_PP_Low_Slow );
#endif
  GPIO_Init( GPIOC, GPIO_Pin_All ^ BUTTON, GPIO_Mode_Out_PP_Low_Slow );
  GPIO_Init( GPIOD, GPIO_Pin_All, GPIO_Mode_Out_PP_Low_Slow );
  GPIO_Init( GPIOB, RFM69_POWER , GPIO_Mode_Out_PP_High_Slow );
#ifdef CODE_AS_SENSOR
  GPIOExtiConf();
#endif
  
  RTC_ClearITPendingBit( RTC_IT_WUT );
  
  //IWDG_ReloadCounter();
  IWDG->KR = IWDG_KEY_REFRESH;
}

/**
  * @brief  Sets wake up counter and enters the low power mode.
  * @param  haltTime: It indicates the time for the wake up counter. It can be
  *             @arg zero: for unconditional halt mode
  *             @arg non-zero: time (in seconds) for active-halt mode
  * @retval None
  */
void EnterLowPowerMode( uint8_t haltTime )
{
  LowPowerConf();
  if( haltTime )                                                                // haltTime = 0 indicates error in peripheral checking
  {
    RTC_SetWakeUpCounter( haltTime );
    RTC_WakeUpCmd( ENABLE );
  }
  else
  {
    LEDFaultyBlink();
  }
  halt();
  
  // after halt()
  RTC_WakeUpCmd( DISABLE );
  CLK->CKDIVR = (uint8_t) CLK_SYSCLKDiv_1;                                      // clock divider initialized back to 1
  //asm( "db 0x75" );
}

/**
  * @brief  Changes the mode of operation of STM8L micro controller.
  * @param  None
  * @retval None
  */
void ChangePowerMode()
{
  if ( STM8L_Mode == STM8L_MODE_POWER_DOWN )
  {
    EnterLowPowerMode( STM8L_POWER_DOWN_TIME_SEC );                             // uC goes into halt mode
    STM8L_Mode = ( STM8L_Mode & STM8L_MODE_DEBUG ) | STM8L_MODE_POWER_UP;       // preserves STM8L_MODE_DEBUG if it is enabled
  }
  if ( ( STM8L_Mode & STM8L_MODE_POWER_UP ) == STM8L_MODE_POWER_UP )
  {
    SetUpPeripherals();
    STM8L_Mode = ( STM8L_Mode & STM8L_MODE_DEBUG ) | STM8L_MODE_PACKET_SEND_INITIATE;      // preserves STM8L_MODE_DEBUG if it is enabled
  }
}

/****************************INTERRUPT HANDLER**********************************/
/**
  * @brief  Interrupt Service Routine (ISR) to handle wake up timer interrupt.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER(RTC_CSSLSE_IRQHandler, 4)
{
  RTC_ClearITPendingBit( RTC_IT_WUT );
}