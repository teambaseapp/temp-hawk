/**
  ******************************************************************************
  * @file    stm8l15x_usartutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with USART peripheral.
  *          
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_usartutils.h"

/**
  * @brief  Sets up USART peripheral with the required settings.
  * @param  none
  * @retval none
  */
void SetUpUSART()
{
  /* Sets up USART with the minimal configation */
  USARTClockConf();
  // GPIOUSARTConf();                                                           // dealt with in main.c
  USARTReMapPin();
  USARTInit();
  USARTEnable();
  // enable all interrupts inorder to use interrupts
}

/**
  * @brief  Initialises the GPIO pins connected to USART peripheral
  * @param  None
  * @retval None
  */
void GPIOUSARTConf()
{
  /* GPIO Configration */
  GPIO_Init( USART_PORT, USART_TX, GPIO_Mode_Out_PP_High_Fast );
  GPIO_Init( USART_PORT, USART_RX, GPIO_Mode_In_PU_No_IT );
  
  //GPIO_ExternalPullUpConfig( USART_PORT, USART_TX, ENABLE );
  //GPIO_ExternalPullUpConfig( USART_PORT, USART_RX, ENABLE );
}

/**
  * @brief  Configures USART peripheral.
  * @param  none
  * @retval none
  */
void USARTInit()
{
  /* USART 1 initialization */
  USART_Init( USART, 9600, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, USART_Mode_Tx | USART_Mode_Rx );
  //USART_Init( USART, 57600, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, USART_Mode_Tx | USART_Mode_Rx );
  //USART_ITConfig( USART, USART_IT_TC, ENABLE );
  USART_ITConfig( USART, USART_IT_RXNE, ENABLE );
}

/**
  * @brief  Enables USART peripheral.
  * @param  none
  * @retval none
  */
void USARTEnable()                              // enable USART
{
  /* enable USART */
  USART_Cmd( USART, ENABLE );
}

/**
  * @brief  Gates USART peripheral clock.
  * @param  none
  * @retval none
  */
void USARTClockConf()                           // configures clock for USART
{
  CLK_PeripheralClockConfig( CLK_Peripheral_USART1, ENABLE );
}

/**
  * @brief  Remaps the USART output pins to PORT A, Pins 2 and 3.
  * @param  none
  * @retval none
  */
void USARTReMapPin()
{
  /* change functioinality of USART pins from default I/O types to USART function */
  SYSCFG_REMAPPinConfig( REMAP_Pin_USART1TxRxPortA, ENABLE );
}

#ifdef DEBUG_USART
/**
  * @brief  Sends the data packet information send via 'line' to the USART pin.
  * @note   This function disables all the interrupts, uses bit banging method 
  *         to send data and then re-enables the interrupt. Thus, some 
  *         interrupts may not be served during this time.
  * @param  line: The data packet array which needs to be send.
  * @retval none
  */
void USART_SendData( char * line )
{
  uint8_t i, offset;
  disableInterrupts();
  if( !CalculateOffset() )
    return;
  offset = GetOffset();
  for( i=offset+RFM69_DATA_INDEX_DEVICE_ID; i<RFM69_DATA_SIZE+offset-1; i++ )   // data from RFM69_DATA_INDEX_DEVICE_ID needs to be send
  {
    //USART_SendData8( USART1, line[i] );
    USART->DR = line[i];
    while( !USART_GetFlagStatus( USART, USART_FLAG_TC ) );
  }
  //USART_SendData8( USART1, line[i] );
  USART->DR = '\n';                                                             // to send a '\n' to indicate the end of data packet
  while( !USART_GetFlagStatus( USART, USART_FLAG_TC ) );

  USART_ClearITPendingBit( USART, USART_IT_TC );
  EXTI_ClearITPendingBit( EXTI_IT_Pin5 );
  enableInterrupts();
}

/**
  * @brief  Sends the character array refered to by 'line' to the USART pin
  * @note   This function disables all the interrupts, uses bit banging method 
  *         to send data and then re-enables the interrupt. Thus, some 
  *         interrupts may not be served during this time.
  * @note   The character array passed should be terminated by '\0'.
  * @param  line: The data packet array which needs to be send.
  * @retval none
  */
void USART_String( char *line )
{
  while( *line )
  {
    USART->DR = *line;
    while( !USART_GetFlagStatus( USART, USART_FLAG_TC ) );
    line++;
  }
}
#endif


/****************************INTERRUPT HANDLER**********************************/
#ifdef DEBUG_USART
/**
  * @brief  Interrupt Service Routine (ISR) to handle USART Rx. It can be used 
  *         by the device connected in the USART (LinkIT) to probe the micro 
  *         controller.
  * @param  none
  * @retval none
  */
INTERRUPT_HANDLER(USART1_RX_IRQHandler, 28)
{
  uint8_t receivedDigit;
  USART_ClearFlag( USART, USART_FLAG_RXNE );
  receivedDigit = USART_ReceiveData8( USART );
  
  if( receivedDigit == '*' )
  {
    asm( "db 0x75" );                                                           // resets the microcontroller
  }
  else if( receivedDigit == '+' )
  {
    USART_String( "OK" );
  }
}
#endif