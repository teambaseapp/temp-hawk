/**
  ******************************************************************************
  * @file    stm8l15x_extiutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with external 
  *          interrupt sources.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_extiutils.h"

#ifdef CODE_AS_SENSOR
/**
  * @brief  Sets up GPIO pins connected to external interrupts
  * @param  None
  * @retval None
  */
void GPIOExtiConf()
{
  GPIO_Init( BUTTON_PORT, BUTTON, GPIO_Mode_In_PU_IT );
  EXTI_SetPinSensitivity( EXTI_Pin_5, EXTI_Trigger_Rising );
  while( EXTI_GetPinSensitivity( EXTI_Pin_5 ) != EXTI_Trigger_Rising );         // PC5 = BUTTON
  
  GPIO_Init( SENSOR1_PORT, SENSOR1, GPIO_Mode_In_PU_IT );
  EXTI_SetPinSensitivity( EXTI_Pin_3, EXTI_Trigger_Rising_Falling );
  while( EXTI_GetPinSensitivity( EXTI_Pin_3 ) != EXTI_Trigger_Rising_Falling ); // PC3 = SENSOR1
  
  GPIO_Init( SENSOR2_PORT, SENSOR2, GPIO_Mode_In_PU_IT );
  EXTI_SetPinSensitivity( EXTI_Pin_2, EXTI_Trigger_Rising_Falling );
  while( EXTI_GetPinSensitivity( EXTI_Pin_2 ) != EXTI_Trigger_Rising_Falling ); // PC2 = SENSOR2
}
#endif

#ifdef CODE_AS_SENSOR
/**
  * @brief  Gets the value of the sensor connected to the STM8L
  * @param  None
  * @retval Returns the status of the sensors.
  */
uint8_t GetSensorStatus()
{
  uint8_t value = 0;
  if( GPIO_ReadInputDataBit( SENSOR1_PORT, SENSOR1 ) )
    value = RFM69_DATA_SENSOR1_OPEN;
  else
    value = RFM69_DATA_SENSOR1_CLOSED;
  
  if( GPIO_ReadInputDataBit( SENSOR2_PORT, SENSOR2 ) )
    value |= RFM69_DATA_SENSOR2_OPEN;
  else
    value |= RFM69_DATA_SENSOR2_CLOSED;
  
  return value;
}

/************************************INTERRUPT HANDLER**************************/
/**
  * @brief  Interrupt Service Routine (ISR) to handle GPIO pin 5 (button) interrupt.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER( EXTI5_IRQHandler, 13 )
{
  EXTI_ClearITPendingBit( EXTI_IT_Pin5 );
  if( STM8L_Mode == STM8L_MODE_POWER_DOWN )                                     // when power down, it initiates ending the data again
    STM8L_Mode = STM8L_MODE_DEBUG | STM8L_MODE_PACKET_SEND_INITIATE;
  else
    STM8L_Mode |= STM8L_MODE_DEBUG_INITIATE;                                    // while sending data if the interrup occurs, another packet is send again
  SetDeviceStatus();
}

/**
  * @brief  Interrupt Service Routine (ISR) to handle GPIO pin 3 (Sensor1)interrupt.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER( EXTI3_IRQHandler, 11 )
{
  EXTI_ClearITPendingBit( EXTI_IT_Pin3 );
  if( STM8L_Mode == STM8L_MODE_POWER_DOWN )                                     // when power down, it initiates sending the data again
    STM8L_Mode = STM8L_MODE_POWER_UP;
  else
    STM8L_Mode |= STM8L_MODE_POWER_UP_INITIATE;                                 // while sending data if the interrupt occurs, another packet is send again
}

/**
  * @brief  Interrupt Service Routine (ISR) to handle GPIO pin 2 (Sensor2) interrupt.
  * @param  None
  * @retval None
  */
INTERRUPT_HANDLER( EXTI2_IRQHandler, 10 )
{
  EXTI_ClearITPendingBit( EXTI_IT_Pin2 );
  if( STM8L_Mode == STM8L_MODE_POWER_DOWN )                                     // when power down, it initiates sending the data again
    STM8L_Mode = STM8L_MODE_POWER_UP;
  else
    STM8L_Mode |= STM8L_MODE_POWER_UP_INITIATE;                                 // while sending data if the interrupt occurs, another packet is send again
}
#endif