/**
  ******************************************************************************
  * @file    stm8l15x_timer3utils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with Timer 3 
  *          peripheral.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_timer3utils.h"

/**
  * @brief  Gates Timer 3 peripheral clock.
  * @param  none
  * @retval none
  */
void Timer3ClockConfig()
{
  CLK_PeripheralClockConfig( CLK_Peripheral_TIM3, ENABLE );
  /* Enables the TIM3 internal Clock */
  TIM3->SMCR &=  (uint8_t)(~TIM_SMCR_SMS);
}

/**
  * @brief  Configures Timer 3 peripheral.
  * @param  none
  * @retval none
  */
void Timer3Init()
{
  TIM3_TimeBaseInit( TIM3_Prescaler_16, TIM3_CounterMode_Up, 1 );
  TIM3_PrescalerConfig( TIM3_Prescaler_16, TIM3_PSCReloadMode_Immediate );
  TIM3_ARRPreloadConfig( DISABLE );
}

/**
  * @brief  Sets up Timer 3 peripheral with the required settings.
  * @param  none
  * @retval none
  */
void SetUpTimer3()
{
  //TIM3_DeInit();
  Timer3ClockConfig();
  Timer3Init();
}

/**
  * @brief  Delays the execution of the micro controller by the specified value
  *         (in milli seconds).
  * @param  ms: The value of delay time in milli seconds
  * @retval none
  */
void delay_ms( uint16_t ms )
{
  uint16_t i = 0;
  for(i=0; i<ms; i++)
  {
    delay_us( 1000 );
  }
}

/**
  * @brief  Delays the execution of the micro controller by the specified value
  *         (in micro seconds).
  * @param  us: The value of delay time in micro seconds
  * @retval none
  */
void delay_us( uint16_t us )
{
  if( us > 1000 )
  {
    delay_ms( us / 1000 );
    if( !( us = us % 1000 ) )
      return;
  }
  
  TIM3_SetAutoreload( us );
  
  TIM3_Cmd( ENABLE );
  
  while( TIM3_GetFlagStatus( TIM3_FLAG_Update ) == RESET);
  TIM3_ClearFlag( TIM3_FLAG_Update );
  
  TIM3_Cmd( DISABLE );
}