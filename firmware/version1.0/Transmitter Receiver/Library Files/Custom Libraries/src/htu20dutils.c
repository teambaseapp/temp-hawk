/**
  ******************************************************************************
  * @file    htu20dutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with HTU20D.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "htu20dutils.h"

#ifdef CODE_AS_SENSOR
/**
  * @brief  Initialises GPIO pins connected to HTU20D 
  * @param  None
  * @retval None
  */
void GPIOHTU20D()
{
  GPIO_Init( HTU20D_POWER_PORT, HTU20D_POWER, GPIO_Mode_Out_PP_Low_Slow );
}

/**
  * @brief  Powers Up HTU20D
  * @param  None
  * @retval None
  */
void HTU20DPowerUp()
{
  HTU20D_POWER_PORT->ODR |= HTU20D_POWER;
}

/**
  * @brief  Powers Down HTU20D
  * @param  None
  * @retval None
  */
void HTU20DPowerDown()
{
  HTU20D_POWER_PORT->ODR &= (uint8_t)(~HTU20D_POWER);
}

/**
  * @brief  Checks the I2C device by reading a register from the device via 
  *             I2C protocol.
  * @note   This function uses bit banging method to collect the value from
  *         the I2C device.
  * @param  deviceAddress: the address of the I2C device connected in the bus.
  * @retval Returns the 16 bit data collected form the I2C device.
  */
bool HTU20D_CheckDevice()
{
  //while( I2C_GetFlagStatus( I2C1, I2C_FLAG_BUSY ) );

  /* enabling acknowledgement */
  I2C->CR2 |= I2C_CR2_ACK;
  
  //I2C_GenerateSTART( I2C, ENABLE );
  I2C->CR2 |= I2C_CR2_START;
  delay_us( 100 );
  if( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_MODE_SELECT ) )
    return FALSE;
  //I2C_GenerateSTART( I2C, DISABLE );
  I2C->CR2 &= (uint8_t)(~I2C_CR2_START);
  
  //I2C_Send7bitAddress( I2C, deviceAddress, I2C_Direction_Transmitter );
  I2C->DR = ( I2C_HTU20D_ADDRESS << 1 ) & I2C_DEVICE_ENABLE_WRITE_MODE;
  delay_us( 100 );
  if( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED ) )
    return FALSE;
    
  I2C_SendData( I2C, HTU20D_SOFTRESET_REG );
  delay_us( 100 );
  if( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED ) )
    return FALSE;

  //I2C_GenerateSTOP( I2C, ENABLE );
  I2C->CR2 |= I2C_CR2_STOP;
  delay_us( 10 );
  //I2C_GenerateSTOP( I2C, DISABLE );
  I2C->CR2 &= (uint8_t)(~I2C_CR2_STOP);
  
  return TRUE;
}
#endif