/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with LEDs connected to the STM8L micro controller.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

#ifndef __STM8L15X_LEDUTILS_H__
#define __STM8L15X_LEDUTILS_H__

/* Includes ------------------------------------------------------------------*/
#include "header.h"

typedef enum
{
  LED_YELLOW    = 0x00,
  LED_GREEN     = 0x01
} LED_COLOUR;

typedef enum
{
  LED_OFF       = 0x00,
  LED_ON        = 0x01
} LED_STATUS;

void GPIOLEDInit();
void OnOffLED( LED_COLOUR LED_Colour, LED_STATUS LED_Status );
void LEDFaultyBlink();
void StartUpBlinkLED();
void NotifyYellowLED();
void NotifyGreenLED();

#endif