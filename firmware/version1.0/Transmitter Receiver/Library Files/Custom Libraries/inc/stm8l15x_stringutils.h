/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with the string functions used in the program. It 
  *          maintains and edits global character array variable 'RFM69_Data'.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

#ifndef __STM8L15X_STRINGUTILS_H__
#define __STM8L15X_STRINGUTILS_H__

/* Includes ------------------------------------------------------------------*/
#include "header.h"
#include "stm8l15x.h"

#define RFM69_DATA_SYNC_BYTE            0xFF
#define RFM69_DATA_CONFIG_ACK_BYTE      0x3D
#define RFM69_DATA_CONFIG_REQ_ACK       0xF0
#define RFM69_DATA_CONFIG_REQ_NOACK     0x0F

#define RFM69_DATA_SENSOR1_OPEN         0x02
#define RFM69_DATA_SENSOR1_CLOSED       0x00
#define RFM69_DATA_SENSOR2_OPEN         0x01
#define RFM69_DATA_SENSOR2_CLOSED       0x00

/** @defgroup Data_Packet_Details
  * @{
  */
#define RFM69_DATA_INDEX_SYNC_BYTE      0                                       /*!< Sync Byte */
#define RFM69_DATA_INDEX_CONFIG         ( RFM69_DATA_INDEX_SYNC_BYTE    + 1 )   /*!< Configuration Byte */
#define RFM69_DATA_INDEX_DEVICE_ID      ( RFM69_DATA_INDEX_CONFIG       + 1 )   /*!< Unique Device ID */
#define RFM69_DATA_INDEX_TEMPERATURE    ( RFM69_DATA_INDEX_DEVICE_ID    + 2 )   /*!< Temperature Raw Data */
#define RFM69_DATA_INDEX_HUMIDITY       ( RFM69_DATA_INDEX_TEMPERATURE  + 2 )   /*!< Humidity Raw Data */
#define RFM69_DATA_INDEX_SENSOR1        ( RFM69_DATA_INDEX_HUMIDITY     + 1 )   /*!< Sensor 1 Data */
#define RFM69_DATA_INDEX_SENSOR2        ( RFM69_DATA_INDEX_HUMIDITY     + 1 )   /*!< Sensor 2 Data */
#define RFM69_DATA_INDEX_BATTERY        ( RFM69_DATA_INDEX_HUMIDITY     + 2 )   /*!< Bateery Voltage Raw Data */
#define RFM69_DATA_INDEX_END_OF_PACKET  ( RFM69_DATA_INDEX_BATTERY      + 1 )   /*!< End of Packet Byte */
#define RFM69_DATA_SIZE                 ( RFM69_DATA_INDEX_END_OF_PACKET+ 1 )   /*!< Size of the Packet */
/**
  * @}
  */

extern uint8_t RFM69_Data[RFM69_DATA_SIZE];

uint8_t GetOffset();
bool CalculateOffset();
void GetData();
bool CheckDeviceID();
bool CheckAcknowledgement();
void GetAcknowledgementData();
bool CheckForAcknowledgementRequest();

#endif