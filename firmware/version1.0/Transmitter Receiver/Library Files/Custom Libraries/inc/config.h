/**
  ******************************************************************************
  * @file
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles how the source code should work. The source code is
  *          designed in such a way that it integrates both the Sensor and
  *          Gateway code. The code is compiled according to the preprocessor
  *          macors defined. It also takes care of the optional debugging
  *          code snippets which is used for debugging and thus may not be
  *          useful in production line products.
  *
  *  @verbatim
  *
  *  @endverbatim
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  *
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
#ifndef __CONFIG_H__
#define __CONFIG_H__

/* select the version number of TempHawk */
#define TEMPHAWK_VERSION_2_0

/* select if the code should act as a sensor or as a gateway */
#define CODE_AS_SENSOR
//#define CODE_AS_GATEWAY

#ifdef CODE_AS_SENSOR
//#define LED_FOR_DEBUG_OPERATION
#define LED_FOR_NORMAL_OPERATION
#endif

#ifdef CODE_AS_GATEWAY
#define DEBUG_USART
#endif

/*
 * Providing 70s would yeild an actual time of 60s due to the difference in
 * frequency. If 32.768KHz was used providing 60 would have done the deal,
 * but we are using internal low frequency clock clocked at 38KHz.
 */
#define STM8L_POWER_DOWN_TIME_SEC       70              // ideally 60s; but 70s to correct the RTC internal clock frequency difference


/*
 * Environment checking.
 */
#if defined(CODE_AS_SENSOR) && defined(CODE_AS_GATEWAY)
  #error "Please define the compilation process for either transmitter (Sensor) or receiver\
 (Gateway) and not for both. You have defined it to act as both, which is ambiguous.\
 Please refer 'config.h' file for more details."
#endif

#if !defined(CODE_AS_SENSOR) && !defined(CODE_AS_GATEWAY)
  #error "Please define the compilation process for either transmitter (Sensor) or receiver\
 (Gateway). You have not defined as to how the code should be compiled.\
 Please refer 'config.h\' file for more details."
#endif

//#pragma warning( disable : Pa050 )

#endif