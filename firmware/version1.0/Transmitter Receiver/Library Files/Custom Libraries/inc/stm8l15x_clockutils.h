/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with STM8L clock and powerdown configuration.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 
#ifndef __STM8L15X_RTCUTILS_H__
#define __STM8L15X_RTCUTILS_H__

/* Includes ------------------------------------------------------------------*/
#include "header.h"
#include "stm8l15x_rtc.h"
#include "stm8l15x_usartutils.h"

void ClockConf();
void SetUpRTC();
void LowPowerConf();
void EnterLowPowerMode( uint8_t haltTime );
void ChangePowerMode();

#endif