/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with I2C peripheral.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

#ifndef __STM8L15X_I2CUTILS_H__
#define __STM8L15X_I2CUTILS_H__

/* Includes ------------------------------------------------------------------*/
#include "header.h"
#include "stm8l15x_i2c.h"

#define I2C_DEVICE_ENABLE_WRITE_MODE    0xFE
#define I2C_DEVICE_ENABLE_READ_MODE     0x01

void SetUpI2C();
void GPIOI2CConf();
uint16_t I2CRead( uint8_t deviceAddress, uint8_t registerAddress );
void I2CWrite( uint8_t deviceAddress, uint8_t registerAddress, uint8_t data );
void I2CSoftReset( uint8_t deviceAddress, uint8_t registerAddress );

#endif