/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with the whole project. It also defines all the GPIO
  *          pins associated with the STM8L micro controller.
  *          
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

#ifndef __HEADER_H__
#define __HEADER_H__

#include "config.h"

#include "stm8l15x.h"
#include "stm8l15x_conf.h"
#include "stm8l15x_spiutils.h"
#include "stm8l15x_i2cutils.h"
#include "stm8l15x_usartutils.h"
#include "stm8l15x_timer3utils.h"
#include "stm8l15x_stringutils.h"
#include "stm8l15x_clockutils.h"
#include "stm8l15x_flashutils.h"
#include "stm8l15x_extiutils.h"
#include "stm8l15x_iwdgutils.h"
#include "stm8l15x_adcutils.h"
#include "stm8l15x_ledutils.h"

#include "rfm69registers.h"
#include "rfm69fsk.h"
#include "rfm69fskutils.h"

#include "htu20dutils.h"

#define USART                   USART1                                          // USART
#define REMAP_Pin_USARTTxRxPortA        REMAP_Pin_USART1TxRxPortA
#define USART_PORT              GPIOA
#define USART_TX                GPIO_Pin_2
#define USART_RX                GPIO_Pin_3

#define SPI                     SPI1                                            // SPI
#define CLK_Peripheral_SPI      CLK_Peripheral_SPI1
#define SPI_PORT                GPIOB
#define SPI_MISO                GPIO_Pin_7
#define SPI_MOSI                GPIO_Pin_6
#define SPI_SCL                 GPIO_Pin_5
#define SPI_CS                  GPIO_Pin_4


#ifdef CODE_AS_SENSOR
#define I2C                     I2C1                                            // I2C
#define CLK_Peripheral_I2C      CLK_Peripheral_I2C1
#define I2C_PORT                GPIOC
#define I2C_SDA                 GPIO_Pin_0
#define I2C_SCL                 GPIO_Pin_1
#define I2C_HOST_ADDRESS        0xA0

#define EXTI_RFM69_DIO0_PORT    GPIOC                                           // DIO0 of the RFM69 Sensor
#define EXTI_RFM69_DIO0         GPIO_Pin_6

#define GREEN_LED_PORT          GPIOB                                           // LED for visual indication
#define GREEN_LED               GPIO_Pin_0
#define YELLOW_LED_PORT         GPIOD                                           // LED for visual indication
#define YELLOW_LED              GPIO_Pin_0

#define BUTTON_PORT             GPIOC                                           // Press Switch
#define BUTTON                  GPIO_Pin_5
#define SENSOR1_PORT            GPIOB
#define SENSOR1                 GPIO_Pin_3
#define SENSOR2_PORT            GPIOB
#define SENSOR2                 GPIO_Pin_2

#define RFM69_POWER_PORT        GPIOB
#define RFM69_POWER             GPIO_Pin_1
#define HTU20D_POWER_PORT       GPIOC
#define HTU20D_POWER            GPIO_Pin_4
#endif


#ifdef CODE_AS_GATEWAY
#ifdef TEMPHAWK_VERSION_2_0
#define EXTI_RFM69_DIO0_PORT    GPIOC                                           // DIO0 of the RFM69 Gateway
#define EXTI_RFM69_DIO0         GPIO_Pin_6
#else
#define EXTI_RFM69_DIO0_PORT    GPIOB                                           // DIO0 of the RFM69 Gateway
#define EXTI_RFM69_DIO0         GPIO_Pin_2
#endif

#define BUTTON_PORT             GPIOB                                           // Press Switch
#define BUTTON                  GPIO_Pin_1

#define RFM69_POWER_PORT        GPIOC
#define RFM69_POWER             GPIO_Pin_4
#define U_485_POWER_PORT        GPIOB
#define U_485_POWER             GPIO_Pin_3
#endif

/** @defgroup STM8L_Mode
  * @{
  */
typedef enum                                                                    // each bit position represents a mode
{
  STM8L_MODE_POWER_DOWN                         = 0x00,                         /*!< Power down mode */
  STM8L_MODE_POWER_UP                           = 0x80,                         /*!< Power on mode */
  STM8L_MODE_PACKET_SEND_INITIATE               = 0x01,                         /*!< Packet send initiate request by an interrupt */
  STM8L_MODE_PACKET_SEND                        = 0x02,                         /*!< Packet send */
  STM8L_MODE_PACKET_RECEIVED                    = 0x04,                         /*!< Packet received */
  STM8L_MODE_POWER_UP_INITIATE                  = 0x10,                         /*!< Power on initiate request by an interrupt */
  STM8L_MODE_DEBUG                              = 0x40,                         /*!< Debug mode */
  STM8L_MODE_DEBUG_INITIATE                     = 0x20                          /*!< Debug initiate request by an interrupt */
} STM8L_MODE;
/**
  * @}
  */

extern STM8L_MODE STM8L_Mode;

void GPIOConf();
void GPIOLEDInit();
void InterruptPriority();
void ClockConf();
void CheckPeripherals();
void SetUpPeripherals();

#endif