/**
  ******************************************************************************
  *
  * @file
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   This is the main .c file
  *
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  *
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "header.h"

/**
  * @brief  Initialises the GPIO peripherals for the project.
  * @param  None
  * @retval None
  */
void GPIOConf()
{
  GPIO_DeInit( GPIOA );
  GPIO_DeInit( GPIOB );
  GPIO_DeInit( GPIOC );
  GPIO_DeInit( GPIOD );
  EXTI_DeInit();
  GPIOSPIConf();
  GPIORFM69();
#ifdef CODE_AS_SENSOR
  //GPIOI2CConf();
  GPIOHTU20D();
  GPIOExtiConf();
#elif defined(CODE_AS_GATEWAY)
  GPIOUSARTConf();
#endif
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
  GPIOLEDInit();
#endif
}

/**
  * @brief  Checks the initialisation and working of all the peripherals.
  *         If any of the peripherals is found to be non functional, then
  *         STM8L goes to halt mode indefenitely (to save power).
  * @param  None
  * @retval None
  */
void CheckPeripherals()
{
  /* RFM69 Peripheral Checking */
#ifdef LED_FOR_DEBUG_OPERATION
  OnOffLED( LED_YELLOW, LED_ON );                                               // LED to indicate error
#endif
#ifdef CODE_AS_SENSOR
  if( !( RFM69_CheckDevice() && HTU20D_CheckDevice() ) )
#elif defined(CODE_AS_GATEWAY)
  if( !( RFM69_CheckDevice() ) )
#endif
  {
    EnterLowPowerMode( 0 );                                                     // error with RFM69HW or HTU20D initialization detected
  }
#ifdef LED_FOR_DEBUG_OPERATION
  OnOffLED( LED_YELLOW, LED_OFF );                                              // LED to indicate error
  StartUpBlinkLED();
#endif
  //RST_ClearFlag( RST_FLAG_PORF );
  RST->SR = (uint8_t) RST_FLAG_PORF;
}

/**
  * @brief  Sets up the associated peripherals for the project.
  * @param  None
  * @retval None
  */
void SetUpPeripherals()
{
  /* set up all the associated peripherals */
  disableInterrupts();
  ClockConf();
  GPIOConf();
  SetUpTimer3();
#ifdef CODE_AS_SENSOR
  HTU20DPowerUp();
  PWR_FastWakeUpCmd( DISABLE );
  PWR_UltraLowPowerCmd( DISABLE );

#endif
  RFM69PowerUp();
  delay_ms( 15 );
  SetUpSPI();
  SetUpEEPROM();
#ifdef CODE_AS_SENSOR
  SetUpI2C();
  SetUpRTC();
  ADC_VRef_Init();
  delay_ms( 15 );
  RFM69_EntryTx();
  SetUpWatchDog();
#elif defined(CODE_AS_GATEWAY)
  SetUpUSART();
  delay_ms( 15 );
  RFM69_EntryRx();
#endif
  /* check the associated peripherals */
  //if( RST_GetFlagStatus( RST_FLAG_PORF )
  if( RST->SR & RST_FLAG_PORF )
  {
    CheckPeripherals();
  }
  /* enable interrupts */
  enableInterrupts();
}

STM8L_MODE STM8L_Mode = STM8L_MODE_POWER_UP;


/**
  * @brief  Program execution starts here.
  * @param  None
  * @retval None
  */
void main()
{
  ClockConf();                                                                  // needs to be done only once
  GPIOConf();                                                                   // needs to be done only once

#ifdef CODE_AS_GATEWAY
  SetUpPeripherals();
#endif

  while(1)
  {
#ifdef CODE_AS_SENSOR
    ChangePowerMode();
    SendData();
#endif

#ifdef CODE_AS_GATEWAY
    ReceiveData();
#endif
  }
}