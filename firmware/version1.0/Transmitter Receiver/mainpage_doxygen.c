/** 
@mainpage 

@section INTRODUCTION
 
@author  Allan Tom Mathew

	email: allantommathew@gmail.com

@version 1.0

@date    08th November, 2016


******************************************************************************
@attention <h2><b><center> BASEAPP SYSTEMS, 2016 </center></b></h2>  
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
******************************************************************************

@subsection STATUS

Working and Deployed.

@warning I2C start signal generation fails randomly. WatchDog timer has been implemented to hanle any types of situation like this. Thus it resets the device after waiting for a specified amount of time.


@subsection WORKING

Sensor sends burst of packet data via RFM69HW at 433.9MHz at an interval of 60 seconds and the Gateway receives it and pushes it to LinkIT. Both can be made to work in debug mode to make sure the whole set up is working properly.

It is a packet of data consisting of:

  * 1. Sync Byte                         1 byte
  * 2. Configuration Status              1 byte
  * 3. Device ID                         2 bytes
  * 4. Temperature raw data              2 bytes
  * 5. Humidity raw data                 14 bytes
  * 6. Sensor1 Status                    1 bit
  * 7. Sensor2 Status                    1 bit
  * 8. Battery Level                     1 byte
  * 9. End of packet                     1 byte
  *
To calculate Temperature    : 

	temperature  = -46.85 + 175.72 * temperatureReturnValue/65536;

To calculate Humidity       : 

	humidity     = -6 + 125 * humidityReturnValue/65536.0;

To calculate Battery Voltage: 

	voltage      = 1.2 * 255 / voltageReturnValue;


@subsection INTERRUPTS-USED

  * 1. Pin 5                             - for button press
  * 2. Pin 3                             - for sensor1 status detection
  * 3. Pin 2                             - for sensor2 status detection
  * 4. Wake up Timer Interrupt           - Active-Halt mode
  * 
@note RFM69HW, SPI, I2C and USART are not interrupt driven; bit banging method has been implemented

  */ 


