import os
from subprocess import check_output
import logging
from logging.handlers import RotatingFileHandler

ssd_logging = False
logger = ''
logpath = ""
loglevel = logging.DEBUG
logname = ""

def init_params(_logpath,_logname,_loglevel=10):
    global logpath,loglevel,logname
    logpath = _logpath
    loglevel = _loglevel
    logname = _logname    

def create_rotating_log(path):
    logger = logging.getLogger(logname)
    logger.setLevel(loglevel)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')        

    if ssd_logging:
	handler = RotatingFileHandler(path,maxBytes=1024*1024*1024,backupCount=10)
        handler.setFormatter(formatter)
	logger.addHandler(handler)
	    
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    
    return logger

def log(stuff,lvl=4):
    global logger
    if logger == '':        
        logger = create_rotating_log(logpath)
    
        
    
    {0:logger.critical,
     1:logger.error,
     2:logger.warning, 
     3:logger.info,
     4:logger.debug,
     }[lvl](stuff)

if not os.path.exists("/root/sdcard/temphawk.log"):
    try:
        check_output("mount /dev/mmcblk0p1 /root/sdcard", shell=True)
	ssd_logging = True
    except:
        print("Mount error",2)


