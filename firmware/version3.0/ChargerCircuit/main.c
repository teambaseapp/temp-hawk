#include "header.h"

/**
 * Functionality:
 * 1. Vinbattery > 'ADC_BATTERY_LINE_VOLTAGE_FULL_THRESHOLD'
 *      MOSFET  : ON
 *      GreenLED: ON (disabled)
 *      RedLED  : OFF
 * 2. ADC_BATTERY_LINE_VOLTAGE_CRITICALLY_LOW_THRESHOLD < Vinbattery < ADC_BATTERY_LINE_VOLTAGE_LOW_THRESHOLD
 *      MOSFET  : ON
 *      GreenLED: OFF
 *      RedLED  : OFF
 * 3. Vinbattery < ADC_BATTERY_LINE_VOLTAGE_CRITICALLY_LOW_THRESHOLD
 *      MOSFET  : OFF
 *      GreenLED: OFF
 *      RedLED  : BLINKS
 * 
 * ADC_BATTERY_LINE_VOLTAGE_FULL_THRESHOLD              = 13.8V
 * ADC_BATTERY_LINE_VOLTAGE_LOW_THRESHOLD               = 11.5V
 * ADC_BATTERY_LINE_VOLTAGE_CRITICALLY_LOW_THRESHOLD    = 11.0V
 * 
 * The resistor values for the voltage divider ciruit is:
 *      ______________o Vcc
 *      |
 *     |_| 10K
 *      |       
 *      |_____________o  Vinbattery
 *      |       
 *     |_| 5.6K       
 *      |
 *      |_____________o GND
 * Hardware has to be changed to give more range of values.
 * Min: 0V
 * Max: 13.8V
 */

uint16_t timeSec = 0;
uint16_t timeMilliSec = 0;
uint8_t oneSecUpdate = 0;
Errors_Typedef errors = ERROR_NIL;

void ClockInit()
{
  CLK_HSECmd(ENABLE);
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
  CLK_SYSCLKConfig(CLK_PRESCALER_HSIDIV1);
}

void GPIOInit()
{
  GPIO_Init(RED_LED_PORT, RED_LED_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
  GPIO_Init(GREEN_LED_PORT, GREEN_LED_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
  GPIO_Init(TRANSISTOR_CONTROL_PORT, TRANSISTOR_CONTROL_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
  
  GPIO_Init(VIN_SUPPLY_PORT, VIN_SUPPLY_PIN, GPIO_MODE_IN_FL_NO_IT);
  GPIO_Init(VIN_BATTERY_PORT, VIN_BATTERY_PIN, GPIO_MODE_IN_FL_NO_IT);
}

void Timer4Init()
{
  CLK_PeripheralClockConfig(CLK_PERIPHERAL_TIMER4, ENABLE);
  TIM4_TimeBaseInit(TIM4_PRESCALER_128, 124);                                   // 1ms cycle (value should be 125)
  TIM4_ITConfig(TIM4_IT_UPDATE, ENABLE);
  TIM4_ARRPreloadConfig(ENABLE);

  TIM4_Cmd(ENABLE);
  enableInterrupts();
}

void IndicateResponse()
{
  switch(errors)
  {
  case ERROR_BATTERY_FULL:
    SWITCH_TRANSISTOR_ON;
//    SWITCH_GREEN_LED_ON;
    SWITCH_RED_LED_OFF;
      break;
  case ERROR_BATTERY_CRITICALLY_LOW:
    SWITCH_TRANSISTOR_OFF;
    SWITCH_GREEN_LED_OFF;
    //SWITCH_RED_LED_ON;
    SWITCH_RED_LED_TOGGLE;
    break;
  case ERROR_BATTERY_LOW:
    SWITCH_TRANSISTOR_ON;
    SWITCH_GREEN_LED_OFF;
    SWITCH_RED_LED_OFF;
    break;
  default:
    SWITCH_TRANSISTOR_ON;
    SWITCH_GREEN_LED_OFF;
    SWITCH_RED_LED_OFF;
  }
}

 INTERRUPT_HANDLER(TIM4_UPD_OVF_IRQHandler, 23)
{
  if(TIM4_GetITStatus(TIM4_IT_UPDATE))
  {
    timeMilliSec++;
    TIM4_ClearITPendingBit(TIM4_IT_UPDATE);
    if(timeMilliSec >= 1000)
    {
      timeMilliSec = 0;
      timeSec++;
      oneSecUpdate = 1;
    }
  }
}

int main( void )
{
  ClockInit();
  GPIOInit();
  Timer4Init();
  while(1)
  {
    if(oneSecUpdate)
    {
      oneSecUpdate = 0;
      CheckBatteryLineVoltage();
      IndicateResponse();
      /*
      if(CheckBatteryLineVoltage() == TRUE)
      {
        oneSecUpdate = 0;
        SWITCH_TRANSISTOR_OFF;
        while(CheckBatteryLineVoltage() == TRUE);
        SWITCH_TRANSISTOR_ON;
        timeMilliSec = 0;
        timeSec = 0;
      }
      */
    }
    if(timeSec > 3600*4)
    {
      timeSec = 0;
      SWITCH_TRANSISTOR_OFF;
      while(1)
      {
        if(timeSec > 5)
        {
          timeSec = 0;
          SWITCH_TRANSISTOR_ON;
          break;
        }
      }
    }
  }
}

#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
