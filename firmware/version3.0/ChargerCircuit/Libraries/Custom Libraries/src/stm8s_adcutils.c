#include "stm8s_adcutils.h"

//source: http://www.electroons.com/blog/stm8-tutorials-3-adc-interfacing/

uint16_t ReadADC1(uint8_t channel)
{
  // ADC returns a 10-bit value
  unsigned int value=0;
  //using ADC in single conversion mode
  ADC1->CSR |= (ADC1_CSR_CH & channel);                                         // select channel
  ADC1->CR2 |= ADC1_ALIGN_RIGHT;                                                // Right Aligned Data
  ADC1->CR1 |= ADC1_CR1_ADON;                                                   // ADC ON (writing 1 to 0 powers up the ADC from sleep)
  ADC1->CR1 |= ADC1_CR1_ADON;                                                   // ADC Start Conversion (writing 1 to 1 initiates the ADC for conversion)
  while(((ADC1->CSR) & ADC1_CSR_EOC) == RESET);                                 // Wait till EOC
  value |= (unsigned int) ADC1->DRL;
  value |= (unsigned int) ADC1->DRH << 8;
  ADC1->CR1 &= ~(ADC1_CSR_EOC);                                                 // ADC Stop Conversion and put it to power down mode
  value &= 0x03ff;
  ADC1_DeInit();
  return value;
}

bool CheckBatteryLineVoltage()
{
  uint16_t adcValue = ReadADC1(VIN_BATTERY_ADC_CHANNEL);
  if(adcValue > ADC_BATTERY_LINE_VOLTAGE_FULL_THRESHOLD)
    errors = ERROR_BATTERY_FULL;
  else if(adcValue < ADC_BATTERY_LINE_VOLTAGE_CRITICALLY_LOW_THRESHOLD)
    errors = ERROR_BATTERY_CRITICALLY_LOW;
  else if(adcValue < ADC_BATTERY_LINE_VOLTAGE_LOW_THRESHOLD)
    errors = ERROR_BATTERY_LOW;
  else
    errors = ERROR_NIL;
  //return (bool)(errors == ERROR_BATTERY_CRITICALLY_LOW);
  if(errors == ERROR_BATTERY_CRITICALLY_LOW)
    return TRUE;
  
  return FALSE;
}