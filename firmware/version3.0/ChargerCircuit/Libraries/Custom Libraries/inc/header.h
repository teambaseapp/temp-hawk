#include "stm8s.h"

#define VIN_SUPPLY_PORT                 GPIOC
#define VIN_SUPPLY_PIN                  GPIO_PIN_6
//#define VIN_SUPPLY_ADC_CHANNEL          ADC1_CHANNEL_
#define VIN_BATTERY_PORT                GPIOD
#define VIN_BATTERY_PIN                 GPIO_PIN_2
#define VIN_BATTERY_ADC_CHANNEL         ADC1_CHANNEL_3

#define GREEN_LED_PORT                  GPIOD
#define GREEN_LED_PIN                   GPIO_PIN_5
#define RED_LED_PORT                    GPIOD
#define RED_LED_PIN                     GPIO_PIN_6

#define TRANSISTOR_CONTROL_PORT         GPIOD
#define TRANSISTOR_CONTROL_PIN          GPIO_PIN_3

#define SWITCH_TRANSISTOR_ON            GPIO_WriteHigh(TRANSISTOR_CONTROL_PORT, TRANSISTOR_CONTROL_PIN);
#define SWITCH_TRANSISTOR_OFF           GPIO_WriteLow(TRANSISTOR_CONTROL_PORT, TRANSISTOR_CONTROL_PIN);

#define SWITCH_GREEN_LED_ON             GPIO_WriteHigh(GREEN_LED_PORT, GREEN_LED_PIN);
#define SWITCH_GREEN_LED_OFF            GPIO_WriteLow(GREEN_LED_PORT, GREEN_LED_PIN);
#define SWITCH_GREEN_LED_TOGGLE         GPIO_WriteReverse(GREEN_LED_PORT, GREEN_LED_PIN);
#define SWITCH_RED_LED_ON               GPIO_WriteHigh(RED_LED_PORT, RED_LED_PIN);
#define SWITCH_RED_LED_OFF              GPIO_WriteLow(RED_LED_PORT, RED_LED_PIN);
#define SWITCH_RED_LED_TOGGLE           GPIO_WriteReverse(RED_LED_PORT, RED_LED_PIN);

typedef enum
{
  ERROR_NIL                             = 0,
  ERROR_BATTERY_FULL,
  ERROR_BATTERY_LOW,
  ERROR_BATTERY_CRITICALLY_LOW
} Errors_Typedef;

extern Errors_Typedef errors;

// pull high for switching it on

