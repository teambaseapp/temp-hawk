#ifndef __STM8S_ADCUTILS_H__
#define __STM8S_ADCUTILS_H__

#include "header.h"

#define ADC_SWITCH_PRESS_NONE                                   0xFF            //This val is returned by GetRemoteCmd when no key is pressed

#define ADC_BATTERY_LINE_VOLTAGE_FULL_THRESHOLD                 1023            //value corresponding to 13.8V
#define ADC_BATTERY_LINE_VOLTAGE_LOW_THRESHOLD                  890             //value corresponding to 12V point something
#define ADC_BATTERY_LINE_VOLTAGE_CRITICALLY_LOW_THRESHOLD       890             //value corresponding to 12V

/**
 * Battery line voltage value table
 *      Vbat            Vin(5V scale)           ADC value
 *      13.8            5                       1023                            
 *      13.6            4.927                   1009                            
 *      12              4.348                   890                             
 *      11.5            4.167                   853                             
 *      11.5            4.167                   853                             
 *      11.0            3.985                   816                             
 */

uint16_t readADC1(uint8_t channel);
bool CheckBatteryLineVoltage();
void ADCInit();
void ADCGetValue();

#endif //__STM8S_ADCUTILS_H__