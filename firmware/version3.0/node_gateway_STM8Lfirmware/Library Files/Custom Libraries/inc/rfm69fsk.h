/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with RFM69HW configurations and basic functionalities.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 
#ifndef __RFM69FSK_H__
#define __RFM69FSK_H__

#include "header.h"
#include "rfm69registers.h"
#include "rfm69fsk.h"

#define RFM69READMASK           0x7F
#define RFM69WRITEMASK          0x80
#define REG_TEMP1               0x4E
#define REG_TEMP2               0x4F
#define RF69_TEMP1_MEAS_START   0x09
#define RF69_TEMP1_MEAS_RUNNING 0x04
#define COURSE_TEMP_COEF        -89     // starter callibration figure

#define RFM69_FREQ_315          0
#define RFM69_FREQ_434          1
#define RFM69_FREQ_868          2
#define RFM69_FREQ_915          3

#define RFM69_RATE_1200         0
#define RFM69_RATE_2400         1
#define RFM69_RATE_4800         2
#define RFM69_RATE_9600         3

#define RFM69_POWER_20DB        0
#define RFM69_POWER_17DB        1
#define RFM69_POWER_14DB        2
#define RFM69_POWER_11DB        3

#define RFM69_REG_VERSION       0x10
#define RFM69_VERSION_NUMBER    0x24
    
    
#define FIXEDTHRESHOLDVALUE     80

/**********************************************************
**Variable define
**********************************************************/
extern uint8_t gb_WaitStableFlag;                                    //State stable flag
/*
extern uint8_t gb_FreqBuf_Addr;
extern uint8_t gb_RateBuf_Addr;
extern uint8_t gb_PowerBuf_Addr;
*/
extern uint8_t gb_StatusTx;
extern uint16_t gw_TxTimer;
extern uint8_t  gb_ErrorFlag;
extern uint16_t gw_RF_SentInterval; 
extern uint8_t  gb_SysTimer_1S;
extern uint8_t gb_StatusRx;

void RFM69_FSK_Config(void);
void RFM69_FSK_EntryTx(void);
void RFM69_FSK_EntryRx(void);
void RFM69_SlaveDisable();
void RFM69_SlaveEnable();
bool RFM69_CheckDevice();

uint8_t RFM69_Read( uint8_t address );
void RFM69_Write( uint16_t address_data );
void RFM69_BurstWrite( uint8_t address , uint8_t *packet , int no_of_bytes);
void RFM69_BurstRead( uint8_t address , uint8_t *packet ,int no_of_bytes );

#endif