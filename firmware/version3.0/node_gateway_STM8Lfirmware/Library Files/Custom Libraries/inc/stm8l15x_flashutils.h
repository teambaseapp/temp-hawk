/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with EEPROM.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

#ifndef __STM8L15X_FLASHUTILS_H__
#define __STM8L15X_FLASHUTILS_H__

/* Includes ------------------------------------------------------------------*/
#include "header.h"
#include "stm8l15x_flash.h"

#define EEPROM_ADDRESS_DEVICEID_BYTE0   (uint32_t) 0x00001002
#define EEPROM_ADDRESS_DEVICEID_BYTE1   (uint32_t)( EEPROM_ADDRESS_DEVICEID_BYTE0 + 1 )

#define EEPROM_ADDRESS_STM8L_MODE       (uint32_t)( EEPROM_ADDRESS_DEVICEID_BYTE1 + 1 )

void SetUpEEPROM();
void SetDeviceStatus();
uint8_t GetDeviceStatus();
void SetDeviceID( uint16_t deviceID );
uint16_t GetDeviceID();

#endif