/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with LEDs connected to the STM8L micro controller.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

#ifndef __STM8L15X_LEDUTILS_H__
#define __STM8L15X_LEDUTILS_H__

/* Includes ------------------------------------------------------------------*/
#include "header.h"

#define GREEN_LED_ON                    (GREEN_LED_PORT->ODR |= GREEN_LED)
#define GREEN_LED_OFF                   (GREEN_LED_PORT->ODR &= (uint8_t)(~GREEN_LED))
#define YELLOW_LED_ON                   (YELLOW_LED_PORT->ODR |= YELLOW_LED)
#define YELLOW_LED_OFF                  (YELLOW_LED_PORT->ODR &= (uint8_t)(~YELLOW_LED))

void GPIOLEDInit();
void LEDFaultyBlink();
void StartUpBlinkLED();
void NotifyYellowLED();
void NotifyGreenLED();

#endif