/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with HTU20D.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

#ifndef __HTU20DUTILS_H__
#define __HTU20DUTILS_H__

/* Includes ------------------------------------------------------------------*/
#include "header.h"

/** I2C address of HTU20D */
#define I2C_HTU20D_ADDRESS      0x40
/** Temperature register address */
#define HTU20D_TEMPERATURE_REG  0xE3
/** Humidity register address */
#define HTU20D_HUMIDITY_REG     0xE5
/** Soft reset for HTU20D */
#define HTU20D_SOFTRESET_REG    0xFE

void GPIOHTU20D();
void HTU20DPowerUp();
void HTU20DPowerDown();
bool HTU20D_CheckDevice();

#endif