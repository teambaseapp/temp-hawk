/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with USART peripheral.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

#ifndef __STM8L15X_USARTUTILS_H__
#define __STM8L15X_USARTUTILS_H__

/* Includes ------------------------------------------------------------------*/
#include "header.h"
#include "stm8l15x.h"
#include "stm8l15x_usart.h"
#include "stm8l15x_syscfg.h"

#define DATA_FROM_IRMS_SENSOR                   0x02
#define DATA_FROM_OOK_RELAY_SENSOR              0x01
#define DATA_FROM_NORMAL_SENSOR                 0x00

void SetUpUSART();
void GPIOUSARTConf();
void USARTInit();
void USARTEnable();
void USARTClockConf();
void USARTReMapPin();
void USART_SendData( char * line );
void USART_String( char *line );

#endif