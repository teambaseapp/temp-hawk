#ifndef __STM8L15x_TIMER4UTILS_H__
#define __STM8L15x_TIMER4UTILS_H__

#include "header.h"
#include "stm8l15x.h"
#include "stm8l15x_it.h"
#include "stm8l15x_itc.h"
#include "stm8l15x_clk.h"
#include "stm8l15x_tim4.h"
//#include "micros.h"

void Timer4ClockConfig();
void Timer4Init();
void Timer4IT();
void Timer4Enable();
void SetUpTimer4();

extern volatile uint32_t micros;


#endif