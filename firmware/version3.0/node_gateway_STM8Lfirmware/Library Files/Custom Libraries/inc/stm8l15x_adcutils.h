/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with ADC and internal reference voltage (VREF).
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 
#ifndef __STM8L15X_ADCUTILS_H__
#define __STM8L15X_ADCUTILS_H__

/* Includes ------------------------------------------------------------------*/
#include "header.h"
#include "stm8l15x_adc.h"
#include "math.h"

#define ICAL            89.77

void ADC_VRef_Init(void);
uint8_t ADC_VRef_Measure(void);
void ADC_VRef_Disable();
uint8_t GetVRefValue();
uint16_t GetIrms();

#endif