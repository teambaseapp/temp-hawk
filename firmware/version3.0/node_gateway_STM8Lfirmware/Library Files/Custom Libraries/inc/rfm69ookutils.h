#ifndef __RFM69OOKUTILS_H__
#define __RFM69OOKUTILS_H__

#include "header.h"
#include "stm8l15x.h"
#include "rfm69ook.h"
#include "stm8l15x_exti.h"

struct HighLow 
{
     uint8_t high;
     uint8_t low;
};

struct Protocol 
{
    int pulseLength;
    struct HighLow syncFactor;
    struct HighLow zero;
    struct HighLow one;
        // @brief if true inverts the high and low logic levels in the HighLow structs
    bool invertedSignal;
};

/****************************Receive Prototypes********************************/

#define RCSWITCH_MAX_CHANGES 67

void GPIORxConf();
void ExtiInit();
void SetUpRxIT();
void SetRx();
void enableReceive();
void disableReceive();
bool available();
void resetAvailable();
void SendOOKData();
unsigned long getReceivedValue();
unsigned int getReceivedBitlength();
unsigned int getReceivedDelay();
unsigned int getReceivedProtocol();
unsigned int* getReceivedRawdata();
void setReceiveTolerance(int nPercent);
void rx_setProtocol(int nProtocol);
static void handleInterrupt();
static bool receiveProtocol(const int p, unsigned int changeCount);

static int nReceiveTolerance;
static unsigned long nReceivedValue;
static unsigned int nReceivedBitlength;
static unsigned int nReceivedDelay;
static unsigned int nReceivedProtocol;
const static unsigned int nSeparationLimit = 4600;
static unsigned int timings[RCSWITCH_MAX_CHANGES];

#endif //__RFM69OOKUTILS_H__