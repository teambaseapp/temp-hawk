/**
  ******************************************************************************
  * @file    
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function prototypes, macros, typedefs and enums 
  *          associated with RFM69HW.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 
#ifndef __RFM69FSKUTILS_H__
#define __RFM69FSKUTILS_H__

#define   nIRQ0         (EXTI_RFM69_DIO0_PORT->IDR & (uint8_t)EXTI_RFM69_DIO0)

#define RFM69_FSK_MODEREADY_TX    0xA0
#define RFM69_FSK_MODEREADY_RX    0xC0

/* Includes ------------------------------------------------------------------*/
#include "header.h"
#include "stm8l15x_spi.h"
#include "rfm69fsk.h"

typedef enum
{
  RFM69_FSK_CHANGEMODE_TX = 0x00,
  RFM69_FSK_CHANGEMODE_RX = 0x01
} RFM69_FSK_ChangeModeTypedef;

void GPIORFM69();
void RFM69TxIT();
void RFM69PowerUp();
void RFM69PowerDown();
void SetUpRFM69_Tx();
void RFM69_ClearFIFO(void);
void RFM69_Sleep(void);
void RFM69_Tx_Mode( void );
void RFM69_Rx_Mode( void );
void RFM69_Standby(void);
void RFM69_Sleep_Int(void);
void RFM69Restart();
void TransmitPacket();
void SendData();
void RFM69_FSK_ChangeMode( RFM69_FSK_ChangeModeTypedef RFM69_NewMode );

void ReceiveData();

#endif