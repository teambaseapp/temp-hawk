#ifndef __RFM69OOK_H__
#define __RFM69OOK_H__

#include "header.h"
#include "stm8l15x.h"
#include "stm8l15x_spi.h"

#define RFM69READMASK           0x7F
#define RFM69WRITEMASK          0x80
#define RF69_TEMP1_MEAS_START   0x09
#define RF69_TEMP1_MEAS_RUNNING 0x04
#define COURSE_TEMP_COEF        -89             // starter callibration figure

#define NULL                    (char *) 0
#define RF69OOK_FSTEP 61.03515625               // == FXOSC/2^19 = 32mhz/2^19 (p13 in DS)

typedef enum
{
  RFM69_OOK_MODE_SLEEP       = 0x10,              // XTAL OFF => SLEEP 
  RFM69_OOK_MODE_STANDBY     = 0x11,              // XTAL ON => STANDBY 
  RFM69_OOK_MODE_SYNTH       = 0x12,              // PLL ON 
  RFM69_OOK_MODE_RX          = 0x13,              // RX MODE 
  RFM69_OOK_MODE_TX          = 0x14               // TX MODE 
} RFM69_OOK_MODE_TypeDef;

void RFM69_InitOOK();
void RFM69_Write_OOK( uint8_t address, uint8_t data );
void RFM69_OOK_ModeSwitch( RFM69_OOK_MODE_TypeDef mode );
uint8_t RF69_ReadTemperature(uint8_t userCalibration);

/*
bool poll();
void send( BitAction signal );
*/
void transmitBegin();
void transmitEnd();
void receiveBegin();
void receiveEnd();
void setHighPower(bool onOff);
void setHighPowerRegs(bool onOff);
void setPowerLevel(uint8_t powerLevel);
uint32_t getFrequency();
void setFrequencyMHz(float f);
void setFrequency(uint32_t freqHz);
void setBitrate(uint32_t bitrate);
void setBandwidth(uint8_t bw);
void setRSSIThreshold(int8_t rssi);
void setFixedThreshold(uint8_t threshold);
void setSensitivityBoost(uint8_t value);
void sleep();
int8_t readRSSI(bool forceTrigger);
void rcCalibration();

#endif