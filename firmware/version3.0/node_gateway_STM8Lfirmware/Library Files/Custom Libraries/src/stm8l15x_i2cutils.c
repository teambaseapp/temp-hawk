/**
  ******************************************************************************
  * @file    stm8l15x_i2cutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with I2C peripheral.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_i2cutils.h"

#ifdef CODE_AS_SENSOR
/**
  * @brief  Sets up I2C peripheral with the required settings.
  * @param  None
  * @retval None
  */
void SetUpI2C()
{
  I2C_DeInit( I2C );
  
  //CLK_PeripheralClockConfig( CLK_Peripheral_I2C, ENABLE );
  CLK->PCKENR1 |= 1 << CLK_Peripheral_I2C;                                      // enable I2C clock peripheral

  I2C_Init( I2C, 100000, I2C_HOST_ADDRESS, I2C_Mode_I2C, I2C_DutyCycle_2, I2C_Ack_Enable, I2C_AcknowledgedAddress_7bit );
  //I2C_Cmd( I2C, ENABLE );
  I2C->CR1 |= I2C_CR1_PE;
  
  //I2C_GenerateSTART( I2C, DISABLE );
  //I2C->CR2 &= (uint8_t)(~I2C_CR2_START);
  //I2C_GenerateSTOP( I2C, DISABLE );
  //I2C->CR2 &= (uint8_t)(~I2C_CR2_STOP);
}

/**
  * @brief  Initialises the GPIO pins connected to HTU20D via I2C protocol.
  * @param  None
  * @retval None
  */
void GPIOI2CConf()
{
  GPIO_Init( I2C_PORT, I2C_SDA | I2C_SCL, GPIO_Mode_Out_OD_HiZ_Fast );
  //GPIO_Init( I2C_PORT, I2C_SCL, GPIO_Mode_Out_OD_HiZ_Fast );
}

/**
  * @brief  Reads a 16-bit value from any device via I2C protocol.
  * @note   This function uses bit banging method to collect the value from
  *         the I2C device.
  * @param  deviceAddress: the address of the I2C device connected in the bus.
  * @retval Returns the 16 bit data collected form the I2C device.
  */
uint16_t I2CRead( uint8_t deviceAddress, uint8_t registerAddress )
{
  uint16_t returnValue;
  uint8_t delay;
  //while( I2C_GetFlagStatus( I2C1, I2C_FLAG_BUSY ) );
  
  /* starting the I2C communication with write instruction */
  //I2C_GenerateSTART( I2C, ENABLE );
  I2C->CR2 |= I2C_CR2_START;
  delay_us( 100 );
  if( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_MODE_SELECT ) )
  {
#ifdef LED_FOR_DEBUG_OPERATION
    //NotifyYellowLED();
#endif
    //asm( "db 0x75" );
    STM8L_Error = STM8L_ERROR_HTU20D_I2C_NOT_RESPONDING;
    return 0;
  }
  //I2C_GenerateSTART( I2C, DISABLE );
  I2C->CR2 &= (uint8_t)(~I2C_CR2_START);
  
  //I2C_Send7bitAddress( I2C, deviceAddress<<1, I2C_Direction_Receiver );
  I2C->DR = (deviceAddress << 1) & I2C_DEVICE_ENABLE_WRITE_MODE;
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED ) );
  for( delay=0; delay<10; delay++);
  
  I2C_SendData( I2C, registerAddress );
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED ) );
  for( delay=0; delay<10; delay++);
  
  /* restarting the I2C communication with read instruction */
  //I2C_GenerateSTART( I2C, ENABLE );
  I2C->CR2 |= I2C_CR2_START;
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_MODE_SELECT ) );
  for( delay=0; delay<10; delay++);
  
  //I2C_Send7bitAddress( I2C, deviceAddress<<1, I2C_Direction_Receiver );
  I2C->DR = (deviceAddress << 1) | I2C_DEVICE_ENABLE_READ_MODE;
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED ) );
  for( delay=0; delay<10; delay++);
  
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_BYTE_RECEIVED ) );
  returnValue = I2C_ReceiveData( I2C );
  
  /* disabling ACK inorder to generate NACK */
  //I2C->CR2 &= (uint8_t)(~I2C_CR2_ACK);
  returnValue = ( (uint8_t) returnValue ) << 8;
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_BYTE_RECEIVED ) );
  returnValue |= (uint8_t) I2C_ReceiveData( I2C );
  
  /*
  returnValue = 0;
  for( uint8_t loop=0; loop<numberOfBytes; loop++ )
  {
    if( loop )
    {
      returnValue = returnValue<<8;
      // disabling ACK inorder to generate NACK 
      I2C->CR2 &= (uint8_t)(~I2C_CR2_ACK);                                      // assuming max 2 bytes are read by max with this function
    }
    while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_BYTE_RECEIVED ) );
    returnValue |= I2C_ReceiveData( I2C );
  }
  */
  
  
  //I2C_GenerateSTOP( I2C, ENABLE );
  I2C->CR2 |= I2C_CR2_STOP;
  for( delay=0; delay<10; delay++);
  I2C->CR2 |= I2C_CR2_STOP;
 
  /* enabling acknowledgement */
  I2C->CR2 |= I2C_CR2_ACK;
  delay_us( 10 );
  //I2C_GenerateSTOP( I2C, DISABLE );
  I2C->CR2 &= (uint8_t)(~I2C_CR2_STOP);

  return returnValue;
}

/**
  * @brief  Writes a 16-bit value to any device via I2C protocol.
  * @note   This function uses bit banging method to write the value on
  *         the I2C device.
  * @param  deviceAddress: the address of the I2C device connected in the bus.
  * @param  registerAddress: destination register address.
  * @param  data: data to be written.
  * @retval Returns the 16 bit data collected form the I2C device.
  */
void I2CWrite( uint8_t deviceAddress, uint8_t registerAddress, uint8_t data )
{
  //while( I2C_GetFlagStatus( I2C1, I2C_FLAG_BUSY ) );

  /* enabling acknowledgement */
  I2C->CR2 |= I2C_CR2_ACK;
  //I2C_GenerateSTART( I2C, ENABLE );
  I2C->CR2 |= I2C_CR2_START;
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_MODE_SELECT ) );
  //I2C_GenerateSTART( I2C, DISABLE );
  I2C->CR2 &= (uint8_t)(~I2C_CR2_START);
  
  //I2C_Send7bitAddress( I2C, deviceAddress<<1, I2C_Direction_Transmitter );
  I2C->DR = (deviceAddress << 1) & I2C_DEVICE_ENABLE_WRITE_MODE;
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED ) );
  
  I2C_SendData( I2C, registerAddress );
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED ) );
  
  I2C_SendData( I2C, data );
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED ) );
  
  //I2C_GenerateSTOP( I2C, ENABLE );
  I2C->CR2 |= I2C_CR2_STOP;
  delay_us( 10 );
  //I2C_GenerateSTOP( I2C, DISABLE );
  I2C->CR2 &= (uint8_t)(~I2C_CR2_STOP);
}

/**
  * @brief  Sends a soft reset command to any device via I2C protocol.
  * @note   This function uses bit banging method to write the value on
  *         the I2C device.
  * @param  deviceAddress: the address of the I2C device connected in the bus.
  * @param  registerAddress: the command for soft reset.
  * @retval Returns the 16 bit data collected form the I2C device.
  */
void I2CSoftReset( uint8_t deviceAddress, uint8_t registerAddress )
{
  //while( I2C_GetFlagStatus( I2C1, I2C_FLAG_BUSY ) );

  /* enabling acknowledgement */
  I2C->CR2 |= I2C_CR2_ACK;
  
  //I2C_GenerateSTART( I2C, ENABLE );
  I2C->CR2 |= I2C_CR2_START;
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_MODE_SELECT ) );
  //I2C_GenerateSTART( I2C, DISABLE );
  I2C->CR2 &= (uint8_t)(~I2C_CR2_START);
  
  //I2C_Send7bitAddress( I2C, deviceAddress, I2C_Direction_Transmitter );
  I2C->DR = ( deviceAddress << 1 ) & I2C_DEVICE_ENABLE_WRITE_MODE;
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED ) );
    
  I2C_SendData( I2C, registerAddress );
  while( !I2C_CheckEvent( I2C, I2C_EVENT_MASTER_BYTE_TRANSMITTED ) );

  //I2C_GenerateSTOP( I2C, ENABLE );
  I2C->CR2 |= I2C_CR2_STOP;
  delay_us( 10 );
  //I2C_GenerateSTOP( I2C, DISABLE );
  I2C->CR2 &= (uint8_t)(~I2C_CR2_STOP);
}
#endif