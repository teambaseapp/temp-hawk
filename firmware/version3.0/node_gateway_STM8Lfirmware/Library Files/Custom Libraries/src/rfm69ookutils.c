#include "rfm69ookutils.h"

#ifdef CODE_AS_SENSOR_OOK

/* 
 * stm8l15x_extiutils.c
 */

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_exti.h"
#include "rfm69ookutils.h"

static unsigned long nReceivedValue = 0;
struct Protocol rx_protocol;
struct Protocol protocol;
static struct Protocol proto[] = 
{
  { 350, {  1, 31 }, {  1,  3 }, {  3,  1 }, FALSE },    // protocol 1
  { 650, {  1, 10 }, {  1,  2 }, {  2,  1 }, FALSE },    // protocol 2
  { 100, { 30, 71 }, {  4, 11 }, {  9,  6 }, FALSE },    // protocol 3
  { 380, {  1,  6 }, {  1,  3 }, {  3,  1 }, FALSE },    // protocol 4
  { 500, {  6, 14 }, {  1,  2 }, {  2,  1 }, FALSE },    // protocol 5
  { 450, { 23,  1 }, {  1,  2 }, {  2,  1 }, TRUE }      // protocol 6 (HT6P20B)
};

enum 
{
   numProto = sizeof(proto) / sizeof(proto[0])
};

void GPIORxConf()
{
  GPIO_Init( EXTI_RFM69_DIO2_PORT, EXTI_RFM69_DIO2, GPIO_Mode_In_PU_IT );
  EXTI_SetPinSensitivity( EXTI_Pin_6, EXTI_Trigger_Rising_Falling );
  while( EXTI_GetPinSensitivity( EXTI_Pin_6 ) != EXTI_Trigger_Rising_Falling ); // PC6 = DIO2
}

void SetRx() 
{
  rx_setProtocol( 1 );
  setReceiveTolerance( 60 );
  nReceivedValue = 0;
  nReceivedBitlength = 0;
}

void rx_setProtocol(int nProtocol) 
{
  if (nProtocol < 1 || nProtocol > numProto) 
  {
    nProtocol = 1;
  }
  rx_protocol = proto[nProtocol-1];
}

void setReceiveTolerance(int nPercent) 
{
  nReceiveTolerance = nPercent;
}

unsigned long getReceivedValue() 
{
  return nReceivedValue;
}

unsigned int getReceivedBitlength() 
{
  return nReceivedBitlength;
}

unsigned int getReceivedProtocol() 
{
  return nReceivedProtocol;
}

void resetAvailable() 
{
  nReceivedValue = 0;
}

void SendOOKData()
{
  if(getReceivedValue())                                                        // fetch the received value
  {
#ifdef LED_FOR_DEBUG_OPERATION                                                  // LED to indicate error
    StartUpBlinkLED();
#endif
#ifdef LED_FOR_NORMAL_OPERATION
    GREEN_LED_PORT->ODR |= GREEN_LED;
#endif
    delay_ms(50);
    //RFM69_FSK_EntryRx();
    RFM69_FSK_EntryTx();
    while( ( RFM69_Read( 0x27 ) ) & RFM69_FSK_MODEREADY_TX != RFM69_FSK_MODEREADY_TX );
    STM8L_Mode = STM8L_MODE_PACKET_SEND_INITIATE;
    SendData();
    RFM69_InitOOK();
    RFM69_OOK_ModeSwitch(RFM69_OOK_MODE_RX);
    resetAvailable();
#ifdef LED_FOR_NORMAL_OPERATION
    GREEN_LED_PORT->ODR &= (~GREEN_LED);
#endif
  }
}

long int diff( uint32_t value1, uint32_t value2 )
{
  long int val = value1 - value2;
  if( val < 0 )
  {
    val = -val;
  }
  return val;
}

bool receiveProtocol(const int p, unsigned int changeCount) 
{  
  struct Protocol pro = proto[p-1];
  unsigned long code = 0;
  //Assuming the longer pulse length is the pulse captured in timings[0]
  const unsigned int syncLengthInPulses =  ((pro.syncFactor.low) > (pro.syncFactor.high)) ? (pro.syncFactor.low) : (pro.syncFactor.high);
  const unsigned int delay = timings[0] / syncLengthInPulses;
  const unsigned int delayTolerance = delay * nReceiveTolerance / 100;
  
  /* For protocols that start low, the sync period looks like
   *               _________
   * _____________|         |XXXXXXXXXXXX|
   *
   * |--1st dur--|-2nd dur-|-Start data-|
   *
   * The 3rd saved duration starts the data.
   *
   * For protocols that start high, the sync period looks like
   *
   *  ______________
   * |              |____________|XXXXXXXXXXXXX|
   *
   * |-filtered out-|--1st dur--|--Start data--|
   *
   * The 2nd saved duration starts the data
   */
  const unsigned int firstDataTiming = (pro.invertedSignal) ? (2) : (1);

  for (unsigned int i = firstDataTiming; i < changeCount - 1; i += 2) 
  {
    code <<= 1;
    if (diff(timings[i], delay * pro.zero.high) < delayTolerance && diff(timings[i + 1], delay * pro.zero.low) < delayTolerance) 
    {
      // zero
    } 
    else if (diff(timings[i], delay * pro.one.high) < delayTolerance && diff(timings[i + 1], delay * pro.one.low) < delayTolerance) 
    {
      // one
      code |= 1;
    } 
    else 
    {
      // Failed
      return FALSE;
    }
  }

  if (changeCount > 7)     // ignore very short transmissions: no device sends them, so this must be noise
  {
    if( code )
    {
      nReceivedValue = code;
//      STM8L_Mode_Status |= STM8L_MODE_RFM69_RX;
//      STM8L_Mode |= STM8L_MODE_RFM69_RX;
      nReceivedBitlength = (changeCount - 1) / 2;
      nReceivedDelay = delay;
      nReceivedProtocol = p;
    }
  }
  return TRUE;
}

/****************************INTERRUPT HANDLER**********************************/
/*
 * PC6 is connected to DIO2 of RFM69
 * DIO2 is used to send/receive war OOK data with RFM69
 * The interrupt is set to be triggered at EXTI_Trigger_Rising_Falling
 */
INTERRUPT_HANDLER(EXTI6_IRQHandler, 14)
{
  static unsigned int changeCount = 0;
  static unsigned long lastTime = 0;
  static unsigned int repeatCount = 0;
  const long time = micros; 
  const unsigned int duration = time - lastTime;
  
  EXTI_ClearITPendingBit( EXTI_IT_Pin6 );
  // while( EXTI_GetITStatus( EXTI_IT_Pin6 ) != RESET );

  if (duration > nSeparationLimit) 
  {
    // A long stretch without signal level change occurred. This could
    // be the gap between two transmission.
    if (diff(duration, timings[0]) < 200) 
    {
      // This long signal is close in length to the long signal which
      // started the previously recorded timings; this suggests that
      // it may indeed by a a gap between two transmissions (we assume
      // here that a sender will send the signal multiple times,
      // with roughly the same gap between them).
      repeatCount++;
      if (repeatCount == 2) 
      {
        for(unsigned int i = 1; i <= numProto; i++) 
        {
          if (receiveProtocol(i, changeCount)) 
          {
            // receive succeeded for protocol i
            break;
          }
        }
        repeatCount = 0;
      }
    }
    changeCount = 0;
  }
  // detect overflow
  if (changeCount >= RCSWITCH_MAX_CHANGES) 
  {
    changeCount = 0;
    repeatCount = 0;
  }
  timings[changeCount++] = duration;
  lastTime = time;  
}

#endif