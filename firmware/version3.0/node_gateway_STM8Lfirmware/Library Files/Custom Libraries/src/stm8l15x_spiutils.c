/**
  ******************************************************************************
  * @file    stm8l15x_spiutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with 
  *          SPI peripheral.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_spiutils.h"

/**
  * @brief  Gates SPI peripheral clock.
  * @param  none
  * @retval none
  */
void SPIClock()
{
  //CLK_PeripheralClockConfig( CLK_Peripheral_SPI, ENABLE );
  CLK->PCKENR1 |= 1 << CLK_Peripheral_SPI;                                      // enable SPI clock peripheral
}

/**
  * @brief  Initialises the GPIO pins connected to SPI peripheral
  * @param  None
  * @retval None
  */
void GPIOSPIConf()
{
  GPIO_Init( SPI_PORT, SPI_MOSI, GPIO_Mode_Out_PP_Low_Fast );
  GPIO_Init( SPI_PORT, SPI_MISO, GPIO_Mode_In_PU_No_IT );
  GPIO_Init( SPI_PORT, SPI_SCL | SPI_CS , GPIO_Mode_Out_PP_Low_Slow );
}

/**
  * @brief  Configures SPI peripheral.
  * @param  none
  * @retval none
  */
void SPIConf()
{
  // SPI_DeInit( SPI );
  // SPI_Init( SPI, SPI_FirstBit_MSB, SPI_BaudRatePrescaler_2, SPI_Mode_Master, SPI_CPOL_Low, SPI_CPHA_1Edge, SPI_Direction_2Lines_FullDuplex, SPI_NSS_Soft, 0x07 );

  SPI->CR3    = SPI_CR3_RESET_VALUE;
  SPI->SR     = SPI_SR_RESET_VALUE;
/* Frame Format, BaudRate, Clock Polarity and Phase configuration */
  SPI->CR1 = (uint8_t)((uint8_t)((uint8_t)SPI_FirstBit_MSB |
                                  (uint8_t)SPI_BaudRatePrescaler_2) |
                        (uint8_t)((uint8_t)SPI_CPOL_Low |
                                  SPI_CPHA_1Edge));
  /* Data direction configuration: BDM, BDOE and RXONLY bits */
  SPI->CR2 = (uint8_t)((uint8_t)(SPI_Direction_2Lines_FullDuplex) | (uint8_t)(SPI_NSS_Soft));
  /* Master Mode */
  SPI->CR2 |= (uint8_t)SPI_CR2_SSI;
  /* Master/Slave mode configuration */
  SPI->CR1 |= (uint8_t)(SPI_Mode_Master);
  /* CRC configuration */
  SPI->CRCPR = (uint8_t)0x07;
  
}

/**
  * @brief  Sets up SPI peripheral with the required settings.
  * @param  none
  * @retval none
  */
void SetUpSPI()
{
  SPIClock();                           /* configure pheripheral clock for SPI */
  SPIConf();                            /* configure SPI */
  //GPIOSPIConf();                        /* enable GPIO pins for SPI */
                                        // dealt with in main.c 
  SPI->CR1 |= SPI_CR1_SPE;              /* Enable the SPI peripheral*/
}