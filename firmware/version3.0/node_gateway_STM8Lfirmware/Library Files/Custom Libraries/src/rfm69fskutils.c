/**
  ******************************************************************************
  * @file    rfm69fskutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with RFM69HW.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "rfm69fskutils.h"

/**
  * @brief  Initialises GPIO pins connected to RFM69HW
  * @param  None
  * @retval None
  */
void GPIORFM69()
{
  GPIO_Init( PERIPHERAL_POWER_PORT, PERIPHERAL_POWER, GPIO_Mode_Out_PP_Low_Slow );
  GPIO_Init( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0, GPIO_Mode_In_PU_No_IT );
  //GPIO_Init( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0, GPIO_Mode_In_PU_IT );
  //RFM69TxIT();
}

/**
  * @brief  Configures the interrupt pins for RFM69HW DIO0
  * @param  None
  * @retval None
  */
void RFM69TxIT()                                                                  // interrupt configuration
{
  EXTI_SetPinSensitivity( EXTI_Pin_6, EXTI_Trigger_Rising );
  while( EXTI_GetPinSensitivity( EXTI_Pin_6 ) != EXTI_Trigger_Rising );         // PC6 = DIO0
}

/**
  * @brief  Powers down RFM69HW
  * @param  None
  * @retval None
  */
void RFM69PowerDown()
{
  PERIPHERAL_POWER_PORT->ODR |= PERIPHERAL_POWER;                                         // POWER DOWN RFM69 (supplying HIGH to MOSFET gate)
}

/**
  * @brief  Powers up RFM69HW
  * @param  None
  * @retval None
  */
void RFM69PowerUp()
{
  PERIPHERAL_POWER_PORT->ODR &= (uint8_t)(~PERIPHERAL_POWER);                             // POWER UP RFM69 (supplying LOW to MOSFET gate)
}

/**
  * @brief  Change to RxMode from StandbyMode, can clear FIFO buffer
  * @param  None
  * @retval None
  */
void RFM69_ClearFIFO(void)
{
  RFM69_Write(0x0104);                                        //Standby
  RFM69_Write(0x0110);                                        //entry RxMode
}

/**
  * @brief  Set RFM69HW to sleep mode
  * @param  None
  * @retval None
  */
void RFM69_Sleep(void)
{
  RFM69_Write(0x0100);                                        //Sleep
}

/**
  * @brief  Set RFM69HW to Transmit mode
  * @param  None
  * @retval None
  */
void RFM69_Tx_Mode( void )
{
  RFM69_Write(0x010C);
  while( ( RFM69_Read( 0x27 ) ) & RFM69_FSK_MODEREADY_TX != RFM69_FSK_MODEREADY_TX );
}

/**
  * @brief  Set RFM69HW to Transmit mode
  * @param  None
  * @retval None
  */
void RFM69_Rx_Mode( void )
{
  RFM69_Write(0x0110);
  while( ( RFM69_Read( 0x27 ) ) & RFM69_FSK_MODEREADY_RX != RFM69_FSK_MODEREADY_RX );
}

/**
  * @brief  Set RFM69HW to Standby mode
  * @param  None
  * @retval None
  */
void RFM69_Standby(void)
{
  RFM69_Write(0x0104);                                                          //Standby
}

/**
  * @brief  Set RFM69HW to sleep mode
  * @param  None
  * @retval None
  */
void RFM69_Sleep_Int(void)
{
  RFM69_Write(0x0100);
  RFM69_Write(0x3B3B);
}

/**
  * @brief  Restarts RFM69HW
  * @note Utilises delay_ms() function to wait for RFM69HW to be functional
  * @param  None
  * @retval None
  */
void RFM69Restart()
{
  RFM69PowerDown();
  delay_ms( 5 );
  RFM69PowerUp();
  delay_ms( 15 );
}


/**
  * @brief  Changes RFM69HW mode with the required delays
  * @param  RFM69_NewMode: It can be any of the two options
  *             @arg RFM69_FSK_CHANGEMODE_TX: To change to transmit mode
  *             @arg RFM69_FSK_CHANGEMODE_RX: To change to reception mode
  * @retval None
  */
void RFM69_FSK_ChangeMode( RFM69_FSK_ChangeModeTypedef RFM69_NewMode )
{
  RFM69_ClearFIFO();
  while( ( RFM69_Read( 0x27 ) ) & RFM69_FSK_MODEREADY_RX != RFM69_FSK_MODEREADY_RX );
  if( RFM69_NewMode == RFM69_FSK_CHANGEMODE_TX )
  {
    RFM69_FSK_EntryTx();
    while( ( RFM69_Read( 0x27 ) ) & RFM69_FSK_MODEREADY_TX != RFM69_FSK_MODEREADY_TX );
  }
  else if( RFM69_NewMode == RFM69_FSK_CHANGEMODE_RX )
  {
    RFM69_Standby();
    RFM69_FSK_EntryRx();
    while( ( RFM69_Read( 0x27 ) ) & RFM69_FSK_MODEREADY_RX != RFM69_FSK_MODEREADY_RX );
  }
}

/**
  * @brief  Transmits a packet of data RFM69HW to Standby mode
  * @note   It requires 'RFM69_Data' as input array and 'RFM69_DATA_SIZE' as
            data size limit.
  * @param  None
  * @retval None
  */
void RFM69_TransmitPacket()
{
  RFM69_FSK_ChangeMode( RFM69_FSK_CHANGEMODE_TX );
  delay_ms( 5 );                                                                // delay wothout which it won't work
  RFM69_BurstWrite( 0x00, (uint8_t *) RFM69_Data, RFM69_DATA_SIZE );
  delay_ms( 1 );                                                                // delay wothout which it won't work
  RFM69_Tx_Mode();
  RFM69_Write( 0x0000 );
  RFM69_Read( 0x00 );
}

/**
  * @brief  Receives a data packet
  * @note   DIO0 should be checked before calling this function. It stores the 
            result into 'RFM69_Data' with data limit size 'RFM69_DATA_SIZE'.
  * @param  None
  * @retval None
  */
void RFM69_ReceivePacket()
{
  RFM69_BurstRead( 0x00, (uint8_t *) RFM69_Data, RFM69_DATA_SIZE );
  RFM69_ClearFIFO();
  while( ( RFM69_Read( 0x27 ) ) & RFM69_FSK_MODEREADY_RX != RFM69_FSK_MODEREADY_RX );
}

#if defined(CODE_AS_SENSOR) || defined(CODE_AS_SENSOR_OOK) || defined(CODE_AS_SENSOR_IRMS)
/**
  * @brief  Receives a packet, checks for an acknowledgement and notifies the 
            the result.
  * @param  None
  * @retval None
  */
void ReceiveAcknowledgement()
{
  if( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );
  {
    STM8L_Mode |= STM8L_MODE_PACKET_RECEIVED;
    RFM69_ReceivePacket();
    if( CheckAcknowledgement() )
    {
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
      NotifyGreenLED();                                                         // successfully received an acknowledgment
#endif
      return;
    }
  }
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
  NotifyYellowLED();                                                            // failed to recieve a proper acknowledgment
#endif
}

/**
  * @brief  Acquires data and sends it via RFM69HW. If in debug mode, it will 
            wait for an acknowledgement and verifies it.
  * @param  None
  * @retval None
  */
void SendData()
{
  
  if(STM8L_Error == STM8L_ERROR_RFM69_NO_DEVICE)
    return;
  
  if( ( STM8L_Mode & STM8L_MODE_PACKET_SEND_INITIATE ) == STM8L_MODE_PACKET_SEND_INITIATE )
  {
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
    if(STM8L_Error == STM8L_ERROR_NO_ERROR)
      GREEN_LED_ON;
    else
      YELLOW_LED_ON;
#endif
    //STM8L_Mode |= GetDeviceStatus();
    GetData();                                                                  // fetches the required data packet to be send
    RFM69_TransmitPacket();                                                     // sends the packet
    
    while( !GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );   // waits for packet send confirmation
    RFM69_ClearFIFO();
    STM8L_Mode ^= ( STM8L_MODE_PACKET_SEND_INITIATE | STM8L_MODE_PACKET_SEND ); // toggles status from 'STM8L_MODE_DEBUG_PACKET_SEND_INITIATE' to 'STM8L_MODE_DEBUG_PACKET_SEND'
  }
  
  if( ( STM8L_Mode & STM8L_MODE_PACKET_SEND ) == STM8L_MODE_PACKET_SEND )
  {
    if( ( STM8L_Mode & STM8L_MODE_DEBUG ) != STM8L_MODE_DEBUG )                 // if in debug mode
    {
      STM8L_Mode = STM8L_MODE_POWER_DOWN;                                       // initiate power down
      if( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) )      // waits till the pin goes low
        while( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );
      //IWDG_ReloadCounter();
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
      GREEN_LED_OFF;
      YELLOW_LED_OFF;
#endif
      IWDG->KR = IWDG_KEY_REFRESH;
      return;
    }
    RFM69Restart();
    //IWDG_ReloadCounter();
    IWDG->KR = IWDG_KEY_REFRESH;

    
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
    GREEN_LED_OFF;
    YELLOW_LED_ON;
#endif

    RFM69_FSK_ChangeMode( RFM69_FSK_CHANGEMODE_RX );                                    // change RFM69 mode to Rx
    delay_ms( 1000 );                                                           // waits for 1s after which it checks for data reception
    
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
    YELLOW_LED_OFF;
#endif
    ReceiveAcknowledgement();                                                   // receives data packet and checks if the received packet is a proper acknowledgement
    STM8L_Mode = STM8L_MODE_POWER_DOWN;                                         // initiate power down
  }
  
  SetDeviceStatus();
  if( ( STM8L_Mode & STM8L_MODE_POWER_UP_INITIATE ) == STM8L_MODE_POWER_UP_INITIATE ) // over rides power down when any of the sensor status is changed
    STM8L_Mode = STM8L_MODE_POWER_UP;            
  else if( ( STM8L_Mode & STM8L_MODE_DEBUG_INITIATE ) == STM8L_MODE_DEBUG_INITIATE ) // over rides power down when button pressed is recorded
    STM8L_Mode = STM8L_MODE_DEBUG | STM8L_MODE_PACKET_SEND_INITIATE;
  
  //if( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) )          // waits till the pin goes low
    //while( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );
}
#endif


#ifdef CODE_AS_GATEWAY
/**
  * @brief  Receives data from RFM69HW, processes it, checks for request for 
            acknowledgement and responds to an acknowledgement (if any).
  * @param  None
  * @retval None
  */
void ReceiveData()
{
  uint8_t counter = 1;
  while( !GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );     // waits for a packet to be received
  STM8L_Mode = STM8L_MODE_PACKET_RECEIVED;
  
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
  GREEN_LED_ON;
#endif
  RFM69_ReceivePacket();

#ifdef DEBUG_USART
  USART_SendData( (char *) RFM69_Data );                                        // it will return without sending the data if the packet format is not correct
#endif
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
  GREEN_LED_OFF;
#endif
  
  if( CheckForAcknowledgementRequest() )
  {
    STM8L_Mode = STM8L_MODE_DEBUG;
  }
  else
  {
    STM8L_Mode = STM8L_MODE_POWER_UP;
    //if( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) )        // waits till the pin goes low
      //while( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );
    return;
  }
  
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
  YELLOW_LED_ON;
#endif
  GetAcknowledgementData();                                                     // get data packet which is to be send as acknowledgement
  STM8L_Mode |= STM8L_MODE_PACKET_SEND_INITIATE;
  if( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) )          // waits till the pin goes low
    while( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );
  do
  {
    RFM69_TransmitPacket();                                                     // transmits the data packet
    
    while( !GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );   // waits for packet send confirmation
    RFM69_ClearFIFO();
    STM8L_Mode ^= ( STM8L_MODE_PACKET_SEND_INITIATE | STM8L_MODE_PACKET_SEND ); // toggles status from 'PACKET_SEND_INITIATE' to 'PACKET_SEND'
    delay_ms( 100 );
    if( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) )        // waits till the pin goes low
      while( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );
  } while( counter-- );
  RFM69Restart();
  RFM69_FSK_ChangeMode( RFM69_FSK_CHANGEMODE_RX );
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
  YELLOW_LED_OFF;
#endif
  STM8L_Mode = STM8L_MODE_POWER_UP;
  if( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) )          // waits till the pin goes low
    while( GPIO_ReadInputDataBit( EXTI_RFM69_DIO0_PORT, EXTI_RFM69_DIO0 ) );
}
#endif



/************************************INTERRUPT HANDLER**************************/
/** 
  * This interrupt is not used in the program 
  */
#ifdef CODE_AS_SENSOR
INTERRUPT_HANDLER( EXTI6_IRQHandler, 14 )
{
  EXTI_ClearITPendingBit( EXTI_IT_Pin6 );
  
  if( ( STM8L_Mode & STM8L_MODE_PACKET_SEND_INITIATE ) == STM8L_MODE_PACKET_SEND_INITIATE )
  {
    RFM69_ClearFIFO();
    STM8L_Mode ^= ( STM8L_MODE_PACKET_SEND_INITIATE | STM8L_MODE_PACKET_SEND );
    // toggles status from 'STM8L_MODE_DEBUG_PACKET_SEND_INITIATE' to 'STM8L_MODE_DEBUG_PACKET_SEND'
  }
  else if( ( STM8L_Mode & STM8L_MODE_PACKET_SEND ) == STM8L_MODE_PACKET_SEND )
  {
    STM8L_Mode |= STM8L_MODE_PACKET_RECEIVED;
  }
}
#endif

#ifdef CODE_AS_GATEWAY
#ifdef TEMPHAWK_VERSION_2_0
/** 
  * This interrupt is not used in the program 
  */
INTERRUPT_HANDLER( EXTI1_IRQHandler, 9 )
{
  EXTI_ClearITPendingBit( EXTI_IT_Pin1 );
  
  if( ( STM8L_Mode & STM8L_MODE_PACKET_SEND_INITIATE ) == STM8L_MODE_PACKET_SEND_INITIATE )
  {
    RFM69_ClearFIFO();
    STM8L_Mode = ( STM8L_MODE_DEBUG | STM8L_MODE_PACKET_SEND );
  }
  else
  {
    STM8L_Mode = ( STM8L_MODE_PACKET_RECEIVED );
  }
}
#else

/** 
  * This interrupt is not used in the program 
  */
INTERRUPT_HANDLER( EXTI0_IRQHandler, 8 )
{
  EXTI_ClearITPendingBit( EXTI_IT_Pin0 );
  
  if( ( STM8L_Mode & STM8L_MODE_PACKET_SEND_INITIATE ) == STM8L_MODE_PACKET_SEND_INITIATE )
  {
    RFM69_ClearFIFO();
    STM8L_Mode = ( STM8L_MODE_DEBUG | STM8L_MODE_PACKET_SEND );
  }
  else
  {
    STM8L_Mode = ( STM8L_MODE_PACKET_RECEIVED );
  }
}
#endif
#endif
