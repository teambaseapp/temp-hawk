/* 
 * stm8l15x_timer4utils.c
 */

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_timer4utils.h"

volatile uint32_t micros = 0;

void SetUpTimer4()
{
  CLK_PeripheralClockConfig( CLK_Peripheral_TIM4, ENABLE );
  TIM4_InternalClockConfig();
  TIM4_DeInit();
  TIM4_TimeBaseInit( TIM4_Prescaler_16, 100 );
  TIM4_ClearFlag( TIM4_FLAG_Update );
  TIM4_ITConfig( TIM4_IT_Update, ENABLE );
  TIM4_Cmd( ENABLE );
}

/****************************INTERRUPT HANDLER**********************************/
INTERRUPT_HANDLER(TIM4_UPD_OVF_TRG_IRQHandler, 25)
{
  if( !TIM4_GetFlagStatus( TIM4_FLAG_Update ) )
  {
    return;
  }
  
  TIM4_ClearITPendingBit( TIM4_IT_Update );
  
  if( micros > ( uint32_t ) 4294967100 )
  {
     micros = 0;
  }
  micros += 100;
}