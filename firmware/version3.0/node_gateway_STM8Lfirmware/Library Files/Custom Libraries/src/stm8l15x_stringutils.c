/**
  ******************************************************************************
  * @file    stm8l15x_stringutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with the string 
  *          functions used in the program. It maintains and edits global 
  *          character array variable 'RFM69_Data'.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_stringutils.h"
   
/* Global Variables-----------------------------------------------------------*/
/**
  * @var    RFM69_Data[]
  * @brief  This is the global char array variable used as a buffer to transfer
  *         information from an information source to a destination. It is also  
  *         used to acquire, recieve and edit information so as to cater to the
  *         different sources and destinations.
  */
uint8_t RFM69_Data[RFM69_DATA_SIZE] = { 0 };

/**
  * @var    offset
  * @brief  This static variable defines the shift in data which may be possible
  *         upon transmission and reception of data packets over RFM69HW.
  */
static int8_t offset = -1;


/**
  * @brief  Calculates the offset required to synchronise the received packet
  *         data. If the offset is not an acceptable value, then the function
  *         returns a 'FALSE' boolean value.
  * @note   In order to get the value if the calculated offset value, use the 
  *         function 'GetOffset()'
  * @param  none
  * @retval Return value can be
  *             @arg TRUE: if the offset is an acceptable value. 
  *             @arg FALSE: if the offset is not an acceptable value. 
  */
bool CalculateOffset()
{
  offset = -1;
  for( uint8_t i=0; i<2; i++ )                                                  // check for sync byte
  {
    if( RFM69_Data[i] == RFM69_DATA_SYNC_BYTE && 
        ( RFM69_Data[i + 1] == RFM69_DATA_CONFIG_ACK_BYTE
          || RFM69_Data[i + 1] == RFM69_DATA_CONFIG_REQ_ACK
          || RFM69_Data[i + 1] == RFM69_DATA_CONFIG_REQ_NOACK 
          || RFM69_Data[i + 1] == RFM69_DATA_CONFIG_SENSOR_OOK 
          || RFM69_Data[i + 1] == RFM69_DATA_CONFIG_SENSOR_IRMS ) )
    {
      offset = i;
      break;
    }
  }
  if( offset < 0 || offset > 1 )                                                // if sync byte not found or if the information is incomplete
    return FALSE;
  
  return TRUE;
}

/**
  * @brief  Returns the calculated value of 'offset'
  * @note   This should be called after the proper execution of CalculateOffset()
  *         function. Else it will return an improper value while running.
  * @param  none
  * @retval Returns the calculated offset value.
  */
uint8_t GetOffset()
{
  return offset;
}

#if defined(CODE_AS_SENSOR) || defined(CODE_AS_SENSOR_OOK) || defined(CODE_AS_SENSOR_IRMS)
/**
  * @brief  This fetches data to be transfered via RFM69HW. It is programmed to
  *         fetch the correct configuration byte according to the STM8L mode.
  * @param  none
  * @retval none
  */
void GetData()
{
  uint32_t returnValue;
  
  RFM69_Data[RFM69_DATA_INDEX_SYNC_BYTE] = RFM69_DATA_SYNC_BYTE;

#if defined(CODE_AS_SENSOR)
  if( ( STM8L_Mode & STM8L_MODE_DEBUG ) != STM8L_MODE_DEBUG )
    RFM69_Data[RFM69_DATA_INDEX_CONFIG] = RFM69_DATA_CONFIG_REQ_NOACK;          // generate packet with no acknowledgement request
  else
    RFM69_Data[RFM69_DATA_INDEX_CONFIG] = RFM69_DATA_CONFIG_REQ_ACK;            // generate packet with acknowledgement request
#elif defined(CODE_AS_SENSOR_OOK)
  RFM69_Data[RFM69_DATA_INDEX_CONFIG] = RFM69_DATA_CONFIG_SENSOR_OOK;
#elif defined(CODE_AS_SENSOR_IRMS)
  RFM69_Data[RFM69_DATA_INDEX_CONFIG] = RFM69_DATA_CONFIG_SENSOR_IRMS;
#endif
  
  returnValue = GetDeviceID();
  RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID] = (uint8_t)( returnValue >> 8 );
  RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID + 1] = (uint8_t) returnValue;
  
#if defined(CODE_AS_SENSOR)
  returnValue = I2CRead( I2C_HTU20D_ADDRESS, HTU20D_TEMPERATURE_REG ) & 0xFFFC;
  RFM69_Data[RFM69_DATA_INDEX_TEMPERATURE] = (uint8_t) ( returnValue >> 8 );
  RFM69_Data[RFM69_DATA_INDEX_TEMPERATURE + 1] = (uint8_t) returnValue;
  
  RFM69_Data[RFM69_DATA_INDEX_SENSOR1] = GetSensorStatus();
  
  RFM69_Data[RFM69_DATA_INDEX_BATTERY] = ADC_VRef_Measure();
  RFM69_Data[RFM69_DATA_INDEX_END_OF_PACKET] = '\0';
  
  delay_us( 200 );
  
  returnValue = I2CRead( I2C_HTU20D_ADDRESS, HTU20D_HUMIDITY_REG ) & 0xFFFC;
  RFM69_Data[RFM69_DATA_INDEX_HUMIDITY] = (uint8_t) ( returnValue >> 8 );
  // take care: LSB also contains info about sensors
  RFM69_Data[RFM69_DATA_INDEX_HUMIDITY + 1] |= (uint8_t) returnValue;
  
#elif defined(CODE_AS_SENSOR_OOK)
  returnValue = getReceivedValue();                                             // received ASK value
  RFM69_Data[RFM69_DATA_INDEX_TEMPERATURE] = (uint8_t) (returnValue >> 24);
  RFM69_Data[RFM69_DATA_INDEX_TEMPERATURE + 1] = (uint8_t) (returnValue >> 16);
  RFM69_Data[RFM69_DATA_INDEX_HUMIDITY] = (uint8_t) (returnValue >> 8);
  RFM69_Data[RFM69_DATA_INDEX_HUMIDITY + 1] = (uint8_t) (returnValue);
  
#elif defined(RFM69_DATA_CONFIG_SENSOR_IRMS)
  returnValue = GetIrms();                                                      // Get calculated Irms value multiplied by 100
  RFM69_Data[RFM69_DATA_INDEX_TEMPERATURE] = (uint8_t) (returnValue >> 8);
  RFM69_Data[RFM69_DATA_INDEX_TEMPERATURE + 1] = (uint8_t) (returnValue);
#endif
  
  RFM69_Data[RFM69_DATA_INDEX_ERRORS] = STM8L_Error;
}

/**
  * @brief  Checks if the received message is a proper acknowledgement to the
  *         acknowledgement request made (by the Sensor module).
  * @param  none
  * @retval Return value can be
  *             @arg TRUE: if the received message is a proper acknowledgement 
  *             @arg FALSE: if the received message not is a proper acknowledgement 
  */
bool CheckAcknowledgement()
{
  uint16_t ack_Device_ID;
  if( !CalculateOffset() )                                                      // if sync data not found or if the packet is out of sync
    return FALSE;
  if( RFM69_Data[RFM69_DATA_INDEX_CONFIG + offset] != RFM69_DATA_CONFIG_ACK_BYTE )       // if there is no reply to acknowledgement
    return FALSE;
  ack_Device_ID = RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID + offset] << 8;
  ack_Device_ID |= RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID + offset + 1];
  if( ack_Device_ID != GetDeviceID() )                                          // if device ID doesn't match
    return FALSE;
  
  return TRUE;
}
#endif


#ifdef CODE_AS_GATEWAY
/**
  * @brief  Checks if the device ID embedded in the received packet data and 
  *         the programmed device ID (as in EEPROM) are the same.
  * @param  none
  * @retval Return value can be
  *             @arg TRUE: if the IDs are the same
  *             @arg FALSE: if the IDs are not the same
  */
bool CheckDeviceID()
{
  uint16_t received_Data_Device_ID;
  if( !CalculateOffset() )                                                      // if sync data not found or if the packet is out of sync
    return FALSE;
  received_Data_Device_ID = RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID + offset] << 8;
  received_Data_Device_ID |= RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID + offset + 1];
  if( received_Data_Device_ID != GetDeviceID() )                                // if device ID doesn't match
    return FALSE;
  
  return TRUE;
}

/**
  * @brief  Checks if the received packet data has an acknowledgement request 
  *         associated with it.
  * @param  none
  * @retval Return value can be
  *             @arg TRUE: if there is an acknowledgement request.
  *             @arg FALSE: if there is no acknowledgement request.
  */
bool CheckForAcknowledgementRequest()
{
  if( !CalculateOffset() )                                                      // if sync data not found or if the packet is out of sync
    return FALSE;
  if( RFM69_Data[RFM69_DATA_INDEX_CONFIG + offset] != RFM69_DATA_CONFIG_REQ_ACK )       // if config byte doesn't match
    return FALSE;
  
  return TRUE;
}

/**
  * @brief  Fetches the string required to be send as a reply for an 
  *         acknowledgement request by a Sensor module.
  * @param  none
  * @retval none
  */
void GetAcknowledgementData()
{
  if( !CalculateOffset() )                                                      // if sync data not found or if the packet is out of sync
    return;
  
  RFM69_Data[RFM69_DATA_INDEX_SYNC_BYTE] = RFM69_DATA_SYNC_BYTE;
  RFM69_Data[RFM69_DATA_INDEX_CONFIG] = RFM69_DATA_CONFIG_ACK_BYTE;
  
  RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID] = RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID + offset];             // copy device ID MSB
  RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID + 1] = RFM69_Data[RFM69_DATA_INDEX_DEVICE_ID + offset + 1];     // copy device ID LSB
  
  RFM69_Data[RFM69_DATA_INDEX_TEMPERATURE] = 0x00;
  RFM69_Data[RFM69_DATA_INDEX_TEMPERATURE + 1] = 0x00;
  RFM69_Data[RFM69_DATA_INDEX_HUMIDITY] = 0x00;
  RFM69_Data[RFM69_DATA_INDEX_HUMIDITY + 1] = 0x00;
  RFM69_Data[RFM69_DATA_INDEX_BATTERY] = 0x00;
  RFM69_Data[RFM69_DATA_INDEX_END_OF_PACKET] = '\0';
}
#endif