/**
  ******************************************************************************
  * @file    rfm69fsk.c
  * @author  Geman Deng and Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with 
  *          RFM69HW configurations and basic functionalities.
  *         
  * 
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  * I would like to testify that this file contains code by <i>Geman Deng</i>
  * without whose codes I wouldn't be able to do this project. But I haven't
  * used all his work and I have developed my own code to suite the specific
  * requirements of this project. I'm write the whole code myself which may
  * take some time.
  * 
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "rfm69fsk.h"

/**********************************************************
**Parameter table define
**********************************************************/
const uint16_t RFM69FreqTbl[4][3] = 
{ 
  {0x074e, 0x08c0, 0x0900}, //315MHz
  {0x076c, 0x0880, 0x0900}, //434MHz
  {0x07d9, 0x0800, 0x0900}, //868MHz
  {0x07e4, 0x08c0, 0x0900}, //915MHz
};

const uint16_t RFM69RateTbl[4][2] = 
{
  {0x0368, 0x042B},         //BR=1.2K BW=83.333K
  {0x0334, 0x0415},         //BR=2.4K BW=83.333K  
  {0x031A, 0x040B},         //BR=4.8K BW=83.333K
  {0x030D, 0x0405},         //BR=9.6K BW=83.333K
};

const uint16_t RFM69PowerTbl[4] = 
{
  0x117F,                   //20dbm  
  0x117C,                   //17dbm
  0x1179,                   //14dbm
  0x1176,                   //11dbm 
};

const uint16_t RFM69ConfigTbl[17] = 
{ 
  0x0200,                   //RegDataModul, FSK Packet  
  0x0502,                   //RegFdevMsb, 241*61Hz = 35KHz  
  0x0641,                   //RegFdevLsb
  0x1952,                   //RegRxBw , RxBW, 83KHz
  
  0x2C00,                   //RegPreambleMsb
  0x2D05,                   //RegPreambleLsb, 5Byte Preamble
  0x2E90,                   //enable Sync.Word, 2+1=3bytes
  //0x2ED0,
  0x2FAA,                   //0xAA, SyncWord = aa2dd4
  0x302D,                   //0x2D
  0x31D4,                   //0xD4  
  0x3700,                   //RegPacketConfig1, Disable CRC��NRZ encode
  0x3800 | RFM69_DATA_SIZE,                   
                            //RegPayloadLength, 21bytes for length & Fixed length = 0x3815
  //0x3C95,                   //RegFiFoThresh   
  0x3C00 | RFM69_DATA_SIZE,
  0x1888,                   //RegLNA, 200R  
  0x581B,                   //RegTestLna, Normal sensitivity
  //0x582D,                   //RegTestLna, increase sensitivity with LNA (Note: consumption also increase!)
  0x6F30,                   //RegTestDAGC, Improved DAGC
  //0x6F00,                   //RegTestDAGC, Normal DAGC
  0x0104,                   //Enter standby mode
};
   

/**
  * @brief  Initialize RFM69HW & set its entry to standby mode
  * @param  none
  * @retval none
  */
void RFM69_FSK_Config(void)
{
  uint8_t i;
  
  for(i=0;i<3;i++)                      
    RFM69_Write(RFM69FreqTbl[RFM69_FREQ_434][i]);           //setting frequency parameter
  for(i=0;i<2;i++)
    RFM69_Write(RFM69RateTbl[RFM69_RATE_4800][i]);           //setting rf rate parameter
  for(i=0;i<17;i++)
    RFM69_Write(RFM69ConfigTbl[i]);                          //setting base parameter
}

/**
  * @brief  Configure RFM69HW for Transmit mode.
  * @param  none
  * @retval none
  */
void RFM69_FSK_EntryTx(void)
{
  const uint16_t RFM69TxTbl[7] = 
  {
    0x3C00 | RFM69_DATA_SIZE,
    0x2504,                   //DIO Mapping for Tx
    0x130F,                   //Disable OCP
    0x5A5D,                   //High power mode
    0x5C7C,                   //High power mode
    //0x010C
    0x0180,                   //Enter Tx mode
    0x3B3B
  };

  uint8_t i;
  RFM69_FSK_Config();                                         //Module parameter setting  
  RFM69_Write(RFM69PowerTbl[RFM69_POWER_14DB]);              //Setting output power parameter
  for(i=0;i<7;i++)                                         //Define to Tx mode  
    RFM69_Write(RFM69TxTbl[i]);
}

/**
  * @brief  Configure RFM69HW for Receive mode.
  * @param  none
  * @retval none
  */
void RFM69_FSK_EntryRx(void)
{
  const uint16_t RFM69RxTbl[7] = 
  {   
    0x3C80 | RFM69_DATA_SIZE,
    0x119F,                   //      
    0x2540,                   //DIO Mapping for Rx
    0x1310,                   //OCP enabled
    0x5A55,                   //Normal and Rx
    0x5C70,                   //Normal and Rx   
    0x0110                    //Enter Rx mode
  }; 
  
  uint8_t i; 
  RFM69_FSK_Config();                                         //Module parameter setting
  
  for(i=0;i<7;i++)                                         //Define to Rx mode  
    RFM69_Write( RFM69RxTbl[i] );
}

/**
  * @brief  Disable RFM69HW
  * @param  none
  * @retval none
  */
void RFM69_SlaveDisable()
{
  GPIO_WriteBit( SPI_PORT, SPI_CS, SET );
}

/**
  * @brief  Enable RFM69HW
  * @param  none
  * @retval none
  */
void RFM69_SlaveEnable()
{
  GPIO_WriteBit( SPI_PORT, SPI_CS, RESET );
}

/**
  * @brief  Checks if communication between STM8L and RFM69HW via SPI is 
  *         working properly.
  * @param  none
  * @retval Return value indicates if the communication is proper. It can be
  *             @arg TRUE: if the communication is properly established.
  *             @arg FALSE: if the communication is not properly established.
  */
bool RFM69_CheckDevice()
{
  /* RFM69 Peripheral Checking */
  uint8_t temp;
  temp = RFM69_Read( RFM69_REG_VERSION );
  if( temp != RFM69_VERSION_NUMBER )
  {
    STM8L_Error = STM8L_ERROR_RFM69_NO_DEVICE;
    return FALSE;
  }
  return TRUE;
}

/**
  * @brief  Reads an 8-bit value from RFM69HW via SPI peripheral.
  * @param  address: the 8-bit register address which should be read for 
  *             the value.
  * @retval none
  */
uint8_t RFM69_Read( uint8_t address )
{
  uint8_t fetchValue = 0;
  RFM69_SlaveEnable();

  while( !SPI_GetFlagStatus(SPI, SPI_FLAG_TXE) );
  SPI_SendData( SPI, address & RFM69READMASK );
  while( SPI_GetFlagStatus(SPI, SPI_FLAG_BSY) );
  while( !SPI_GetFlagStatus(SPI, SPI_FLAG_RXNE) );
  fetchValue = SPI_ReceiveData( SPI );

  while( !SPI_GetFlagStatus(SPI, SPI_FLAG_TXE) );
  SPI_SendData( SPI, 0x00 );
  while ( SPI_GetFlagStatus(SPI, SPI_FLAG_BSY) );
  while( !SPI_GetFlagStatus(SPI, SPI_FLAG_RXNE) );
  fetchValue = SPI_ReceiveData( SPI );

  RFM69_SlaveDisable();
  return( fetchValue );
}

/**
  * @brief  Writes an 8-bit value to the specified register of RFM69HW via 
  *         SPI peripheral.
  * @param  address_data: it is a 16-bit value which consists of address of 
  *         the register as its MSB and the value to be written on the register 
  *         as LSB.
  * @retval None
  */
void RFM69_Write( uint16_t address_data )
{
  uint8_t address = (uint8_t)((uint16_t)((uint16_t)address_data >> 8 ));
  uint8_t data = (uint8_t)address_data;

  RFM69_SlaveEnable();

  while( !SPI_GetFlagStatus( SPI, SPI_FLAG_TXE ) );
  SPI_SendData( SPI, address | RFM69WRITEMASK );

  while( !SPI_GetFlagStatus( SPI, SPI_FLAG_TXE ) );
  SPI_SendData( SPI, data );

  while( !SPI_GetFlagStatus( SPI, SPI_FLAG_TXE ) );
  for(int i=0; i<32; i++);

  RFM69_SlaveDisable();
}

/**
  * @brief  Writes a series of 8-bit values to the specified register of RFM69HW 
  *         via SPI peripheral.
  * @note   This is to be used to write RFM69HW FIFO only.
  * @param  address_data: address of the destination register.
  *         packet: the array of data to be written.
  *         no_of_bytes: the number of bytes to be written.
  * @retval None
  */
void RFM69_BurstWrite( uint8_t address , uint8_t *packet , int no_of_bytes)
{
  uint8_t i;
  address |= 0x80;
  RFM69_SlaveEnable();
  while( SPI_GetFlagStatus( SPI, SPI_FLAG_TXE ) == RESET );  
  SPI_SendData( SPI, address );  
  
  for( i=0; i<no_of_bytes; i++)
  {
    while( SPI_GetFlagStatus( SPI, SPI_FLAG_TXE ) == RESET );  
    SPI_SendData( SPI, *packet++ );
  }
  
  while( SPI_GetFlagStatus( SPI, SPI_FLAG_TXE ) == RESET ); 
  RFM69_SlaveDisable();
}

/**
  * @brief  Reads a series of 8-bit values from the specified register of RFM69HW 
  *         via SPI peripheral.
  * @note   This is to be used to read RFM69HW FIFO only.
  * @param  address_data: address of the source register.
  *         packet: the address of the memory location where the data should be 
  *             stored.
  *         no_of_bytes: the number of bytes to be read.
  * @retval None
  */
void RFM69_BurstRead( uint8_t address , uint8_t *packet ,int no_of_bytes )
{
  uint8_t i;
  RFM69_SlaveEnable();
  while( SPI_GetFlagStatus( SPI, SPI_FLAG_TXE ) == RESET );  
  SPI_SendData( SPI, address );
  
  for( i=0; i<no_of_bytes; i++)
  {
    while (SPI_GetFlagStatus( SPI, SPI_FLAG_BSY ) == SET );  
    while( SPI_GetFlagStatus( SPI, SPI_FLAG_RXNE ) == RESET );  
    *packet++ = SPI_ReceiveData( SPI );
    while(SPI_GetFlagStatus( SPI, SPI_FLAG_TXE ) == RESET );
    SPI_SendData( SPI, 0x00 );       
  }
  while (SPI_GetFlagStatus( SPI, SPI_FLAG_BSY ) == SET );  
  while( SPI_GetFlagStatus( SPI, SPI_FLAG_TXE ) == RESET ); 
  RFM69_SlaveDisable();
}