/**
  ******************************************************************************
  * @file    stm8l15x_flashutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with EEPROM.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_flashutils.h"

/**
  * @brief  Sets up the EEPROM.
  * @param  None
  * @retval None
  */
void SetUpEEPROM()
{
  FLASH_DeInit();
  FLASH_Lock( FLASH_MemType_Data );
}

/**
  * @brief  Stores the current debug status in the EEPROM for later recovery.
  * @note   It accessess the 'STM8L_Mode' global variable to fetch current status.
  * @param  None
  * @retval None
  */
void SetDeviceStatus()
{
  //FLASH_Unlock( FLASH_MemType_Data );
  FLASH->DUKR = FLASH_RASS_KEY2; /* Warning: keys are reversed on data memory !!! */
  FLASH->DUKR = FLASH_RASS_KEY1;
  //FLASH_ProgramByte( EEPROM_ADDRESS_DEVICEID_BYTE0, (uint8_t) ( deviceID >> 8 ) );
  *(PointerAttr uint8_t*) ( MemoryAddressCast ) EEPROM_ADDRESS_STM8L_MODE = STM8L_Mode & STM8L_MODE_DEBUG;
  //FLASH_Lock( FLASH_MemType_Data );
  FLASH->IAPSR &= (uint8_t) FLASH_MemType_Data;
}

/**
  * @brief  Retrieves the debug status of the device (especially after a reset).
  * @note   It accessess the 'STM8L_Mode' global variable to store current status.
  * @param  None
  * @retval None
  */
uint8_t GetDeviceStatus()
{
  uint8_t deviceMode;
  //FLASH_Unlock( FLASH_MemType_Data );
  FLASH->DUKR = FLASH_RASS_KEY2; /* Warning: keys are reversed on data memory !!! */
  FLASH->DUKR = FLASH_RASS_KEY1;
  //deviceID = FLASH_ReadByte( EEPROM_ADDRESS_DEVICEID_BYTE0 );
  deviceMode = *(PointerAttr uint8_t *) (MemoryAddressCast) EEPROM_ADDRESS_STM8L_MODE;
  
  return deviceMode;
}

/**
  * @brief  Retrieves the unique Device ID stored in the EEPROM
  * @param  None
  * @retval Returns the unique device ID
  */
uint16_t GetDeviceID()
{
  uint16_t deviceID;
  //FLASH_Unlock( FLASH_MemType_Data );
  FLASH->DUKR = FLASH_RASS_KEY2; /* Warning: keys are reversed on data memory !!! */
  FLASH->DUKR = FLASH_RASS_KEY1;
  //deviceID = FLASH_ReadByte( EEPROM_ADDRESS_DEVICEID_BYTE0 );
  deviceID = *(PointerAttr uint8_t *) (MemoryAddressCast) EEPROM_ADDRESS_DEVICEID_BYTE0;
  deviceID = ( (uint8_t) deviceID ) << 8;
  //deviceID |= FLASH_ReadByte( EEPROM_ADDRESS_DEVICEID_BYTE1 );
  deviceID |= *(PointerAttr uint8_t *) (MemoryAddressCast) EEPROM_ADDRESS_DEVICEID_BYTE1;
  //FLASH_Lock( FLASH_MemType_Data );0
  FLASH->IAPSR &= (uint8_t) FLASH_MemType_Data;
  
  return deviceID;
}