/**
  ******************************************************************************
  * @file    stm8l15x_adcutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with ADC and 
  *          internal reference voltage (VREF).
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_adcutils.h"

#if defined(CODE_AS_SENSOR) || defined(CODE_AS_SENSOR_IRMS)

/**
  * @brief  Initialises ADC for internal voltage reference
  * @param  None
  * @retval None
  */
void ADC_VRef_Init() 
{
  CLK_PeripheralClockConfig( CLK_Peripheral_ADC1, ENABLE );
  
  //ADC_Init( ADC1, ADC_ConversionMode_Single, ADC_Resolution_8Bit, ADC_Prescaler_1 );
  // clear CR1 register
  ADC1->CR1 &= (uint8_t)~(ADC_CR1_CONT | ADC_CR1_RES);
  // set the resolution and the conversion mode 
  ADC1->CR1 |= (uint8_t)((uint8_t) ADC_ConversionMode_Single | (uint8_t) ADC_Resolution_8Bit);
  //clear CR2 register 
  ADC1->CR2 &= (uint8_t)~(ADC_CR2_PRESC);
  // set the Prescaler 
  ADC1->CR2 |= (uint8_t) ADC_Prescaler_1;
  
  //ADC_VrefintCmd( ENABLE );
  ADC1->TRIGR[0] |= (uint8_t)(ADC_TRIGR1_VREFINTON);
  
  //ADC_SamplingTimeConfig( ADC1, ADC_Group_FastChannels, ADC_SamplingTime_16Cycles );
  //ADC1->CR3 &= (uint8_t)~ADC_CR3_SMPT2;
  ADC1->CR3 |= (uint8_t)(ADC_SamplingTime_16Cycles << 5);

  //ADC_DMACmd( ADC1, DISABLE );
  //Enable VREF channel for conversion
  ADC1->SQR[0] |= ( ADC_SQR1_DMAOFF | ADC_Channel_Vrefint );
  
  //ADC_SchmittTriggerConfig( ADC1, ADC_Channel_Vrefint, ENABLE );
  //ADC1->TRIGR[0] &= (uint8_t)(~(uint8_t) ADC_Channel_Vrefint);                  // this should have been set instead of cleared
  ADC1->TRIGR[0] |= (uint8_t) (ADC_Channel_Vrefint);
  
  //ADC_Cmd( ADC1, DISABLE );
  ADC1->CR1 &= (uint8_t)~ADC_CR1_ADON;
    
  /*****************************************************************************
  ADC1_CR1_bit.ADON = 1; // Enable ADC
  ADC1_TRIGR1_bit.VREFINTON = 1; // Enable internal reference voltage
  ADC1_SQR1_bit.CHSEL_S28 = 1; // Enable CHSEL_SVREFINT fast ADC channel
  ADC1_CR3 = 0x80; // Sampling time = 48 ADC clock cycles, disable analog watchdog channels
  ADC1_SQR1_bit.DMAOFF = 1; // DMA off
  */
}

/**
  * @brief  Measure the value of internal reference voltage with respect to the
  *         supply voltage.
  * @param  None
  * @retval The battery voltage (in effect, value of reference voltage to 256).
  */
uint8_t ADC_VRef_Measure()
{
  uint8_t adc_Result = 0;
  uint8_t counter;
  
  //ADC_Cmd( ADC1, ENABLE );
  ADC1->CR1 |= ADC_CR1_ADON;
  // wait for some time - typical value given by the datasheet is 3us
  delay_us( 10 );
  
  for( counter=0; counter<4; counter++ )
  {
    //ADC_SoftwareStartConv( ADC1 );
    ADC1->CR1 |= ADC_CR1_START;
    
    //ADC_GetFlagStatus( ADC1, ADC_FLAG_EOC );
    while( ( ADC1->SR & ADC_FLAG_EOC ) == RESET );
  
    //adc_Result = ADC_GetConversionValue( ADC1 );
    //adc_Result = (uint16_t)(ADCx->DRH);
    adc_Result += (uint8_t) ADC1->DRL;
    
    if( counter )
      adc_Result = adc_Result>>1;
  }
    
  //ADC_Cmd( ADC1, DISABLE );
  ADC1->CR1 &= (uint8_t)~ADC_CR1_ADON;
  
  return adc_Result;
  
  /*****************************************************************************
  for (cntr = 0; cntr < 4; cntr++) 
  {
    ADC1_CR1_bit.START = 1; // Start ADC conversion, by software trigger
    while (!ADC1_SR_bit.EOC); // Wait for the conversion ends
    adc_res  = (ADC1_DRH << 8); // Get ADC converted data
    adc_res |= ADC1_DRL;
    value += adc_res;
    if (cntr) value >>= 1;
  }
  return value;
  */
}

/**
  * @brief  Disables internal reference voltage, ADC channel and ADC module.
  * @param  None
  * @retval None
  */
void ADC_VRef_Disable() 
{
  //ADC_VrefintCmd( DISABLE );
  ADC1->TRIGR[0] &= (uint8_t) (~ADC_TRIGR1_VREFINTON);
  
  //ADC_SchmittTriggerConfig( ADC1, ADC_Channel_Vrefint, DISABLE );
  ADC1->TRIGR[0] |= (uint8_t) (ADC_Channel_Vrefint);
  
  //ADC_Cmd( ADC1, DISABLE );
  ADC1->CR1 &= (uint8_t)~ADC_CR1_ADON;

  /*****************************************************************************
  ADC1_TRIGR1_bit.VREFINTON = 0; // Disable internal reference voltage
  ADC1_SQR1_bit.CHSEL_S28 = 0; // Disable CHSEL_SVREFINT fast ADC channel
  ADC1_CR1_bit.ADON = 0; // Disable ADC
  */
}
#endif

#ifdef CODE_AS_SENSOR_IRMS

double offsetI = (1 << 11);
double filteredI;

void GPIOADCConfig()
{
  GPIO_Init( ADC_PORT, ADC_PIN, GPIO_Mode_In_FL_No_IT );
}

void SetUpADC()
{
  ADC_DeInit(ADC1);
  CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, ENABLE);
  ADC_Init(ADC1, ADC_ConversionMode_Continuous, ADC_Resolution_12Bit , ADC_Prescaler_2);
/*
  ADC_SamplingTimeConfig(ADC, ADC_Group_SlowChannels ,  ADC_SamplingTime_4Cycles);
  ADC_SchmittTriggerConfig(ADC, ADC_CHANNEL_LEVEL_1 | ADC_CHANNEL_LEVEL_2 | ADC_CHANNEL_LEVEL_3 | ADC_CHANNEL_LEVEL_4 | ADC_CHANNEL_LEVEL_5, DISABLE);
  */
}

double CalculateADCValue(uint16_t numberOfSamples)
{
  double sumI = 0;
  uint16_t sampleI;
  ADC_ChannelCmd(ADC1, ADC_CHANNEL, ENABLE );
  ADC_Cmd(ADC1, ENABLE);
  
  for (uint16_t loop = 0; loop < numberOfSamples; loop++)
  {
    ADC_SoftwareStartConv(ADC1);
    while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);                      // wait for end of conversion
    sampleI = ADC_GetConversionValue(ADC1);                                     // get ADC value

    // Digital low pass filter extracts the 2.5 V or 1.65 V dc offset,
    //  then subtract this - signal is now centered on 0 counts.
    offsetI = (offsetI + (sampleI-offsetI)/4096);
    filteredI = sampleI - offsetI;
    
    sumI += filteredI * filteredI;
  }
  ADC_ChannelCmd(ADC1, ADC_CHANNEL, DISABLE);                                  // disable the channel
  ADC_Cmd(ADC1, DISABLE);
//  sumI = sumI/numberOfSamples;
//  sumI = (uint16_t) sqrt(sumI);
  
  double batteryVoltage;
  batteryVoltage = 1.2 * 255 / GetVRefValue();
  
  double I_RATIO = ICAL *((batteryVoltage/1000.0) / 12);
  double Irms = I_RATIO * sqrt(sumI / numberOfSamples);
  
  return Irms;
}

uint8_t GetVRefValue()
{
  uint8_t value_VRef = 0;
  ADC_VRef_Init();
  delay_ms(15);
  value_VRef = ADC_VRef_Measure();
  ADC_VRef_Disable();
  return value_VRef;
}

uint16_t GetIrms()
{
  double Irms = 0;
  GPIOADCConfig();
  SetUpADC();
  Irms = CalculateADCValue(1480);
  ADC_DeInit(ADC1);
  CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, DISABLE);
  STM8L_Mode = ( STM8L_Mode & STM8L_MODE_DEBUG ) | STM8L_MODE_PACKET_SEND_INITIATE;
  return (uint16_t) (Irms * 100);
}
#endif