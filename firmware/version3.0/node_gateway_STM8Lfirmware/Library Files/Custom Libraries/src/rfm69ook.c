/* 
 * rfm69ook.c
 */

/* Includes ------------------------------------------------------------------*/
#include "rfm69ook.h"

  uint8_t _mode = 1;
  uint8_t _powerLevel = 31;
  bool _isRFM69HW = TRUE;
 
/*
 * This function initializes pins for SPI for communication and configures RFM69
 */
void RFM69_InitOOK()
{
  const uint8_t CONFIG[][2] =
  {
    /* 0x01 */ { REG_OPMODE, RF_OPMODE_SEQUENCER_OFF | RF_OPMODE_LISTEN_OFF | RF_OPMODE_STANDBY },
    /* 0x02 */ { REG_DATAMODUL, RF_DATAMODUL_DATAMODE_CONTINUOUSNOBSYNC | RF_DATAMODUL_MODULATIONTYPE_OOK | RF_DATAMODUL_MODULATIONSHAPING_00 }, // no shaping
    /* 0x03 */ { REG_BITRATEMSB, 0x03},                                 // bitrate: 32768 Hz
    /* 0x04 */ { REG_BITRATELSB, 0xD1},
    /* 0x19 */ { REG_RXBW, RF_RXBW_DCCFREQ_010 | RF_RXBW_MANT_24 | RF_RXBW_EXP_4}, // BW: 10.4 kHz
    /* 0x1B */ { REG_OOKPEAK, RF_OOKPEAK_THRESHTYPE_PEAK | RF_OOKPEAK_PEAKTHRESHSTEP_000 | RF_OOKPEAK_PEAKTHRESHDEC_000 },
    /* 0x1D */ { REG_OOKFIX, RF_OOKFIX_FIXEDTHRESH_VALUE },             // Fixed threshold value (in dB) in the OOK demodulator (default)
    /* 0x29 */ { REG_RSSITHRESH, 140 },                                 // RSSI threshold in dBm = -(REG_RSSITHRESH / 2)                        // not defined constant
    /* 0x6F */ { REG_TESTDAGC, RF_DAGC_IMPROVED_LOWBETA0 },             // run DAGC continuously in RX mode, recommended default for AfcLowBetaOn=0
    {255, 0}
  };

  for (uint8_t i = 0; CONFIG[i][0] != 255; i++)
  {
    RFM69_Write_OOK(CONFIG[i][0], CONFIG[i][1]);
  }

  setHighPower( _isRFM69HW );                                             // called regardless if it's a RFM69W or RFM69HW
  RFM69_OOK_ModeSwitch( RFM69_OOK_MODE_STANDBY );
  RFM69_SlaveDisable();
  
  setBandwidth( OOK_BW_100_0 );
  setFixedThreshold( FIXEDTHRESHOLDVALUE );
  setFrequencyMHz( 433.9 );
  setPowerLevel( 30 );
}

void RFM69_Write_OOK( uint8_t address, uint8_t data )
{
  RFM69_Write( (uint16_t) ((address << 8) | data));
}

/**
 * Change the RFM69 operating mode to a new one.
 * @param newMode The value representing the new mode (see datasheet for
 * further information). The MODE bits are masked in the register, i.e. only
 * bits 2-4 of newMode are ovewritten in the register.
 * @returns RFM_OK for success, RFM_FAIL for failure.
 *
 */
void RFM69_OOK_ModeSwitch(RFM69_OOK_MODE_TypeDef newMode)
{
  if ( newMode == _mode ) 
    return;

  switch( newMode ) 
  {
    case RFM69_OOK_MODE_TX:
      RFM69_Write_OOK( REG_OPMODE, ( RFM69_Read( REG_OPMODE ) & 0xE3 ) | RF_OPMODE_TRANSMITTER );
      if ( _isRFM69HW ) 
        setHighPowerRegs( TRUE );
      break;
      
    case RFM69_OOK_MODE_RX:
      RFM69_Write_OOK( REG_OPMODE, ( RFM69_Read(REG_OPMODE ) & 0xE3 ) | RF_OPMODE_RECEIVER );
      if ( _isRFM69HW ) 
        setHighPowerRegs( FALSE );
      break;
      
    case RFM69_OOK_MODE_SYNTH:
      RFM69_Write_OOK( REG_OPMODE, ( RFM69_Read(REG_OPMODE ) & 0xE3 ) | RF_OPMODE_SYNTHESIZER );
      break;
      
    case RFM69_OOK_MODE_STANDBY:
      RFM69_Write_OOK( REG_OPMODE, ( RFM69_Read( REG_OPMODE) & 0xE3 ) | RF_OPMODE_STANDBY );
      break;
      
    case RFM69_OOK_MODE_SLEEP:
      RFM69_Write_OOK( REG_OPMODE, ( RFM69_Read( REG_OPMODE ) & 0xE3 ) | RF_OPMODE_SLEEP );
      break;
      
    default: return;
  }

  // waiting for mode ready is necessary when going from sleep because the FIFO may not be immediately available from previous mode
  while ( _mode == RFM69_OOK_MODE_SLEEP && ( RFM69_Read(REG_IRQFLAGS1) & RF_IRQFLAGS1_MODEREADY) == 0x00 ); // Wait for ModeReady
  
  _mode = newMode;
}

// Turn the radio into transmission mode
void transmitBegin()
{
  RFM69_OOK_ModeSwitch( RFM69_OOK_MODE_TX );
  // detachInterrupt( _interruptNum ); // not needed in TX mode
  // pinMode( _interruptPin, OUTPUT );
}

// Turn the radio back to standby
void transmitEnd()
{
  // pinMode( _interruptPin, INPUT );
  RFM69_OOK_ModeSwitch( RFM69_OOK_MODE_STANDBY );
}

// Turn the radio into OOK listening mode
void receiveBegin()
{
  // pinMode(_interruptPin, INPUT);
  // attachInterrupt(_interruptNum, isr0, CHANGE); // generate interrupts in RX mode
  RFM69_OOK_ModeSwitch( RFM69_OOK_MODE_RX );
}

// Turn the radio back to standby
void receiveEnd()
{
  RFM69_OOK_ModeSwitch( RFM69_OOK_MODE_STANDBY );
  // detachInterrupt(_interruptNum); // make sure there're no surprises
}

void setHighPower(bool onOff) 
{
  _isRFM69HW = onOff;
  RFM69_Write_OOK( REG_OCP, _isRFM69HW ? RF_OCP_OFF : RF_OCP_ON );
  if (_isRFM69HW) // turning ON
    RFM69_Write_OOK( REG_PALEVEL, ( RFM69_Read(REG_PALEVEL) & 0x1F) | RF_PALEVEL_PA1_ON | RF_PALEVEL_PA2_ON); // enable P1 & P2 amplifier stages
  else
    RFM69_Write_OOK( REG_PALEVEL, RF_PALEVEL_PA0_ON | RF_PALEVEL_PA1_OFF | RF_PALEVEL_PA2_OFF | _powerLevel); // enable P0 only
}

void setHighPowerRegs(bool onOff) 
{
  RFM69_Write_OOK(REG_TESTPA1, onOff ? 0x5D : 0x55);
  RFM69_Write_OOK(REG_TESTPA2, onOff ? 0x7C : 0x70);
}

// set output power: 0=min, 31=max
// this results in a "weaker" transmitted signal, and directly results in a lower RSSI at the receiver
void setPowerLevel(uint8_t powerLevel)
{
  _powerLevel = powerLevel;
  RFM69_Write_OOK(REG_PALEVEL, (RFM69_Read(REG_PALEVEL) & 0xE0) | (_powerLevel > 31 ? 31 : _powerLevel));
}

/*
// return the frequency (in Hz)
uint32_t getFrequency()
{
  return RF69OOK_FSTEP * (((uint32_t)RFM69_Read(REG_FRFMSB)<<16) + ((uint16_t)RFM69_Read(REG_FRFMID)<<8) + RFM69_Read(REG_FRFLSB));
}
*/

// Set literal frequency using floating point MHz value
void setFrequencyMHz(float f)
{
  setFrequency( (uint32_t) ( f * 1000000 ) );
}

// set the frequency (in Hz)
void setFrequency(uint32_t freqHz)
{
  // TODO: p38 hopping sequence may need to be followed in some cases
  freqHz /= RF69OOK_FSTEP;              // divide down by FSTEP to get FRF
  RFM69_Write_OOK(REG_FRFMSB, freqHz >> 16);
  RFM69_Write_OOK(REG_FRFMID, freqHz >> 8);
  RFM69_Write_OOK(REG_FRFLSB, freqHz);
}

/*
// Set bitrate
void setBitrate(uint32_t bitrate)
{
  bitrate = 32000000 / bitrate;          // 32M = XCO freq.
  RFM69_Write_OOK(REG_BITRATEMSB, bitrate >> 8);
  RFM69_Write_OOK(REG_BITRATELSB, bitrate);
}
*/

// set OOK bandwidth
void setBandwidth(uint8_t bw)
{
  RFM69_Write_OOK(REG_RXBW, RFM69_Read(REG_RXBW) & 0xE0 | bw);
}

// set RSSI threshold
void setRSSIThreshold(int8_t rssi)
{
  RFM69_Write_OOK(REG_RSSITHRESH, (-rssi << 1));
}

// set OOK fixed threshold
void setFixedThreshold(uint8_t threshold)
{
  RFM69_Write_OOK(REG_OOKFIX, threshold);
}

/*
// set sensitivity boost in REG_TESTLNA
// see: http://www.sevenwatt.com/main/rfm69-ook-dagc-sensitivity-boost-and-modulation-index
void setSensitivityBoost(uint8_t value)
{
  RFM69_Write_OOK(REG_TESTLNA, value);
}
*/