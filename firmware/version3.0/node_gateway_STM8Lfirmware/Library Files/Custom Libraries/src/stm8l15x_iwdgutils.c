/**
  ******************************************************************************
  * @file    stm8l15x_iwdgutils.c
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   Handles the function definitions associated with independent 
  *          watchdog timer.
  *         
  *  @verbatim
  *           
  *  @endverbatim
  *         
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  * 
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8l15x_iwdgutils.h"

/**
  * @brief  Sets up the independent watchdog timer and reloads the counter.
  * @param  none
  * @retval none
  */
void SetUpWatchDog()
{
  //IWDG_Enable();
  IWDG->KR = IWDG_KEY_ENABLE;
  //IWDG_WriteAccessCmd( IWDG_WriteAccess_Enable );
  IWDG->KR = IWDG_WriteAccess_Enable;
  //IWDG_SetPrescaler( IWDG_Prescaler_256 );
  IWDG->PR = IWDG_Prescaler_256;
  //IWDG_SetReload( OxFF );
  IWDG->RLR = 0xFF;
  //IWDG_ReloadCounter();
  IWDG->KR = IWDG_KEY_REFRESH;
}
