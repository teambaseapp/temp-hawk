/**
  ******************************************************************************
  *
  * @file
  * @author  Allan Tom Mathew
  * @version 1.0
  * @date    08th November, 2016
  * @brief   This is the main .c file
  *
  *
  ******************************************************************************
  * @attention
  *
  * <h2><center>BASEAPP SYSTEMS, 2016 </center></h2>
  *
  *
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "header.h"

/**
  * @brief  Initialises the GPIO peripherals for the project.
  * @param  None
  * @retval None
  */
void GPIOConf()
{
  GPIO_DeInit( GPIOA );
  GPIO_DeInit( GPIOB );
  GPIO_DeInit( GPIOC );
  GPIO_DeInit( GPIOD );
  EXTI_DeInit();
  GPIOSPIConf();
  GPIORFM69();
#ifdef CODE_AS_SENSOR
  GPIOExtiConf();
#elif defined(CODE_AS_GATEWAY)
  GPIOUSARTConf();
#elif defined(CODE_AS_SENSOR_OOK)
  GPIORxConf();
#elif defined(CODE_AS_SENSOR_IRMS)
  GPIOExtiConf();
#endif
#if defined(LED_FOR_DEBUG_OPERATION) || defined(LED_FOR_NORMAL_OPERATION)
  GPIOLEDInit();
#endif
}

/**
  * @brief  Checks the initialisation and working of all the peripherals.
  *         If any of the peripherals is found to be non functional, then
  *         STM8L goes to halt mode indefenitely (to save power).
  * @param  None
  * @retval None
  */
void CheckPeripherals()
{
  STM8L_Error = STM8L_ERROR_NO_ERROR;
  /* RFM69 Peripheral Checking */
#ifdef LED_FOR_DEBUG_OPERATION
  YELLOW_LED_ON;                                                                // LED to indicate error
#endif
#if defined(CODE_AS_SENSOR)
  if( !( RFM69_CheckDevice() && HTU20D_CheckDevice() ) )
#elif defined(CODE_AS_GATEWAY) || defined(CODE_AS_SENSOR_OOK) || defined(CODE_AS_SENSOR_IRMS)
  if( !( RFM69_CheckDevice() ) )
#endif
  {
    //EnterLowPowerMode( 0 );                                                     // error with RFM69HW or HTU20D initialization detected
    STM8L_Mode = STM8L_MODE_POWER_DOWN;
    ChangePowerMode();
  }
#ifdef LED_FOR_DEBUG_OPERATION
  YELLOW_LED_OFF;                                                               // LED to indicate error
  StartUpBlinkLED();
#endif
  //RST_ClearFlag( RST_FLAG_PORF );
  RST->SR = (uint8_t) RST_FLAG_PORF;
}

void InterruptPriority()
{
  /* sets interrupt priority */
  ITC_SetSoftwarePriority( TIM4_UPD_OVF_TRG_IRQn, ITC_PriorityLevel_0 );
  ITC_SetSoftwarePriority( EXTI6_IRQn, ITC_PriorityLevel_1 );
}

/**
  * @brief  Sets up the associated peripherals for the project.
  * @param  None
  * @retval None
  */
void SetUpPeripherals()
{
  /* set up all the associated peripherals */
  disableInterrupts();
  ClockConf();
  GPIOConf();
  SetUpTimer3();
#if defined (CODE_AS_SENSOR) || defined(CODE_AS_SENSOR_IRMS)
  PWR_FastWakeUpCmd( DISABLE );
  PWR_UltraLowPowerCmd( DISABLE );
  delay_ms( 15 );
#endif
  
  RFM69PowerUp();
  delay_ms( 15 );
  SetUpSPI();
  SetUpEEPROM();
#ifdef CODE_AS_SENSOR
  SetUpI2C();
  SetUpRTC();
  ADC_VRef_Init();
  delay_ms( 15 );
  RFM69_FSK_EntryTx();
  SetUpWatchDog();
#elif defined(CODE_AS_GATEWAY)
  SetUpUSART();
  delay_ms( 15 );
  RFM69_FSK_EntryRx();
  SetUpWatchDog();
#elif defined(CODE_AS_SENSOR_OOK)
  SetUpTimer4();
  RFM69_InitOOK();
  receiveBegin();
  SetRx();
  delay_ms( 15 );
  InterruptPriority();
#elif defined(CODE_AS_SENSOR_IRMS)
  SetUpRTC();
  RFM69_FSK_EntryTx();
  SetUpWatchDog();
#endif
  /* check the associated peripherals */
  //if( RST_GetFlagStatus( RST_FLAG_PORF )
  if( RST->SR & RST_FLAG_PORF )
  {
    CheckPeripherals();
  }
#ifdef LED_FOR_DEBUG_OPERATION
  StartUpBlinkLED();
#endif
  /* enable interrupts */
#if defined(CODE_AS_SENSOR) || defined(CODE_AS_SENSOR_OOK) || defined(CODE_AS_SENSOR_IRMS)
  RTC_ClearITPendingBit( RTC_IT_WUT );
  enableInterrupts();
#endif
}

STM8L_MODE STM8L_Mode = STM8L_MODE_POWER_UP;
STM8L_ERROR STM8L_Error = STM8L_ERROR_NO_ERROR;


/**
  * @brief  Program execution starts here.
  * @param  None
  * @retval None
  */
void main()
{
#if defined(CODE_AS_GATEWAY) || defined(CODE_AS_SENSOR_OOK)
  SetUpPeripherals();
#endif

  while(1)
  {
#if defined(CODE_AS_SENSOR) || defined(CODE_AS_SENSOR_IRMS)
    ChangePowerMode();
    SendData();
    STM8L_Mode = STM8L_MODE_POWER_DOWN;
#endif

#ifdef CODE_AS_GATEWAY
    ReceiveData();
    IWDG->KR = IWDG_KEY_REFRESH;
#endif
    
#ifdef CODE_AS_SENSOR_OOK
    SendOOKData();
#endif
  }
}