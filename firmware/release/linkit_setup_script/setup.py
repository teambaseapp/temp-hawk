pendrive_partition = '/dev/sda1'

print("###########TempHawk Gateway setup script#########")

import serial
import threading
import time
import glob

SHUTDOWN = False

last_cmd_result = ""
catch_result = ""
catch_flag = False

port = glob.glob("/dev/ttyUSB*")
if len(port) > 1:
    for i,p in enumerate(port):
        print("%d:%s"%(i,p))
    try:
        x = int(input("Pick a port:"))
        port = port[x]
    except:
        print("wrong input")
        exit(1)
else:
    port = port[0]


ser = serial.Serial(port, 57600, timeout=5)

def serial_monitor():
    global last_cmd_result, catch_flag, catch_result
    while not SHUTDOWN:
        try:
            x = ser.readline().decode()
            last_cmd_result = x
            if len(catch_result) > 0 and catch_result in last_cmd_result:
                catch_flag = True

            print("tty: %s"%x, end='')
        except:
            print("decode error")


smon = threading.Thread(target=serial_monitor)
smon.start()


def cmd(s):
    ser.write(bytes('%s\r'%s, 'utf-8'))

def catch(s):
    global catch_flag, catch_result
    catch_flag = False
    catch_result = s

step1 = step2 = step3 = False

try:
    print("Waiting for console to get ready")
    while not step1:
        time.sleep(1)
        catch('root@mylinkit:')

        while not catch_flag:
            cmd('')
            time.sleep(2)

        if len(last_cmd_result) != 0:
            # it worked, next step
            step1 = True
            print("console available, proceeding")

    while not step2:
    #if 1:
        time.sleep(1)
        # assuming we got it working
        cmd('cd /root/')
        catch(pendrive_partition)
        cmd('ls /dev/sd*')
        time.sleep(1)
        if not catch_flag:
            print("USB drive not detected on linkit... is it attached? (attach it to the port that says 'HOST')")
        else:
            # we got the usb drive
            cmd('mkdir flashdir/')

            catch("failed")
            cmd('mount '+pendrive_partition+' flashdir/')
            time.sleep(1)
            if catch_flag:
                # failed!
                print("Mounting error, is it FAT formatted?  Try again")
                cmd('umount '+pendrive_partition)
                continue

            step2 = True

    while not step3:
        time.sleep(1)
        print("Invoking flash script; timeout in 600 seconds")
        catch('DONE')
        cmd('/root/flashdir/flash')

        timeout = 600
        while not catch_flag:
            time.sleep(1)
            timeout -= 1
            if timeout < 0:
                print("Failed!")
                SHUTDOWN = True
                exit(1)

        cmd('/etc/init.d/cron enable')
        time.sleep(1)
        cmd('/etc/init.d/cron start')
        
        print("Setup Success")
        cmd('umount '+pendrive_partition)

        time.sleep(1)
        cmd('rm -r /root/flashdir')
        time.sleep(1)
        cmd('ls /root/')
        time.sleep(1)

        step3 = True

    print("==============Exiting================")
    SHUTDOWN = True

except KeyboardInterrupt:
    SHUTDOWN = True

