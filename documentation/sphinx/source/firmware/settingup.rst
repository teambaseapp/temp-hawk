--------------------------
Setting Up The Environment
--------------------------

IAR Embedded Workbench
^^^^^^^^^^^^^^^^^^^^^^

If a new project is created
---------------------------

   1. Starting a new project

      a. Open IAR Embedded Workbench
		
      b. Click on to Project > Create New Project  

      .. image:: ./pics/startup/createproject_1.jpg
         :align: center

      c. Select 'C' as the programming language

      .. image:: ./pics/startup/createproject_2.jpg
         :align: center

      d. Save the project with a custom name

      .. image:: ./pics/startup/createproject_3.jpg
         :align: center

      e. To add libraries, add folders to the existing project

      .. image:: ./pics/startup/createproject_4.jpg
         :align: center

		
      f. To add library files, add files to the existing project

      .. image:: ./pics/startup/createproject_5.jpg
         :align: center

	
   2. Copy the library files into the working directory (along with main.c).
	
   3. Setting the variables in IAR Embedded Workbench

      a. Open the 'Options' tab of the project.

      .. image:: ./pics/startup/iarconf_1.jpg
         :align: center

      b. Set micro controller as 'STM8L051F3' (or the one that you are using).

      .. image:: ./pics/startup/iarconf_2.jpg
         :align: center

      c. Give the location to the files which needs to be included.

      .. image:: ./pics/startup/iarconf_3.jpg
         :align: center

      d. Set the debugger as 'ST-LINK' (if you are using it).

      .. image:: ./pics/startup/iarconf_4.jpg
         :align: center

   4. Save your workspace with a user defined name.
	
   5. Click on to 'Download and Debug' option to burn the code and start the debugging session.

   .. image:: ./pics/startup/iarconf_5.jpg
      :align: center
	  
   6. You can end the debugging session if not needed. The program will start execution.
	
If the project is cloned from GitHub
------------------------------------

   1. Clone the repository from GitHub.
   
   2. Check if the variables in IAR Embedded Workbench are correct (especially the path for pre processor to include files). Refer step 3 before.
   
   3. Refer step 5 before.