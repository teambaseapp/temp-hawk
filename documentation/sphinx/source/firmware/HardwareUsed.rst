-------------
Hardware Used
-------------

The hardware used for the project are:
 | ``STM8L Microcontroller``
 | ``RFM69HW Transceiver Module``
 | ``HTU20D Temperature-Humidity Sensor``


STM8L Microcontroller
---------------------
ST\’s ultra-low-power product lines support a wide number of applications where consumption is critical, such as in portable devices. The STM8L, based on the 8-bit STM8 core, benefits from our proprietary ultra-low-leakage process, shared with the STM32L family, and features an ultra-low power consumption of 0.30 µA with the lowest power mode.

 | IC Used : STM8L051F3

Features: 	
 
			* Low power features
			 	* - 5 low power modes: Wait, Low power run (5.1 µA), Low power wait (3 µA), Active-halt with RTC (1.3 µA), Halt (350 nA)
				* – Ultra-low leakage per I/0: 50 nA
				* – Fast wakeup from Halt: 5 µs
			* Clock management
				* – 32 kHz and 1 to 16 MHz crystal oscillators
				* – Internal 16 MHz factory-trimmed RC
				* – Internal 38 kHz low consumption RC
				* – Clock security system
			* Low power RTC
				* – BCD calendar with alarm interrupt
				* – Digital calibration with +/- 0.5 ppm accuracy
				* – LSE security system
				* – Auto-wakeup from Halt w/ periodic interrupt
			* Timers
				* – Two 16-bit timers with 2 channels (used as
				* IC, OC, PWM), quadrature encoder
				* – One 8-bit timer with 7-bit prescaler
			* Communication interfaces
				* – Synchronous serial interface (SPI)
				* – Fast I2C 400 kHz SMBus and PMBus
				* – USART
			* Up to 18 I/Os, all mappable on interrupt vectors
			* Development support
				* – Fast on-chip programming and nonintrusive debugging with SWIM
				* – Bootloader using USART
				
The microcontroller is used in both Sensor (which acts as transmitter) and Gateway (which acts as a receiver).

Sensor (as transmitter)
^^^^^^^^^^^^^^^^^^^^^^^

The Sensor transmits information acquired throught its sensors via onboard RFM69HW transceiver chip. The information that it will send are:
1. Configuration byte
2. Device ID
3. Temperature raw data
4. Humidity raw data
5. Door sensor status
6. Battery output voltage status

Gateway (as receiver)
^^^^^^^^^^^^^^^^^^^^^

The Gateway receives the information transmitted by Sensor with the help of onboard RFM69HW transceiver chip. The RFM69HW transceiver chip acts as a receiver for most of its working time. It receives a predefined lenggth of data and interrupts the microcontroller to take forward the processing of the data.

The information is processed and made sure that it corresponds to the concerned Sensor with which the Gateway is paired. If the information contains a device ID which doesn't match with the pre-stored Sensor device ID, then the packet is discarded and it waits for the next packet to be received. The received packet, if it belongs to the paired Sensor, is processed and then pushed to the USART where the LINKIT is connected.
				


RFM69HW ISM Transceiver Module
------------------------------
The RFM69HW is a transceiver module capable of operation over a wide frequency range, including the 315,433,868 and 915MHz license-free ISM (Industry Scientific and Medical) frequency bands. All major RF communication parameters are programmable and most of them can be dynamically set. The RFM69HW offers the unique advantage of programmable narrow-band and wide- band communication modes. The RFM69HW is optimized for low power consumption while offering high RF output power and channelized operation. Compliance ETSI and FCC regulations. In order to better use RFM69HW modules, this specification also involves a large number of the parameters and functions of its core chip RF69H's,including those IC pins which are not leaded out. All of these can help customers gain a better understanding of the performance of RFM69HW modules, and enhance the application skills. 

 | IC Used : RFM69HW

Features: 
			* +20 dBm - 100 mW Power Output Capability
 			* High Sensitivity: down to -120 dBm at 1.2 kbps
 			* High Selectivity: 16-tap FIR Channel Filter
 			* Bullet-proof front end: IIP3 = -18 dBm, IIP2 = +35 dBm,80dB Blocking Immunity, no Image Frequency response
 			* Low current: Rx = 16 mA, 100nA register retention
 			* Programmable Pout: -18 to +20 dBm in 1dB steps
 			* Constant RF performance over voltage range of module
 			* FSK Bit rates up to 300 kb/s
 			* Fully integrated synthesizer with a resolution of 61 Hz
 			* FSK, GFSK, MSK, GMSK and OOK modulations
 			* Built-in Bit Synchronizer performing Clock Recovery
 			* Incoming Sync Word Recognition
 			* 115 dB+ Dynamic Range RSSI
 			* Automatic RF Sense with ultra-fast AFC
 			* Packet engine with CRC-16, AES-128, 66-byte FIFO
 			* Built-in temperature sensor
 			* Module Size:19.7X16mm

HTU20D Temperature-Humidity Sensor
----------------------------------

