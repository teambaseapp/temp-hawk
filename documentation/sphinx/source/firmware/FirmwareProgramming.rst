----------------
Firmware Program
----------------

The firmware is a single program which takes care of both Sensor and Gateway functionalities. This is implemented as the code for both the programs are similar in nature and they share similar functionalities. Conditional compilation is employeed to differentiate between and generate seperate code for the Sensor and Gateway code. Thus, using ``#define`` can change the code from Sensor to Gateway and vice-versa.

The conditional compilation also have several other advantages of which the prominant one is, a single change in the firmware code can affect both Sensor and Gateway, instead of editing individual project files of Sensor and Gateway. This comes in handy when the project is further developed or adapted to implement in a different environment.

Some of the modes and designs of the program has been done keeping in mind the easiness to edit, debug and expand the project into different levels. Different functionalities have been handled in different files with functions for each functionality making it more portable and easy to use. Sufficient comments have been done to make it more readable too.

Program Flow
^^^^^^^^^^^^

Sensor Module
-------------

The RFM69HW in Sensor module is by default in Transmit mode. This makes the device, by default, a transmitter. The module will send a packet of data in 60s (as defined by the macro ``STM8L_POWER_DOWN_TIME_SEC`` in the ``config.h`` file) and goes to active-halt mode, thereby taking only a nominal power. This will ensure that the module, which is to be placed in a remote location, will be functional for a long time without the human intervention. The value of the configuration byte for a normal packet will be ``RFM69_DATA_CONFIG_REQ_NOACK``.

The Sensor can also work in a debug mode. Debug mode can be used by pressing the on board button switch. Sensor, in debug mode, in effect, sends a packet with an acknowledgement request, waits for acknowledgement and processes the received data. Thus, debug mode is used effectively to ensure the proper working of the whole set-up at a given place and time. This mode can be used to:
	
	* check the range of the device
	* check if the Sensor module is working
	* check if the Gateway is working
	* check the battery of the Sensor module
	
In debug mode, the Sensor sends a packet to the Gateway with the configuration byte set to ``RFM69_DATA_CONFIG_REQ_ACK``. This informs the Gateway that the Sensor is in debug mode and it would take necessary actions. After sending a packet with acknowledgement request, the Sensor switches its RFM69HW to reciever mode, so as to receive an incoming packet, and waits for one second. After the wait, the Sensor checks for any incoming data. The received packet should have the configuration byte set to ``RFM69_DATA_CONFIG_ACK_BYTE`` and the device ID set to the ID present in its EEPROM.

Gateway Module
--------------

The RFM69HW in Gateway modile is by default in receive mode. This makes the module, by default, a receiver. The module is tuned to receive packet of data from any Sensor in its vicinity. It would make sure that the received data packet is a valid one by ensuring that the packet follows the standard packet format set for the project. The proper packet is then trimmed to get the relevant data, considering the offset that may occur. The data is then transmitted to LINKIT, connected to its USART. The packet details such as ``RFM69_DATA_INDEX_SYNC_BYTE``, ``RFM69_DATA_INDEX_CONFIG`` and ``RFM69_DATA_INDEX_END_OF_PACKET`` will be omitted. A de-limiter ``'\n'`` will added at the end to mark the end of data transmission. The standard packet data format will be followed at LINKIT reception for its data interpretation.

The Gateway can respond to a acknowledgement request by any Sensor by going to debug mode. If the Gateway receives a data packet with its configuration byte set as ``RFM69_DATA_CONFIG_REQ_ACK``, then the Gateway goes to debug mode.

In debug mode, the Gateway carry out all the functionalities as in a normal mode. Along with that, it goes into transmit mode, creates a packet with the device ID of the requested Sensor and with configuration byte as ``RFM69_DATA_CONFIG_ACK_BYTE`` and transmits the packet (twice). Thus it will cater to the acknowledgement request of the Sensor module.

STM8L Modes
^^^^^^^^^^^
To handle different situations, STM8L has to work accordingly. For better understanding and easy debugging, the different situations that STM8L goes through has been named accordingly. Effort had been made to make the names of the modes as close as to what they perform, but I should say that I'm not really good at naming ;-)

The modes defined in the program are:

	* STM8L_MODE_POWER_DOWN: 
		* (Sensor): Initiates a power down.
		* (Gateway): NA
	* STM8L_MODE_POWER_UP: 
		* (Sensor): After a wakeup or at boot time this mode is used to initialize the peripherals and devices.
		* (Gateway): By default, STM8L will be in power up mode.
	* STM8L_MODE_PACKET_SEND_INITIATE: 
		* (Sensor): Initiates a packet send command.
		* (Gateway): Initiates a packet send command.
	* STM8L_MODE_PACKET_SEND: 
		* (Sensor): This mode is set when the packet is confirmed to be send.
		* (Gateway): This mode is set when the packet is confirmed to be send.
	* STM8L_MODE_PACKET_RECEIVED: 
		* (Sensor): This mode is set when a new packet has been received.
		* (Gateway): This mode is set when a new packet has been received.
	* STM8L_MODE_POWER_UP_INITIATE: 
		* (Sensor): When the door sensor status is toggled when in power up mode, this mode initiates sending a new packet of data with the updated value of door sensor.
		* (Gateway): NA
	* STM8L_MODE_DEBUG: 
		* (Sensor): This mode is set when the button is pressed, requesting for an ACK. 
		* (Gateway): This mode is set when Sensor requests for an ACK.
	* STM8L_MODE_DEBUG_INITIATE: 
		* (Sensor): In sensor, if the button is pressed while in power up mode, then this mode initiates sending another packet with ACK request after the current packet is send.
		* (Gateway): NA
		
STM8L Errors
^^^^^^^^^^^^

The recent update records the onboard peripheral errors occurs. This is transmitted over the transceiver to the receiver so that the error can be recorded in the database. Ofcourse, the error on the transceiver cannot be recorded, obviously.

The errors that are handled are:

	* STM8L_ERROR_NO_ERROR
	* STM8L_ERROR_RFM69_NO_DEVICE
	* STM8L_ERROR_HTU20D_NO_DEVICE
	* STM8L_ERROR_HTU20D_I2C_ERROR
	* STM8L_ERROR_HTU20D_I2C_NOT_RESPONDING

Data Packet
^^^^^^^^^^^

The transmitter trasmits and the receiver interpretes the data as a data packet in a specified format. This is specified in ``stm8l15x_stringutils.h`` (just in case I forget to update the documentation for a small change in the program, this should come in handy ;-) ).

The packet format for temperature-humidity-sensor is:

+-------+-------------------------------+---------------+----------------------------+
| Sl.No.|              FIELD            |     VALUE     |        DESCRIPTION         |
+=======+===============================+===============+============================+
|   01. | RFM69_DATA_INDEX_SYNC_BYTE    | Fixed value:  | Indicates start of data    |
|       |                               | 0xFF (1 byte) | (along with config byte).  |
+-------+-------------------------------+---------------+----------------------------+
|   02. | RFM69_DATA_INDEX_CONFIG       | 1 byte data   | Indicates what device the  |
|       |                               |               | transmitter is.            |
+-------+-------------------------------+---------------+----------------------------+ 
|   03. | RFM69_DATA_INDEX_DEVICE_ID    | 2 byte data   | Indicates the device ID    |  
|       |                               |               | written into the STM8L     |
|       |                               |               | EEPROM.                    |
+-------+-------------------------------+---------------+----------------------------+
|   04. | RFM69_DATA_INDEX_ERRORS       | 1 byte data   | Indicates the errors:      | 
|       |                               |               |  | 0x00 - no error         |
|       |                               |               |  | else - error            |  
|       |                               |               |                            |
+-------+-------------------------------+---------------+----------------------------+
|   05. | RFM69_DATA_INDEX_TEMPERATURE  | 2 byte data   | Temperature raw data value.|
+-------+-------------------------------+---------------+----------------------------+
|   06. | RFM69_DATA_INDEX_HUMIDITY     | 14 bit data   | Humidity raw data value.   |
+-------+-------------------------------+---------------+----------------------------+
|   07. | RFM69_DATA_INDEX_SENSOR1      | 1 bit data    | Sensor1 data               |
+-------+-------------------------------+---------------+----------------------------+
|   08. | RFM69_DATA_INDEX_SENSOR2      | 1 bit data    | Sensor2 data               |
+-------+-------------------------------+---------------+----------------------------+
|   09. | RFM69_DATA_INDEX_DOOR_BATTERY | 1 byte data   | First bit has door status  |
|       |                               |               | and the rest seven bits    | 
|       |                               |               | have battery level status. |
+-------+-------------------------------+---------------+----------------------------+
|   10. | RFM69_DATA_INDEX_END_OF_PACKET| Fixed value:  | Indicates the end of       |
|       |                               | 0x00 (1 byte) | packet data.               |
+-------+-------------------------------+---------------+----------------------------+   

The packet format for current-sensor is:

+-------+-------------------------------+---------------+----------------------------+
| Sl.No.|              FIELD            |     VALUE     |        DESCRIPTION         |
+=======+===============================+===============+============================+
|   01. | RFM69_DATA_INDEX_SYNC_BYTE    | Fixed value:  | Indicates start of data    |
|       |                               | 0xFF (1 byte) | (along with config byte).  |
+-------+-------------------------------+---------------+----------------------------+
|   02. | RFM69_DATA_INDEX_CONFIG       | 1 byte data   | Indicates what device the  |
|       |                               |               | transmitter is.            |
+-------+-------------------------------+---------------+----------------------------+ 
|   03. | RFM69_DATA_INDEX_DEVICE_ID    | 2 byte data   | Indicates the device ID    |  
|       |                               |               | written into the STM8L     |
|       |                               |               | EEPROM.                    |
+-------+-------------------------------+---------------+----------------------------+
|   04. | RFM69_DATA_INDEX_ERRORS       | 1 byte data   | Indicates the errors:      | 
|       |                               |               |  | 0x00 - no error         |
|       |                               |               |  | else - error            |  
|       |                               |               |                            |
+-------+-------------------------------+---------------+----------------------------+
|   05. | RFM69_DATA_INDEX_ERRORS + 1   | 2 byte data   | Irms Value (in cA).        |
+-------+-------------------------------+---------------+----------------------------+  
|   06. | RFM69_DATA_INDEX_ERRORS + 2   | 3 byte data   | Fixed value: 0x00          |
+-------+-------------------------------+---------------+----------------------------+
|   10. | RFM69_DATA_INDEX_END_OF_PACKET| Fixed value:  | Indicates the end of       |
|       |                               | 0x00 (1 byte) | packet data.               |
+-------+-------------------------------+---------------+----------------------------+  

The packet format for OOK-relay-sensor is:

+-------+-------------------------------+---------------+----------------------------+
| Sl.No.|              FIELD            |     VALUE     |        DESCRIPTION         |
+=======+===============================+===============+============================+
|   01. | RFM69_DATA_INDEX_SYNC_BYTE    | Fixed value:  | Indicates start of data    |
|       |                               | 0xFF (1 byte) | (along with config byte).  |
+-------+-------------------------------+---------------+----------------------------+
|   02. | RFM69_DATA_INDEX_CONFIG       | 1 byte data   | Indicates what device the  |
|       |                               |               | transmitter is.            |
+-------+-------------------------------+---------------+----------------------------+ 
|   03. | RFM69_DATA_INDEX_DEVICE_ID    | 2 byte data   | Indicates the device ID    |  
|       |                               |               | written into the STM8L     |
|       |                               |               | EEPROM.                    |
+-------+-------------------------------+---------------+----------------------------+
|   04. | RFM69_DATA_INDEX_ERRORS       | 1 byte data   | Indicates the errors:      | 
|       |                               |               |  | 0x00 - no error         |
|       |                               |               |  | else - error            |  
|       |                               |               |                            |
+-------+-------------------------------+---------------+----------------------------+
|   05. | RFM69_DATA_INDEX_ERRORS + 1   | 4 byte data   | OOK device ID value.       |
+-------+-------------------------------+---------------+----------------------------+
|   06. | RFM69_DATA_INDEX_ERRORS + 5   | 1 byte data   | Fixed value: 0x00          |
+-------+-------------------------------+---------------+----------------------------+
|   10. | RFM69_DATA_INDEX_END_OF_PACKET| Fixed value:  | Indicates the end of       |
|       |                               | 0x00 (1 byte) | packet data.               |
+-------+-------------------------------+---------------+----------------------------+


Sensor (as transmitter)
^^^^^^^^^^^^^^^^^^^^^^^

Sensors Interfaced
	* RFM69HW (SPI)
	* HTU20 (I2C)
	* Door Status Indicator (GPIO)

Periperals Used
	* EEPROM
	* RTC
	* SPI
	* I2C
	* ADC
	* Timer 3
	* Independent Watch Dog Timer
	
Interrupts Used
	* External Interrupt Pin 3 : For Door Sensor
	* External Interrupt Pin 5 : For Button Press
	* RTC Wake up Timer Interrupt : For Low Power Modes

Points to be noted while programming the Sensor
	* Wake up time for RFM69HW: Register values will indicate the change of state. Poll these and perform suitable actions.
	* Wake up time for HTU20 = max 15ms (in case of a soft reset).
	
Gateway (as receiver)
^^^^^^^^^^^^^^^^^^^^^

Sensors Interfaced
	* LINKIT (USART)
	* RFM69HW (SPI)

Periperals Used
	* EEPROM
	* SPI
	* USART

Interrupts Used
	* USART Rx (used to probe the Gateway)

Points to be noted while programming the Gateway
	* There is a time window to respond to a acknowledgement request. The Sensor takes about 20ms to restart the RFM69HW and then turn on to receiver mode. Any packets send before this time will not be picked up by the Sensor. The Sensor waits for one second after requesting for an acknowledgement and thus the reply should be send within one second.


Data Acquisition
^^^^^^^^^^^^^^^^

RFM69_DATA_INDEX_DEVICE_ID: Each Sensor board has a unique two byte device ID programmed into the EEPROM of the STM8L micro controller. This ensures that multiple Sensor boards can be employed and the communication to the Gateway doesn't get mixed up. The EEPROM can retain the two byte device ID even after a power failure and thus it ensures a permanent means of storing the ID.

RFM69_DATA_INDEX_TEMPERATURE: The onboard HTU20D IC collects the temperature value. HTU20D is programmed to give a 14-bit value to calculate temperature. Temperature can be calculated from the formula:

.. math::

   Temperature = -46.85 + ( 175.72 * value ) / 65536

RFM69_DATA_INDEX_HUMIDITY: The onboard HTU20D IC collects the humidity value too. It is programmed to give a 12-bit value to calculate humidity. Humidity can be calculated from the formula:

.. math::

   Humidity = -6 + ( 125 * value ) / 65536

RFM69_DATA_INDEX_DOOR_BATTERY: The status of the door [open\close] is monitored by a GPIO pin of STM8L. The battery voltage with which the board works is acquired with the means of using reference voltage of STM8L (reference voltage of STM8L with which the input voltage is compared is 1.2V). For this, the internal refernce voltage is enabled (note that it takes more power) and is fed into one of the channels of ADC to get a digital value as the output. This value can be used to find out the actual voltage with which the device is working. The formula to find out the voltage is:

.. math::

   Voltage = ( 1.2 * 255 ) / value

NOTE: The voltage to be measured for this is in the range 2.4V to 3.3V. Any value more or less than this limit will not yield a correct answer with this formula. This code can be modified to work with wider range but it is not in the scope of this project. It would take more code and memory space and thus not implemented here.


