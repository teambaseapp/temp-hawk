var group___t_i_m2___input___capture___prescaler =
[
    [ "TIM2_ICPSC_TypeDef", "group___t_i_m2___input___capture___prescaler.html#ga0f15999e6dcf204eac258ab3ad871d37", [
      [ "TIM2_ICPSC_DIV1", "group___t_i_m2___input___capture___prescaler.html#gga0f15999e6dcf204eac258ab3ad871d37ad92db63fc27cc1e4c547f7a94df769f5", null ],
      [ "TIM2_ICPSC_DIV2", "group___t_i_m2___input___capture___prescaler.html#gga0f15999e6dcf204eac258ab3ad871d37a8276209193f70bc9dcad52ad357a8949", null ],
      [ "TIM2_ICPSC_DIV4", "group___t_i_m2___input___capture___prescaler.html#gga0f15999e6dcf204eac258ab3ad871d37a4fe960c276764853d1cb8efe0353b9f9", null ],
      [ "TIM2_ICPSC_DIV8", "group___t_i_m2___input___capture___prescaler.html#gga0f15999e6dcf204eac258ab3ad871d37ad64f46ce16effbdf1c953b985d86f087", null ]
    ] ]
];