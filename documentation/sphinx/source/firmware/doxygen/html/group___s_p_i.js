var group___s_p_i =
[
    [ "SPI_Exported_Types", "group___s_p_i___exported___types.html", "group___s_p_i___exported___types" ],
    [ "SPI_Exported_Macros", "group___s_p_i___exported___macros.html", null ],
    [ "SPI_Private_Functions", "group___s_p_i___private___functions.html", "group___s_p_i___private___functions" ],
    [ "SPI_BiDirectionalLineConfig", "group___s_p_i.html#ga50646069062aa24309e80de3d2c5f0ea", null ],
    [ "SPI_CalculateCRCCmd", "group___s_p_i.html#gaa936b41c6f1c8010749991a7f1014d9b", null ],
    [ "SPI_ClearFlag", "group___s_p_i.html#ga84cc626656a536a16f5c0c0bbf896686", null ],
    [ "SPI_ClearITPendingBit", "group___s_p_i.html#gacc28dcf2c3ee71bd2181945ef7bf85bc", null ],
    [ "SPI_Cmd", "group___s_p_i.html#gaa31357879a65ee1ed7223f3b9114dcf3", null ],
    [ "SPI_DeInit", "group___s_p_i.html#gae33578348c53fb7140d994563bdf8ae4", null ],
    [ "SPI_DMACmd", "group___s_p_i.html#ga2a486478b63bfaee5502287ed2b494b0", null ],
    [ "SPI_GetCRC", "group___s_p_i.html#gafccc31b40ebfd15731224471594a7669", null ],
    [ "SPI_GetCRCPolynomial", "group___s_p_i.html#ga05cdf3960458dfa79cba51ea58480162", null ],
    [ "SPI_GetFlagStatus", "group___s_p_i.html#gabf038f294dae6cce6bec69363efbbbbe", null ],
    [ "SPI_GetITStatus", "group___s_p_i.html#ga751336d92aedea121635bd9f62fbc57f", null ],
    [ "SPI_Init", "group___s_p_i.html#gacac5219c062caa8f557e47100b56a9ae", null ],
    [ "SPI_ITConfig", "group___s_p_i.html#ga372b5fe70c731c2292c394a0b84434c3", null ],
    [ "SPI_NSSInternalSoftwareCmd", "group___s_p_i.html#gac1f0e025eb134259716595eb958de87d", null ],
    [ "SPI_ReceiveData", "group___s_p_i.html#gae9642bca158c93c67cd25e2df85c66ca", null ],
    [ "SPI_ResetCRC", "group___s_p_i.html#ga27ad061b3ab8e3492bfde4e9d573e1db", null ],
    [ "SPI_SendData", "group___s_p_i.html#gad860e6d7900346070bc7840e12d8a897", null ],
    [ "SPI_TransmitCRC", "group___s_p_i.html#gace8b1058e09bab150b0dbe5978810273", null ]
];