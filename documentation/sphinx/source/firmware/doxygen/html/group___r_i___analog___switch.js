var group___r_i___analog___switch =
[
    [ "RI_AnalogSwitch_TypeDef", "group___r_i___analog___switch.html#ga439c0d6cedb8b3bf9df66feff701a71e", [
      [ "RI_AnalogSwitch_0", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71ea03b0d270594764efa27c4857f5c52c2d", null ],
      [ "RI_AnalogSwitch_1", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71ea7137e7e772eebd7be69ee223ee120a49", null ],
      [ "RI_AnalogSwitch_2", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71eae2f0c94df38fc690afc297949c9827d1", null ],
      [ "RI_AnalogSwitch_3", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71ea87625fcaca4fab7e4ea8af4ceeba5ce5", null ],
      [ "RI_AnalogSwitch_4", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71eaefa732271a3fa78c9ff836a4a3cc1b56", null ],
      [ "RI_AnalogSwitch_5", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71eaf7503bf7ffdcb6df6ff8bfadc86d5d55", null ],
      [ "RI_AnalogSwitch_6", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71eab1c4dfeebbdee3a9e3fbb0897b7eec3c", null ],
      [ "RI_AnalogSwitch_7", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71eabab5359e1fee402f124aced7a0ec2011", null ],
      [ "RI_AnalogSwitch_8", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71ea7d0444cc2b933c9331bbae4ceac23260", null ],
      [ "RI_AnalogSwitch_9", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71ea9db29f29f648c0ef3138c53dcab315ad", null ],
      [ "RI_AnalogSwitch_10", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71eacde5dff8cdf5e42895f994aa7962add3", null ],
      [ "RI_AnalogSwitch_11", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71ea980f75022e8c80201bf55b0270d12657", null ],
      [ "RI_AnalogSwitch_14", "group___r_i___analog___switch.html#gga439c0d6cedb8b3bf9df66feff701a71ea19f5a8dee5ef0e52958ef06d66f23e2a", null ]
    ] ]
];