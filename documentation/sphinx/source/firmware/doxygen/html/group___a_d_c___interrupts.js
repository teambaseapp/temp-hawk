var group___a_d_c___interrupts =
[
    [ "ADC_IT_TypeDef", "group___a_d_c___interrupts.html#ga8252d206617ecd3d8e631c9819c8723a", [
      [ "ADC_IT_EOC", "group___a_d_c___interrupts.html#gga8252d206617ecd3d8e631c9819c8723aa7dfdb96f2f332f604215f90af4735a6b", null ],
      [ "ADC_IT_AWD", "group___a_d_c___interrupts.html#gga8252d206617ecd3d8e631c9819c8723aa0b1c6bea170a6133a010f2f2dc69c91a", null ],
      [ "ADC_IT_OVER", "group___a_d_c___interrupts.html#gga8252d206617ecd3d8e631c9819c8723aa0af30dee42122e65b4aec6970777765d", null ]
    ] ]
];