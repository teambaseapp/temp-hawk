var group___c_l_k___system___clock___divider =
[
    [ "CLK_SYSCLKDiv_TypeDef", "group___c_l_k___system___clock___divider.html#gabbba644738a39aa9345e6e7412de1323", [
      [ "CLK_SYSCLKDiv_1", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323aa9a13655163781dc0325bebba137cac3", null ],
      [ "CLK_SYSCLKDiv_2", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323a61601741fece994c7449a40cd58827aa", null ],
      [ "CLK_SYSCLKDiv_4", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323a2d7ce7257bb97f707c98653b4bc7796c", null ],
      [ "CLK_SYSCLKDiv_8", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323a6fda4c47578a4b603a2767ac5a1c7efa", null ],
      [ "CLK_SYSCLKDiv_16", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323ad3a5ff09f1de0aada554d1b824c861e3", null ],
      [ "CLK_SYSCLKDiv_32", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323ace1c7ff6d9177afc26394f7204ec64db", null ],
      [ "CLK_SYSCLKDiv_64", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323aa84d6f9fa295e0ad400a46778550fe2b", null ],
      [ "CLK_SYSCLKDiv_128", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323ab3a8a9d2543956ff7629ce726e61c6d2", null ]
    ] ]
];