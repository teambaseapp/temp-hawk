var group___t_i_m1___group3 =
[
    [ "TIM1_GetCapture1", "group___t_i_m1___group3.html#ga672bf2825ee50908f82ee72eaa52d7ba", null ],
    [ "TIM1_GetCapture2", "group___t_i_m1___group3.html#ga24f1288b22a4b332f7c67df16c4ad00e", null ],
    [ "TIM1_GetCapture3", "group___t_i_m1___group3.html#gadbce3918632dbc1ecb131161ea4239f2", null ],
    [ "TIM1_GetCapture4", "group___t_i_m1___group3.html#gaf8a0278ad0125abbb9039c7b09ee253f", null ],
    [ "TIM1_ICInit", "group___t_i_m1___group3.html#ga3d30a7e0a85a985fdf8f6d4cb5544bad", null ],
    [ "TIM1_PWMIConfig", "group___t_i_m1___group3.html#gafc5fd5696318e1db2d1c00891f7e6875", null ],
    [ "TIM1_SetIC1Prescaler", "group___t_i_m1___group3.html#ga4ffdd3fe818c5f5c43b6c662bd4fdb70", null ],
    [ "TIM1_SetIC2Prescaler", "group___t_i_m1___group3.html#ga7efb5262ab2a225c62bf6414f02ab635", null ],
    [ "TIM1_SetIC3Prescaler", "group___t_i_m1___group3.html#ga0aad403881fd1c4cf9ab7cceed902e59", null ],
    [ "TIM1_SetIC4Prescaler", "group___t_i_m1___group3.html#ga7ca43f015e245fcda56e7372e572a07f", null ]
];