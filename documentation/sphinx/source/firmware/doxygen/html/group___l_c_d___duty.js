var group___l_c_d___duty =
[
    [ "LCD_Duty_TypeDef", "group___l_c_d___duty.html#ga542cb34a2f78c34c0fda1d2c7b05e967", [
      [ "LCD_Duty_Static", "group___l_c_d___duty.html#gga542cb34a2f78c34c0fda1d2c7b05e967aeef89a9f326fb42313320572510b9b09", null ],
      [ "LCD_Duty_1_2", "group___l_c_d___duty.html#gga542cb34a2f78c34c0fda1d2c7b05e967a3ee147331c63f2acb8ce2d0ade247aeb", null ],
      [ "LCD_Duty_1_3", "group___l_c_d___duty.html#gga542cb34a2f78c34c0fda1d2c7b05e967ad1aa5dddae75c4419f9b412eb644a7af", null ],
      [ "LCD_Duty_1_4", "group___l_c_d___duty.html#gga542cb34a2f78c34c0fda1d2c7b05e967a07e37bce82924366156fa057fd499f18", null ],
      [ "LCD_Duty_1_8", "group___l_c_d___duty.html#gga542cb34a2f78c34c0fda1d2c7b05e967ac1dc4d8cc5d953174910ed2bbd63603c", null ]
    ] ]
];