var group___u_s_a_r_t___private___functions =
[
    [ "Initialization and Configuration functions", "group___u_s_a_r_t___group1.html", "group___u_s_a_r_t___group1" ],
    [ "Data transfers functions", "group___u_s_a_r_t___group2.html", "group___u_s_a_r_t___group2" ],
    [ "MultiProcessor Communication functions", "group___u_s_a_r_t___group3.html", "group___u_s_a_r_t___group3" ],
    [ "Halfduplex mode function", "group___u_s_a_r_t___group4.html", "group___u_s_a_r_t___group4" ],
    [ "Smartcard mode functions", "group___u_s_a_r_t___group5.html", "group___u_s_a_r_t___group5" ],
    [ "IrDA mode functions", "group___u_s_a_r_t___group6.html", "group___u_s_a_r_t___group6" ],
    [ "DMA transfers management functions", "group___u_s_a_r_t___group7.html", "group___u_s_a_r_t___group7" ],
    [ "Interrupts and flags management functions", "group___u_s_a_r_t___group8.html", "group___u_s_a_r_t___group8" ]
];