var group___s_p_i___direction___mode =
[
    [ "SPI_DirectionMode_TypeDef", "group___s_p_i___direction___mode.html#ga240d445db61962084c2af41635ef6b6c", [
      [ "SPI_Direction_2Lines_FullDuplex", "group___s_p_i___direction___mode.html#gga240d445db61962084c2af41635ef6b6ca71992216e4296704e7067bd8e933c318", null ],
      [ "SPI_Direction_2Lines_RxOnly", "group___s_p_i___direction___mode.html#gga240d445db61962084c2af41635ef6b6ca18dd82351780eaacb0da2ee356423fce", null ],
      [ "SPI_Direction_1Line_Rx", "group___s_p_i___direction___mode.html#gga240d445db61962084c2af41635ef6b6cabbbb9d8f08ae8c76a078145b0b53b461", null ],
      [ "SPI_Direction_1Line_Tx", "group___s_p_i___direction___mode.html#gga240d445db61962084c2af41635ef6b6ca57c7ec178e7d3951b483cee4e85e1e19", null ]
    ] ]
];