var group___t_i_m4___exported___macros =
[
    [ "IS_TIM4_DMA_SOURCE", "group___t_i_m4___exported___macros.html#ga2219747198f91717307eecf4a2f92508", null ],
    [ "IS_TIM4_EVENT_SOURCE", "group___t_i_m4___exported___macros.html#gae95239cc9d8a916686c8a4aea7eb24fb", null ],
    [ "IS_TIM4_GET_FLAG", "group___t_i_m4___exported___macros.html#gaa5806a5976f7b9f226d212e728bea52e", null ],
    [ "IS_TIM4_IT", "group___t_i_m4___exported___macros.html#ga2d75edf82b1adacae00cf0805b7f0b0e", null ],
    [ "IS_TIM4_OPM_MODE", "group___t_i_m4___exported___macros.html#gae714c09449d424c2933fe1542b286769", null ],
    [ "IS_TIM4_Prescaler", "group___t_i_m4___exported___macros.html#ga9ea404521ed0853355697a9ba80d9259", null ],
    [ "IS_TIM4_Prescaler_RELOAD", "group___t_i_m4___exported___macros.html#gac8ffb8c077cce0809697f6931f134d96", null ],
    [ "IS_TIM4_SLAVE_MODE", "group___t_i_m4___exported___macros.html#ga7e24a87e32b3f94d515a5fab46ff4ac4", null ],
    [ "IS_TIM4_TRGO_SOURCE", "group___t_i_m4___exported___macros.html#gac82334fd8867e097e3c5c21c24a3948d", null ],
    [ "IS_TIM4_TRIGGER_SELECTION", "group___t_i_m4___exported___macros.html#ga135774592f1486b4f3cc80fd63fbb89f", null ],
    [ "IS_TIM4_UPDATE_SOURCE", "group___t_i_m4___exported___macros.html#gaa5d9a262f8917df1a526e05455f9c565", null ]
];