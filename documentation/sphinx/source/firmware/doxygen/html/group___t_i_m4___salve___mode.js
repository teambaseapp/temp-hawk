var group___t_i_m4___salve___mode =
[
    [ "TIM4_SlaveMode_TypeDef", "group___t_i_m4___salve___mode.html#gac22be9bb6535a9ea3ed45c0893367aab", [
      [ "TIM4_SlaveMode_Disable", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba13764467dbcfa31ea39332d689ba6cbd", null ],
      [ "TIM4_SlaveMode_Reset", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba8a41acd86a1589a8d4907ac6717cfc85", null ],
      [ "TIM4_SlaveMode_Gated", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba5b0c8750acc9a973cf1c518630edbca7", null ],
      [ "TIM4_SlaveMode_Trigger", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba3bee656f4434a9b4d911d90016d59efb", null ],
      [ "TIM4_SlaveMode_External1", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba4cad33d754b4e25a8484ec6922ee8a0d", null ]
    ] ]
];