var group___l_c_d___clock___prescaler =
[
    [ "LCD_Prescaler_TypeDef", "group___l_c_d___clock___prescaler.html#gab35551ef7a4288e4fd6dff37b170f28f", [
      [ "LCD_Prescaler_1", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fad17e6116c3740b1668f3c1b695fe2e72", null ],
      [ "LCD_Prescaler_2", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa9611f700aec8e59685e11f3b7e9e598d", null ],
      [ "LCD_Prescaler_4", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28faa0438f90675e2d989d523492a3262395", null ],
      [ "LCD_Prescaler_8", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fabcff862528e8cc4a1890538030a5a49d", null ],
      [ "LCD_Prescaler_16", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa2eb12850f864b186c54a78c0a4835763", null ],
      [ "LCD_Prescaler_32", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa01f90f7654babee6eaba376427375455", null ],
      [ "LCD_Prescaler_64", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28facf7644198f28e7a89d3ff89585cf6ebb", null ],
      [ "LCD_Prescaler_128", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa0143878a05aea99db4d4a849de120b85", null ],
      [ "LCD_Prescaler_256", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa16435bfe0d9c5359fd3bad6d4c396bef", null ],
      [ "LCD_Prescaler_512", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fad06331d8e1095798e047a0efad7b7e9d", null ],
      [ "LCD_Prescaler_1024", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa328fa97cee5e5b95233144b06cb8f57d", null ],
      [ "LCD_Prescaler_2048", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28faefa7a03ab13b826f903550b6aafb929f", null ],
      [ "LCD_Prescaler_4096", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa74c373be336f60d450f5b299138b9f46", null ],
      [ "LCD_Prescaler_8192", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa303de6b3c590d86848b6c4606c9cb75a", null ],
      [ "LCD_Prescaler_16384", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa1f8dfeac2ddf2028e04bc02519897fac", null ],
      [ "LCD_Prescaler_32768", "group___l_c_d___clock___prescaler.html#ggab35551ef7a4288e4fd6dff37b170f28fa55a1a8e971b6ae55fecbd32c74b70b3c", null ]
    ] ]
];