var group___r_e_m_a_p___pin =
[
    [ "REMAP_Pin_TypeDef", "group___r_e_m_a_p___pin.html#ga0c61ece92bb8a0cf914b552fe8487640", [
      [ "REMAP_Pin_USART1TxRxPortA", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a28da5f8091cc490a857f674dc0054c38", null ],
      [ "REMAP_Pin_USART1TxRxPortC", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640aff1c9e5065ef865e3be2968e55cf369c", null ],
      [ "REMAP_Pin_USART1Clk", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a712e9e63490a59db9bfb8f0f0f233e61", null ],
      [ "REMAP_Pin_SPI1Full", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640accedc6d64a5092189fda2bcbb949e822", null ],
      [ "REMAP_Pin_ADC1ExtTRIG1", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a56e2944bbfd3198cb0f060354dff1875", null ],
      [ "REMAP_Pin_TIM2TRIGPortA", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640ac1a5c703aa5ab91130bd47789fe97cb3", null ],
      [ "REMAP_Pin_TIM3TRIGPortA", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640aa69fc4fe1c67ab1bc6c04b15d9dbcf5c", null ],
      [ "REMAP_Pin_TIM2TRIGLSE", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a835fb21186160691d99061547757ba43", null ],
      [ "REMAP_Pin_TIM3TRIGLSE", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a1aa20530eda2600ab2d030dbcf9cba99", null ],
      [ "REMAP_Pin_SPI2Full", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a12b5fd86da71241ca3af7feaf812c111", null ],
      [ "REMAP_Pin_TIM3TRIGPortG", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a1b1704135e078f16b050197d24232353", null ],
      [ "REMAP_Pin_TIM23BKIN", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a96f2541927e4305b913e26257232d3f9", null ],
      [ "REMAP_Pin_SPI1PortF", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640af406caf284c8af2ddfe4aebaea0ed748", null ],
      [ "REMAP_Pin_USART3TxRxPortF", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640ade0a05a3652a4e622f532b07f35e0cdc", null ],
      [ "REMAP_Pin_USART3Clk", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640ae1bbaecad5ac3d69c8846e5172ee8f61", null ],
      [ "REMAP_Pin_TIM3Channel1", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a125a7e9802f30daf1a712f31aea42798", null ],
      [ "REMAP_Pin_TIM3Channel2", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a41fbcf5f16357287929fce818f1b22e0", null ],
      [ "REMAP_Pin_CCO", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a7baa2049972a130ba73620113d41a5e2", null ],
      [ "REMAP_Pin_TIM2Channel1", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640ad505214c8347794cf213057b772992b6", null ],
      [ "REMAP_Pin_TIM2Channel2", "group___r_e_m_a_p___pin.html#gga0c61ece92bb8a0cf914b552fe8487640a1907f5d11a8bf119d15529548c295fc6", null ]
    ] ]
];