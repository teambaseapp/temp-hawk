var group___i2_c___group5 =
[
    [ "I2C_CheckEvent", "group___i2_c___group5.html#ga521444fcd698cb72c6a1de082fa74bfa", null ],
    [ "I2C_ClearFlag", "group___i2_c___group5.html#ga136bbff39fe5ee329b82222b59fe07c4", null ],
    [ "I2C_ClearITPendingBit", "group___i2_c___group5.html#ga9fe406c192af437c2c84c44b11b9553f", null ],
    [ "I2C_GetFlagStatus", "group___i2_c___group5.html#ga662fb6b0d31ffdfbec1611344b210a7c", null ],
    [ "I2C_GetITStatus", "group___i2_c___group5.html#ga3e913c1d33a485912b5a7a05ce350414", null ],
    [ "I2C_GetLastEvent", "group___i2_c___group5.html#gac5449cba88a33db9cbdb7da2ca6bbe95", null ],
    [ "I2C_ITConfig", "group___i2_c___group5.html#gaf6fe3e132b0ea57e9a39c4f5ed8575e4", null ],
    [ "I2C_ReadRegister", "group___i2_c___group5.html#ga23afb6c322a4fd77c455451cbaf6bee9", null ]
];