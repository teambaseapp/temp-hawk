var stm8l15x__dma_8c =
[
    [ "DMA_ClearFlag", "group___d_m_a___group3.html#ga0063297f18de3167658c463357ab8686", null ],
    [ "DMA_ClearITPendingBit", "group___d_m_a___group3.html#ga2f422ecd7419a3a0aa1438ac96fc3dbd", null ],
    [ "DMA_Cmd", "group___d_m_a___group1.html#ga8ccbd3d4d9474f1deb863764f6cedac8", null ],
    [ "DMA_DeInit", "group___d_m_a___group1.html#ga0349a6c19a891b73b084bd416a4c262a", null ],
    [ "DMA_GetCurrDataCounter", "group___d_m_a___group2.html#ga3f64eeb2a2ec2503495b0cfbfe89c5d2", null ],
    [ "DMA_GetFlagStatus", "group___d_m_a___group3.html#ga387655c480f0bd470236b7b942c34679", null ],
    [ "DMA_GetITStatus", "group___d_m_a___group3.html#ga7322b7d0d82cac72f9e19ac42b01564d", null ],
    [ "DMA_GlobalCmd", "group___d_m_a___group1.html#ga21cf4640a3e3b9dc913492c6058e6478", null ],
    [ "DMA_GlobalDeInit", "group___d_m_a___group1.html#gab652552339b6124b0e87713d738135fc", null ],
    [ "DMA_Init", "group___d_m_a___group1.html#gad4265428d8ee58df39cbddc0d6be60ec", null ],
    [ "DMA_ITConfig", "group___d_m_a___group3.html#ga706009972e7ca3a21c13780481f1866d", null ],
    [ "DMA_SetCurrDataCounter", "group___d_m_a___group2.html#ga505849e107066de90369a7297ba70fb8", null ],
    [ "DMA_SetTimeOut", "group___d_m_a___group1.html#ga2bcc772a1005733c85ecbf37cc806302", null ]
];