var group___i2_c___group1 =
[
    [ "I2C_AcknowledgeConfig", "group___i2_c___group1.html#ga7bb44e894d68a7991f564c43fb187486", null ],
    [ "I2C_AckPositionConfig", "group___i2_c___group1.html#gaf1270fe97ef57b19f66ff7a87b8795a7", null ],
    [ "I2C_ARPCmd", "group___i2_c___group1.html#ga66d86742bf1be58b17ef8779ffc79d02", null ],
    [ "I2C_Cmd", "group___i2_c___group1.html#ga7e1323c9133c2cb424dfb5b10b7d2f0b", null ],
    [ "I2C_DeInit", "group___i2_c___group1.html#ga2ee214364603059ad5d9089f749f5bfd", null ],
    [ "I2C_DualAddressCmd", "group___i2_c___group1.html#ga02145a333a56e79557d6ef4ea03fc313", null ],
    [ "I2C_FastModeDutyCycleConfig", "group___i2_c___group1.html#ga7c0846858f419302378d31e1191ff3f9", null ],
    [ "I2C_GeneralCallCmd", "group___i2_c___group1.html#ga65c740fc8d7b3b9f15cc432d8699d471", null ],
    [ "I2C_GenerateSTART", "group___i2_c___group1.html#ga36c522b471588be9779c878222ccb20f", null ],
    [ "I2C_GenerateSTOP", "group___i2_c___group1.html#ga5c92cb573ca0ae58cc465e5400246561", null ],
    [ "I2C_Init", "group___i2_c___group1.html#ga7c8b9bd292ed516a7e15d9b826d94fdf", null ],
    [ "I2C_OwnAddress2Config", "group___i2_c___group1.html#ga7be2cc634a613c8e3539137e897a22df", null ],
    [ "I2C_Send7bitAddress", "group___i2_c___group1.html#gaa938cdbb3256dab565079ca25e671d9e", null ],
    [ "I2C_SMBusAlertConfig", "group___i2_c___group1.html#gab030585acd827291c5df83415c2e42f9", null ],
    [ "I2C_SoftwareResetCmd", "group___i2_c___group1.html#ga1289c908aeb882443aba323b459c638b", null ],
    [ "I2C_StretchClockCmd", "group___i2_c___group1.html#ga7459feb3b1dfcd3e4f6574002ca7d3bd", null ]
];