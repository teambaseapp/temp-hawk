var group___d_m_a___exported___types =
[
    [ "DMA_Data_Transfer_Direction", "group___d_m_a___data___transfer___direction.html", "group___d_m_a___data___transfer___direction" ],
    [ "DMA_Mode", "group___d_m_a___mode.html", "group___d_m_a___mode" ],
    [ "DMA_Incremented_Mode", "group___d_m_a___incremented___mode.html", "group___d_m_a___incremented___mode" ],
    [ "DMA_Priority", "group___d_m_a___priority.html", "group___d_m_a___priority" ],
    [ "DMA_Memory_Data_Size", "group___d_m_a___memory___data___size.html", "group___d_m_a___memory___data___size" ],
    [ "DMA_Flags", "group___d_m_a___flags.html", "group___d_m_a___flags" ],
    [ "DMA_One_Channel_Interrupts", "group___d_m_a___one___channel___interrupts.html", "group___d_m_a___one___channel___interrupts" ],
    [ "DMA_Interrupts", "group___d_m_a___interrupts.html", "group___d_m_a___interrupts" ]
];