var stm8l15x__beep_8h =
[
    [ "BEEP_Frequency_TypeDef", "group___b_e_e_p___frequency.html#ga0f7c41a259b1d3f6d2b4cc338a70d58b", [
      [ "BEEP_Frequency_1KHz", "group___b_e_e_p___frequency.html#gga0f7c41a259b1d3f6d2b4cc338a70d58ba8092ce42f6a469543b0af4bf4345ea02", null ],
      [ "BEEP_Frequency_2KHz", "group___b_e_e_p___frequency.html#gga0f7c41a259b1d3f6d2b4cc338a70d58bad9dd85e026033368a6b87090edcce0bf", null ],
      [ "BEEP_Frequency_4KHz", "group___b_e_e_p___frequency.html#gga0f7c41a259b1d3f6d2b4cc338a70d58ba5af98cd947658100c34abc3f3196bca6", null ]
    ] ],
    [ "BEEP_Cmd", "group___b_e_e_p.html#gaa83f09563e4d520e949b061b7f98e8c5", null ],
    [ "BEEP_DeInit", "group___b_e_e_p.html#gaa51205c91b68013ff6c5bb1fa724bccc", null ],
    [ "BEEP_Init", "group___b_e_e_p.html#gae29084588d06e1aa2872ed306daed265", null ],
    [ "BEEP_LSClockToTIMConnectCmd", "group___b_e_e_p.html#ga6d9a5615d6ee8133b6b6e3939c38ffe6", null ],
    [ "BEEP_LSICalibrationConfig", "group___b_e_e_p.html#gaa84c5df6579ebee737a8d4192a01dc58", null ]
];