var stm8l15x__syscfg_8c =
[
    [ "SYSCFG_REMAPDeInit", "group___s_y_s_c_f_g___group2.html#gaa517ee155ea523f2868edcfa4c1f624e", null ],
    [ "SYSCFG_REMAPDMAChannelConfig", "group___s_y_s_c_f_g___group2.html#ga78b8f91af0a066d3505da7ed2a07c019", null ],
    [ "SYSCFG_REMAPPinConfig", "group___s_y_s_c_f_g___group2.html#ga79ea5bb15ac56cc379bc42f0d8869d03", null ],
    [ "SYSCFG_RIAnalogSwitchConfig", "group___s_y_s_c_f_g___group1.html#ga62d85a51d6104dd7eeadc79d2a3744ea", null ],
    [ "SYSCFG_RIDeInit", "group___s_y_s_c_f_g___group1.html#gafab651b7c7ec03cacd5b8da27e830a9d", null ],
    [ "SYSCFG_RIIOSwitchConfig", "group___s_y_s_c_f_g___group1.html#ga8650b07326e86a563614d60c98e6bb57", null ],
    [ "SYSCFG_RIResistorConfig", "group___s_y_s_c_f_g___group1.html#ga81091cfaf8d9f9ffc016d47072371a4f", null ],
    [ "SYSCFG_RITIMInputCaptureConfig", "group___s_y_s_c_f_g___group1.html#gae9d33d1ebc33a07a7f0b0423cf508f24", null ]
];