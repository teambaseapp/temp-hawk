var group___r_t_c___tamper___precharge___duration =
[
    [ "RTC_TamperPrechargeDuration_TypeDef", "group___r_t_c___tamper___precharge___duration.html#ga348c619cb46e1afeb4f63b9b9d268b72", [
      [ "RTC_TamperPrechargeDuration_None", "group___r_t_c___tamper___precharge___duration.html#gga348c619cb46e1afeb4f63b9b9d268b72a1fed3d569aa361c3bf0d0552b03eb6af", null ],
      [ "RTC_TamperPrechargeDuration_1RTCCLK", "group___r_t_c___tamper___precharge___duration.html#gga348c619cb46e1afeb4f63b9b9d268b72aeddf3bd3bd415a63e6bb01c21b0ab0eb", null ],
      [ "RTC_TamperPrechargeDuration_2RTCCLK", "group___r_t_c___tamper___precharge___duration.html#gga348c619cb46e1afeb4f63b9b9d268b72adaa7a813060bc2423e203e499630d642", null ],
      [ "RTC_TamperPrechargeDuration_4RTCCLK", "group___r_t_c___tamper___precharge___duration.html#gga348c619cb46e1afeb4f63b9b9d268b72a3cf8c9df337a38617f6ad67be839c9c4", null ],
      [ "RTC_TamperPrechargeDuration_8RTCCLK", "group___r_t_c___tamper___precharge___duration.html#gga348c619cb46e1afeb4f63b9b9d268b72a95eb875b2ff45189f090703ac69a4989", null ]
    ] ]
];