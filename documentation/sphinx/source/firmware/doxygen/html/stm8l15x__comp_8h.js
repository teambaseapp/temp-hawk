var stm8l15x__comp_8h =
[
    [ "COMP_Edge_TypeDef", "group___c_o_m_p___edge.html#ga034a737e01648f495084b91395043406", [
      [ "COMP_Edge_Falling", "group___c_o_m_p___edge.html#gga034a737e01648f495084b91395043406aed95463c7acc9edeb7fd8c37f30c51b3", null ],
      [ "COMP_Edge_Rising", "group___c_o_m_p___edge.html#gga034a737e01648f495084b91395043406a00f7d60e8855f425152ea3fb92c85347", null ],
      [ "COMP_Edge_Rising_Falling", "group___c_o_m_p___edge.html#gga034a737e01648f495084b91395043406a4196a73cdff61cf22b2d734a7f8a7885", null ]
    ] ],
    [ "COMP_InvertingInput_Typedef", "group___c_o_m_p___inverting___input___selection.html#gaacd9fca051beb79f6d8bfd9c8ec26ae9", [
      [ "COMP_InvertingInput_IO", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9a9c1f5b5ff61ac52afee457f8a941786c", null ],
      [ "COMP_InvertingInput_VREFINT", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9abd6749d804fe3f9eb8c40910fb011de8", null ],
      [ "COMP_InvertingInput_3_4VREFINT", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9a8cbf9a86e0ae04062043073d99f7665f", null ],
      [ "COMP_InvertingInput_1_2VREFINT", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9a59b6d17fb9c7fe1ea80f5f30fa2eceac", null ],
      [ "COMP_InvertingInput_1_4VREFINT", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9ac51a911dcaab8d16666a4d34366056ef", null ],
      [ "COMP_InvertingInput_DAC1", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9acd18eb133022fe8d6735ed3330db5cbe", null ],
      [ "COMP_InvertingInput_DAC2", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9af968302729dfabdd8b00f316ca00b0c5", null ]
    ] ],
    [ "COMP_OutputLevel_TypeDef", "group___c_o_m_p___output___level.html#gade0e058a433acf3a7f326e2b6dc40e07", [
      [ "COMP_OutputLevel_Low", "group___c_o_m_p___output___level.html#ggade0e058a433acf3a7f326e2b6dc40e07a2c7b2574f78285bfeac832ea715aaab5", null ],
      [ "COMP_OutputLevel_High", "group___c_o_m_p___output___level.html#ggade0e058a433acf3a7f326e2b6dc40e07ab0cdd7dff7c8352db217d401e7e61692", null ]
    ] ],
    [ "COMP_OutputSelect_Typedef", "group___c_o_m_p2___output___selection.html#ga00b47e0f0d99287a8cadfd97c94cc3ec", [
      [ "COMP_OutputSelect_TIM2IC2", "group___c_o_m_p2___output___selection.html#gga00b47e0f0d99287a8cadfd97c94cc3eca854e3125cd14f1c2f281d4d517204baf", null ],
      [ "COMP_OutputSelect_TIM3IC2", "group___c_o_m_p2___output___selection.html#gga00b47e0f0d99287a8cadfd97c94cc3ecac132d904c43617168d3740877b058335", null ],
      [ "COMP_OutputSelect_TIM1BRK", "group___c_o_m_p2___output___selection.html#gga00b47e0f0d99287a8cadfd97c94cc3ecab2131d17cba32360aae1996dff4b5863", null ],
      [ "COMP_OutputSelect_TIM1OCREFCLR", "group___c_o_m_p2___output___selection.html#gga00b47e0f0d99287a8cadfd97c94cc3ecab50520baa983bf9a149b3a8f3de24b8b", null ]
    ] ],
    [ "COMP_Selection_TypeDef", "group___c_o_m_p___selection.html#gad3edb4f414107c243ae5ce9b6798f371", [
      [ "COMP_Selection_COMP1", "group___c_o_m_p___selection.html#ggad3edb4f414107c243ae5ce9b6798f371a859837ad30d3915f301b5ae9d9d2f123", null ],
      [ "COMP_Selection_COMP2", "group___c_o_m_p___selection.html#ggad3edb4f414107c243ae5ce9b6798f371a02520d1756177c571e391ecace0a17ea", null ]
    ] ],
    [ "COMP_Speed_TypeDef", "group___c_o_m_p___speed.html#gaa338bfedd33d15fd1c0bd835d09fe1a6", [
      [ "COMP_Speed_Slow", "group___c_o_m_p___speed.html#ggaa338bfedd33d15fd1c0bd835d09fe1a6a996aff1bb428aa8297e82bb28ff91ece", null ],
      [ "COMP_Speed_Fast", "group___c_o_m_p___speed.html#ggaa338bfedd33d15fd1c0bd835d09fe1a6abc58a07c2b1cb31e196e6f73e6a47f3f", null ]
    ] ],
    [ "COMP_TriggerGroup_TypeDef", "group___c_o_m_p___trigger___group.html#ga8fae4f4600845fd05328df9a739ba2a4", [
      [ "COMP_TriggerGroup_InvertingInput", "group___c_o_m_p___trigger___group.html#gga8fae4f4600845fd05328df9a739ba2a4a0a6beff945e7cc56ecbf663cde8b7c62", null ],
      [ "COMP_TriggerGroup_NonInvertingInput", "group___c_o_m_p___trigger___group.html#gga8fae4f4600845fd05328df9a739ba2a4a42b07803841f817033a692459bdd49fe", null ],
      [ "COMP_TriggerGroup_VREFINTOutput", "group___c_o_m_p___trigger___group.html#gga8fae4f4600845fd05328df9a739ba2a4ac0b3b27fbedf8f6cc8e7eee2a9f36b84", null ],
      [ "COMP_TriggerGroup_DACOutput", "group___c_o_m_p___trigger___group.html#gga8fae4f4600845fd05328df9a739ba2a4a3c7f84a44c530f13cea67305e8552364", null ]
    ] ],
    [ "COMP_TriggerPin_TypeDef", "group___c_o_m_p___trigger___pin.html#gac4b387d2e61169e44b5e90679fcab369", [
      [ "COMP_TriggerPin_0", "group___c_o_m_p___trigger___pin.html#ggac4b387d2e61169e44b5e90679fcab369a95369d2103de9005920299baaac03b14", null ],
      [ "COMP_TriggerPin_1", "group___c_o_m_p___trigger___pin.html#ggac4b387d2e61169e44b5e90679fcab369a69569e48e661e01bf4d958417fe10f21", null ],
      [ "COMP_TriggerPin_2", "group___c_o_m_p___trigger___pin.html#ggac4b387d2e61169e44b5e90679fcab369a802e7f057519af9878062bdf02494904", null ]
    ] ],
    [ "COMP_ClearFlag", "group___c_o_m_p.html#gae6f1c1ce82575c7507e819248e76b34d", null ],
    [ "COMP_ClearITPendingBit", "group___c_o_m_p.html#ga30f23b9298a45bb664af297493ff366b", null ],
    [ "COMP_DeInit", "group___c_o_m_p.html#ga6425d7445b304d2dd1bf57c1fb5e70c2", null ],
    [ "COMP_EdgeConfig", "group___c_o_m_p.html#ga2024f9f2ba375b51e49c6a1fbd749584", null ],
    [ "COMP_GetFlagStatus", "group___c_o_m_p.html#ga07711477a11774cf11a982db191cac9e", null ],
    [ "COMP_GetITStatus", "group___c_o_m_p.html#gaef83885a9947d64b394bd2a12fb16c64", null ],
    [ "COMP_GetOutputLevel", "group___c_o_m_p.html#ga85d4acd8f0b09d19f533b1dc3c60fd61", null ],
    [ "COMP_Init", "group___c_o_m_p.html#ga6b0439181e7fe8c1f89a34c8a8c33527", null ],
    [ "COMP_ITConfig", "group___c_o_m_p.html#gab3d794eaa8bc584ae195c044b00aa573", null ],
    [ "COMP_SchmittTriggerCmd", "group___c_o_m_p.html#gaa249e923a4b1b122bf1574218a0da284", null ],
    [ "COMP_TriggerConfig", "group___c_o_m_p.html#ga5f5899c5799a4275aadc5fe2b3f8950b", null ],
    [ "COMP_VrefintOutputCmd", "group___c_o_m_p.html#ga7cc2f05181085dda3c4f0e35351e7cff", null ],
    [ "COMP_VrefintToCOMP1Connect", "group___c_o_m_p.html#gaf51bd5556f4085b68ee156b484200483", null ],
    [ "COMP_WindowCmd", "group___c_o_m_p.html#ga65b8886e4554b35de6211f46c7b55b41", null ]
];