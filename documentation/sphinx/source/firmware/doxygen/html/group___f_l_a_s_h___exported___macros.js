var group___f_l_a_s_h___exported___macros =
[
    [ "IS_FLASH_ADDRESS", "group___f_l_a_s_h___exported___macros.html#gaf307c2d9a20b7b11b5fb39e16db7adf3", null ],
    [ "IS_FLASH_DATA_EEPROM_ADDRESS", "group___f_l_a_s_h___exported___macros.html#ga92390502b3b3806050f2689dd5388563", null ],
    [ "IS_FLASH_DATA_EEPROM_BLOCK_NUMBER", "group___f_l_a_s_h___exported___macros.html#ga0aaae19ab063f0af40bc7f3138603712", null ],
    [ "IS_FLASH_FLAGS", "group___f_l_a_s_h___exported___macros.html#ga324247ea3312bd68c053620b1e6024fd", null ],
    [ "IS_FLASH_MEMORY_TYPE", "group___f_l_a_s_h___exported___macros.html#gaa51e737a4b64855dae0d324dbc080e9e", null ],
    [ "IS_FLASH_POWER", "group___f_l_a_s_h___exported___macros.html#gaf67d1f03ae92b29e2cae7aa773174bfe", null ],
    [ "IS_FLASH_POWERSTATUS", "group___f_l_a_s_h___exported___macros.html#gaff65817615fdaa6c89c0810dd5da6fe3", null ],
    [ "IS_FLASH_PROGRAM_ADDRESS", "group___f_l_a_s_h___exported___macros.html#gaec93a51b4ca2287a4cae7483db004773", null ],
    [ "IS_FLASH_PROGRAM_BLOCK_NUMBER", "group___f_l_a_s_h___exported___macros.html#gad760c6bef06e998831ba972b9438cc9b", null ],
    [ "IS_FLASH_PROGRAM_MODE", "group___f_l_a_s_h___exported___macros.html#gaf97eb1ca93233076dcb96e3f6eb8bf6b", null ],
    [ "IS_FLASH_PROGRAM_TIME", "group___f_l_a_s_h___exported___macros.html#gaccd7b93d128f3d8e8bd91807ab99d3b2", null ],
    [ "IS_OPTION_BYTE_ADDRESS", "group___f_l_a_s_h___exported___macros.html#gaf9f68b44dd15ed4458ceee1494fd74f3", null ]
];