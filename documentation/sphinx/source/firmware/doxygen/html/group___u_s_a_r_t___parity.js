var group___u_s_a_r_t___parity =
[
    [ "USART_Parity_TypeDef", "group___u_s_a_r_t___parity.html#ga57d987f474e5fd47d4760c4178c7f0d5", [
      [ "USART_Parity_No", "group___u_s_a_r_t___parity.html#gga57d987f474e5fd47d4760c4178c7f0d5a450f31c2a62208a5822453892be791de", null ],
      [ "USART_Parity_Even", "group___u_s_a_r_t___parity.html#gga57d987f474e5fd47d4760c4178c7f0d5a085e8a9f6589df2981d52310e3a1f2da", null ],
      [ "USART_Parity_Odd", "group___u_s_a_r_t___parity.html#gga57d987f474e5fd47d4760c4178c7f0d5ac72eee3e77c2ff9c6a09399938890845", null ]
    ] ]
];