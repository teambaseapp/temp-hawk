var stm8l15x__wwdg_8h =
[
    [ "WWDG_Enable", "group___w_w_d_g.html#ga10dc2554d0b504b5472e3ecf0f02a9e6", null ],
    [ "WWDG_GetCounter", "group___w_w_d_g.html#gaaf6e0a92a3e4395a074f060cca1e2670", null ],
    [ "WWDG_Init", "group___w_w_d_g.html#ga6e9f5281e1b9674c00c160bba949620c", null ],
    [ "WWDG_SetCounter", "group___w_w_d_g.html#ga6e44cc35f133b28b9ad861f459bf8d76", null ],
    [ "WWDG_SetWindowValue", "group___w_w_d_g.html#gaf44a7bf8bf6b11b41cd89ff521fdd5a5", null ],
    [ "WWDG_SWReset", "group___w_w_d_g.html#gaf59f6cc54b5ba02dd121a3c8aac5ea39", null ]
];