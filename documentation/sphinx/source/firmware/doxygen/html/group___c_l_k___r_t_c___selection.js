var group___c_l_k___r_t_c___selection =
[
    [ "CLK_RTCCLKSource_TypeDef", "group___c_l_k___r_t_c___selection.html#ga5c75b763f690d8faaad8c0b8df6b0b23", [
      [ "CLK_RTCCLKSource_Off", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23a036e973d78e658b78ca44a8b3d8a8881", null ],
      [ "CLK_RTCCLKSource_HSI", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23ad3a345f9f21fa25b5c93de1cfbd879e0", null ],
      [ "CLK_RTCCLKSource_LSI", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23a9754923a68ddbd16c949589535d8ab05", null ],
      [ "CLK_RTCCLKSource_HSE", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23a0331f69c466def532e1f7638da1705bf", null ],
      [ "CLK_RTCCLKSource_LSE", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23ae783ecea4fc242af5e97307f292bc300", null ]
    ] ]
];