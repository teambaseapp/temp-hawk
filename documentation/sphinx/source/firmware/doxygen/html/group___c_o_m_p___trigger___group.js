var group___c_o_m_p___trigger___group =
[
    [ "COMP_TriggerGroup_TypeDef", "group___c_o_m_p___trigger___group.html#ga8fae4f4600845fd05328df9a739ba2a4", [
      [ "COMP_TriggerGroup_InvertingInput", "group___c_o_m_p___trigger___group.html#gga8fae4f4600845fd05328df9a739ba2a4a0a6beff945e7cc56ecbf663cde8b7c62", null ],
      [ "COMP_TriggerGroup_NonInvertingInput", "group___c_o_m_p___trigger___group.html#gga8fae4f4600845fd05328df9a739ba2a4a42b07803841f817033a692459bdd49fe", null ],
      [ "COMP_TriggerGroup_VREFINTOutput", "group___c_o_m_p___trigger___group.html#gga8fae4f4600845fd05328df9a739ba2a4ac0b3b27fbedf8f6cc8e7eee2a9f36b84", null ],
      [ "COMP_TriggerGroup_DACOutput", "group___c_o_m_p___trigger___group.html#gga8fae4f4600845fd05328df9a739ba2a4a3c7f84a44c530f13cea67305e8552364", null ]
    ] ]
];