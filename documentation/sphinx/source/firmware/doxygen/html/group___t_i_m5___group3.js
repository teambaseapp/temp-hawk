var group___t_i_m5___group3 =
[
    [ "TIM5_GetCapture1", "group___t_i_m5___group3.html#gadce87cdf2f02645f64b16012f4536d6a", null ],
    [ "TIM5_GetCapture2", "group___t_i_m5___group3.html#ga37ae416e15ff96bed14126defd6568e4", null ],
    [ "TIM5_ICInit", "group___t_i_m5___group3.html#gad7295ea4e0d1c758775569ebdb0e9425", null ],
    [ "TIM5_PWMIConfig", "group___t_i_m5___group3.html#ga560f717b4a369056cf5e81669d6dbfa9", null ],
    [ "TIM5_SetIC1Prescaler", "group___t_i_m5___group3.html#ga578de8b51215192a140453f654441933", null ],
    [ "TIM5_SetIC2Prescaler", "group___t_i_m5___group3.html#gac69828b04314da2328375812c50d5d77", null ]
];