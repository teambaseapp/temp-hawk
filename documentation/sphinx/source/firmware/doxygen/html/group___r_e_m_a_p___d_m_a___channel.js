var group___r_e_m_a_p___d_m_a___channel =
[
    [ "REMAP_DMAChannel_TypeDef", "group___r_e_m_a_p___d_m_a___channel.html#ga1d71de9a84add708be3f86a25b8b4679", [
      [ "REMAP_DMA1Channel_ADC1ToChannel0", "group___r_e_m_a_p___d_m_a___channel.html#gga1d71de9a84add708be3f86a25b8b4679a7fec91b086f663e575a3e6ed91e7d8ba", null ],
      [ "REMAP_DMA1Channel_ADC1ToChannel1", "group___r_e_m_a_p___d_m_a___channel.html#gga1d71de9a84add708be3f86a25b8b4679a6d75f562bd790736ed60f491249fdd5b", null ],
      [ "REMAP_DMA1Channel_ADC1ToChannel2", "group___r_e_m_a_p___d_m_a___channel.html#gga1d71de9a84add708be3f86a25b8b4679a419a8021ce2b36909903bf6dad66b7d0", null ],
      [ "REMAP_DMA1Channel_ADC1ToChannel3", "group___r_e_m_a_p___d_m_a___channel.html#gga1d71de9a84add708be3f86a25b8b4679a722b512ed692128e4d07bec768217c3f", null ],
      [ "REMAP_DMA1Channel_TIM4ToChannel0", "group___r_e_m_a_p___d_m_a___channel.html#gga1d71de9a84add708be3f86a25b8b4679a3fed89fa31198d72186fdb3e2aba3a55", null ],
      [ "REMAP_DMA1Channel_TIM4ToChannel1", "group___r_e_m_a_p___d_m_a___channel.html#gga1d71de9a84add708be3f86a25b8b4679a0f0795657c0c9390894bc317a27ca109", null ],
      [ "REMAP_DMA1Channel_TIM4ToChannel2", "group___r_e_m_a_p___d_m_a___channel.html#gga1d71de9a84add708be3f86a25b8b4679a378002ea8cd65abae22153595ed53e1e", null ],
      [ "REMAP_DMA1Channel_TIM4ToChannel3", "group___r_e_m_a_p___d_m_a___channel.html#gga1d71de9a84add708be3f86a25b8b4679aa2765b7d643df12f030d7d4b912e994c", null ]
    ] ]
];