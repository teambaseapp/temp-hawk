var group___l_c_d___clock___divider =
[
    [ "LCD_Divider_TypeDef", "group___l_c_d___clock___divider.html#ga5a4bcc23728c936d2c2426b0dcb171f5", [
      [ "LCD_Divider_16", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5ae20a3d39f3557c43f5d885d73c400d82", null ],
      [ "LCD_Divider_17", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a7cb5ea1b009e3ebfcc91d425f12c9c44", null ],
      [ "LCD_Divider_18", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a6d203a3141b415cee401c8ad12d6fa57", null ],
      [ "LCD_Divider_19", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a94699bf787769fb00edfed9d2af196eb", null ],
      [ "LCD_Divider_20", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5aa4555c99e2b99c6a1f58d17099a0c21b", null ],
      [ "LCD_Divider_21", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a0b1a7fb06b146b6a35f1a34cdec69f86", null ],
      [ "LCD_Divider_22", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a94489ebb750e15ff585fa5b508747be9", null ],
      [ "LCD_Divider_23", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5abc3506a8ed1c4b8fce18894940bd978f", null ],
      [ "LCD_Divider_24", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5ab1e9dcf3d92aac306c3bb6294a9caf10", null ],
      [ "LCD_Divider_25", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5aeee1f8c6c25d0cc868ad1c6547cc29e6", null ],
      [ "LCD_Divider_26", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a13879fa9e8142e50a8af61ff392e49fd", null ],
      [ "LCD_Divider_27", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a07b0bc934c8ae120fb3f7033de8cadfb", null ],
      [ "LCD_Divider_28", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a19bd6f7fd0f10ffbc9ce6b0da553f2b4", null ],
      [ "LCD_Divider_29", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a04148a71cc2510d3090b3ebd5bbe8bd0", null ],
      [ "LCD_Divider_30", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a157cb53315007e5a287d0b6149ac6a81", null ],
      [ "LCD_Divider_31", "group___l_c_d___clock___divider.html#gga5a4bcc23728c936d2c2426b0dcb171f5a7665f3230433c31db2c15d7ae0b5b0ad", null ]
    ] ]
];