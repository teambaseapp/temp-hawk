var group___t_i_m3___counter_mode =
[
    [ "TIM3_CounterMode_TypeDef", "group___t_i_m3___counter_mode.html#ga39fd7fc87071542d8c2a6b4eb75d4f57", [
      [ "TIM3_CounterMode_Up", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57afc218d81454b1534a1e01ccb535cb803", null ],
      [ "TIM3_CounterMode_Down", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57af104aaf2ce189fafaad8b26a661a8aa7", null ],
      [ "TIM3_CounterMode_CenterAligned1", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57ade3b9ca14d8aa2d285ba32ef61e97827", null ],
      [ "TIM3_CounterMode_CenterAligned2", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57a54e0cf4777a6dcea3eedff27b2502364", null ],
      [ "TIM3_CounterMode_CenterAligned3", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57a0ed3b57649b0b573e8dcfde338d09828", null ]
    ] ]
];