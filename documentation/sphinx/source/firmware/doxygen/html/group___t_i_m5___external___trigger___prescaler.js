var group___t_i_m5___external___trigger___prescaler =
[
    [ "TIM5_ExtTRGPSC_TypeDef", "group___t_i_m5___external___trigger___prescaler.html#ga586ca24f963cb6b4c697de21f9d8e4ca", [
      [ "TIM5_ExtTRGPSC_OFF", "group___t_i_m5___external___trigger___prescaler.html#gga586ca24f963cb6b4c697de21f9d8e4caa98953a99ffa176d411a655c8762d9c59", null ],
      [ "TIM5_ExtTRGPSC_DIV2", "group___t_i_m5___external___trigger___prescaler.html#gga586ca24f963cb6b4c697de21f9d8e4caac6b89880102d57ebe4fba91c34a75fb0", null ],
      [ "TIM5_ExtTRGPSC_DIV4", "group___t_i_m5___external___trigger___prescaler.html#gga586ca24f963cb6b4c697de21f9d8e4caa68d44027d9835afe7962c6a36f9ebc74", null ],
      [ "TIM5_ExtTRGPSC_DIV8", "group___t_i_m5___external___trigger___prescaler.html#gga586ca24f963cb6b4c697de21f9d8e4caac5e39dec16e3cb6df4edaaa82981872d", null ]
    ] ]
];