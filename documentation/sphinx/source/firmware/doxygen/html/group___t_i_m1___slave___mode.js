var group___t_i_m1___slave___mode =
[
    [ "TIM1_SlaveMode_TypeDef", "group___t_i_m1___slave___mode.html#ga6d4b46e1bc7cda38dba3718c3f00b191", [
      [ "TIM1_SlaveMode_Reset", "group___t_i_m1___slave___mode.html#gga6d4b46e1bc7cda38dba3718c3f00b191af7cb344def5d0bbbad0abaf09620887e", null ],
      [ "TIM1_SlaveMode_Gated", "group___t_i_m1___slave___mode.html#gga6d4b46e1bc7cda38dba3718c3f00b191a2f7955bc75c2b9b11c50e73d9ccd83c3", null ],
      [ "TIM1_SlaveMode_Trigger", "group___t_i_m1___slave___mode.html#gga6d4b46e1bc7cda38dba3718c3f00b191a2242b4a2869b1f83c55ef58458f5db23", null ],
      [ "TIM1_SlaveMode_External1", "group___t_i_m1___slave___mode.html#gga6d4b46e1bc7cda38dba3718c3f00b191ab2d65d594e3d06f370195af74d650500", null ]
    ] ]
];