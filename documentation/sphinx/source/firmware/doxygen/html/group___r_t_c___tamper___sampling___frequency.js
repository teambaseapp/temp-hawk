var group___r_t_c___tamper___sampling___frequency =
[
    [ "RTC_TamperSamplingFreq_TypeDef", "group___r_t_c___tamper___sampling___frequency.html#gae1bb14c2e963918473c28e1a85011102", [
      [ "RTC_TamperSamplingFreq_RTCCLK_Div32768", "group___r_t_c___tamper___sampling___frequency.html#ggae1bb14c2e963918473c28e1a85011102aeeb8e091e7d4c1169bfe1a96c6984de2", null ],
      [ "RTC_TamperSamplingFreq_RTCCLK_Div16384", "group___r_t_c___tamper___sampling___frequency.html#ggae1bb14c2e963918473c28e1a85011102ae9f7e9634e66601ebbbcbc034ec21155", null ],
      [ "RTC_TamperSamplingFreq_RTCCLK_Div8192", "group___r_t_c___tamper___sampling___frequency.html#ggae1bb14c2e963918473c28e1a85011102a223381822108c7ceb86bacf393892152", null ],
      [ "RTC_TamperSamplingFreq_RTCCLK_Div4096", "group___r_t_c___tamper___sampling___frequency.html#ggae1bb14c2e963918473c28e1a85011102aa7d1cd217eee66b590802f86ee62137b", null ],
      [ "RTC_TamperSamplingFreq_RTCCLK_Div2048", "group___r_t_c___tamper___sampling___frequency.html#ggae1bb14c2e963918473c28e1a85011102a42742f02d574622a3831a8e7bf441bed", null ],
      [ "RTC_TamperSamplingFreq_RTCCLK_Div1024", "group___r_t_c___tamper___sampling___frequency.html#ggae1bb14c2e963918473c28e1a85011102a42cc56e91e85a6a5cf4c02d94d12e1d1", null ],
      [ "RTC_TamperSamplingFreq_RTCCLK_Div512", "group___r_t_c___tamper___sampling___frequency.html#ggae1bb14c2e963918473c28e1a85011102a309fd0ae2c20a36023c9e806aa13aac9", null ],
      [ "RTC_TamperSamplingFreq_RTCCLK_Div256", "group___r_t_c___tamper___sampling___frequency.html#ggae1bb14c2e963918473c28e1a85011102a83e1155f911c79e029d355da9b1bed28", null ]
    ] ]
];