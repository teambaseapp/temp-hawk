var group___t_i_m2___prescaler =
[
    [ "TIM2_Prescaler_TypeDef", "group___t_i_m2___prescaler.html#ga6e423660d6269f78d8d36a5c7c432c21", [
      [ "TIM2_Prescaler_1", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21af43057e167fbb7be8e9e10ebf284e919", null ],
      [ "TIM2_Prescaler_2", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a025f9529c218e26ac71af0300e1d9e04", null ],
      [ "TIM2_Prescaler_4", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a6b1e4a77590709c8e279ca30104e90eb", null ],
      [ "TIM2_Prescaler_8", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21acc85dea82c69cfba403fe97f0474af4f", null ],
      [ "TIM2_Prescaler_16", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a07bf3c13654d704c9ffe504964f51134", null ],
      [ "TIM2_Prescaler_32", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a20ad02de589c85026c7e675d0e00bedb", null ],
      [ "TIM2_Prescaler_64", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a2b106b7736eaad6326fe5290799ab53b", null ],
      [ "TIM2_Prescaler_128", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a08e00fd2e38ab568b7355aeb4ef6d1f3", null ]
    ] ]
];