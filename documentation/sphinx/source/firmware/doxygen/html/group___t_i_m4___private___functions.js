var group___t_i_m4___private___functions =
[
    [ "TimeBase management functions", "group___t_i_m4___group1.html", "group___t_i_m4___group1" ],
    [ "Interrupts DMA and flags management functions", "group___t_i_m4___group2.html", "group___t_i_m4___group2" ],
    [ "Clocks management functions", "group___t_i_m4___group3.html", "group___t_i_m4___group3" ],
    [ "Synchronization management functions", "group___t_i_m4___group4.html", "group___t_i_m4___group4" ]
];