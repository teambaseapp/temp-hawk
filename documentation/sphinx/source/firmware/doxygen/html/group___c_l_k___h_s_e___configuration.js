var group___c_l_k___h_s_e___configuration =
[
    [ "CLK_HSE_TypeDef", "group___c_l_k___h_s_e___configuration.html#ga951515846e0f9aed5ba42be70bb859be", [
      [ "CLK_HSE_OFF", "group___c_l_k___h_s_e___configuration.html#gga951515846e0f9aed5ba42be70bb859bea6883a0645dd4330572afaaa1fb81f8cb", null ],
      [ "CLK_HSE_ON", "group___c_l_k___h_s_e___configuration.html#gga951515846e0f9aed5ba42be70bb859bea41d32df15b92ac1b3676e910d37aef1b", null ],
      [ "CLK_HSE_Bypass", "group___c_l_k___h_s_e___configuration.html#gga951515846e0f9aed5ba42be70bb859beac2c0aee89595bc282bcac3a1a4eadd4f", null ]
    ] ]
];