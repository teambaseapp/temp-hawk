var group___t_i_m4___prescaler =
[
    [ "TIM4_Prescaler_TypeDef", "group___t_i_m4___prescaler.html#gafc1afaaf36a9535c236a7642014c840e", [
      [ "TIM4_Prescaler_1", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840eae60f3bc3b43d5d18be4b360116002506", null ],
      [ "TIM4_Prescaler_2", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea6ee2eff1b3c4a6cfea2f53682060a667", null ],
      [ "TIM4_Prescaler_4", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea6a13cbe45fb532436e92abefc0b25cad", null ],
      [ "TIM4_Prescaler_8", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea329ea7a6cf2925a265aca11b0eee6923", null ],
      [ "TIM4_Prescaler_16", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840eac454dd17ba1625409bbf765345be84a4", null ],
      [ "TIM4_Prescaler_32", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea205a13c0c0296d18855e92c31de3f4bc", null ],
      [ "TIM4_Prescaler_64", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea9e075a6fa77ecb89d73cae127bb460d1", null ],
      [ "TIM4_Prescaler_128", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840eaaa297dc8d951acf60f9e6f761abbd916", null ],
      [ "TIM4_Prescaler_256", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea06ca9484dab970e0a83b00827a19a259", null ],
      [ "TIM4_Prescaler_512", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea28be2cafcb57dc93353c9e7bdc6bd389", null ],
      [ "TIM4_Prescaler_1024", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea9bdc20e4571ab3165e877b1cc4f0d3b6", null ],
      [ "TIM4_Prescaler_2048", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ead2e73b2ca44ef2f72b8a53bd10edc38f", null ],
      [ "TIM4_Prescaler_4096", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea39ad8c3842c73362720b05c56295132c", null ],
      [ "TIM4_Prescaler_8192", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea3067d04a06287b01e7d164a20892614d", null ],
      [ "TIM4_Prescaler_16384", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea62cc746f7be212225c9df9ffc9160a7c", null ],
      [ "TIM4_Prescaler_32768", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840eae8f7c211cc2e76da66d399c334a28b43", null ]
    ] ]
];