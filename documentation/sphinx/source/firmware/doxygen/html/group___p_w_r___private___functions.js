var group___p_w_r___private___functions =
[
    [ "PVD configuration functions", "group___p_w_r___group1.html", "group___p_w_r___group1" ],
    [ "Ultra Low Power mode configuration functions", "group___p_w_r___group2.html", "group___p_w_r___group2" ],
    [ "Interrupts and Flags management functions", "group___p_w_r___group3.html", "group___p_w_r___group3" ]
];