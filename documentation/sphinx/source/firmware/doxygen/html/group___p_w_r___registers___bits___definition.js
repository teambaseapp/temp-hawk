var group___p_w_r___registers___bits___definition =
[
    [ "PWR_CR2_VREFINTF", "group___p_w_r___registers___bits___definition.html#ga369bf2633bee25a6dc1636008f4ddfee", null ],
    [ "PWR_CSR1_PLS", "group___p_w_r___registers___bits___definition.html#gaf52ccc9607fac533977a0e479af9ba45", null ],
    [ "PWR_CSR1_PVDE", "group___p_w_r___registers___bits___definition.html#gad81fbc108737b17dc1da465b275491d5", null ],
    [ "PWR_CSR1_PVDIEN", "group___p_w_r___registers___bits___definition.html#gaf0b5580ec34ff2f0994070f0137be571", null ],
    [ "PWR_CSR1_PVDIF", "group___p_w_r___registers___bits___definition.html#ga22df1c8d4e50d398d4138cbe744fbc6a", null ],
    [ "PWR_CSR1_PVDOF", "group___p_w_r___registers___bits___definition.html#ga6a2e9132f762025901e1b915c72457df", null ],
    [ "PWR_CSR2_FWU", "group___p_w_r___registers___bits___definition.html#gab80bcf823e4b2e53a8d6d5b21e3b0ba0", null ],
    [ "PWR_CSR2_ULP", "group___p_w_r___registers___bits___definition.html#ga05d91eebdc834fd853a8548bdb77765b", null ]
];