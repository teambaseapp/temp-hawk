var group___c_l_k___halt___configuration =
[
    [ "CLK_Halt_TypeDef", "group___c_l_k___halt___configuration.html#ga4bf6cd3a577dd72a06a370751f987a6d", [
      [ "CLK_Halt_BEEPRunning", "group___c_l_k___halt___configuration.html#gga4bf6cd3a577dd72a06a370751f987a6da73a4beda096fc859bdd657c65a7f7957", null ],
      [ "CLK_Halt_FastWakeup", "group___c_l_k___halt___configuration.html#gga4bf6cd3a577dd72a06a370751f987a6da4633b3a5db271ac6bda6a577b4eaa0c9", null ],
      [ "CLK_Halt_SlowWakeup", "group___c_l_k___halt___configuration.html#gga4bf6cd3a577dd72a06a370751f987a6da41ce4f492744dfb7bcd53b1a9866bd80", null ]
    ] ]
];