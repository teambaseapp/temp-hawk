var stm8l15x__iwdg_8c =
[
    [ "IWDG_Enable", "group___i_w_d_g___group2.html#ga479b2921c86f8c67b819f5c4bea6bdb6", null ],
    [ "IWDG_ReloadCounter", "group___i_w_d_g___group1.html#ga7147ebabdc3fef97f532b171a4e70d49", null ],
    [ "IWDG_SetPrescaler", "group___i_w_d_g___group1.html#ga6ac608d9b248753dd41ca8a5bc88189a", null ],
    [ "IWDG_SetReload", "group___i_w_d_g___group1.html#ga47510486822000d8175cf4d5d2188af7", null ],
    [ "IWDG_WriteAccessCmd", "group___i_w_d_g___group1.html#ga6d0fd5e5aa35335fed842b6a5acedd4c", null ]
];