var group___t_i_m1___input___capture___prescaler =
[
    [ "TIM1_ICPSC_TypeDef", "group___t_i_m1___input___capture___prescaler.html#ga61aeaccf7362311cce28473c16255372", [
      [ "TIM1_ICPSC_DIV1", "group___t_i_m1___input___capture___prescaler.html#gga61aeaccf7362311cce28473c16255372ae7ba7c283927f0f056655439e93a04b9", null ],
      [ "TIM1_ICPSC_DIV2", "group___t_i_m1___input___capture___prescaler.html#gga61aeaccf7362311cce28473c16255372ad34d0b6c26049a0a74b1b3c6826a2bce", null ],
      [ "TIM1_ICPSC_DIV4", "group___t_i_m1___input___capture___prescaler.html#gga61aeaccf7362311cce28473c16255372ac0a5e90154ad4cc412c23daf6dbe8259", null ],
      [ "TIM1_ICPSC_DIV8", "group___t_i_m1___input___capture___prescaler.html#gga61aeaccf7362311cce28473c16255372a0504127b4da74356651c1dfc8184a5ae", null ]
    ] ]
];