var NAVTREE =
[
  [ "TempHawk", "index.html", [
    [ "INTRODUCTION", "index.html#INTRODUCTION", [
      [ "STATUS", "index.html#STATUS", null ],
      [ "WORKING", "index.html#WORKING", null ],
      [ "INTERRUPTS-USED", "index.html#INTERRUPTS-USED", null ]
    ] ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", "globals_enum" ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a4cf027852a5fffaaabf695e8f03c3fac",
"group___a_e_s___group2.html#gad43f6e214017ca49141cea2c61270717",
"group___c_l_k___group4.html#ga3faa3a80338b0a15995052e0077074f1",
"group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9a59b6d17fb9c7fe1ea80f5f30fa2eceac",
"group___d_m_a.html",
"group___e_x_t_i___half___port.html#ggab54a0eac30b4632084cff3d28fc1a6faaf7d1ca460071c398f33716c93e72e6a6",
"group___f_l_a_s_h___programming___mode.html#ga1e86181281cd4fca8e74de7343f99446",
"group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a6c19b142f8f4af7dc992d98ee5676bab",
"group___i2_c__mode.html#ga38b8f4a77788f3c603f18f6d16db42ea",
"group___l_c_d.html#gad9916d38e5f5a7d127bf08c3711ddab5",
"group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a191c04af2580157cb6485c5182707be4",
"group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a124957b320acefcea33abcbbacb5f70c",
"group___r_t_c___alarm___mask.html#ggaf0ce5c02209f0e35584ed7915265f839ac2edc2cf637e927a3c427c23118f7b5d",
"group___r_t_c___sub___second___alarm___mask.html#gga99342e1082ba1290dcc5a0c0c24501bca07cb436bd8efc69585c7b50706857c50",
"group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a73dd6b0fcc56efe5dba09db395a838dd",
"group___t_i_m1.html#ga9ab2786a870a28a3c83a8d720c016be6",
"group___t_i_m1___group2.html#ga1996dfbc5424aff723745c3089d4b3e1",
"group___t_i_m2.html#ga0af4dc138283377e9d80ab0995692715",
"group___t_i_m2___group1.html#gae8a75637978adac6a17d6082f0c86748",
"group___t_i_m2___t_i___external___clock___source.html#gga9bd7ef2ae447e185a6e0d8d33d5035b3a2085d138e26bc490f230d3557a45b7b4",
"group___t_i_m3___forced___action.html#gga89600966a87e647b67aa63984e88a07ca9e67c0f5b2b582856afe720b44d45a00",
"group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa97e8c07033ed548d160820dd885ac944",
"group___t_i_m4___salve___mode.html#gac22be9bb6535a9ea3ed45c0893367aab",
"group___t_i_m5___forced___action.html#ggabc585cd126b42eb1dfbd9e64eeebaa4aa2a7f29aa4bd874fc87a61a9838eff065",
"group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba8fdd99d1b9d5f3ba81b6d4fb2298a2df",
"group___u_s_a_r_t___last_bit.html#gga8a48870ad18c4ab43de5229080f76602a127963d66fe07579a49a9014b4b45e70",
"rfm69fskutils_8c.html#ac06acf95c396602d268e8a71ebb4ed6c",
"stm8l15x__usartutils_8c.html#a194c3d3eb59206cdb8666a5faa67873c",
"struct_r_t_c___time_type_def.html#a477121009a567d6dd0e5fa1310574d32"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';