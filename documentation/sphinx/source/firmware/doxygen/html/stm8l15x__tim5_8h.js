var stm8l15x__tim5_8h =
[
    [ "TIM5_AutomaticOutput_TypeDef", "group___t_i_m5___automatic___output.html#gaf0e6e4b54ce3f4edd975ddf8093e989e", [
      [ "TIM5_AutomaticOutput_Enable", "group___t_i_m5___automatic___output.html#ggaf0e6e4b54ce3f4edd975ddf8093e989ea9e2687d65091442a119b484e21ae9dd0", null ],
      [ "TIM5_AutomaticOutput_Disable", "group___t_i_m5___automatic___output.html#ggaf0e6e4b54ce3f4edd975ddf8093e989ea896ec999fb6e37e013f3ae630976e786", null ]
    ] ],
    [ "TIM5_BreakPolarity_TypeDef", "group___t_i_m5___break___polarity.html#ga626bc6554ef3a7597bc8d3d0c1ca1b81", [
      [ "TIM5_BreakPolarity_High", "group___t_i_m5___break___polarity.html#gga626bc6554ef3a7597bc8d3d0c1ca1b81ab48c798c13762bb9aefdf0fb7890b91f", null ],
      [ "TIM5_BreakPolarity_Low", "group___t_i_m5___break___polarity.html#gga626bc6554ef3a7597bc8d3d0c1ca1b81a436d82c91a72b3eb26b63386a6ccf416", null ]
    ] ],
    [ "TIM5_BreakState_TypeDef", "group___t_i_m5___break___state.html#gabab1e95494ca95131d9d2a1767a772d3", [
      [ "TIM5_BreakState_Disable", "group___t_i_m5___break___state.html#ggabab1e95494ca95131d9d2a1767a772d3aa6d75e32b24f4e6a5f9cd23ee7e96302", null ],
      [ "TIM5_BreakState_Enable", "group___t_i_m5___break___state.html#ggabab1e95494ca95131d9d2a1767a772d3a655adb1ef93f75bc7b1a0a85c6eb9f0a", null ]
    ] ],
    [ "TIM5_Channel_TypeDef", "group___t_i_m5___channel.html#gac9f76b1b79bfad926d10b2a487ccde6c", [
      [ "TIM5_Channel_1", "group___t_i_m5___channel.html#ggac9f76b1b79bfad926d10b2a487ccde6cafffd3de24921675cb2778da75efe7760", null ],
      [ "TIM5_Channel_2", "group___t_i_m5___channel.html#ggac9f76b1b79bfad926d10b2a487ccde6ca7397f00dc576380f311e293deae38143", null ]
    ] ],
    [ "TIM5_CounterMode_TypeDef", "group___t_i_m5___counter_mode.html#gaea5468eaa9f95d94daabd2882628d47d", [
      [ "TIM5_CounterMode_Up", "group___t_i_m5___counter_mode.html#ggaea5468eaa9f95d94daabd2882628d47da34807db792f5c4879c12e3bb45d0d542", null ],
      [ "TIM5_CounterMode_Down", "group___t_i_m5___counter_mode.html#ggaea5468eaa9f95d94daabd2882628d47dab1a1fd1bc082c7c39e26685f20c3daa4", null ],
      [ "TIM5_CounterMode_CenterAligned1", "group___t_i_m5___counter_mode.html#ggaea5468eaa9f95d94daabd2882628d47da5804163514c58d8ae15eee4cc3949c13", null ],
      [ "TIM5_CounterMode_CenterAligned2", "group___t_i_m5___counter_mode.html#ggaea5468eaa9f95d94daabd2882628d47daa1ddef783d58547ea5f5f11860479401", null ],
      [ "TIM5_CounterMode_CenterAligned3", "group___t_i_m5___counter_mode.html#ggaea5468eaa9f95d94daabd2882628d47da4f05fad10c73e8cad08eb3360ac0ebfb", null ]
    ] ],
    [ "TIM5_DMASource_TypeDef", "group___t_i_m5___d_m_a___source___requests.html#ga87227334a87c01943a84b4521f8cedd7", [
      [ "TIM5_DMASource_Update", "group___t_i_m5___d_m_a___source___requests.html#gga87227334a87c01943a84b4521f8cedd7a1f59f580fa508135374355b9df394eb4", null ],
      [ "TIM5_DMASource_CC1", "group___t_i_m5___d_m_a___source___requests.html#gga87227334a87c01943a84b4521f8cedd7a8b9d75c3eb8fa51979c5c48c14a48c77", null ],
      [ "TIM5_DMASource_CC2", "group___t_i_m5___d_m_a___source___requests.html#gga87227334a87c01943a84b4521f8cedd7aa8825420497378a491dced0f3de3695a", null ]
    ] ],
    [ "TIM5_EncoderMode_TypeDef", "group___t_i_m5___encoder___mode.html#gab1b248d7aeb0f158882575fed01d347c", [
      [ "TIM5_EncoderMode_TI1", "group___t_i_m5___encoder___mode.html#ggab1b248d7aeb0f158882575fed01d347ca7df6e9b73ff1066f6f01e2d26c6528fa", null ],
      [ "TIM5_EncoderMode_TI2", "group___t_i_m5___encoder___mode.html#ggab1b248d7aeb0f158882575fed01d347caac674d6432e6e80ad9c71f15e6dcd799", null ],
      [ "TIM5_EncoderMode_TI12", "group___t_i_m5___encoder___mode.html#ggab1b248d7aeb0f158882575fed01d347cab5f6e3f239a778644f357eb31bb545a0", null ]
    ] ],
    [ "TIM5_EventSource_TypeDef", "group___t_i_m5___event___source.html#ga0b98d0955370301bc8fb9fe9623f6bbc", [
      [ "TIM5_EventSource_Update", "group___t_i_m5___event___source.html#gga0b98d0955370301bc8fb9fe9623f6bbca146e7af5e69483ea0a13e7c703453a1a", null ],
      [ "TIM5_EventSource_CC1", "group___t_i_m5___event___source.html#gga0b98d0955370301bc8fb9fe9623f6bbcab98d6cdf8cacc889714a33dd6aa45023", null ],
      [ "TIM5_EventSource_CC2", "group___t_i_m5___event___source.html#gga0b98d0955370301bc8fb9fe9623f6bbca16a3d223b36092f972de0483c6fcf8c8", null ],
      [ "TIM5_EventSource_Trigger", "group___t_i_m5___event___source.html#gga0b98d0955370301bc8fb9fe9623f6bbca124b8044cffca5c95c511a1de50f4e9d", null ],
      [ "TIM5_EventSource_Break", "group___t_i_m5___event___source.html#gga0b98d0955370301bc8fb9fe9623f6bbcad087c9f150a96be916fd0a273676355b", null ]
    ] ],
    [ "TIM5_ExtTRGPolarity_TypeDef", "group___t_i_m5___external___trigger___polarity.html#gab44ee2c33684fd854a66794fbcab3b4d", [
      [ "TIM5_ExtTRGPolarity_Inverted", "group___t_i_m5___external___trigger___polarity.html#ggab44ee2c33684fd854a66794fbcab3b4da8bd67dce39ea66c4a50f4b66fe8a34b7", null ],
      [ "TIM5_ExtTRGPolarity_NonInverted", "group___t_i_m5___external___trigger___polarity.html#ggab44ee2c33684fd854a66794fbcab3b4dab9203e8eae52464896c95ee9941ce5c1", null ]
    ] ],
    [ "TIM5_ExtTRGPSC_TypeDef", "group___t_i_m5___external___trigger___prescaler.html#ga586ca24f963cb6b4c697de21f9d8e4ca", [
      [ "TIM5_ExtTRGPSC_OFF", "group___t_i_m5___external___trigger___prescaler.html#gga586ca24f963cb6b4c697de21f9d8e4caa98953a99ffa176d411a655c8762d9c59", null ],
      [ "TIM5_ExtTRGPSC_DIV2", "group___t_i_m5___external___trigger___prescaler.html#gga586ca24f963cb6b4c697de21f9d8e4caac6b89880102d57ebe4fba91c34a75fb0", null ],
      [ "TIM5_ExtTRGPSC_DIV4", "group___t_i_m5___external___trigger___prescaler.html#gga586ca24f963cb6b4c697de21f9d8e4caa68d44027d9835afe7962c6a36f9ebc74", null ],
      [ "TIM5_ExtTRGPSC_DIV8", "group___t_i_m5___external___trigger___prescaler.html#gga586ca24f963cb6b4c697de21f9d8e4caac5e39dec16e3cb6df4edaaa82981872d", null ]
    ] ],
    [ "TIM5_FLAG_TypeDef", "group___t_i_m5___flags.html#gac3c87c24e78755fa23ee8587541ec128", [
      [ "TIM5_FLAG_Update", "group___t_i_m5___flags.html#ggac3c87c24e78755fa23ee8587541ec128a660f6cabedaa3e603f05b8b342134c39", null ],
      [ "TIM5_FLAG_CC1", "group___t_i_m5___flags.html#ggac3c87c24e78755fa23ee8587541ec128a21d56de42a1a2c5fd6e95d9426445017", null ],
      [ "TIM5_FLAG_CC2", "group___t_i_m5___flags.html#ggac3c87c24e78755fa23ee8587541ec128ad3ed100e512ec4c729846f2a43b1131b", null ],
      [ "TIM5_FLAG_Trigger", "group___t_i_m5___flags.html#ggac3c87c24e78755fa23ee8587541ec128ae4fe4ff87fed5bbe58df208c3a534f91", null ],
      [ "TIM5_FLAG_Break", "group___t_i_m5___flags.html#ggac3c87c24e78755fa23ee8587541ec128af21f91efa02281ab3348c377842c3e65", null ],
      [ "TIM5_FLAG_CC1OF", "group___t_i_m5___flags.html#ggac3c87c24e78755fa23ee8587541ec128a352bcbb800ab17030df5e7641e9afcf7", null ],
      [ "TIM5_FLAG_CC2OF", "group___t_i_m5___flags.html#ggac3c87c24e78755fa23ee8587541ec128a1d23bcb2d4f259251bed7689af4654ca", null ]
    ] ],
    [ "TIM5_ForcedAction_TypeDef", "group___t_i_m5___forced___action.html#gabc585cd126b42eb1dfbd9e64eeebaa4a", [
      [ "TIM5_ForcedAction_Active", "group___t_i_m5___forced___action.html#ggabc585cd126b42eb1dfbd9e64eeebaa4aaf4ff748feed3b1468ff1393160f2c474", null ],
      [ "TIM5_ForcedAction_Inactive", "group___t_i_m5___forced___action.html#ggabc585cd126b42eb1dfbd9e64eeebaa4aa2a7f29aa4bd874fc87a61a9838eff065", null ]
    ] ],
    [ "TIM5_ICPolarity_TypeDef", "group___t_i_m5___input___capture___polarity.html#ga634d1b237b4d1f1fa7e98127610e5406", [
      [ "TIM5_ICPolarity_Rising", "group___t_i_m5___input___capture___polarity.html#gga634d1b237b4d1f1fa7e98127610e5406ac706aded84a53b021fe895ae12360e33", null ],
      [ "TIM5_ICPolarity_Falling", "group___t_i_m5___input___capture___polarity.html#gga634d1b237b4d1f1fa7e98127610e5406a6ad56406a08786dca057bbbd93a7e8ac", null ]
    ] ],
    [ "TIM5_ICPSC_TypeDef", "group___t_i_m5___input___capture___prescaler.html#gabbe6730c304221b6ebdd6bdcaa40b0da", [
      [ "TIM5_ICPSC_DIV1", "group___t_i_m5___input___capture___prescaler.html#ggabbe6730c304221b6ebdd6bdcaa40b0daa5d83ba8c4790ec7a7ba9d3d6161d4c8f", null ],
      [ "TIM5_ICPSC_DIV2", "group___t_i_m5___input___capture___prescaler.html#ggabbe6730c304221b6ebdd6bdcaa40b0daa2f056cf93a39e5dd3e6286c67a21fadb", null ],
      [ "TIM5_ICPSC_DIV4", "group___t_i_m5___input___capture___prescaler.html#ggabbe6730c304221b6ebdd6bdcaa40b0daaa6cef143339eb8cda54c3ed01270c4dc", null ],
      [ "TIM5_ICPSC_DIV8", "group___t_i_m5___input___capture___prescaler.html#ggabbe6730c304221b6ebdd6bdcaa40b0daa3e1dc94a24cfc1b6f06c785965793020", null ]
    ] ],
    [ "TIM5_ICSelection_TypeDef", "group___t_i_m5___input___capture___selection.html#gaf043cac9140343fad1b29d2eb2c4dcff", [
      [ "TIM5_ICSelection_DirectTI", "group___t_i_m5___input___capture___selection.html#ggaf043cac9140343fad1b29d2eb2c4dcffa3849ca5cfa3f3030d6bca496d68d1e9f", null ],
      [ "TIM5_ICSelection_IndirectTI", "group___t_i_m5___input___capture___selection.html#ggaf043cac9140343fad1b29d2eb2c4dcffa83ddc64a0621aaeb5277dc11d023be33", null ],
      [ "TIM5_ICSelection_TRGI", "group___t_i_m5___input___capture___selection.html#ggaf043cac9140343fad1b29d2eb2c4dcffaafb94e65d3224c6e7cb0081a9ecffe4f", null ]
    ] ],
    [ "TIM5_IT_TypeDef", "group___t_i_m5___interrupts.html#ga6e54e38b3d03bb930143828160cbd46f", [
      [ "TIM5_IT_Update", "group___t_i_m5___interrupts.html#gga6e54e38b3d03bb930143828160cbd46fa2728fe6a1174486c1b3c8b3a6ec5fb4e", null ],
      [ "TIM5_IT_CC1", "group___t_i_m5___interrupts.html#gga6e54e38b3d03bb930143828160cbd46fa2c1f74ed1645b011fbe4880795c5b8ee", null ],
      [ "TIM5_IT_CC2", "group___t_i_m5___interrupts.html#gga6e54e38b3d03bb930143828160cbd46facf435d5529fda640c25b34d54c81fb82", null ],
      [ "TIM5_IT_Trigger", "group___t_i_m5___interrupts.html#gga6e54e38b3d03bb930143828160cbd46faecbf2c0f5f3c733addcd9a56b75d65f8", null ],
      [ "TIM5_IT_Break", "group___t_i_m5___interrupts.html#gga6e54e38b3d03bb930143828160cbd46fa6e03dcbf6f7f5b46fb3b19f43d01a64b", null ]
    ] ],
    [ "TIM5_LockLevel_TypeDef", "group___t_i_m5___lock___level.html#ga48789fd038a12f3914c8f024923c41e5", [
      [ "TIM5_LockLevel_Off", "group___t_i_m5___lock___level.html#gga48789fd038a12f3914c8f024923c41e5adac52b740f694be1951177906b20e9f7", null ],
      [ "TIM5_LockLevel_1", "group___t_i_m5___lock___level.html#gga48789fd038a12f3914c8f024923c41e5a87730ce769bc8fc57c3ac3b2d555c2e8", null ],
      [ "TIM5_LockLevel_2", "group___t_i_m5___lock___level.html#gga48789fd038a12f3914c8f024923c41e5ae81089f073c13dfef3adbf3563d514d4", null ],
      [ "TIM5_LockLevel_3", "group___t_i_m5___lock___level.html#gga48789fd038a12f3914c8f024923c41e5a3c6a09faf0d34216d9a6f9a59b3534d2", null ]
    ] ],
    [ "TIM5_OCIdleState_TypeDef", "group___t_i_m5___output___compare___idle__state.html#ga84c53fad2ec9c51f850d8a02f70781a6", [
      [ "TIM5_OCIdleState_Reset", "group___t_i_m5___output___compare___idle__state.html#gga84c53fad2ec9c51f850d8a02f70781a6ad0d5c0e26eb10c1b65f48dbe25d6217b", null ],
      [ "TIM5_OCIdleState_Set", "group___t_i_m5___output___compare___idle__state.html#gga84c53fad2ec9c51f850d8a02f70781a6a83697da69c73d8511fcdaeceed91514e", null ]
    ] ],
    [ "TIM5_OCMode_TypeDef", "group___t_i_m5___o_c_mode.html#ga4add6a98f46e8d7a14e2c762fe594346", [
      [ "TIM5_OCMode_Timing", "group___t_i_m5___o_c_mode.html#gga4add6a98f46e8d7a14e2c762fe594346afa4ba086985c9dcf49fe26f515252703", null ],
      [ "TIM5_OCMode_Active", "group___t_i_m5___o_c_mode.html#gga4add6a98f46e8d7a14e2c762fe594346afac76e8e66fa66cbaa7a55ab86a95c92", null ],
      [ "TIM5_OCMode_Inactive", "group___t_i_m5___o_c_mode.html#gga4add6a98f46e8d7a14e2c762fe594346a7a661ee4c54706adedfe6a4a65b99f03", null ],
      [ "TIM5_OCMode_Toggle", "group___t_i_m5___o_c_mode.html#gga4add6a98f46e8d7a14e2c762fe594346a806f8c4045df19d11e195c23e8ea7ade", null ],
      [ "TIM5_OCMode_PWM1", "group___t_i_m5___o_c_mode.html#gga4add6a98f46e8d7a14e2c762fe594346a3495f6ffe584885424b6e4027a0346ec", null ],
      [ "TIM5_OCMode_PWM2", "group___t_i_m5___o_c_mode.html#gga4add6a98f46e8d7a14e2c762fe594346a7c5e3f3a9a0dcf3eb4b6fa5a4e590105", null ]
    ] ],
    [ "TIM5_OCPolarity_TypeDef", "group___t_i_m5___output___compare___polarity.html#ga544175cdfeef81a4f5832e9cdba8a35d", [
      [ "TIM5_OCPolarity_High", "group___t_i_m5___output___compare___polarity.html#gga544175cdfeef81a4f5832e9cdba8a35dae8a66de9b136b170bbc3b20a3a4ad2df", null ],
      [ "TIM5_OCPolarity_Low", "group___t_i_m5___output___compare___polarity.html#gga544175cdfeef81a4f5832e9cdba8a35dab0a95c52088caf962a0116479564e503", null ]
    ] ],
    [ "TIM5_OPMode_TypeDef", "group___t_i_m5___one_pulse_mode.html#ga44391d8782c0b820c1a2f8b3e2eeee89", [
      [ "TIM5_OPMode_Single", "group___t_i_m5___one_pulse_mode.html#gga44391d8782c0b820c1a2f8b3e2eeee89ab321bea8553210883d310ccfe1c201d2", null ],
      [ "TIM5_OPMode_Repetitive", "group___t_i_m5___one_pulse_mode.html#gga44391d8782c0b820c1a2f8b3e2eeee89a3370753c6173bcf16b31a10e6ce74271", null ]
    ] ],
    [ "TIM5_OSSIState_TypeDef", "group___t_i_m5___o_s_s_i___state.html#gaa04bf444e4940f133b7f116d38fa44db", [
      [ "TIM5_OSSIState_Enable", "group___t_i_m5___o_s_s_i___state.html#ggaa04bf444e4940f133b7f116d38fa44dba329ff7030bd0e90a946f98efdd41557f", null ],
      [ "TIM5_OSSIState_Disable", "group___t_i_m5___o_s_s_i___state.html#ggaa04bf444e4940f133b7f116d38fa44dba450b50f2614e208ee8ebaa531b7550d6", null ]
    ] ],
    [ "TIM5_OutputState_TypeDef", "group___t_i_m5___output___state.html#gaf0cba7bb42d042b7b4cac0fed31f5adf", [
      [ "TIM5_OutputState_Disable", "group___t_i_m5___output___state.html#ggaf0cba7bb42d042b7b4cac0fed31f5adfa0807ce64a24de0d44d39ce996c0ee3ce", null ],
      [ "TIM5_OutputState_Enable", "group___t_i_m5___output___state.html#ggaf0cba7bb42d042b7b4cac0fed31f5adfaf1f76bd978c2e4e3f02dac1a4cb4711c", null ]
    ] ],
    [ "TIM5_Prescaler_TypeDef", "group___t_i_m5___prescaler.html#gac77bd9584f9efee7d9a13e55e0c8427b", [
      [ "TIM5_Prescaler_1", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba8fdd99d1b9d5f3ba81b6d4fb2298a2df", null ],
      [ "TIM5_Prescaler_2", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba0534bf5037c442316cd2d4302f60d263", null ],
      [ "TIM5_Prescaler_4", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427baea4433cf8dfbe771ecb8f77d11458faa", null ],
      [ "TIM5_Prescaler_8", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba484263125b7793d50fc1f8d425cdeb3f", null ],
      [ "TIM5_Prescaler_16", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba97895e9ac6ec18cb384b96c5e509787d", null ],
      [ "TIM5_Prescaler_32", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427baa43047fe49934ac61434b782aff17f23", null ],
      [ "TIM5_Prescaler_64", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba6ca21073c23d394fe922c618e68cd345", null ],
      [ "TIM5_Prescaler_128", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba45079c3c92efe551253f615da497f694", null ]
    ] ],
    [ "TIM5_PSCReloadMode_TypeDef", "group___t_i_m5___prescaler___reload___mode.html#ga57982329133eceaa348853e8e9f5e801", [
      [ "TIM5_PSCReloadMode_Update", "group___t_i_m5___prescaler___reload___mode.html#gga57982329133eceaa348853e8e9f5e801a6cf0446be4c389769a4e4d6e583729c1", null ],
      [ "TIM5_PSCReloadMode_Immediate", "group___t_i_m5___prescaler___reload___mode.html#gga57982329133eceaa348853e8e9f5e801a89361135f1825e6ab4c36ea218f5d29f", null ]
    ] ],
    [ "TIM5_SlaveMode_TypeDef", "group___t_i_m5___slave___mode.html#ga14fbbfc108c8430840978c90cdfb0102", [
      [ "TIM5_SlaveMode_Reset", "group___t_i_m5___slave___mode.html#gga14fbbfc108c8430840978c90cdfb0102aed2466cc10c6f26ac8b40902a9563a01", null ],
      [ "TIM5_SlaveMode_Gated", "group___t_i_m5___slave___mode.html#gga14fbbfc108c8430840978c90cdfb0102a416a0cfb4bb6890302703704dd860b56", null ],
      [ "TIM5_SlaveMode_Trigger", "group___t_i_m5___slave___mode.html#gga14fbbfc108c8430840978c90cdfb0102ac60e1d73f1aaf115b4ae47c845f36c03", null ],
      [ "TIM5_SlaveMode_External1", "group___t_i_m5___slave___mode.html#gga14fbbfc108c8430840978c90cdfb0102a7c25040481f1fbdb74d346e4e9943482", null ]
    ] ],
    [ "TIM5_TIxExternalCLK1Source_TypeDef", "group___t_i_m5___t_i___external___clock___source.html#gab66a49026c1f93443c620cbf5a91b637", [
      [ "TIM5_TIxExternalCLK1Source_TI1ED", "group___t_i_m5___t_i___external___clock___source.html#ggab66a49026c1f93443c620cbf5a91b637adfd27701785e47d62fb790be4f550446", null ],
      [ "TIM5_TIxExternalCLK1Source_TI1", "group___t_i_m5___t_i___external___clock___source.html#ggab66a49026c1f93443c620cbf5a91b637a71002412d33774793e65304c85ad7156", null ],
      [ "TIM5_TIxExternalCLK1Source_TI2", "group___t_i_m5___t_i___external___clock___source.html#ggab66a49026c1f93443c620cbf5a91b637ae608ac9f3e288735ce5a8c8829220150", null ]
    ] ],
    [ "TIM5_TRGOSource_TypeDef", "group___t_i_m5___trigger___output___source.html#ga7c5599cf024fac13a094e4534776b870", [
      [ "TIM5_TRGOSource_Reset", "group___t_i_m5___trigger___output___source.html#gga7c5599cf024fac13a094e4534776b870a8a1add4917f86511036185e059806adc", null ],
      [ "TIM5_TRGOSource_Enable", "group___t_i_m5___trigger___output___source.html#gga7c5599cf024fac13a094e4534776b870ab5c5905736e7907976c4c163a2384a79", null ],
      [ "TIM5_TRGOSource_Update", "group___t_i_m5___trigger___output___source.html#gga7c5599cf024fac13a094e4534776b870aac6e69d75e0e0dab67926030fe938470", null ],
      [ "TIM5_TRGOSource_OC1", "group___t_i_m5___trigger___output___source.html#gga7c5599cf024fac13a094e4534776b870a18d94f08b2fb3cd76287eb89ea104225", null ],
      [ "TIM5_TRGOSource_OC1REF", "group___t_i_m5___trigger___output___source.html#gga7c5599cf024fac13a094e4534776b870ae33e943958a2ee4c28dd621a50f63865", null ],
      [ "TIM5_TRGOSource_OC2REF", "group___t_i_m5___trigger___output___source.html#gga7c5599cf024fac13a094e4534776b870a4bc3a7c970a69f0f24a3d3ca2ac9c554", null ]
    ] ],
    [ "TIM5_TRGSelection_TypeDef", "group___t_i_m5___internal___trigger___selection.html#ga77df2b0019fdda2f1ad7aa9b083d92b1", [
      [ "TIM5_TRGSelection_TIM4", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1ae3488c3e9b50b167016269a0a03b0aca", null ],
      [ "TIM5_TRGSelection_TIM1", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1a86be1af63e71e90bffb462e8f94a59fd", null ],
      [ "TIM5_TRGSelection_TIM3", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1ad61aa17de2f3aeaf5683e7606927cee5", null ],
      [ "TIM5_TRGSelection_TIM2", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1a9aeaf87e6596605ab9ef0bb55198edfd", null ],
      [ "TIM5_TRGSelection_TI1F_ED", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1ac366914a4361e70da0b156060dbe272a", null ],
      [ "TIM5_TRGSelection_TI1FP1", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1ab0a160f69e822297a7f00c4df7c68295", null ],
      [ "TIM5_TRGSelection_TI2FP2", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1aebb523129174e94419f3f7ae0599fbd5", null ],
      [ "TIM5_TRGSelection_ETRF", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1a88129fef0592043e97b4af9e8667cd8a", null ]
    ] ],
    [ "TIM5_UpdateSource_TypeDef", "group___t_i_m5___update___source.html#ga041b1f26c24f3e19f0a08bcdde1b3022", [
      [ "TIM5_UpdateSource_Global", "group___t_i_m5___update___source.html#gga041b1f26c24f3e19f0a08bcdde1b3022a7ea4096f37bfae994a6862023278323c", null ],
      [ "TIM5_UpdateSource_Regular", "group___t_i_m5___update___source.html#gga041b1f26c24f3e19f0a08bcdde1b3022a4e8bd653ede2b4872d7615b0b3a4db64", null ]
    ] ],
    [ "TIM5_ARRPreloadConfig", "group___t_i_m5.html#ga028e838bbdf926145f0c0df0a42cbfb2", null ],
    [ "TIM5_BKRConfig", "group___t_i_m5.html#ga7175b49cdc94615438db911b3ae67219", null ],
    [ "TIM5_CCxCmd", "group___t_i_m5.html#ga869d6041a28bd998489231dafa195e85", null ],
    [ "TIM5_ClearFlag", "group___t_i_m5.html#ga8bd56e6dcb6ad69042cade7eccaa0312", null ],
    [ "TIM5_ClearITPendingBit", "group___t_i_m5.html#ga662ce45c3bbc00018d160b9df8e65360", null ],
    [ "TIM5_Cmd", "group___t_i_m5.html#ga7398d4769dc83ee17dd88477286e8e33", null ],
    [ "TIM5_CounterModeConfig", "group___t_i_m5.html#ga53f10ed4d9ba371fbc4b42f08bfdbe70", null ],
    [ "TIM5_CtrlPWMOutputs", "group___t_i_m5.html#gae5d298e623fd220441989a7182e43eb2", null ],
    [ "TIM5_DeInit", "group___t_i_m5.html#gafa10e2573fe38ee52ff48c95960f87a3", null ],
    [ "TIM5_DMACmd", "group___t_i_m5.html#ga0d9314194f748725e38ffd3a9e263cb2", null ],
    [ "TIM5_EncoderInterfaceConfig", "group___t_i_m5.html#gad6e0b6e64d89430557442bd1ee371842", null ],
    [ "TIM5_ETRClockMode1Config", "group___t_i_m5.html#ga6688e6d33bd0b5bcf85e86f963163ab5", null ],
    [ "TIM5_ETRClockMode2Config", "group___t_i_m5.html#ga33f527e86554fa2bb8e4bf5278e8c8f5", null ],
    [ "TIM5_ETRConfig", "group___t_i_m5.html#ga0a5784a46375fce22864dc3b021feeaa", null ],
    [ "TIM5_ForcedOC1Config", "group___t_i_m5.html#ga94e625d9ac70bc97a0e8c5b142af9942", null ],
    [ "TIM5_ForcedOC2Config", "group___t_i_m5.html#ga2194c1941550a024daf07af7ce1e0843", null ],
    [ "TIM5_GenerateEvent", "group___t_i_m5.html#gab432a14c278f430e674b7c52a8d13e24", null ],
    [ "TIM5_GetCapture1", "group___t_i_m5.html#gadce87cdf2f02645f64b16012f4536d6a", null ],
    [ "TIM5_GetCapture2", "group___t_i_m5.html#ga37ae416e15ff96bed14126defd6568e4", null ],
    [ "TIM5_GetCounter", "group___t_i_m5.html#gaaef6e616cd97ca6bfdc4d29b086d2a5f", null ],
    [ "TIM5_GetFlagStatus", "group___t_i_m5.html#ga3898fff5e733720fe7fa0467d35b33f9", null ],
    [ "TIM5_GetITStatus", "group___t_i_m5.html#gab879fde33535c32f9c031431a0c328bd", null ],
    [ "TIM5_GetPrescaler", "group___t_i_m5.html#ga20ee0b9891042ac01c0563b81e98500f", null ],
    [ "TIM5_ICInit", "group___t_i_m5.html#gad7295ea4e0d1c758775569ebdb0e9425", null ],
    [ "TIM5_InternalClockConfig", "group___t_i_m5.html#gaa3045d85bc98b193e9adfc2eae4c9ca9", null ],
    [ "TIM5_ITConfig", "group___t_i_m5.html#gaf117cf95222101910eb9513821433871", null ],
    [ "TIM5_OC1FastConfig", "group___t_i_m5.html#ga659db37fcb9eaa35d3b05bb848b7930c", null ],
    [ "TIM5_OC1Init", "group___t_i_m5.html#gac819de5f9b152d46deab5ffd3259808a", null ],
    [ "TIM5_OC1PolarityConfig", "group___t_i_m5.html#ga496cda36432f4a5c920d8514efc308b3", null ],
    [ "TIM5_OC1PreloadConfig", "group___t_i_m5.html#ga90f17f570e5f3ce99855acecd7631df1", null ],
    [ "TIM5_OC2FastConfig", "group___t_i_m5.html#gaf5947b4436a247b09c0d4b6d1efdce90", null ],
    [ "TIM5_OC2Init", "group___t_i_m5.html#ga2d91748bec2748703bcee424647eb4d8", null ],
    [ "TIM5_OC2PolarityConfig", "group___t_i_m5.html#gac71dd04fdb47ef8f2ffe5ed0c96a069d", null ],
    [ "TIM5_OC2PreloadConfig", "group___t_i_m5.html#ga2d6d670f1dbc8b373ff74630f3abe7af", null ],
    [ "TIM5_PrescalerConfig", "group___t_i_m5.html#gab80fdb3c07a11d46b09b268ad13b853d", null ],
    [ "TIM5_PWMIConfig", "group___t_i_m5.html#ga560f717b4a369056cf5e81669d6dbfa9", null ],
    [ "TIM5_SelectCCDMA", "group___t_i_m5.html#ga523d3459687ba79f669522db077f16c4", null ],
    [ "TIM5_SelectHallSensor", "group___t_i_m5.html#gae98d8a10d1685d75304163fbac2eae16", null ],
    [ "TIM5_SelectInputTrigger", "group___t_i_m5.html#ga27f2f833fbe047f14cf8311bccdeb977", null ],
    [ "TIM5_SelectMasterSlaveMode", "group___t_i_m5.html#gaa545b8b6e169a5d6b86eaa11d7fb2f33", null ],
    [ "TIM5_SelectOCxM", "group___t_i_m5.html#ga0b792818264874ef476999fe0e018864", null ],
    [ "TIM5_SelectOnePulseMode", "group___t_i_m5.html#gabad8083e39d0afd84a0b399607fa4404", null ],
    [ "TIM5_SelectOutputTrigger", "group___t_i_m5.html#ga2a51d91552b089566f58e1af8c7923ae", null ],
    [ "TIM5_SelectSlaveMode", "group___t_i_m5.html#ga9dc79c20fdeec4bde35e8ed5d019022b", null ],
    [ "TIM5_SetAutoreload", "group___t_i_m5.html#gac089b0a3758bc0baabf25d1854117f19", null ],
    [ "TIM5_SetCompare1", "group___t_i_m5.html#ga848fbefa25baa2ea26260b8941b7a9bc", null ],
    [ "TIM5_SetCompare2", "group___t_i_m5.html#ga0de74897146ade82ca1bbc7fb2cd55ed", null ],
    [ "TIM5_SetCounter", "group___t_i_m5.html#gab9922abb371c6bd777872d91e135e490", null ],
    [ "TIM5_SetIC1Prescaler", "group___t_i_m5.html#ga578de8b51215192a140453f654441933", null ],
    [ "TIM5_SetIC2Prescaler", "group___t_i_m5.html#gac69828b04314da2328375812c50d5d77", null ],
    [ "TIM5_TimeBaseInit", "group___t_i_m5.html#ga9168c46c087454475d8d6b977650a141", null ],
    [ "TIM5_TIxExternalClockConfig", "group___t_i_m5.html#gaf6a9e78ee41fcdb0d538cb5051a8b665", null ],
    [ "TIM5_UpdateDisableConfig", "group___t_i_m5.html#gaba4bf3912e855633a5b3db931edd2a6f", null ],
    [ "TIM5_UpdateRequestConfig", "group___t_i_m5.html#ga27850fca7a4c129a59d1026e024fac4a", null ]
];