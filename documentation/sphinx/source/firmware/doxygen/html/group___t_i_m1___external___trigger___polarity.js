var group___t_i_m1___external___trigger___polarity =
[
    [ "TIM1_ExtTRGPolarity_TypeDef", "group___t_i_m1___external___trigger___polarity.html#gac537b5e4ea05418288bce8e6d2b39480", [
      [ "TIM1_ExtTRGPolarity_Inverted", "group___t_i_m1___external___trigger___polarity.html#ggac537b5e4ea05418288bce8e6d2b39480abf4f8b906d2be59a0df0b6a6e82b3346", null ],
      [ "TIM1_ExtTRGPolarity_NonInverted", "group___t_i_m1___external___trigger___polarity.html#ggac537b5e4ea05418288bce8e6d2b39480a68f2e4bfc3a4e025e57074114a418965", null ]
    ] ]
];