var group___d_m_a___data___transfer___direction =
[
    [ "DMA_DIR_TypeDef", "group___d_m_a___data___transfer___direction.html#ga8b9323d1fd5324f2c3b8b5ec1f718049", [
      [ "DMA_DIR_PeripheralToMemory", "group___d_m_a___data___transfer___direction.html#gga8b9323d1fd5324f2c3b8b5ec1f718049a7ebf8dbdbce18575423953bf1893a0be", null ],
      [ "DMA_DIR_MemoryToPeripheral", "group___d_m_a___data___transfer___direction.html#gga8b9323d1fd5324f2c3b8b5ec1f718049aae59fbef0f5f4f55bbec5dca6293df0c", null ],
      [ "DMA_DIR_Memory0ToMemory1", "group___d_m_a___data___transfer___direction.html#gga8b9323d1fd5324f2c3b8b5ec1f718049a440279ceb694b8af8230777f7791bcfc", null ]
    ] ]
];