var group___t_i_m3___slave___mode =
[
    [ "TIM3_SlaveMode_TypeDef", "group___t_i_m3___slave___mode.html#gaf0a169c439c94b689bc3680c54c80725", [
      [ "TIM3_SlaveMode_Reset", "group___t_i_m3___slave___mode.html#ggaf0a169c439c94b689bc3680c54c80725ae83e348e5283f36812b43e8fa861d45a", null ],
      [ "TIM3_SlaveMode_Gated", "group___t_i_m3___slave___mode.html#ggaf0a169c439c94b689bc3680c54c80725a9be0d4aa41bf6812300aa0bb9b396bfa", null ],
      [ "TIM3_SlaveMode_Trigger", "group___t_i_m3___slave___mode.html#ggaf0a169c439c94b689bc3680c54c80725a6e95ce37d637b61ba81c1cd9317468b2", null ],
      [ "TIM3_SlaveMode_External1", "group___t_i_m3___slave___mode.html#ggaf0a169c439c94b689bc3680c54c80725ac1f1dbf992b5780a84a3f0d86ffd905e", null ]
    ] ]
];