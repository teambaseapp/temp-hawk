var group___f_l_a_s_h___power___status =
[
    [ "FLASH_PowerStatus_TypeDef", "group___f_l_a_s_h___power___status.html#ga78a91d30cbc11fa4601a5a4f21c38678", [
      [ "FLASH_PowerStatus_IDDQDuringWaitMode", "group___f_l_a_s_h___power___status.html#gga78a91d30cbc11fa4601a5a4f21c38678a354ebd7d4dcea8a690f2354707c1b14e", null ],
      [ "FLASH_PowerStatus_IDDQDuringRunMode", "group___f_l_a_s_h___power___status.html#gga78a91d30cbc11fa4601a5a4f21c38678a6391225c519a4c1c07732136ed2b0bf7", null ],
      [ "FLASH_PowerStatus_IDDQDuringWaitAndRunModes", "group___f_l_a_s_h___power___status.html#gga78a91d30cbc11fa4601a5a4f21c38678a5e634aa3ff25648fcdf7b22ceb4a6656", null ],
      [ "FLASH_PowerStatus_On", "group___f_l_a_s_h___power___status.html#gga78a91d30cbc11fa4601a5a4f21c38678a36d84f127083d442185142ca9a04ad63", null ]
    ] ]
];