var stm8l15x__tim4_8h =
[
    [ "TIM4_DMASource_TypeDef", "group___t_i_m4___d_m_a__source__requests.html#ga54d93fcfd1a9c9cdb38827bafec0b487", [
      [ "TIM4_DMASource_Update", "group___t_i_m4___d_m_a__source__requests.html#gga54d93fcfd1a9c9cdb38827bafec0b487a5c4045a5696a715e7d0ada0cf1743a91", null ]
    ] ],
    [ "TIM4_EventSource_TypeDef", "group___t_i_m4___event___source.html#ga80b69b456aacbc5712b9c9114dd677d4", [
      [ "TIM4_EventSource_Update", "group___t_i_m4___event___source.html#gga80b69b456aacbc5712b9c9114dd677d4a830ddc74dbf788efbb7fd4e665ee48e0", null ],
      [ "TIM4_EventSource_Trigger", "group___t_i_m4___event___source.html#gga80b69b456aacbc5712b9c9114dd677d4a9d7d984178d5b346ab19b7bd17174fc4", null ]
    ] ],
    [ "TIM4_FLAG_TypeDef", "group___t_i_m4___flags.html#ga08a669a75d8f4d30c16da24643fc30a6", [
      [ "TIM4_FLAG_Update", "group___t_i_m4___flags.html#gga08a669a75d8f4d30c16da24643fc30a6abbaed831340cb493c05374559315888b", null ],
      [ "TIM4_FLAG_Trigger", "group___t_i_m4___flags.html#gga08a669a75d8f4d30c16da24643fc30a6a0bc0cf79a23ce3c0882dd053bb09ede5", null ]
    ] ],
    [ "TIM4_IT_TypeDef", "group___t_i_m4___interrupts.html#ga060727a206d708b1d176992dc21c7c8f", [
      [ "TIM4_IT_Update", "group___t_i_m4___interrupts.html#gga060727a206d708b1d176992dc21c7c8fa7bb0daf67d3e8d6aac64434317be879e", null ],
      [ "TIM4_IT_Trigger", "group___t_i_m4___interrupts.html#gga060727a206d708b1d176992dc21c7c8fa4a0bde4623c74144ce9365e0edf96314", null ]
    ] ],
    [ "TIM4_OPMode_TypeDef", "group___t_i_m4___one___pulse___mode.html#ga8a2aae545fff5cdf282c594a2ac27871", [
      [ "TIM4_OPMode_Single", "group___t_i_m4___one___pulse___mode.html#gga8a2aae545fff5cdf282c594a2ac27871a76be04097af9a612dac8b1126dc20a76", null ],
      [ "TIM4_OPMode_Repetitive", "group___t_i_m4___one___pulse___mode.html#gga8a2aae545fff5cdf282c594a2ac27871a7f487acdc7381c4915ae6beb7ccd9032", null ]
    ] ],
    [ "TIM4_Prescaler_TypeDef", "group___t_i_m4___prescaler.html#gafc1afaaf36a9535c236a7642014c840e", [
      [ "TIM4_Prescaler_1", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840eae60f3bc3b43d5d18be4b360116002506", null ],
      [ "TIM4_Prescaler_2", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea6ee2eff1b3c4a6cfea2f53682060a667", null ],
      [ "TIM4_Prescaler_4", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea6a13cbe45fb532436e92abefc0b25cad", null ],
      [ "TIM4_Prescaler_8", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea329ea7a6cf2925a265aca11b0eee6923", null ],
      [ "TIM4_Prescaler_16", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840eac454dd17ba1625409bbf765345be84a4", null ],
      [ "TIM4_Prescaler_32", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea205a13c0c0296d18855e92c31de3f4bc", null ],
      [ "TIM4_Prescaler_64", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea9e075a6fa77ecb89d73cae127bb460d1", null ],
      [ "TIM4_Prescaler_128", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840eaaa297dc8d951acf60f9e6f761abbd916", null ],
      [ "TIM4_Prescaler_256", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea06ca9484dab970e0a83b00827a19a259", null ],
      [ "TIM4_Prescaler_512", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea28be2cafcb57dc93353c9e7bdc6bd389", null ],
      [ "TIM4_Prescaler_1024", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea9bdc20e4571ab3165e877b1cc4f0d3b6", null ],
      [ "TIM4_Prescaler_2048", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ead2e73b2ca44ef2f72b8a53bd10edc38f", null ],
      [ "TIM4_Prescaler_4096", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea39ad8c3842c73362720b05c56295132c", null ],
      [ "TIM4_Prescaler_8192", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea3067d04a06287b01e7d164a20892614d", null ],
      [ "TIM4_Prescaler_16384", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840ea62cc746f7be212225c9df9ffc9160a7c", null ],
      [ "TIM4_Prescaler_32768", "group___t_i_m4___prescaler.html#ggafc1afaaf36a9535c236a7642014c840eae8f7c211cc2e76da66d399c334a28b43", null ]
    ] ],
    [ "TIM4_PSCReloadMode_TypeDef", "group___t_i_m4___reload___mode___prescaler.html#gadd3cfc0cfc518deab4c649403e4ac2dd", [
      [ "TIM4_PSCReloadMode_Update", "group___t_i_m4___reload___mode___prescaler.html#ggadd3cfc0cfc518deab4c649403e4ac2dda58d7130f9f8eb38486614df84592f6c0", null ],
      [ "TIM4_PSCReloadMode_Immediate", "group___t_i_m4___reload___mode___prescaler.html#ggadd3cfc0cfc518deab4c649403e4ac2dda54a5344b8b9bb68d3e3d2ad7e3edafca", null ]
    ] ],
    [ "TIM4_SlaveMode_TypeDef", "group___t_i_m4___salve___mode.html#gac22be9bb6535a9ea3ed45c0893367aab", [
      [ "TIM4_SlaveMode_Disable", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba13764467dbcfa31ea39332d689ba6cbd", null ],
      [ "TIM4_SlaveMode_Reset", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba8a41acd86a1589a8d4907ac6717cfc85", null ],
      [ "TIM4_SlaveMode_Gated", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba5b0c8750acc9a973cf1c518630edbca7", null ],
      [ "TIM4_SlaveMode_Trigger", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba3bee656f4434a9b4d911d90016d59efb", null ],
      [ "TIM4_SlaveMode_External1", "group___t_i_m4___salve___mode.html#ggac22be9bb6535a9ea3ed45c0893367aaba4cad33d754b4e25a8484ec6922ee8a0d", null ]
    ] ],
    [ "TIM4_TRGOSource_TypeDef", "group___t_i_m4___trigger___output___source.html#gadf78b6a17ab3620040fe9b881f007a09", [
      [ "TIM4_TRGOSource_Reset", "group___t_i_m4___trigger___output___source.html#ggadf78b6a17ab3620040fe9b881f007a09aa24c01fb83e9c16b69c3de305a653f7e", null ],
      [ "TIM4_TRGOSource_Enable", "group___t_i_m4___trigger___output___source.html#ggadf78b6a17ab3620040fe9b881f007a09a073788167785921d46f0aa66fda484af", null ],
      [ "TIM4_TRGOSource_Update", "group___t_i_m4___trigger___output___source.html#ggadf78b6a17ab3620040fe9b881f007a09af5353ef31203852402bcc89842a4adbb", null ]
    ] ],
    [ "TIM4_TRGSelection_TypeDef", "group___t_i_m4___internal___trigger___selection.html#gaf6bdf68959e8a469658e84146bd8d250", [
      [ "TIM4_TRGSelection_TIM5", "group___t_i_m4___internal___trigger___selection.html#ggaf6bdf68959e8a469658e84146bd8d250a073839c6d29b339b29f1623dff7696a6", null ],
      [ "TIM4_TRGSelection_TIM1", "group___t_i_m4___internal___trigger___selection.html#ggaf6bdf68959e8a469658e84146bd8d250a3363ef2208ba043493be9c524cc56ef9", null ],
      [ "TIM4_TRGSelection_TIM3", "group___t_i_m4___internal___trigger___selection.html#ggaf6bdf68959e8a469658e84146bd8d250a3bfcddd43268fbba7d3c279a23c61971", null ],
      [ "TIM4_TRGSelection_TIM2", "group___t_i_m4___internal___trigger___selection.html#ggaf6bdf68959e8a469658e84146bd8d250a524a052ed6adca26bd41fbe66f108a6d", null ]
    ] ],
    [ "TIM4_UpdateSource_TypeDef", "group___t_i_m4___update___source.html#ga211732f8eb6c46547c31d9eb32f36e7c", [
      [ "TIM4_UpdateSource_Global", "group___t_i_m4___update___source.html#gga211732f8eb6c46547c31d9eb32f36e7ca06ea10597e6c2d25818644e22a47f185", null ],
      [ "TIM4_UpdateSource_Regular", "group___t_i_m4___update___source.html#gga211732f8eb6c46547c31d9eb32f36e7ca156d945526200c5d3f4dc5bed9e33f75", null ]
    ] ],
    [ "TIM4_ARRPreloadConfig", "group___t_i_m4.html#ga7d55b402ff90834059634919b2b0663c", null ],
    [ "TIM4_ClearFlag", "group___t_i_m4.html#ga68bf7dba212a67b444cb24c98147cb1a", null ],
    [ "TIM4_ClearITPendingBit", "group___t_i_m4.html#ga1baccba593d47c2c563acd1f7a2d4ca0", null ],
    [ "TIM4_Cmd", "group___t_i_m4.html#ga1f5556fdfb8413a83ec47a40a4a213c0", null ],
    [ "TIM4_DeInit", "group___t_i_m4.html#gae69c7fea275362f2fd9a39ae91ed89a0", null ],
    [ "TIM4_DMACmd", "group___t_i_m4.html#ga998eca93852f289e74330cd713046bb3", null ],
    [ "TIM4_GenerateEvent", "group___t_i_m4.html#gaa58252ecf95374571494922bcb5e73cc", null ],
    [ "TIM4_GetCounter", "group___t_i_m4.html#ga38630a02abaa224b8e36c9b4086ef04b", null ],
    [ "TIM4_GetFlagStatus", "group___t_i_m4.html#ga0395b9dd06c2b4d998771a454dc7e33f", null ],
    [ "TIM4_GetITStatus", "group___t_i_m4.html#gacdfd8bbea4ab13ab6f8c73da3109d1bf", null ],
    [ "TIM4_GetPrescaler", "group___t_i_m4.html#gadd4422367bf6a03895e4391b7056bfb9", null ],
    [ "TIM4_InternalClockConfig", "group___t_i_m4.html#ga2ce56860e60bef8d6f44f0a39776b7f7", null ],
    [ "TIM4_ITConfig", "group___t_i_m4.html#ga9f26f1c1147c5b47c78cbc1348b55fc3", null ],
    [ "TIM4_PrescalerConfig", "group___t_i_m4.html#ga21f1a71a2007625d5a934570f50c8b3d", null ],
    [ "TIM4_SelectInputTrigger", "group___t_i_m4.html#ga89e637ea12f2d8bc2c064b9419f2b60a", null ],
    [ "TIM4_SelectMasterSlaveMode", "group___t_i_m4.html#gaf986b3929fb30f472b011c8e53257ff7", null ],
    [ "TIM4_SelectOnePulseMode", "group___t_i_m4.html#gad5c3c213c9375bc4c0555a8ece0c0b14", null ],
    [ "TIM4_SelectOutputTrigger", "group___t_i_m4.html#gab7e4de3979c34fc04aaf7b694157504a", null ],
    [ "TIM4_SelectSlaveMode", "group___t_i_m4.html#ga177282daf1185a5a74479635a9121d03", null ],
    [ "TIM4_SetAutoreload", "group___t_i_m4.html#ga5bb458117fbb808a57aff80165e839b8", null ],
    [ "TIM4_SetCounter", "group___t_i_m4.html#ga06506a236272e5d8a7f6d8157dd0a4f1", null ],
    [ "TIM4_TimeBaseInit", "group___t_i_m4.html#gac1a47a30cf6b1d2934dd2ac95f8db1f4", null ],
    [ "TIM4_UpdateDisableConfig", "group___t_i_m4.html#ga6b95c00006f4436e417fbf7c0654b326", null ],
    [ "TIM4_UpdateRequestConfig", "group___t_i_m4.html#ga5e51b08c778a31ad34c958b0dbe1446e", null ]
];