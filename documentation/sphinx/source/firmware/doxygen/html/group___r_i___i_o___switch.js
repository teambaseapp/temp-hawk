var group___r_i___i_o___switch =
[
    [ "RI_IOSwitch_TypeDef", "group___r_i___i_o___switch.html#gad7cb6b8951dd38e59ff1cec091ecd449", [
      [ "RI_IOSwitch_1", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a20ac2986605220bb16436eaa35200bd4", null ],
      [ "RI_IOSwitch_2", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a71dccc6200b24ef868c75c2073fc8444", null ],
      [ "RI_IOSwitch_3", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449aa100bdcf16acaa4bdbf9135f0620e4bd", null ],
      [ "RI_IOSwitch_4", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449ac3bf138368e50dc4b9c5705c2d3cf05a", null ],
      [ "RI_IOSwitch_5", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449ad34018e98f5a4f285b1612b206d84015", null ],
      [ "RI_IOSwitch_6", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a3063e786d87db35d914acf77e6279023", null ],
      [ "RI_IOSwitch_7", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a11b0df054bf9097db74f295ba3522f73", null ],
      [ "RI_IOSwitch_8", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a124957b320acefcea33abcbbacb5f70c", null ],
      [ "RI_IOSwitch_9", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a46eecf3b2dee630eae9ddf4bef335a11", null ],
      [ "RI_IOSwitch_10", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a4e5d21ef346df37b210e0289bdd989e2", null ],
      [ "RI_IOSwitch_11", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449af2c57ec57bf42f0ecdcc6c0fa34d8642", null ],
      [ "RI_IOSwitch_12", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a3c4313db048d7cfb1a8dc4e0ea25665b", null ],
      [ "RI_IOSwitch_13", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449ae961d2b15e433c7d607eb1a1fee5aa57", null ],
      [ "RI_IOSwitch_14", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a658cf049352723eef887083ec2859dfc", null ],
      [ "RI_IOSwitch_15", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a98144d87055d93c8a2aeb8135ff54c3c", null ],
      [ "RI_IOSwitch_16", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a7094aad08a2ff349b68e47e983be5a14", null ],
      [ "RI_IOSwitch_17", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449aacf42e59a9766e6003417ac7ef4cc552", null ],
      [ "RI_IOSwitch_18", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449ae61715434986750097f83676368833ac", null ],
      [ "RI_IOSwitch_19", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449abfec3c0ca696999971030c163134731c", null ],
      [ "RI_IOSwitch_20", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a3d43f46e500ecdb061dafbb722dedf08", null ],
      [ "RI_IOSwitch_21", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a382af77e9a576bc48ff20ab341f0d0c9", null ],
      [ "RI_IOSwitch_22", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a8f97fee98076d640175f6c821a147a0d", null ],
      [ "RI_IOSwitch_23", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a67ca080f9293c500ab39907c01b1fda0", null ],
      [ "RI_IOSwitch_24", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a1e1a921d4edab9f43db6d19963342d75", null ],
      [ "RI_IOSwitch_26", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449acf15bf08faecb8258544b1fe10d9303e", null ],
      [ "RI_IOSwitch_27", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a5ae338e105cbe17310960c4a127ece48", null ],
      [ "RI_IOSwitch_28", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a6e803fe601b26eabc5e3c36718f08927", null ],
      [ "RI_IOSwitch_29", "group___r_i___i_o___switch.html#ggad7cb6b8951dd38e59ff1cec091ecd449a4270e727ac16dd75e9655616978761cd", null ]
    ] ]
];