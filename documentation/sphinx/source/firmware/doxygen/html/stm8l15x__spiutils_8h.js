var stm8l15x__spiutils_8h =
[
    [ "GPIOSPIConf", "stm8l15x__spiutils_8h.html#a080e49d2df684a04668f713f8fe83a2f", null ],
    [ "SetUpSPI", "stm8l15x__spiutils_8h.html#ad64fd2b0e33ccaac7ae9beea577faae7", null ],
    [ "SPIClock", "stm8l15x__spiutils_8h.html#a2bffec73b96f65f6fb19fe8fc113cd2d", null ],
    [ "SPIConf", "stm8l15x__spiutils_8h.html#a7726c095a105ff90a8af03f52b12f96a", null ],
    [ "SPIEnable", "stm8l15x__spiutils_8h.html#a74f7751cbd6e95b7c47435fe21b0a3d9", null ]
];