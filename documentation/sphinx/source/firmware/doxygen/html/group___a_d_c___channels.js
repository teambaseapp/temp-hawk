var group___a_d_c___channels =
[
    [ "ADC_Channel_TypeDef", "group___a_d_c___channels.html#ga4532b496a94f70aeb665d856bde46974", [
      [ "ADC_Channel_0", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974aff7284d7e58d8953751c937e0f39e5d7", null ],
      [ "ADC_Channel_1", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974aebd77921cc2569f281ec57ef0dc2ea57", null ],
      [ "ADC_Channel_2", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a1968a94de7202bbaabe7166a77cbb241", null ],
      [ "ADC_Channel_3", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974ac059fce228c21fef31d6748ac16f5b91", null ],
      [ "ADC_Channel_4", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a753f1cdc49cd36768c1485817c5e1533", null ],
      [ "ADC_Channel_5", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974af317033ecd320300a36591be74202d0a", null ],
      [ "ADC_Channel_6", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a4d80a8e5a8267f4e8c5f843dab15da83", null ],
      [ "ADC_Channel_7", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a9fa56fbdb55bebecc85eed9544bc63ce", null ],
      [ "ADC_Channel_8", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a07a089cbcaa89f8f39d35388019e4d6e", null ],
      [ "ADC_Channel_9", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974afba208f2f21ef4c1e8a9cd82f76bd6e2", null ],
      [ "ADC_Channel_10", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974aaca9e0205e79bd661ddab441303c9dd0", null ],
      [ "ADC_Channel_11", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a59b6944e32fd76414e3d3c30f111d16e", null ],
      [ "ADC_Channel_12", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a4cf027852a5fffaaabf695e8f03c3fac", null ],
      [ "ADC_Channel_13", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a3b4f5508b3134cd4cc4d2331c33dfa6c", null ],
      [ "ADC_Channel_14", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a84ecd104f3cdb900d732a8b36009f166", null ],
      [ "ADC_Channel_15", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a927efbff4ef790bc729d075a81da8abc", null ],
      [ "ADC_Channel_16", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a792e685ad3afad3a03cb2c7d6244cb46", null ],
      [ "ADC_Channel_17", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a00a4978015ff539db4d900fae61c6ac0", null ],
      [ "ADC_Channel_18", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a9f46a72bcfd9c73719ce969c3c258c1a", null ],
      [ "ADC_Channel_19", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a10f65a5cde24db1c5e4dfe63885d3612", null ],
      [ "ADC_Channel_20", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a0f236ec3a0ad4c3a2bf91f22c42f1851", null ],
      [ "ADC_Channel_21", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a5745be8684535645d54b276099af472f", null ],
      [ "ADC_Channel_22", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974acb182e93c07564004b25fbd2b12bcad5", null ],
      [ "ADC_Channel_23", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974ad772e59283977aa1836f6efdc5c63655", null ],
      [ "ADC_Channel_24", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974ab66621db653a4605014929b99e33b6b8", null ],
      [ "ADC_Channel_25", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a93d4ee7571e7d096a86e8fe552b1fc3f", null ],
      [ "ADC_Channel_26", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a53d9d05bec88fc661d143273132cac5d", null ],
      [ "ADC_Channel_27", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a628d1c4e1a154dcd69b302a5681960ca", null ],
      [ "ADC_Channel_Vrefint", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a0b22ec6dabbe42c6ee5b56bd2ba6ed57", null ],
      [ "ADC_Channel_TempSensor", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a8c0a1ae36edce2f71191eecad5aa27f8", null ],
      [ "ADC_Channel_00To07", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a4f86b7a2dd1829f9df3db8cc667403d2", null ],
      [ "ADC_Channel_08To15", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974ad73a37d58fcc922b17f6989dec5760c9", null ],
      [ "ADC_Channel_16To23", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974a8367e291428c68eef5f987259d25853a", null ],
      [ "ADC_Channel_24To27", "group___a_d_c___channels.html#gga4532b496a94f70aeb665d856bde46974aaa78fea0709e055c67f2cf74995ec7cb", null ]
    ] ]
];