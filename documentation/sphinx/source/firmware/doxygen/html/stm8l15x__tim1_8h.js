var stm8l15x__tim1_8h =
[
    [ "TIM1_AutomaticOutput_TypeDef", "group___t_i_m1___automatic___output.html#gab96cb2fcf28a4da8e0d9d6652cf5b59d", [
      [ "TIM1_AutomaticOutput_Enable", "group___t_i_m1___automatic___output.html#ggab96cb2fcf28a4da8e0d9d6652cf5b59dacbff68bb9e5662efbcbdcd6d46a2e32a", null ],
      [ "TIM1_AutomaticOutput_Disable", "group___t_i_m1___automatic___output.html#ggab96cb2fcf28a4da8e0d9d6652cf5b59da546fcb9b091a740a94f32f1e6de97726", null ]
    ] ],
    [ "TIM1_BreakPolarity_TypeDef", "group___t_i_m1___break___polarity.html#gae0a2d144136d19e3e7678db3e11f2d87", [
      [ "TIM1_BreakPolarity_Low", "group___t_i_m1___break___polarity.html#ggae0a2d144136d19e3e7678db3e11f2d87a12302f883e5a2bf4af7ad064ce2900b5", null ],
      [ "TIM1_BreakPolarity_High", "group___t_i_m1___break___polarity.html#ggae0a2d144136d19e3e7678db3e11f2d87abead5cef8632eff46d5f3d4dfafe33a1", null ]
    ] ],
    [ "TIM1_BreakState_TypeDef", "group___t_i_m1___break___state.html#ga0dc5e6dcc251df12e895d9bf0e68ccea", [
      [ "TIM1_BreakState_Enable", "group___t_i_m1___break___state.html#gga0dc5e6dcc251df12e895d9bf0e68cceaa66dfc8991cb72b4e9e4acd14faf67a63", null ],
      [ "TIM1_BreakState_Disable", "group___t_i_m1___break___state.html#gga0dc5e6dcc251df12e895d9bf0e68cceaa4c75394e81bc599e3ac720508cf54bf4", null ]
    ] ],
    [ "TIM1_Channel_TypeDef", "group___t_i_m1___channels.html#ga61ff0590f0b73af0ea6ce04b172ef1db", [
      [ "TIM1_Channel_1", "group___t_i_m1___channels.html#gga61ff0590f0b73af0ea6ce04b172ef1dbaf8b3d0552c273d19d6b8f90c34426894", null ],
      [ "TIM1_Channel_2", "group___t_i_m1___channels.html#gga61ff0590f0b73af0ea6ce04b172ef1dbae5056857ae0877aa10302848a5ae5ebe", null ],
      [ "TIM1_Channel_3", "group___t_i_m1___channels.html#gga61ff0590f0b73af0ea6ce04b172ef1dba7c14d9709348fe2bf054d354a6eb19f3", null ],
      [ "TIM1_Channel_4", "group___t_i_m1___channels.html#gga61ff0590f0b73af0ea6ce04b172ef1dba7e403560e1a64f8407b846eb847a74f1", null ]
    ] ],
    [ "TIM1_CounterMode_TypeDef", "group___t_i_m1___counter___mode.html#ga852e3b4b649c3d2ec12117d634d8b604", [
      [ "TIM1_CounterMode_Up", "group___t_i_m1___counter___mode.html#gga852e3b4b649c3d2ec12117d634d8b604af1c959ae59093f99789d9d782684d1ef", null ],
      [ "TIM1_CounterMode_Down", "group___t_i_m1___counter___mode.html#gga852e3b4b649c3d2ec12117d634d8b604af561949698f3cd5898761f502a581d89", null ],
      [ "TIM1_CounterMode_CenterAligned1", "group___t_i_m1___counter___mode.html#gga852e3b4b649c3d2ec12117d634d8b604a8370d95ffcfc65d99a6d8e7969c744d6", null ],
      [ "TIM1_CounterMode_CenterAligned2", "group___t_i_m1___counter___mode.html#gga852e3b4b649c3d2ec12117d634d8b604ae3c0f223ed3c53868b4be3c25828a6a8", null ],
      [ "TIM1_CounterMode_CenterAligned3", "group___t_i_m1___counter___mode.html#gga852e3b4b649c3d2ec12117d634d8b604a2229d65fe1b70358389196675b7ee571", null ]
    ] ],
    [ "TIM1_DMABase_TypeDef", "group___t_i_m1___d_m_a___base___address.html#gacfc940c8a262c7b6c8b1219e0fcf549b", [
      [ "TIM1_DMABase_CR1", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba4eb34f61520e905f95fc0755a4b7b80b", null ],
      [ "TIM1_DMABase_CR2", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba895bbb3716789df6699f16ae3250260a", null ],
      [ "TIM1_DMABase_SMCR", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549bab5001ee526555327b611308081a49c25", null ],
      [ "TIM1_DMABase_ETR", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba4c06717d094fcfac72006b0dcd977ec3", null ],
      [ "TIM1_DMABase_DER", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba013986bac53c612f03a6a7f28b2f0168", null ],
      [ "TIM1_DMABase_IER", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba0e03c67f3d5365cdc7c86453da297b53", null ],
      [ "TIM1_DMABase_SR1", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549bae8125d9fef9598d5a898994de398bf6c", null ],
      [ "TIM1_DMABase_SR2", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba4d51bd30f788306b9a576c8dafc7dc4b", null ],
      [ "TIM1_DMABase_EGR", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba53b15e5df161633f398244292b684d3b", null ],
      [ "TIM1_DMABase_CCMR1", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba45e38050394b78bffb0b8726de2d0610", null ],
      [ "TIM1_DMABase_CCMR2", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba1dc43dd888c8500e77f3bc5b72f4ab33", null ],
      [ "TIM1_DMABase_CCMR3", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549bab61e1438332624e4722ebb4a55cc72f5", null ],
      [ "TIM1_DMABase_CCMR4", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba2d541244e3d309b8c6d3725fec78ce46", null ],
      [ "TIM1_DMABase_CCER1", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba1145220b73cf56521adc589968d709ed", null ],
      [ "TIM1_DMABase_CCER2", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549bac74cf3f880efb23af4b8ab9773b8b62d", null ],
      [ "TIM1_DMABase_CNTH", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549bad357984e11e625acae0ff3579f963de5", null ],
      [ "TIM1_DMABase_CNTL", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba7f59bc53b78ac6332966ced6611e8381", null ],
      [ "TIM1_DMABase_PSCH", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba48ebf543d7b0602e39fef3eb4b2bdc0a", null ],
      [ "TIM1_DMABase_PSCL", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba26bd7aca53125415b0b9575ba3a19f06", null ],
      [ "TIM1_DMABase_ARRH", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549bae47d3bc15e70e4f132ed2dad5d4897ca", null ],
      [ "TIM1_DMABase_ARRL", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba8ad5562c751cedd0deb849bd1ec712fb", null ],
      [ "TIM1_DMABase_RCR", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba71ebfc50dd47957af0b19a3ccef2bf42", null ],
      [ "TIM1_DMABase_CCR1H", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549baa14a0f5a3164495aab74c56cb1eb1c4a", null ],
      [ "TIM1_DMABase_CCR1L", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549baa6ae1ea15f21a07fdde9026a82e6223b", null ],
      [ "TIM1_DMABase_CCR2H", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba855ecc0d39eddf34552ae17cacdfffae", null ],
      [ "TIM1_DMABase_CCR2L", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba118b5d38b6b3169bd0d2806952717498", null ],
      [ "TIM1_DMABase_CCR3H", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549baa7cc2f7c45111dd755c6451dfcafc53c", null ],
      [ "TIM1_DMABase_CCR3L", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba7261b0de778557945072e9c6a923a158", null ],
      [ "TIM1_DMABase_CCR4H", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba50d75efe1101703e999ba32a598cd71e", null ],
      [ "TIM1_DMABase_CCR4L", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba14cc2aa97984016c6b55ab9a561b7561", null ],
      [ "TIM1_DMABase_BKR", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549bad5b0ad524d3a264676f301a9b5b3811f", null ],
      [ "TIM1_DMABase_DTR", "group___t_i_m1___d_m_a___base___address.html#ggacfc940c8a262c7b6c8b1219e0fcf549ba0219be2ca6e781dce808ab00e254a75a", null ]
    ] ],
    [ "TIM1_DMABurstLength_TypeDef", "group___t_i_m1___d_m_a___burst___length.html#ga729f239355218cc04d2b1eb30e2d4592", [
      [ "TIM1_DMABurstLength_1Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a698ff25f24e846cd87235d4f4e2ce4c1", null ],
      [ "TIM1_DMABurstLength_2Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a58124e40efd284bbbef9a9b81c8b7d72", null ],
      [ "TIM1_DMABurstLength_3Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a2963af7911b7c6854d3005a3f017bf67", null ],
      [ "TIM1_DMABurstLength_4Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a036da403c40cfdc69e0f1db34d35d72e", null ],
      [ "TIM1_DMABurstLength_5Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a3977883ead045d4a32df80ad4a215c1a", null ],
      [ "TIM1_DMABurstLength_6Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a5624ad5f14abf2022cb560b5ceaa761e", null ],
      [ "TIM1_DMABurstLength_7Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592ad46d63104611e9150dd9a48d7f9a116b", null ],
      [ "TIM1_DMABurstLength_8Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592af8cc052e8f5e980430bfe83c479911ce", null ],
      [ "TIM1_DMABurstLength_9Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592af3abebcb92b247d568d948594437b568", null ],
      [ "TIM1_DMABurstLength_10Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a20e9fb2c2c4bfbb22335cc4ff3fa4244", null ],
      [ "TIM1_DMABurstLength_11Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a150eff12307c20d58502ceb888627049", null ],
      [ "TIM1_DMABurstLength_12Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a4406d700b842113f4df411d3712f5e1a", null ],
      [ "TIM1_DMABurstLength_13Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a0ca250d53aa2a59c8cf0c340f7963c9e", null ],
      [ "TIM1_DMABurstLength_14Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a065760bba26c0662330606b6ec71530d", null ],
      [ "TIM1_DMABurstLength_15Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a34567cb56fa54e01a4308974ba5ac9ba", null ],
      [ "TIM1_DMABurstLength_16Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a6a3aa9a50ab4de98f3f8e004479b2e49", null ],
      [ "TIM1_DMABurstLength_17Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a7b871c039db8d964412f9066baf9a21a", null ],
      [ "TIM1_DMABurstLength_18Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592afc3d35b78ebe9e3e0622397887d8d458", null ],
      [ "TIM1_DMABurstLength_19Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592af7d1af2175cdf0608000711f01c1fd14", null ],
      [ "TIM1_DMABurstLength_20Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a4c486c2d900df0480699c96a36ff93e2", null ],
      [ "TIM1_DMABurstLength_21Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a1e644f26d6b6e3eb58c66b73afc7b7d9", null ],
      [ "TIM1_DMABurstLength_22Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592aa290bb49705db99ad4d395020f265dbd", null ],
      [ "TIM1_DMABurstLength_23Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a05824b337b1e8b23fc6349cbf069db03", null ],
      [ "TIM1_DMABurstLength_24Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a6a2f7d75f2aec304c3e53f8304f77491", null ],
      [ "TIM1_DMABurstLength_25Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a91d77a8d7284a0a4a105dacc772f8a76", null ],
      [ "TIM1_DMABurstLength_26Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a2a4ebe1e04272cddf7bf75d6035284cc", null ],
      [ "TIM1_DMABurstLength_27Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592ad87a6b7423556b437bc6b6e8f5c541a9", null ],
      [ "TIM1_DMABurstLength_28Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a3e45f7a019daf67daee2ae79117129c7", null ],
      [ "TIM1_DMABurstLength_29Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592adac3d0f6ee1859c23bf849de00be0b40", null ],
      [ "TIM1_DMABurstLength_30Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592afed22ba582e929d4dc289076ff2b5105", null ],
      [ "TIM1_DMABurstLength_31Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a0b0459ce79c5d479b7a137c63eb2a9a7", null ],
      [ "TIM1_DMABurstLength_32Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a5b0c7a7ed9736b28ec93bb78a621bdae", null ]
    ] ],
    [ "TIM1_DMASource_TypeDef", "group___t_i_m1___d_m_a___source___requests.html#ga6f259134457d4c37afb9dbf7325e5779", [
      [ "TIM1_DMASource_Update", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779abc4d2413204799460b68d88902a6668a", null ],
      [ "TIM1_DMASource_CC1", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779ae0b10dccb07b4a99eb6dd564362bf61b", null ],
      [ "TIM1_DMASource_CC2", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779a7d0cfbb957e9e64dacd3a5813291a6ed", null ],
      [ "TIM1_DMASource_CC3", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779a0deb0b7fed8f39b96fd0730f73f561a8", null ],
      [ "TIM1_DMASource_CC4", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779a80577529855a1dd2241be8bc0a0f1295", null ],
      [ "TIM1_DMASource_COM", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779a00bef918506e9d49945d262b12b2be34", null ]
    ] ],
    [ "TIM1_EncoderMode_TypeDef", "group___t_i_m1___encoder___mode.html#gab50f50dd43d31d9bc3092aa15f2aa9cd", [
      [ "TIM1_EncoderMode_TI1", "group___t_i_m1___encoder___mode.html#ggab50f50dd43d31d9bc3092aa15f2aa9cdadca944e4444c93bc97fdf3b93da26e91", null ],
      [ "TIM1_EncoderMode_TI2", "group___t_i_m1___encoder___mode.html#ggab50f50dd43d31d9bc3092aa15f2aa9cda5a2e1298490fbc372fcc1c7ecee2157d", null ],
      [ "TIM1_EncoderMode_TI12", "group___t_i_m1___encoder___mode.html#ggab50f50dd43d31d9bc3092aa15f2aa9cda0b2b3e755111909b89d2cdbecac101d8", null ]
    ] ],
    [ "TIM1_EventSource_TypeDef", "group___t_i_m1___event___source.html#gac9dc2ffdeae09b5dbc43a9dbb4968acc", [
      [ "TIM1_EventSource_Update", "group___t_i_m1___event___source.html#ggac9dc2ffdeae09b5dbc43a9dbb4968acca7540aa8791efc473b0d744f0a13c62ae", null ],
      [ "TIM1_EventSource_CC1", "group___t_i_m1___event___source.html#ggac9dc2ffdeae09b5dbc43a9dbb4968acca980ce91c2b2ca495602321f4e89913e2", null ],
      [ "TIM1_EventSource_CC2", "group___t_i_m1___event___source.html#ggac9dc2ffdeae09b5dbc43a9dbb4968acca2b93f16c2ca74a9f56516419efc9cc05", null ],
      [ "TIM1_EventSource_CC3", "group___t_i_m1___event___source.html#ggac9dc2ffdeae09b5dbc43a9dbb4968accaf4ce5328e53277964de334b974baf35a", null ],
      [ "TIM1_EventSource_CC4", "group___t_i_m1___event___source.html#ggac9dc2ffdeae09b5dbc43a9dbb4968acca42e28eb6758f94a3be12dfeed62d8f25", null ],
      [ "TIM1_EventSource_COM", "group___t_i_m1___event___source.html#ggac9dc2ffdeae09b5dbc43a9dbb4968acca1ee7a0a093f6335d4ba32f1ba4144063", null ],
      [ "TIM1_EventSource_Trigger", "group___t_i_m1___event___source.html#ggac9dc2ffdeae09b5dbc43a9dbb4968acca7ff9d09abbc4b5828d73f1d08284d60e", null ],
      [ "TIM1_EventSource_Break", "group___t_i_m1___event___source.html#ggac9dc2ffdeae09b5dbc43a9dbb4968acca02e16cea161c63b0ae861e546f93057a", null ]
    ] ],
    [ "TIM1_ExtTRGPolarity_TypeDef", "group___t_i_m1___external___trigger___polarity.html#gac537b5e4ea05418288bce8e6d2b39480", [
      [ "TIM1_ExtTRGPolarity_Inverted", "group___t_i_m1___external___trigger___polarity.html#ggac537b5e4ea05418288bce8e6d2b39480abf4f8b906d2be59a0df0b6a6e82b3346", null ],
      [ "TIM1_ExtTRGPolarity_NonInverted", "group___t_i_m1___external___trigger___polarity.html#ggac537b5e4ea05418288bce8e6d2b39480a68f2e4bfc3a4e025e57074114a418965", null ]
    ] ],
    [ "TIM1_ExtTRGPSC_TypeDef", "group___t_i_m1___external___trigger___prescaler.html#gaba3c0cdf5e8b3108e84e50b99bbdfff8", [
      [ "TIM1_ExtTRGPSC_OFF", "group___t_i_m1___external___trigger___prescaler.html#ggaba3c0cdf5e8b3108e84e50b99bbdfff8a57ad3a17fec47e5c5826477db97f82e0", null ],
      [ "TIM1_ExtTRGPSC_DIV2", "group___t_i_m1___external___trigger___prescaler.html#ggaba3c0cdf5e8b3108e84e50b99bbdfff8af852c0eca9128fb1d0431f4d9771e64f", null ],
      [ "TIM1_ExtTRGPSC_DIV4", "group___t_i_m1___external___trigger___prescaler.html#ggaba3c0cdf5e8b3108e84e50b99bbdfff8aa9a8b94daed84d0382708011bb066d3b", null ],
      [ "TIM1_ExtTRGPSC_DIV8", "group___t_i_m1___external___trigger___prescaler.html#ggaba3c0cdf5e8b3108e84e50b99bbdfff8a6f80666bfcf36962bb932918afaff6da", null ]
    ] ],
    [ "TIM1_FLAG_TypeDef", "group___t_i_m1___flags.html#gab0521234676bf42634259bb54d312311", [
      [ "TIM1_FLAG_Update", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311aa318a52323f57f8e3c4e4da1b93262cd", null ],
      [ "TIM1_FLAG_CC1", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311a14f1453ebc4da307577bd284cad4c029", null ],
      [ "TIM1_FLAG_CC2", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311af680a90eb9601f93a4ad513b3c109117", null ],
      [ "TIM1_FLAG_CC3", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311abfa412f1326ed3c39b4c371849b9ea63", null ],
      [ "TIM1_FLAG_CC4", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311ae04131d32ba5a35b2922c21de46e1f1b", null ],
      [ "TIM1_FLAG_COM", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311a29ec23862da49ff3f3c2f60826299edc", null ],
      [ "TIM1_FLAG_Trigger", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311a7c17d830d971d5dd46c6f23f9d988d2c", null ],
      [ "TIM1_FLAG_Break", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311a120caf564e0be465c74b7e74a9dc167b", null ],
      [ "TIM1_FLAG_CC1OF", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311a74563db451ff5da4d6e42a34b509417d", null ],
      [ "TIM1_FLAG_CC2OF", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311a1de42745b1265e59e5fc05af5c81a5b8", null ],
      [ "TIM1_FLAG_CC3OF", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311a53010c525cb7286bcad77c1bfa96d718", null ],
      [ "TIM1_FLAG_CC4OF", "group___t_i_m1___flags.html#ggab0521234676bf42634259bb54d312311a91ab4623890884b339e7bf881fa1356a", null ]
    ] ],
    [ "TIM1_ForcedAction_TypeDef", "group___t_i_m1___forced___action.html#gadaca3d02da7d5c2d89f90a4f6837d445", [
      [ "TIM1_ForcedAction_Active", "group___t_i_m1___forced___action.html#ggadaca3d02da7d5c2d89f90a4f6837d445aebbbbc4b35e072048ff64ae455dde02a", null ],
      [ "TIM1_ForcedAction_Inactive", "group___t_i_m1___forced___action.html#ggadaca3d02da7d5c2d89f90a4f6837d445a9a0614018e8f161e8715d870339e21a2", null ]
    ] ],
    [ "TIM1_ICPolarity_TypeDef", "group___t_i_m1___input___capture___polarity.html#gafede307892c51cea6efd22aa772c8d11", [
      [ "TIM1_ICPolarity_Rising", "group___t_i_m1___input___capture___polarity.html#ggafede307892c51cea6efd22aa772c8d11a48ef043113f2a09d5f42b9120d2a1a97", null ],
      [ "TIM1_ICPolarity_Falling", "group___t_i_m1___input___capture___polarity.html#ggafede307892c51cea6efd22aa772c8d11a879e7672223093535d1cd4655584defd", null ]
    ] ],
    [ "TIM1_ICPSC_TypeDef", "group___t_i_m1___input___capture___prescaler.html#ga61aeaccf7362311cce28473c16255372", [
      [ "TIM1_ICPSC_DIV1", "group___t_i_m1___input___capture___prescaler.html#gga61aeaccf7362311cce28473c16255372ae7ba7c283927f0f056655439e93a04b9", null ],
      [ "TIM1_ICPSC_DIV2", "group___t_i_m1___input___capture___prescaler.html#gga61aeaccf7362311cce28473c16255372ad34d0b6c26049a0a74b1b3c6826a2bce", null ],
      [ "TIM1_ICPSC_DIV4", "group___t_i_m1___input___capture___prescaler.html#gga61aeaccf7362311cce28473c16255372ac0a5e90154ad4cc412c23daf6dbe8259", null ],
      [ "TIM1_ICPSC_DIV8", "group___t_i_m1___input___capture___prescaler.html#gga61aeaccf7362311cce28473c16255372a0504127b4da74356651c1dfc8184a5ae", null ]
    ] ],
    [ "TIM1_ICSelection_TypeDef", "group___t_i_m1___input___capture___selection.html#ga9b7e5d781b68ab2e7b8a9f818a9fdf51", [
      [ "TIM1_ICSelection_DirectTI", "group___t_i_m1___input___capture___selection.html#gga9b7e5d781b68ab2e7b8a9f818a9fdf51a34c8a0b7751e7fa4c681896f5c5a0ce5", null ],
      [ "TIM1_ICSelection_IndirectTI", "group___t_i_m1___input___capture___selection.html#gga9b7e5d781b68ab2e7b8a9f818a9fdf51a579c85a70ed659cc58941d45cd812011", null ],
      [ "TIM1_ICSelection_TRGI", "group___t_i_m1___input___capture___selection.html#gga9b7e5d781b68ab2e7b8a9f818a9fdf51a900e383d534adddf98cddf2361625886", null ]
    ] ],
    [ "TIM1_IT_TypeDef", "group___t_i_m1___interrupts.html#gada5cd65bda15a5d2400e3ff05075b85f", [
      [ "TIM1_IT_Update", "group___t_i_m1___interrupts.html#ggada5cd65bda15a5d2400e3ff05075b85fa191e0e4d001a9e22edb5289aaf5fa152", null ],
      [ "TIM1_IT_CC1", "group___t_i_m1___interrupts.html#ggada5cd65bda15a5d2400e3ff05075b85fab193de5413418870a9f2fd21d1909b43", null ],
      [ "TIM1_IT_CC2", "group___t_i_m1___interrupts.html#ggada5cd65bda15a5d2400e3ff05075b85fa99e325e3e293b4609c9f65a3077398e4", null ],
      [ "TIM1_IT_CC3", "group___t_i_m1___interrupts.html#ggada5cd65bda15a5d2400e3ff05075b85fa3140e6f1e4659891b3e755b079fbee11", null ],
      [ "TIM1_IT_CC4", "group___t_i_m1___interrupts.html#ggada5cd65bda15a5d2400e3ff05075b85fa16f8fa020841a688159a80501bf64eea", null ],
      [ "TIM1_IT_COM", "group___t_i_m1___interrupts.html#ggada5cd65bda15a5d2400e3ff05075b85fab909fc5f51ea2a8e8a75d5cf6ddb73ae", null ],
      [ "TIM1_IT_Trigger", "group___t_i_m1___interrupts.html#ggada5cd65bda15a5d2400e3ff05075b85fa6a1f6745d27b8b6046940a37e9ee4f87", null ],
      [ "TIM1_IT_Break", "group___t_i_m1___interrupts.html#ggada5cd65bda15a5d2400e3ff05075b85fadcb2d73caa91c2c6c4d9eac839fed804", null ]
    ] ],
    [ "TIM1_LockLevel_TypeDef", "group___t_i_m1___lock___level.html#ga325a906d797b656263bf549bb6c6e20d", [
      [ "TIM1_LockLevel_Off", "group___t_i_m1___lock___level.html#gga325a906d797b656263bf549bb6c6e20da5ff35cdb47d2a7416b080f33da910ee4", null ],
      [ "TIM1_LockLevel_1", "group___t_i_m1___lock___level.html#gga325a906d797b656263bf549bb6c6e20da8665b4a2a7c1f3878bb1a6c577435644", null ],
      [ "TIM1_LockLevel_2", "group___t_i_m1___lock___level.html#gga325a906d797b656263bf549bb6c6e20da768d695bc8a2a1305810c267d4133c1c", null ],
      [ "TIM1_LockLevel_3", "group___t_i_m1___lock___level.html#gga325a906d797b656263bf549bb6c6e20da503d517b9c76846d433fcb250cb65567", null ]
    ] ],
    [ "TIM1_OCIdleState_TypeDef", "group___t_i_m1___output___compare___idle__state.html#ga42b0c75249388c7c6d8327b906f40153", [
      [ "TIM1_OCIdleState_Set", "group___t_i_m1___output___compare___idle__state.html#gga42b0c75249388c7c6d8327b906f40153a4e10abeb515db6cd556c23877070a6ea", null ],
      [ "TIM1_OCIdleState_Reset", "group___t_i_m1___output___compare___idle__state.html#gga42b0c75249388c7c6d8327b906f40153a1fdf2892da7fbd283cf2a20487c383f3", null ]
    ] ],
    [ "TIM1_OCMode_TypeDef", "group___t_i_m1___output___compare___mode.html#ga89914199b7c4932f1661ce2fdb19d46e", [
      [ "TIM1_OCMode_Timing", "group___t_i_m1___output___compare___mode.html#gga89914199b7c4932f1661ce2fdb19d46ea0a138e8949582788c0da8ffc1d45ff35", null ],
      [ "TIM1_OCMode_Active", "group___t_i_m1___output___compare___mode.html#gga89914199b7c4932f1661ce2fdb19d46ea6ed9ebba297cf4b5d39d90aac42d52d3", null ],
      [ "TIM1_OCMode_Inactive", "group___t_i_m1___output___compare___mode.html#gga89914199b7c4932f1661ce2fdb19d46eae04281aece4c1618a7154689c8cf38b6", null ],
      [ "TIM1_OCMode_Toggle", "group___t_i_m1___output___compare___mode.html#gga89914199b7c4932f1661ce2fdb19d46ead790ccdfbbf64083d55624bbb5fc646e", null ],
      [ "TIM1_OCMode_PWM1", "group___t_i_m1___output___compare___mode.html#gga89914199b7c4932f1661ce2fdb19d46eadcd0b762bec36e405f26faa58b7543e9", null ],
      [ "TIM1_OCMode_PWM2", "group___t_i_m1___output___compare___mode.html#gga89914199b7c4932f1661ce2fdb19d46ea8fab9cd422840aaffd084a5214d83f83", null ]
    ] ],
    [ "TIM1_OCNIdleState_TypeDef", "group___t_i_m1___output___compare___n___idle__state.html#gaec8425fddfacebda306ed895bb9ca08c", [
      [ "TIM1_OCNIdleState_Set", "group___t_i_m1___output___compare___n___idle__state.html#ggaec8425fddfacebda306ed895bb9ca08ca58a4fba72be48090d33ad5e4b419281a", null ],
      [ "TIM1_OCNIdleState_Reset", "group___t_i_m1___output___compare___n___idle__state.html#ggaec8425fddfacebda306ed895bb9ca08cac689e30babb9765a0e25644f0a345d14", null ]
    ] ],
    [ "TIM1_OCNPolarity_TypeDef", "group___t_i_m1___output___compare___n___polarity.html#ga149e684e32ea39c07f8ea2efcf2e255c", [
      [ "TIM1_OCNPolarity_High", "group___t_i_m1___output___compare___n___polarity.html#gga149e684e32ea39c07f8ea2efcf2e255caa5c6a092140a49928593d7e465a9bc1d", null ],
      [ "TIM1_OCNPolarity_Low", "group___t_i_m1___output___compare___n___polarity.html#gga149e684e32ea39c07f8ea2efcf2e255ca95b8f662bbb041ff377a6bfdc34c77f4", null ]
    ] ],
    [ "TIM1_OCPolarity_TypeDef", "group___t_i_m1___output___compare___polarity.html#ga70ac0765bc0adce7849f85adfcc3f69a", [
      [ "TIM1_OCPolarity_High", "group___t_i_m1___output___compare___polarity.html#gga70ac0765bc0adce7849f85adfcc3f69aada60e7daa026a23066c4b05469236515", null ],
      [ "TIM1_OCPolarity_Low", "group___t_i_m1___output___compare___polarity.html#gga70ac0765bc0adce7849f85adfcc3f69aa96e73fa6a0e26f506917dcc73cad59c7", null ]
    ] ],
    [ "TIM1_OCReferenceClear_TypeDef", "group___t_i_m1___output___compare___reference___clear.html#ga0cd540a9dc47382a8e1566f1c9ea245b", [
      [ "TIM1_OCReferenceClear_ETRF", "group___t_i_m1___output___compare___reference___clear.html#gga0cd540a9dc47382a8e1566f1c9ea245bacdeaaf94d03e715572b352df4651b85c", null ],
      [ "TIM1_OCReferenceClear_OCREFCLR", "group___t_i_m1___output___compare___reference___clear.html#gga0cd540a9dc47382a8e1566f1c9ea245bafab35c66be95dae4fcd059849c827fe7", null ]
    ] ],
    [ "TIM1_OPMode_TypeDef", "group___t_i_m1___one___pulse___mode.html#ga2a9e244e5184a6d424e1447f157a5164", [
      [ "TIM1_OPMode_Single", "group___t_i_m1___one___pulse___mode.html#gga2a9e244e5184a6d424e1447f157a5164a14725b16ac249846edcf25641fbde6b0", null ],
      [ "TIM1_OPMode_Repetitive", "group___t_i_m1___one___pulse___mode.html#gga2a9e244e5184a6d424e1447f157a5164af2013215da4878869bb109c4c4341ba6", null ]
    ] ],
    [ "TIM1_OSSIState_TypeDef", "group___t_i_m1___o_s_s_i___state.html#gafbafb193176cef051518fe5bddfbcef9", [
      [ "TIM1_OSSIState_Enable", "group___t_i_m1___o_s_s_i___state.html#ggafbafb193176cef051518fe5bddfbcef9a581d23a511e6111e8a79d3514d5f801e", null ],
      [ "TIM1_OSSIState_Disable", "group___t_i_m1___o_s_s_i___state.html#ggafbafb193176cef051518fe5bddfbcef9af11a0bdc52bf03f296b5635e706b470e", null ]
    ] ],
    [ "TIM1_OutputNState_TypeDef", "group___t_i_m1___output___n___state.html#gafb784321a3014bf12eff6257941a136d", [
      [ "TIM1_OutputNState_Disable", "group___t_i_m1___output___n___state.html#ggafb784321a3014bf12eff6257941a136da7bff928111a8f40ca28fa80552789775", null ],
      [ "TIM1_OutputNState_Enable", "group___t_i_m1___output___n___state.html#ggafb784321a3014bf12eff6257941a136dae5d99f378e08a94b4c8b595bcbfa2a85", null ]
    ] ],
    [ "TIM1_OutputState_TypeDef", "group___t_i_m1___output___state.html#gaa5629200df0a18b5789b5c125c1fe7c6", [
      [ "TIM1_OutputState_Disable", "group___t_i_m1___output___state.html#ggaa5629200df0a18b5789b5c125c1fe7c6ad922f3412652b3890176911a9fd5ca2c", null ],
      [ "TIM1_OutputState_Enable", "group___t_i_m1___output___state.html#ggaa5629200df0a18b5789b5c125c1fe7c6aa073d2c75d76cbe50642df3205fdfd27", null ]
    ] ],
    [ "TIM1_PSCReloadMode_TypeDef", "group___t_i_m1___prescaler___reload___mode.html#gae34dc5cd8e9b1a1fbac3ec1273678149", [
      [ "TIM1_PSCReloadMode_Update", "group___t_i_m1___prescaler___reload___mode.html#ggae34dc5cd8e9b1a1fbac3ec1273678149ae8843dfcd607b7597a1978a5d7618bfd", null ],
      [ "TIM1_PSCReloadMode_Immediate", "group___t_i_m1___prescaler___reload___mode.html#ggae34dc5cd8e9b1a1fbac3ec1273678149a8b3ccce0d8f0419aff47cd680bad02be", null ]
    ] ],
    [ "TIM1_SlaveMode_TypeDef", "group___t_i_m1___slave___mode.html#ga6d4b46e1bc7cda38dba3718c3f00b191", [
      [ "TIM1_SlaveMode_Reset", "group___t_i_m1___slave___mode.html#gga6d4b46e1bc7cda38dba3718c3f00b191af7cb344def5d0bbbad0abaf09620887e", null ],
      [ "TIM1_SlaveMode_Gated", "group___t_i_m1___slave___mode.html#gga6d4b46e1bc7cda38dba3718c3f00b191a2f7955bc75c2b9b11c50e73d9ccd83c3", null ],
      [ "TIM1_SlaveMode_Trigger", "group___t_i_m1___slave___mode.html#gga6d4b46e1bc7cda38dba3718c3f00b191a2242b4a2869b1f83c55ef58458f5db23", null ],
      [ "TIM1_SlaveMode_External1", "group___t_i_m1___slave___mode.html#gga6d4b46e1bc7cda38dba3718c3f00b191ab2d65d594e3d06f370195af74d650500", null ]
    ] ],
    [ "TIM1_TIxExternalCLK1Source_TypeDef", "group___t_i_m1___t_i___external___clock___source.html#ga04a331caee57f3911e978aaca103d637", [
      [ "TIM1_TIxExternalCLK1Source_TI1ED", "group___t_i_m1___t_i___external___clock___source.html#gga04a331caee57f3911e978aaca103d637a01d09a3d2db5eeb44639537747ce571b", null ],
      [ "TIM1_TIxExternalCLK1Source_TI1", "group___t_i_m1___t_i___external___clock___source.html#gga04a331caee57f3911e978aaca103d637a58244e6a1c8bd693f9b3d46b2de500f7", null ],
      [ "TIM1_TIxExternalCLK1Source_TI2", "group___t_i_m1___t_i___external___clock___source.html#gga04a331caee57f3911e978aaca103d637a33233ec36e1cad52a73c3926dc5a66f4", null ]
    ] ],
    [ "TIM1_TRGOSource_TypeDef", "group___t_i_m1___trigger___output___source.html#ga94246b9aa921de8ce75aff0efcf9b79c", [
      [ "TIM1_TRGOSource_Reset", "group___t_i_m1___trigger___output___source.html#gga94246b9aa921de8ce75aff0efcf9b79ca7d50702f2d847f9216694692bc6d959e", null ],
      [ "TIM1_TRGOSource_Enable", "group___t_i_m1___trigger___output___source.html#gga94246b9aa921de8ce75aff0efcf9b79cabc7331ed858ba2cd1c1b9a0f18f4c768", null ],
      [ "TIM1_TRGOSource_Update", "group___t_i_m1___trigger___output___source.html#gga94246b9aa921de8ce75aff0efcf9b79ca61d67d90579f668ffbf78d001e366691", null ],
      [ "TIM1_TRGOSource_OC1", "group___t_i_m1___trigger___output___source.html#gga94246b9aa921de8ce75aff0efcf9b79ca663cb153c1657d0d463d0ceb7e57f628", null ],
      [ "TIM1_TRGOSource_OC1REF", "group___t_i_m1___trigger___output___source.html#gga94246b9aa921de8ce75aff0efcf9b79cacbc1fa2b7ec14f04901be587a1bcefdb", null ],
      [ "TIM1_TRGOSource_OC2REF", "group___t_i_m1___trigger___output___source.html#gga94246b9aa921de8ce75aff0efcf9b79ca0e23bb1bed954755639ba2d756d9f670", null ],
      [ "TIM1_TRGOSource_OC3REF", "group___t_i_m1___trigger___output___source.html#gga94246b9aa921de8ce75aff0efcf9b79caf21e967d83eb5157714d38e4baaf2ce5", null ],
      [ "TIM1_TRGOSource_OC4REF", "group___t_i_m1___trigger___output___source.html#gga94246b9aa921de8ce75aff0efcf9b79cab5c0fdd04466d6fc5593c030405f8dae", null ]
    ] ],
    [ "TIM1_TRGSelection_TypeDef", "group___t_i_m1___internal___trigger___selection.html#gae9870910aaea49794b2e254d4a3054e1", [
      [ "TIM1_TRGSelection_TIM4", "group___t_i_m1___internal___trigger___selection.html#ggae9870910aaea49794b2e254d4a3054e1a41d8ee01049953cf586a251a12d1bac4", null ],
      [ "TIM1_TRGSelection_TIM5", "group___t_i_m1___internal___trigger___selection.html#ggae9870910aaea49794b2e254d4a3054e1a23d54fca5de6cf76a3affcf0588a8bf4", null ],
      [ "TIM1_TRGSelection_TIM3", "group___t_i_m1___internal___trigger___selection.html#ggae9870910aaea49794b2e254d4a3054e1a01f31e74f70c7b3e3581318bbbec7ca0", null ],
      [ "TIM1_TRGSelection_TIM2", "group___t_i_m1___internal___trigger___selection.html#ggae9870910aaea49794b2e254d4a3054e1a88f71a690ea85d96f365b721c0d6c116", null ],
      [ "TIM1_TRGSelection_TI1F_ED", "group___t_i_m1___internal___trigger___selection.html#ggae9870910aaea49794b2e254d4a3054e1afc95f7c9bdf1d1791e922c62aa98ec16", null ],
      [ "TIM1_TRGSelection_TI1FP1", "group___t_i_m1___internal___trigger___selection.html#ggae9870910aaea49794b2e254d4a3054e1a09619ac101a5631fb87dd046e62fe814", null ],
      [ "TIM1_TRGSelection_TI2FP2", "group___t_i_m1___internal___trigger___selection.html#ggae9870910aaea49794b2e254d4a3054e1ad40290696dbc15962dd44c4f20d45372", null ],
      [ "TIM1_TRGSelection_ETRF", "group___t_i_m1___internal___trigger___selection.html#ggae9870910aaea49794b2e254d4a3054e1a71ebe1f02c1932a010b68e00d13d6c14", null ]
    ] ],
    [ "TIM1_UpdateSource_TypeDef", "group___t_i_m1___update___source.html#gaceb3d7e6f8aac3e9de7faffb4241b2d1", [
      [ "TIM1_UpdateSource_Global", "group___t_i_m1___update___source.html#ggaceb3d7e6f8aac3e9de7faffb4241b2d1ad74f385ee7f42d5e095c28a48997b0f5", null ],
      [ "TIM1_UpdateSource_Regular", "group___t_i_m1___update___source.html#ggaceb3d7e6f8aac3e9de7faffb4241b2d1abfec4592d479baa3319404b626c37f3a", null ]
    ] ],
    [ "TIM1_ARRPreloadConfig", "group___t_i_m1.html#gadae07802b1bccb4af3c6af1b9a7d2766", null ],
    [ "TIM1_BDTRConfig", "group___t_i_m1.html#gaec51177abeb40c451ae73c8c2cff32b1", null ],
    [ "TIM1_CCPreloadControl", "group___t_i_m1.html#ga72ad5baa0fe4a3cca4ac10a73bc20fde", null ],
    [ "TIM1_CCxCmd", "group___t_i_m1.html#ga877ae4b3676d62fe05569b61e14e86f5", null ],
    [ "TIM1_CCxNCmd", "group___t_i_m1.html#gaaee884ec153eda3e18c37bed5efd40d3", null ],
    [ "TIM1_ClearFlag", "group___t_i_m1.html#ga64702c99c63b70e7c12e60359d0df605", null ],
    [ "TIM1_ClearITPendingBit", "group___t_i_m1.html#ga41bc99eb90a3bc83eb4895ac9b440c12", null ],
    [ "TIM1_ClearOC1Ref", "group___t_i_m1.html#ga74d2edf4b6bba988b9845af407be6dee", null ],
    [ "TIM1_ClearOC2Ref", "group___t_i_m1.html#ga63d4ec737c1755a927ffb048db7f3658", null ],
    [ "TIM1_ClearOC3Ref", "group___t_i_m1.html#ga206316e8266431794e821cc08906ffa8", null ],
    [ "TIM1_ClearOC4Ref", "group___t_i_m1.html#gadf024c34d64c3ed42c1770ad7823edea", null ],
    [ "TIM1_Cmd", "group___t_i_m1.html#ga348844189cf479ab51e37503aa86a487", null ],
    [ "TIM1_CounterModeConfig", "group___t_i_m1.html#gaaed02dccebdb8491c2d30999c722e42a", null ],
    [ "TIM1_CtrlPWMOutputs", "group___t_i_m1.html#ga028bf79b558219ddb6cf8d15de24e696", null ],
    [ "TIM1_DeInit", "group___t_i_m1.html#gae24f0411974dfdb002feaf46514032db", null ],
    [ "TIM1_DMACmd", "group___t_i_m1.html#ga47f5a1583b1597e71e2617a4d8b81eb1", null ],
    [ "TIM1_DMAConfig", "group___t_i_m1.html#gaf0e0220034f59f93a868b9cb315288e8", null ],
    [ "TIM1_EncoderInterfaceConfig", "group___t_i_m1.html#ga9008ae37002d522b732c134075f4ab5a", null ],
    [ "TIM1_ETRClockMode1Config", "group___t_i_m1.html#ga059e58a1ed6072e8d2908b103b8d4246", null ],
    [ "TIM1_ETRClockMode2Config", "group___t_i_m1.html#ga76950a1cab26697c11039fad03e5227c", null ],
    [ "TIM1_ETRConfig", "group___t_i_m1.html#ga0b416dc27ca3ed92cbfc997c4dc55e50", null ],
    [ "TIM1_ForcedOC1Config", "group___t_i_m1.html#ga2111a1fea923d2fabfaada3d418ab348", null ],
    [ "TIM1_ForcedOC2Config", "group___t_i_m1.html#ga1278646f4e147d62cd9a317e45af6e55", null ],
    [ "TIM1_ForcedOC3Config", "group___t_i_m1.html#gac9fe9586f0a09e1656d31f3a9d8b9892", null ],
    [ "TIM1_GenerateEvent", "group___t_i_m1.html#ga57eaec0db4539fbd44d90be7535fcbda", null ],
    [ "TIM1_GetCapture1", "group___t_i_m1.html#ga672bf2825ee50908f82ee72eaa52d7ba", null ],
    [ "TIM1_GetCapture2", "group___t_i_m1.html#ga24f1288b22a4b332f7c67df16c4ad00e", null ],
    [ "TIM1_GetCapture3", "group___t_i_m1.html#gadbce3918632dbc1ecb131161ea4239f2", null ],
    [ "TIM1_GetCapture4", "group___t_i_m1.html#gaf8a0278ad0125abbb9039c7b09ee253f", null ],
    [ "TIM1_GetCounter", "group___t_i_m1.html#gae0c95720e05d9e9d7ca5c281b2a967db", null ],
    [ "TIM1_GetFlagStatus", "group___t_i_m1.html#gab16d9c57239c823b37e8d9ed5aa3eac7", null ],
    [ "TIM1_GetITStatus", "group___t_i_m1.html#gac3e0693b962ed392606b16d2edde426c", null ],
    [ "TIM1_GetPrescaler", "group___t_i_m1.html#ga100da4bb4cb5a97b9983a750fd2e42a2", null ],
    [ "TIM1_ICInit", "group___t_i_m1.html#ga3d30a7e0a85a985fdf8f6d4cb5544bad", null ],
    [ "TIM1_InternalClockConfig", "group___t_i_m1.html#ga99eb818df93e8c2f068a6bb8b31c53b3", null ],
    [ "TIM1_ITConfig", "group___t_i_m1.html#gabc2909da6cad21c16e512deab23248a7", null ],
    [ "TIM1_OC1FastConfig", "group___t_i_m1.html#ga27fda9cc79593d8fa7841ba6531b473a", null ],
    [ "TIM1_OC1Init", "group___t_i_m1.html#ga0463357de217fc80c51175f0cfc357b7", null ],
    [ "TIM1_OC1NPolarityConfig", "group___t_i_m1.html#gab06c31794ca2eb28201d1888fd8c43fe", null ],
    [ "TIM1_OC1PolarityConfig", "group___t_i_m1.html#gaf868b3b67c68fd8bec155c2cd50b4027", null ],
    [ "TIM1_OC1PreloadConfig", "group___t_i_m1.html#ga03f23e00b483184681e405e913598fba", null ],
    [ "TIM1_OC2FastConfig", "group___t_i_m1.html#ga793f5ee63b9c0b9f88d479d9e6657581", null ],
    [ "TIM1_OC2Init", "group___t_i_m1.html#ga91428d381bf6345f7f02eeb6a245ade2", null ],
    [ "TIM1_OC2NPolarityConfig", "group___t_i_m1.html#gad14cd3a649a39a5bebf91ddfcdb687c1", null ],
    [ "TIM1_OC2PolarityConfig", "group___t_i_m1.html#ga769fbac1f9b92f482b6ce914f36492e6", null ],
    [ "TIM1_OC2PreloadConfig", "group___t_i_m1.html#gad4c98374c728223c9ad6d39ba572962f", null ],
    [ "TIM1_OC3FastConfig", "group___t_i_m1.html#ga5924fff10ec1968149fa2cf3ce1e8194", null ],
    [ "TIM1_OC3Init", "group___t_i_m1.html#ga1ad9a8173c009dbb222548a1d2372dff", null ],
    [ "TIM1_OC3NPolarityConfig", "group___t_i_m1.html#ga1996dfbc5424aff723745c3089d4b3e1", null ],
    [ "TIM1_OC3PolarityConfig", "group___t_i_m1.html#ga2ffcb7cbd8e75711919750e1aabb83ae", null ],
    [ "TIM1_OC3PreloadConfig", "group___t_i_m1.html#ga9ab2786a870a28a3c83a8d720c016be6", null ],
    [ "TIM1_OC4PreloadConfig", "group___t_i_m1.html#ga6aa3b5c8868947732224c511d806a365", null ],
    [ "TIM1_PrescalerConfig", "group___t_i_m1.html#ga11f04dc2b3168c255291b6d3a7c18e96", null ],
    [ "TIM1_PWMIConfig", "group___t_i_m1.html#gafc5fd5696318e1db2d1c00891f7e6875", null ],
    [ "TIM1_SelectCCDMA", "group___t_i_m1.html#gaf33121c5f9eef35f9893705d6f7bc289", null ],
    [ "TIM1_SelectCOM", "group___t_i_m1.html#gac853f081cb4ddf650620ac45c4728baa", null ],
    [ "TIM1_SelectHallSensor", "group___t_i_m1.html#gabae6983e659581a3258f9c036a72d3a0", null ],
    [ "TIM1_SelectInputTrigger", "group___t_i_m1.html#gaae62e36ba9cedaeb1aa72d4804e48d27", null ],
    [ "TIM1_SelectMasterSlaveMode", "group___t_i_m1.html#gab9aa917c74c62af45044af15eca35621", null ],
    [ "TIM1_SelectOCREFClear", "group___t_i_m1.html#gab30341b9eb9bd0fb521a1ed1a438a82b", null ],
    [ "TIM1_SelectOCxM", "group___t_i_m1.html#ga7de94a889cbbfcb9dac32e7063753d9a", null ],
    [ "TIM1_SelectOnePulseMode", "group___t_i_m1.html#ga18c2925a668ccb4e596eed954652e833", null ],
    [ "TIM1_SelectOutputTrigger", "group___t_i_m1.html#ga6041cde8dce27b3f57938304478b2c7f", null ],
    [ "TIM1_SelectSlaveMode", "group___t_i_m1.html#gaf278c6a5bf1ee17751b08addde4cab2d", null ],
    [ "TIM1_SetAutoreload", "group___t_i_m1.html#ga186e77c679592f4382071cece5b73086", null ],
    [ "TIM1_SetCompare1", "group___t_i_m1.html#ga6367ad263aed5468431cabc060118ad4", null ],
    [ "TIM1_SetCompare2", "group___t_i_m1.html#gacc6ac39b426911201b7af95e932541ce", null ],
    [ "TIM1_SetCompare3", "group___t_i_m1.html#ga4412c9ed01c072fad78a00a53b35dd38", null ],
    [ "TIM1_SetCompare4", "group___t_i_m1.html#ga5d87a6ae01026f597b71f70939431ab9", null ],
    [ "TIM1_SetCounter", "group___t_i_m1.html#ga6c874e8a5dbe6160d045d3d554672902", null ],
    [ "TIM1_SetIC1Prescaler", "group___t_i_m1.html#ga4ffdd3fe818c5f5c43b6c662bd4fdb70", null ],
    [ "TIM1_SetIC2Prescaler", "group___t_i_m1.html#ga7efb5262ab2a225c62bf6414f02ab635", null ],
    [ "TIM1_SetIC3Prescaler", "group___t_i_m1.html#ga0aad403881fd1c4cf9ab7cceed902e59", null ],
    [ "TIM1_SetIC4Prescaler", "group___t_i_m1.html#ga7ca43f015e245fcda56e7372e572a07f", null ],
    [ "TIM1_TimeBaseInit", "group___t_i_m1.html#ga85fca4f99f39c04c8de56b77f2ac07eb", null ],
    [ "TIM1_TIxExternalClockConfig", "group___t_i_m1.html#ga9c0038f8971d8cad4a725e7ba9a5e084", null ],
    [ "TIM1_UpdateDisableConfig", "group___t_i_m1.html#ga4e266dc6941598d5327873a10c734161", null ],
    [ "TIM1_UpdateRequestConfig", "group___t_i_m1.html#gaaf11ab3bb004c28c32fb920a0d520420", null ]
];