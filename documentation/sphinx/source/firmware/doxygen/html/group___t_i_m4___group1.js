var group___t_i_m4___group1 =
[
    [ "TIM4_ARRPreloadConfig", "group___t_i_m4___group1.html#ga7d55b402ff90834059634919b2b0663c", null ],
    [ "TIM4_Cmd", "group___t_i_m4___group1.html#ga1f5556fdfb8413a83ec47a40a4a213c0", null ],
    [ "TIM4_DeInit", "group___t_i_m4___group1.html#gae69c7fea275362f2fd9a39ae91ed89a0", null ],
    [ "TIM4_GetCounter", "group___t_i_m4___group1.html#ga38630a02abaa224b8e36c9b4086ef04b", null ],
    [ "TIM4_GetPrescaler", "group___t_i_m4___group1.html#gadd4422367bf6a03895e4391b7056bfb9", null ],
    [ "TIM4_PrescalerConfig", "group___t_i_m4___group1.html#ga21f1a71a2007625d5a934570f50c8b3d", null ],
    [ "TIM4_SelectOnePulseMode", "group___t_i_m4___group1.html#gad5c3c213c9375bc4c0555a8ece0c0b14", null ],
    [ "TIM4_SetAutoreload", "group___t_i_m4___group1.html#ga5bb458117fbb808a57aff80165e839b8", null ],
    [ "TIM4_SetCounter", "group___t_i_m4___group1.html#ga06506a236272e5d8a7f6d8157dd0a4f1", null ],
    [ "TIM4_TimeBaseInit", "group___t_i_m4___group1.html#gac1a47a30cf6b1d2934dd2ac95f8db1f4", null ],
    [ "TIM4_UpdateDisableConfig", "group___t_i_m4___group1.html#ga6b95c00006f4436e417fbf7c0654b326", null ],
    [ "TIM4_UpdateRequestConfig", "group___t_i_m4___group1.html#ga5e51b08c778a31ad34c958b0dbe1446e", null ]
];