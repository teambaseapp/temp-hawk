var group___t_i_m2___internal___trigger___selection =
[
    [ "TIM2_TRGSelection_TypeDef", "group___t_i_m2___internal___trigger___selection.html#ga49555b8d38713e562ff6d53d9bfde806", [
      [ "TIM2_TRGSelection_TIM4", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806ac542215c4ab5cadbcd4933fcad3277cc", null ],
      [ "TIM2_TRGSelection_TIM1", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806a93ac770f86ada9d76dd0b696a7e97b8e", null ],
      [ "TIM2_TRGSelection_TIM3", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806a61431540b1044ac075c3c2f774aa2e2e", null ],
      [ "TIM2_TRGSelection_TIM5", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806ab0240419d4b8a34a4b85d43a67533c76", null ],
      [ "TIM2_TRGSelection_TI1F_ED", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806ad07f5a562b2023acfa5a8de1c5982b2a", null ],
      [ "TIM2_TRGSelection_TI1FP1", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806afbbfebcf493b97d61fd00368cacd7cd6", null ],
      [ "TIM2_TRGSelection_TI2FP2", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806a6af0ac49428df70fa655e8913b253356", null ],
      [ "TIM2_TRGSelection_ETRF", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806ad3c66edabbbd6c12c51ac9798320647c", null ]
    ] ]
];