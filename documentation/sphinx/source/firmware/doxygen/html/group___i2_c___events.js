var group___i2_c___events =
[
    [ "I2C_Event_TypeDef", "group___i2_c___events.html#gadc952f191791c5a48a1b03dd0c0c6940", [
      [ "I2C_EVENT_MASTER_MODE_SELECT", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a3460f01a25c0617f82a8b143c8ae4b12", null ],
      [ "I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a0dd62d5fe8139fc59833ed0d22078c5a", null ],
      [ "I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940aa2190471d61b72baa80a3fd8b1670452", null ],
      [ "I2C_EVENT_MASTER_MODE_ADDRESS10", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a3b93540549a0ff643d24a94df45f5554", null ],
      [ "I2C_EVENT_MASTER_BYTE_RECEIVED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940ae38623dd35f4ace875efbab96e004e75", null ],
      [ "I2C_EVENT_MASTER_BYTE_TRANSMITTING", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940af1cbe909a7d6d3061e56208fee0f7a54", null ],
      [ "I2C_EVENT_MASTER_BYTE_TRANSMITTED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940ae3b657caf12f5f84760310db3c9f2c59", null ],
      [ "I2C_EVENT_SLAVE_RECEIVER_ADDRESS_MATCHED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a414d6e34a1a8a9c5bd6cee6177fd18c2", null ],
      [ "I2C_EVENT_SLAVE_TRANSMITTER_ADDRESS_MATCHED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a238708b3ecc4171ce057a9de5725a173", null ],
      [ "I2C_EVENT_SLAVE_TRANSMITTER_SECONDADDRESS_MATCHED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a21ddc4a08584c3d72bba7c8d1d402796", null ],
      [ "I2C_EVENT_SLAVE_GENERALCALLADDRESS_MATCHED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940aec2253b167e38fa5f705ca49b54e4d1e", null ],
      [ "I2C_EVENT_SLAVE_BYTE_RECEIVED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a6c19b142f8f4af7dc992d98ee5676bab", null ],
      [ "I2C_EVENT_SLAVE_STOP_DETECTED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a45130245afbc751377ef3dd7fec0757f", null ],
      [ "I2C_EVENT_SLAVE_BYTE_TRANSMITTED", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a5ce6a49f619d16f106554359c3b8224e", null ],
      [ "I2C_EVENT_SLAVE_BYTE_TRANSMITTING", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a2855d1b9017678a4d4c945a9c6b94e7b", null ],
      [ "I2C_EVENT_SLAVE_ACK_FAILURE", "group___i2_c___events.html#ggadc952f191791c5a48a1b03dd0c0c6940a8219a1ed637f4b83d356a96bf8fb0537", null ]
    ] ]
];