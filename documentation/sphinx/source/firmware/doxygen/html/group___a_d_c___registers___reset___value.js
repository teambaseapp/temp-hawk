var group___a_d_c___registers___reset___value =
[
    [ "ADC_CR1_RESET_VALUE", "group___a_d_c___registers___reset___value.html#gacbeb0f3c72b47076a1d62cc88b3a81a2", null ],
    [ "ADC_CR2_RESET_VALUE", "group___a_d_c___registers___reset___value.html#gaa4a365df43337b8d9c10892e9cbaeda9", null ],
    [ "ADC_CR3_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga2da21d1e5dbe32155d63b8bb684775ca", null ],
    [ "ADC_DRH_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga9e862bdf7738590ac9aff71d074982d5", null ],
    [ "ADC_DRL_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga40cda96dcfb31e2277bd4d463846f0b8", null ],
    [ "ADC_HTRH_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga9ad1aa3f753d78da3164738301bbbde8", null ],
    [ "ADC_HTRL_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga96036b2e95176eb49004f67b9c5632c9", null ],
    [ "ADC_LTRH_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga5d334ca317b29ecfd5eacc11c81d07b5", null ],
    [ "ADC_LTRL_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga035bd6d3e4a8b3a477690aca874883b6", null ],
    [ "ADC_SQR1_RESET_VALUE", "group___a_d_c___registers___reset___value.html#gad1bd68c62a0f77a740eaa7ce2a770458", null ],
    [ "ADC_SQR2_RESET_VALUE", "group___a_d_c___registers___reset___value.html#gacd4ff7bf138a9db9fa6a83f6e1bc579b", null ],
    [ "ADC_SQR3_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga4a9c858cc54fe4e6cd816639e2f38305", null ],
    [ "ADC_SQR4_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga8b433266035219e3c131e8017a0c9dc9", null ],
    [ "ADC_SR_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga634eda88cf043a3dc9a67b70b0af7ee2", null ],
    [ "ADC_TRIGR1_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga92e90e675755a8f2eceb29420759bdf6", null ],
    [ "ADC_TRIGR2_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga7594ebf19a7d75a896a4bf015a1024a8", null ],
    [ "ADC_TRIGR3_RESET_VALUE", "group___a_d_c___registers___reset___value.html#gadda93839423108b2494bc00c8d6f1df0", null ],
    [ "ADC_TRIGR4_RESET_VALUE", "group___a_d_c___registers___reset___value.html#ga35bdf3ae046709b38681a2dd680a9d5c", null ]
];