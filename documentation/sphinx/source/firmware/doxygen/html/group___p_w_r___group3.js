var group___p_w_r___group3 =
[
    [ "PWR_GetFlagStatus", "group___p_w_r___group3.html#gab84db49950a589c95bbabb62320f25e8", null ],
    [ "PWR_PVDClearFlag", "group___p_w_r___group3.html#ga569fea4abf788c628c79a03e12b18fea", null ],
    [ "PWR_PVDClearITPendingBit", "group___p_w_r___group3.html#ga271e9cdf724871f7133788bbfe870a31", null ],
    [ "PWR_PVDGetITStatus", "group___p_w_r___group3.html#ga6015722ce63503e87d7683b0ad0da6b9", null ],
    [ "PWR_PVDITConfig", "group___p_w_r___group3.html#ga0b582081368ee74e570d4e84a3400558", null ]
];