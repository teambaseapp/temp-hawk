var group___data___packet___structure =
[
    [ "RFM69_DATA_INDEX_BATTERY", "group___data___packet___structure.html#ga359a8f9a437e5cc000a7d1506c499369", null ],
    [ "RFM69_DATA_INDEX_CONFIG", "group___data___packet___structure.html#ga0ba270c6e41d2d88e0fc39425bcade62", null ],
    [ "RFM69_DATA_INDEX_DEVICE_ID", "group___data___packet___structure.html#gaa91c86ef1eedcb75a9871e2edf66e838", null ],
    [ "RFM69_DATA_INDEX_END_OF_PACKET", "group___data___packet___structure.html#gab768f0fbc80ad72a162b7b909240abb2", null ],
    [ "RFM69_DATA_INDEX_HUMIDITY", "group___data___packet___structure.html#gaeca71f1501084d308573f7ca5dcac31a", null ],
    [ "RFM69_DATA_INDEX_SENSOR1", "group___data___packet___structure.html#gaa98b2c84f6e14be2518aed6585cf8f1b", null ],
    [ "RFM69_DATA_INDEX_SENSOR2", "group___data___packet___structure.html#ga770b081fdf683ca665147ec9233612ac", null ],
    [ "RFM69_DATA_INDEX_SYNC_BYTE", "group___data___packet___structure.html#ga77dcfa67210587ce9651e87078adf481", null ],
    [ "RFM69_DATA_INDEX_TEMPERATURE", "group___data___packet___structure.html#ga498416a973d66de2a02fa3edc8fc8a62", null ],
    [ "RFM69_DATA_SIZE", "group___data___packet___structure.html#ga80b16a485ee345bc36c2a45b8bb16f3e", null ]
];