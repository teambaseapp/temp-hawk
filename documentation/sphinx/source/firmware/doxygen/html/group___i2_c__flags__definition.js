var group___i2_c__flags__definition =
[
    [ "I2C_FLAG_TypeDef", "group___i2_c__flags__definition.html#ga03e7dae95040ab39a73ec6a5a73ebde5", [
      [ "I2C_FLAG_TXE", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a601205c9c50293f3579db51eb316a101", null ],
      [ "I2C_FLAG_RXNE", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5ad03255604595b1d344fd18fdcd356eb9", null ],
      [ "I2C_FLAG_STOPF", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a5da962568c0ad1188c775efa50cb58c2", null ],
      [ "I2C_FLAG_ADD10", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a8cd6330e3364b63acb7934d25367ace8", null ],
      [ "I2C_FLAG_BTF", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a9c85b83cc07cf7464f5ea1894e6c8253", null ],
      [ "I2C_FLAG_ADDR", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a2c7bc370d24719920f244dceea9b1d0a", null ],
      [ "I2C_FLAG_SB", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a379344d31d4a4781ae0de7d7c617a56d", null ],
      [ "I2C_FLAG_SMBALERT", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5ab99fe4902310c05dac374df5f1dde66c", null ],
      [ "I2C_FLAG_TIMEOUT", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a011674da72448cdd78f539665cd4f61c", null ],
      [ "I2C_FLAG_WUFH", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5aae7bd08a33fd4bbba3c64fc65402ec1e", null ],
      [ "I2C_FLAG_PECERR", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5aa0ed8ee0cfdaa26b2e9e0f4eb4a9e170", null ],
      [ "I2C_FLAG_OVR", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a14617f1addc1e485764ed9a6e1f5a3d5", null ],
      [ "I2C_FLAG_AF", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5af26a43f1a160e102f7c269e8d0a21bab", null ],
      [ "I2C_FLAG_ARLO", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5af6a5a74cc7986e0727ea0e30a22ea38c", null ],
      [ "I2C_FLAG_BERR", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5adb8cf420aee070b672dc0ad9f4e43c72", null ],
      [ "I2C_FLAG_DUALF", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a4bc9114ec57249c6a1a9614048c53164", null ],
      [ "I2C_FLAG_SMBHOST", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a3db9da68ae43f8c07dc1420b6631bb1b", null ],
      [ "I2C_FLAG_SMBDEFAULT", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5ac001201e3f50421593cb906c297c398a", null ],
      [ "I2C_FLAG_GENCALL", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a3a597a68ebee232bcc2b5a3306391931", null ],
      [ "I2C_FLAG_TRA", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a5e26692233ded744773036b139cca86e", null ],
      [ "I2C_FLAG_BUSY", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a3a167e1216d5407ac9d57bf71363e9f7", null ],
      [ "I2C_FLAG_MSL", "group___i2_c__flags__definition.html#gga03e7dae95040ab39a73ec6a5a73ebde5a5febdf387d9b8e24f17aa7b2544f1b99", null ]
    ] ]
];