var group___a_d_c___external___trigger___sensitivity =
[
    [ "ADC_ExtTRGSensitivity_TypeDef", "group___a_d_c___external___trigger___sensitivity.html#gab2727800c80b3cb734ce729d9221295f", [
      [ "ADC_ExtTRGSensitivity_Rising", "group___a_d_c___external___trigger___sensitivity.html#ggab2727800c80b3cb734ce729d9221295fa0263ed01b4404ef59253a5e424d13f9c", null ],
      [ "ADC_ExtTRGSensitivity_Falling", "group___a_d_c___external___trigger___sensitivity.html#ggab2727800c80b3cb734ce729d9221295faf889685e3c45dbf786f4e852fcaba34d", null ],
      [ "ADC_ExtTRGSensitivity_All", "group___a_d_c___external___trigger___sensitivity.html#ggab2727800c80b3cb734ce729d9221295fa0226fdb56fbdde4fdbd16eece6b5281e", null ]
    ] ]
];