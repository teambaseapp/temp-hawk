var group___r_t_c___group1 =
[
    [ "RTC_BypassShadowCmd", "group___r_t_c___group1.html#gae5e7c682f15b8ae9ddd3b2a85a9df7db", null ],
    [ "RTC_DeInit", "group___r_t_c___group1.html#ga9777c6cc4a99c339ebc527a791b2ebe7", null ],
    [ "RTC_EnterInitMode", "group___r_t_c___group1.html#ga679f8883cbfb267a53ffb1ab4cc5c8c5", null ],
    [ "RTC_ExitInitMode", "group___r_t_c___group1.html#ga87f86f3b794205f09a1eac51738d900f", null ],
    [ "RTC_Init", "group___r_t_c___group1.html#ga8eb747bf9698b2482ba6ef4d811de8e0", null ],
    [ "RTC_RatioCmd", "group___r_t_c___group1.html#ga1f4d5fe751becd7962a57f63207673c3", null ],
    [ "RTC_StructInit", "group___r_t_c___group1.html#gab466f3348de3236976e9aec7d6025dff", null ],
    [ "RTC_WaitForSynchro", "group___r_t_c___group1.html#ga2938febeef6baf0d91cc066ca5caf095", null ],
    [ "RTC_WriteProtectionCmd", "group___r_t_c___group1.html#ga9d4bdfd3ae6957630d15d2497573b7c7", null ]
];