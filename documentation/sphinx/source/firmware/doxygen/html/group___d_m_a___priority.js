var group___d_m_a___priority =
[
    [ "DMA_Priority_TypeDef", "group___d_m_a___priority.html#ga8233b35288d68a265b3d78e6d1d98226", [
      [ "DMA_Priority_Low", "group___d_m_a___priority.html#gga8233b35288d68a265b3d78e6d1d98226a9ab65aa66375451f15c909a31bd08372", null ],
      [ "DMA_Priority_Medium", "group___d_m_a___priority.html#gga8233b35288d68a265b3d78e6d1d98226ac8a104fdc8bbd8fc4c6bdf91e3287ae0", null ],
      [ "DMA_Priority_High", "group___d_m_a___priority.html#gga8233b35288d68a265b3d78e6d1d98226a883256c4d65f9f5b2c7beed9a6413474", null ],
      [ "DMA_Priority_VeryHigh", "group___d_m_a___priority.html#gga8233b35288d68a265b3d78e6d1d98226aae2afc431c42aa0366e46d076d64bdc0", null ]
    ] ]
];