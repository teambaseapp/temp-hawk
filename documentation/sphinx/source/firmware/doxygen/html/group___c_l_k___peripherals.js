var group___c_l_k___peripherals =
[
    [ "CLK_Peripheral_TypeDef", "group___c_l_k___peripherals.html#ga7e317dbac5504fca247dcf82e86c0451", [
      [ "CLK_Peripheral_TIM2", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451af25802a33b551199d920a72cb3185e77", null ],
      [ "CLK_Peripheral_TIM3", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451aa4a746a54f4e39111a090848d13e53b4", null ],
      [ "CLK_Peripheral_TIM4", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a108e2d328f14b851fd53362353223bf5", null ],
      [ "CLK_Peripheral_I2C1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a9516cb719378955fe4701104e413f760", null ],
      [ "CLK_Peripheral_SPI1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a4f390369e27daa77fe786e18d5527f82", null ],
      [ "CLK_Peripheral_USART1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a1d81f794cf66c957956ee3634be4d834", null ],
      [ "CLK_Peripheral_BEEP", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a1e1d181b93e7fb1793f01921702d0db2", null ],
      [ "CLK_Peripheral_DAC", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a9e727edda9178694f4db4b3011671757", null ],
      [ "CLK_Peripheral_ADC1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a17d67982762c71204c21c266215bccba", null ],
      [ "CLK_Peripheral_TIM1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a15569ab862e9e58758109868d5e227c9", null ],
      [ "CLK_Peripheral_RTC", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a9c0557ad4460b46bc5e957e64be0fbf3", null ],
      [ "CLK_Peripheral_LCD", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a6708b7e8b59e9877a6e18faccd6425c2", null ],
      [ "CLK_Peripheral_DMA1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451ab35bb173ece4d4f9d7e95c8a6744ebed", null ],
      [ "CLK_Peripheral_COMP", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451ae4eca68e5b1a4c0c707a31ac72c85dc3", null ],
      [ "CLK_Peripheral_BOOTROM", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451aacfb4d1960496e69d3011766ececfbe4", null ],
      [ "CLK_Peripheral_AES", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451acd5c01a25db5117a154891c8871f16cb", null ],
      [ "CLK_Peripheral_TIM5", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a9cbed31c9a9ff4f0647668c24b7b32a2", null ],
      [ "CLK_Peripheral_SPI2", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a10c8602227404ae69faf7047e1086264", null ],
      [ "CLK_Peripheral_USART2", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451ae8c8f83cb854c151403a6c6d1ee4d968", null ],
      [ "CLK_Peripheral_USART3", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a7897e32bac76e4c7ad23ff8b3d18debf", null ],
      [ "CLK_Peripheral_CSSLSE", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a7fbb37131a41f138798f46bb8f5330db", null ]
    ] ]
];