var group___d_a_c___group1 =
[
    [ "DAC_Cmd", "group___d_a_c___group1.html#gad2fa4067680c58119e4520a3d4a9c2aa", null ],
    [ "DAC_DeInit", "group___d_a_c___group1.html#ga1fae225204e1e049d6795319e99ba8bc", null ],
    [ "DAC_DualSoftwareTriggerCmd", "group___d_a_c___group1.html#gab4d3b364a6b184dcd65f3b294ebf56dc", null ],
    [ "DAC_GetDataOutputValue", "group___d_a_c___group1.html#gae7ccf57321c2adeae4c8e0f6f8383be7", null ],
    [ "DAC_Init", "group___d_a_c___group1.html#gab622f15aacaa0dbaf86371aa023ae3ef", null ],
    [ "DAC_SetChannel1Data", "group___d_a_c___group1.html#ga1a473940a91ef4402ece55a543de3b85", null ],
    [ "DAC_SetChannel2Data", "group___d_a_c___group1.html#ga8a893e4126c65a2406dfef3ca00469e9", null ],
    [ "DAC_SetDualChannelData", "group___d_a_c___group1.html#gac670ae80de1812520fded8d710722dd1", null ],
    [ "DAC_SetNoiseWaveLFSR", "group___d_a_c___group1.html#gafa6684f7e0b64f41bd765a789de113a1", null ],
    [ "DAC_SetTriangleWaveAmplitude", "group___d_a_c___group1.html#ga2fb7989e03b2e7d75ede80638af754a7", null ],
    [ "DAC_SoftwareTriggerCmd", "group___d_a_c___group1.html#ga066d876248dea2a09cd092817784e67e", null ],
    [ "DAC_WaveGenerationCmd", "group___d_a_c___group1.html#ga84d280cad96abdfe54c626a100304207", null ]
];