var group___w_w_d_g___private___functions =
[
    [ "Refresh window and Counter configuration functions", "group___w_w_d_g___group1.html", "group___w_w_d_g___group1" ],
    [ "WWDG activation function", "group___w_w_d_g___group2.html", "group___w_w_d_g___group2" ],
    [ "WWDG counter and software reset management", "group___w_w_d_g___group3.html", "group___w_w_d_g___group3" ]
];