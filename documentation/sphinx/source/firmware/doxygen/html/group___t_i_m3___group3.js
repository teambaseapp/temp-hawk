var group___t_i_m3___group3 =
[
    [ "TIM3_GetCapture1", "group___t_i_m3___group3.html#ga6ce6deb067fd9098e894bdcd5a1304f9", null ],
    [ "TIM3_GetCapture2", "group___t_i_m3___group3.html#ga374bafdb6266c80cce76c6dc7548907d", null ],
    [ "TIM3_ICInit", "group___t_i_m3___group3.html#gaa4d7b32da8d5704bbf189d7a976feab7", null ],
    [ "TIM3_PWMIConfig", "group___t_i_m3___group3.html#gaacce9b8b52d58a71b9fc17d8312a12df", null ],
    [ "TIM3_SetIC1Prescaler", "group___t_i_m3___group3.html#ga9484970a2b1f6397d38af7f830fdc95a", null ],
    [ "TIM3_SetIC2Prescaler", "group___t_i_m3___group3.html#ga959d1ed372816a6990b1c95022e1c645", null ]
];