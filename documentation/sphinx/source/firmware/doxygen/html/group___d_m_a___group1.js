var group___d_m_a___group1 =
[
    [ "DMA_Cmd", "group___d_m_a___group1.html#ga8ccbd3d4d9474f1deb863764f6cedac8", null ],
    [ "DMA_DeInit", "group___d_m_a___group1.html#ga0349a6c19a891b73b084bd416a4c262a", null ],
    [ "DMA_GlobalCmd", "group___d_m_a___group1.html#ga21cf4640a3e3b9dc913492c6058e6478", null ],
    [ "DMA_GlobalDeInit", "group___d_m_a___group1.html#gab652552339b6124b0e87713d738135fc", null ],
    [ "DMA_Init", "group___d_m_a___group1.html#gad4265428d8ee58df39cbddc0d6be60ec", null ],
    [ "DMA_SetTimeOut", "group___d_m_a___group1.html#ga2bcc772a1005733c85ecbf37cc806302", null ]
];