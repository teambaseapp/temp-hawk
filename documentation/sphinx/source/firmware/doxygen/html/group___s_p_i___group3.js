var group___s_p_i___group3 =
[
    [ "SPI_CalculateCRCCmd", "group___s_p_i___group3.html#gaa936b41c6f1c8010749991a7f1014d9b", null ],
    [ "SPI_GetCRC", "group___s_p_i___group3.html#gafccc31b40ebfd15731224471594a7669", null ],
    [ "SPI_GetCRCPolynomial", "group___s_p_i___group3.html#ga05cdf3960458dfa79cba51ea58480162", null ],
    [ "SPI_ResetCRC", "group___s_p_i___group3.html#ga27ad061b3ab8e3492bfde4e9d573e1db", null ],
    [ "SPI_TransmitCRC", "group___s_p_i___group3.html#gace8b1058e09bab150b0dbe5978810273", null ]
];