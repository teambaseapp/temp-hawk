var group___i2_c___exported___macros =
[
    [ "IS_I2C_ACK_POSITION", "group___i2_c___exported___macros.html#ga7e677fbb8467af80fd541c992528ed0b", null ],
    [ "IS_I2C_ACK_STATE", "group___i2_c___exported___macros.html#ga6401bedc842e784a2bb78b3aa21af19d", null ],
    [ "IS_I2C_ACKNOWLEDGE_ADDRESS", "group___i2_c___exported___macros.html#ga3b3b65c0c647606d852f947fad47c6ae", null ],
    [ "IS_I2C_ADDRESS", "group___i2_c___exported___macros.html#ga1d5454fba67cc584f7556f650a5e3518", null ],
    [ "IS_I2C_CLEAR_FLAG", "group___i2_c___exported___macros.html#ga66c8180841350c47627c323acfe42ee9", null ],
    [ "IS_I2C_CLEAR_IT", "group___i2_c___exported___macros.html#ga368c05fc9aadd7bb4d23280791b5a751", null ],
    [ "IS_I2C_CONFIG_IT", "group___i2_c___exported___macros.html#gae2695a081871336881f9105d6c847c4f", null ],
    [ "IS_I2C_DIRECTION", "group___i2_c___exported___macros.html#ga3b728fc6ec7107f8c3e86aa2891d0344", null ],
    [ "IS_I2C_DUTY_CYCLE", "group___i2_c___exported___macros.html#gae9b3276d9b6ec872ee4fafa2b2fafb83", null ],
    [ "IS_I2C_EVENT", "group___i2_c___exported___macros.html#ga4b42e6936006195f89ff4f763d366970", null ],
    [ "IS_I2C_GET_FLAG", "group___i2_c___exported___macros.html#ga1a2c2a7c50cd8e33e532918106b4f6ce", null ],
    [ "IS_I2C_GET_IT", "group___i2_c___exported___macros.html#gab3d113f3d6b6e6f7896c5d7a8d8c2d01", null ],
    [ "IS_I2C_MODE", "group___i2_c___exported___macros.html#ga58464e1fe94a79bab721137a544baf79", null ],
    [ "IS_I2C_OUTPUT_CLOCK_FREQ", "group___i2_c___exported___macros.html#ga7d1992c46342fda23a4f1686e8c98319", null ],
    [ "IS_I2C_OWN_ADDRESS", "group___i2_c___exported___macros.html#ga203d01aaa5ab74ff44c06e051a1b2ab9", null ],
    [ "IS_I2C_PEC_POSITION", "group___i2_c___exported___macros.html#gac8611a62b06256b1b3c60d4859a016ef", null ],
    [ "IS_I2C_REGISTER", "group___i2_c___exported___macros.html#ga37206b258f7930065c308ec7d35f243e", null ],
    [ "IS_I2C_SMBUS_ALERT", "group___i2_c___exported___macros.html#ga61b0c91563eb4bd598329241fe7d3bb4", null ]
];