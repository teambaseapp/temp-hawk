var group___s_p_i___exported___macros =
[
    [ "IS_SPI_BAUDRATE_PRESCALER", "group___s_p_i___exported___macros.html#ga7f6b7af12cf2b34e2b105d0f5cbc938f", null ],
    [ "IS_SPI_CLEAR_FLAG", "group___s_p_i___exported___macros.html#ga0f917455ae6f483311f774d4384dc15c", null ],
    [ "IS_SPI_CLEAR_IT", "group___s_p_i___exported___macros.html#ga72b4d82152edf576315c7e876d9818e8", null ],
    [ "IS_SPI_CONFIG_IT", "group___s_p_i___exported___macros.html#ga5e9b3435a4dcbb704de8b5f8e2365355", null ],
    [ "IS_SPI_CRC", "group___s_p_i___exported___macros.html#ga31fe242594f851558496a93f85def883", null ],
    [ "IS_SPI_CRC_POLYNOMIAL", "group___s_p_i___exported___macros.html#ga6442516f489742275316d9575cdcda54", null ],
    [ "IS_SPI_DATA_DIRECTION", "group___s_p_i___exported___macros.html#gaf4faf209c6757758b77a1b39c2f8811d", null ],
    [ "IS_SPI_DIRECTION", "group___s_p_i___exported___macros.html#ga395047c0518472be50bdff756c85e69c", null ],
    [ "IS_SPI_DMAREQ", "group___s_p_i___exported___macros.html#ga31e157d639dfef23584940abdf0a035e", null ],
    [ "IS_SPI_FIRSTBIT", "group___s_p_i___exported___macros.html#gaf31c469c26727f48d06bb1d746090609", null ],
    [ "IS_SPI_FLAG", "group___s_p_i___exported___macros.html#gadd60242af947815412b57ac799ab9634", null ],
    [ "IS_SPI_GET_IT", "group___s_p_i___exported___macros.html#ga6d35695c9a91b3b845d2287aa81fee9f", null ],
    [ "IS_SPI_MODE", "group___s_p_i___exported___macros.html#ga6a28f3424fc2059554e126b91cfb04c9", null ],
    [ "IS_SPI_PHASE", "group___s_p_i___exported___macros.html#gae6ecbf5ada24f7d3b8091dbb6d191966", null ],
    [ "IS_SPI_POLARITY", "group___s_p_i___exported___macros.html#gaf05ee6d8ff877a098254d0426a79b0a0", null ],
    [ "IS_SPI_SLAVEMANAGEMENT", "group___s_p_i___exported___macros.html#ga17350f5ed9a4499fcfd429fed0c3cd8d", null ]
];