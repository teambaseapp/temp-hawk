var group___r_t_c___smooth___calibration___period =
[
    [ "RTC_SmoothCalibPeriod_TypeDef", "group___r_t_c___smooth___calibration___period.html#ga2ea6dec93e0bff64bc22db994edfaad8", [
      [ "RTC_SmoothCalibPeriod_32sec", "group___r_t_c___smooth___calibration___period.html#gga2ea6dec93e0bff64bc22db994edfaad8a5eed7bf68e8b90786bd85ad433e1a1f9", null ],
      [ "RTC_SmoothCalibPeriod_16sec", "group___r_t_c___smooth___calibration___period.html#gga2ea6dec93e0bff64bc22db994edfaad8adeb8cd3ae42767cf53253c61088a68b6", null ],
      [ "RTC_SmoothCalibPeriod_8sec", "group___r_t_c___smooth___calibration___period.html#gga2ea6dec93e0bff64bc22db994edfaad8ac81168c3252c2332d77276c9112e0149", null ]
    ] ]
];