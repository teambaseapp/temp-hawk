var group___t_i_m1___private___functions =
[
    [ "TimeBase management functions", "group___t_i_m1___group1.html", "group___t_i_m1___group1" ],
    [ "Output Compare management functions", "group___t_i_m1___group2.html", "group___t_i_m1___group2" ],
    [ "Input Capture management functions", "group___t_i_m1___group3.html", "group___t_i_m1___group3" ],
    [ "Interrupts DMA and flags management functions", "group___t_i_m1___group4.html", "group___t_i_m1___group4" ],
    [ "Clocks management functions", "group___t_i_m1___group5.html", "group___t_i_m1___group5" ],
    [ "Synchronization management functions", "group___t_i_m1___group6.html", "group___t_i_m1___group6" ],
    [ "Specific interface management functions", "group___t_i_m1___group7.html", "group___t_i_m1___group7" ]
];