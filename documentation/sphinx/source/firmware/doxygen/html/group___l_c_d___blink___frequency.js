var group___l_c_d___blink___frequency =
[
    [ "LCD_BlinkFrequency_TypeDef", "group___l_c_d___blink___frequency.html#ga5a7d403080ec66ebc8f2ca4d75bd3552", [
      [ "LCD_BlinkFrequency_Div8", "group___l_c_d___blink___frequency.html#gga5a7d403080ec66ebc8f2ca4d75bd3552a87b9e4517a504e0c6f687d21ae431589", null ],
      [ "LCD_BlinkFrequency_Div16", "group___l_c_d___blink___frequency.html#gga5a7d403080ec66ebc8f2ca4d75bd3552abe6c4f155a2c73fa3368fa7a46cc7882", null ],
      [ "LCD_BlinkFrequency_Div32", "group___l_c_d___blink___frequency.html#gga5a7d403080ec66ebc8f2ca4d75bd3552adccf65fc9a8e1fcc37bce56717431646", null ],
      [ "LCD_BlinkFrequency_Div64", "group___l_c_d___blink___frequency.html#gga5a7d403080ec66ebc8f2ca4d75bd3552a713a06332007f1220c37384afec45a92", null ],
      [ "LCD_BlinkFrequency_Div128", "group___l_c_d___blink___frequency.html#gga5a7d403080ec66ebc8f2ca4d75bd3552a6ff790c7d4683e3b0823ce2ccad6f8b1", null ],
      [ "LCD_BlinkFrequency_Div256", "group___l_c_d___blink___frequency.html#gga5a7d403080ec66ebc8f2ca4d75bd3552a7f89ba70df0b448a93e1736ac9dde0d8", null ],
      [ "LCD_BlinkFrequency_Div512", "group___l_c_d___blink___frequency.html#gga5a7d403080ec66ebc8f2ca4d75bd3552a62a22c58bf33c4a0b9c0711662fb4308", null ],
      [ "LCD_BlinkFrequency_Div1024", "group___l_c_d___blink___frequency.html#gga5a7d403080ec66ebc8f2ca4d75bd3552ad1c78064810c838802d96dcdf751feff", null ]
    ] ]
];