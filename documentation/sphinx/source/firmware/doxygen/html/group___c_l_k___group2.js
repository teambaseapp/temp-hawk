var group___c_l_k___group2 =
[
    [ "CLK_GetClockFreq", "group___c_l_k___group2.html#ga2d27cbf7ffb73aa74f9cb03e3afdbf8f", null ],
    [ "CLK_GetSYSCLKSource", "group___c_l_k___group2.html#gaec9e2c58fa0e3c0d4f446ce818f5c832", null ],
    [ "CLK_SYSCLKDivConfig", "group___c_l_k___group2.html#ga0b4a3579f860960e31de0a42cc909ac8", null ],
    [ "CLK_SYSCLKSourceConfig", "group___c_l_k___group2.html#ga3e981572b0df73b902eab6f67c9d1120", null ],
    [ "CLK_SYSCLKSourceSwitchCmd", "group___c_l_k___group2.html#gaa0506d258dce10dd4b7cd4a823db8caa", null ]
];