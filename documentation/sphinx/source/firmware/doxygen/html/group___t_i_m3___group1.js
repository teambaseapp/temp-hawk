var group___t_i_m3___group1 =
[
    [ "TIM3_ARRPreloadConfig", "group___t_i_m3___group1.html#ga82d7a8293971f61e397c94adf19b9545", null ],
    [ "TIM3_Cmd", "group___t_i_m3___group1.html#ga20e045b0cbe41f2b883507ea7093ed4b", null ],
    [ "TIM3_CounterModeConfig", "group___t_i_m3___group1.html#ga8ad34d501534718482440276d083b682", null ],
    [ "TIM3_DeInit", "group___t_i_m3___group1.html#ga7fd3211510c2c62beb66928c64b723de", null ],
    [ "TIM3_GetCounter", "group___t_i_m3___group1.html#gabf8f64df20195071817ab3e3f25fa42a", null ],
    [ "TIM3_GetPrescaler", "group___t_i_m3___group1.html#ga5e4c39064908dc7258ccd6d5d1945767", null ],
    [ "TIM3_PrescalerConfig", "group___t_i_m3___group1.html#gaead9a5eae10e682b062a1597a56a696d", null ],
    [ "TIM3_SelectOnePulseMode", "group___t_i_m3___group1.html#gae74017b0a5dc3e19cb41c665a57839a5", null ],
    [ "TIM3_SetAutoreload", "group___t_i_m3___group1.html#ga36911f2ee2a1fe6c0e45854986373629", null ],
    [ "TIM3_SetCounter", "group___t_i_m3___group1.html#gac5ee4fe8e1ecd33e65e1d1fff927d362", null ],
    [ "TIM3_TimeBaseInit", "group___t_i_m3___group1.html#gac974e817a95e9dbcad021e92a0ff6a88", null ],
    [ "TIM3_UpdateDisableConfig", "group___t_i_m3___group1.html#gab6147cd8eb35ed77992378f920113cc5", null ],
    [ "TIM3_UpdateRequestConfig", "group___t_i_m3___group1.html#ga3f45edf8643e7bcb667956de6183bbf7", null ]
];