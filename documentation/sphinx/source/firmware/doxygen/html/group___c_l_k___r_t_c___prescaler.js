var group___c_l_k___r_t_c___prescaler =
[
    [ "CLK_RTCCLKDiv_TypeDef", "group___c_l_k___r_t_c___prescaler.html#ga1acdb2452689e6994900557f0b5c16c5", [
      [ "CLK_RTCCLKDiv_1", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a45f6c44b3a1931cf94656146f2dae08c", null ],
      [ "CLK_RTCCLKDiv_2", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a8ac62419c9df796d8ab07319d22b0297", null ],
      [ "CLK_RTCCLKDiv_4", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a98e876a824667e5a4fa63d02ef20e446", null ],
      [ "CLK_RTCCLKDiv_8", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5acb3b7a75837a19bb2333343cb17ca6ef", null ],
      [ "CLK_RTCCLKDiv_16", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a43493b075f516cd6efab4994e70acdb4", null ],
      [ "CLK_RTCCLKDiv_32", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a6c2ad0797f094669a1565a70e2bc3618", null ],
      [ "CLK_RTCCLKDiv_64", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a4c6eeca3f44768a6e34d1ed4568e56da", null ]
    ] ]
];