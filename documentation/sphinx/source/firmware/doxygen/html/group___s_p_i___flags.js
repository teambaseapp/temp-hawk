var group___s_p_i___flags =
[
    [ "SPI_FLAG_TypeDef", "group___s_p_i___flags.html#ga4f662a71fef63be8d3a0c7aa23199f71", [
      [ "SPI_FLAG_BSY", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a994865b0a4e40851a11e7f8c3361d65f", null ],
      [ "SPI_FLAG_OVR", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71aa42cf678ab63a86e8102daf3b3da1f2f", null ],
      [ "SPI_FLAG_MODF", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a15c1b612cdcd72c5533efef278020f56", null ],
      [ "SPI_FLAG_CRCERR", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a64c97948ca0770650f215d085883bba1", null ],
      [ "SPI_FLAG_WKUP", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a73dd6b0fcc56efe5dba09db395a838dd", null ],
      [ "SPI_FLAG_TXE", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a9bec46ed6c69999c610a7e92fa7782b2", null ],
      [ "SPI_FLAG_RXNE", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a080bfbf635102da80faacb37a2346f56", null ]
    ] ]
];