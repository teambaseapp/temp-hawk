var group___t_i_m5___input___capture___prescaler =
[
    [ "TIM5_ICPSC_TypeDef", "group___t_i_m5___input___capture___prescaler.html#gabbe6730c304221b6ebdd6bdcaa40b0da", [
      [ "TIM5_ICPSC_DIV1", "group___t_i_m5___input___capture___prescaler.html#ggabbe6730c304221b6ebdd6bdcaa40b0daa5d83ba8c4790ec7a7ba9d3d6161d4c8f", null ],
      [ "TIM5_ICPSC_DIV2", "group___t_i_m5___input___capture___prescaler.html#ggabbe6730c304221b6ebdd6bdcaa40b0daa2f056cf93a39e5dd3e6286c67a21fadb", null ],
      [ "TIM5_ICPSC_DIV4", "group___t_i_m5___input___capture___prescaler.html#ggabbe6730c304221b6ebdd6bdcaa40b0daaa6cef143339eb8cda54c3ed01270c4dc", null ],
      [ "TIM5_ICPSC_DIV8", "group___t_i_m5___input___capture___prescaler.html#ggabbe6730c304221b6ebdd6bdcaa40b0daa3e1dc94a24cfc1b6f06c785965793020", null ]
    ] ]
];