var group___r_t_c___weekdays =
[
    [ "RTC_Weekday_TypeDef", "group___r_t_c___weekdays.html#ga57073c5791a378bc7439d90055e73f7a", [
      [ "RTC_Weekday_Monday", "group___r_t_c___weekdays.html#gga57073c5791a378bc7439d90055e73f7aa2d50fce9f3c1844998858dbbfc73c045", null ],
      [ "RTC_Weekday_Tuesday", "group___r_t_c___weekdays.html#gga57073c5791a378bc7439d90055e73f7aac6b9713b37d965135c3dda650d27f0c2", null ],
      [ "RTC_Weekday_Wednesday", "group___r_t_c___weekdays.html#gga57073c5791a378bc7439d90055e73f7aa28afca6c0f257418b70f78ebd87359f3", null ],
      [ "RTC_Weekday_Thursday", "group___r_t_c___weekdays.html#gga57073c5791a378bc7439d90055e73f7aa9eb7ddf25c6a4fdeb8bfab4f4e495c95", null ],
      [ "RTC_Weekday_Friday", "group___r_t_c___weekdays.html#gga57073c5791a378bc7439d90055e73f7aa0b0c89169ad065f56942b88fd5175d4f", null ],
      [ "RTC_Weekday_Saturday", "group___r_t_c___weekdays.html#gga57073c5791a378bc7439d90055e73f7aa1ea65117a7725d42609e20a40eb206d0", null ],
      [ "RTC_Weekday_Sunday", "group___r_t_c___weekdays.html#gga57073c5791a378bc7439d90055e73f7aacf5b7c69b8ca6b5d7823fff4f59424ee", null ]
    ] ]
];