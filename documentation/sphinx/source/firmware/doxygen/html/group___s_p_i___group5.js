var group___s_p_i___group5 =
[
    [ "SPI_ClearFlag", "group___s_p_i___group5.html#ga84cc626656a536a16f5c0c0bbf896686", null ],
    [ "SPI_ClearITPendingBit", "group___s_p_i___group5.html#gacc28dcf2c3ee71bd2181945ef7bf85bc", null ],
    [ "SPI_GetFlagStatus", "group___s_p_i___group5.html#gabf038f294dae6cce6bec69363efbbbbe", null ],
    [ "SPI_GetITStatus", "group___s_p_i___group5.html#ga751336d92aedea121635bd9f62fbc57f", null ],
    [ "SPI_ITConfig", "group___s_p_i___group5.html#ga372b5fe70c731c2292c394a0b84434c3", null ]
];