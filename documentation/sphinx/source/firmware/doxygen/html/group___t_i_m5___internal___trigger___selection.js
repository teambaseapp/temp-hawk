var group___t_i_m5___internal___trigger___selection =
[
    [ "TIM5_TRGSelection_TypeDef", "group___t_i_m5___internal___trigger___selection.html#ga77df2b0019fdda2f1ad7aa9b083d92b1", [
      [ "TIM5_TRGSelection_TIM4", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1ae3488c3e9b50b167016269a0a03b0aca", null ],
      [ "TIM5_TRGSelection_TIM1", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1a86be1af63e71e90bffb462e8f94a59fd", null ],
      [ "TIM5_TRGSelection_TIM3", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1ad61aa17de2f3aeaf5683e7606927cee5", null ],
      [ "TIM5_TRGSelection_TIM2", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1a9aeaf87e6596605ab9ef0bb55198edfd", null ],
      [ "TIM5_TRGSelection_TI1F_ED", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1ac366914a4361e70da0b156060dbe272a", null ],
      [ "TIM5_TRGSelection_TI1FP1", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1ab0a160f69e822297a7f00c4df7c68295", null ],
      [ "TIM5_TRGSelection_TI2FP2", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1aebb523129174e94419f3f7ae0599fbd5", null ],
      [ "TIM5_TRGSelection_ETRF", "group___t_i_m5___internal___trigger___selection.html#gga77df2b0019fdda2f1ad7aa9b083d92b1a88129fef0592043e97b4af9e8667cd8a", null ]
    ] ]
];