var group___t_i_m1___external___trigger___prescaler =
[
    [ "TIM1_ExtTRGPSC_TypeDef", "group___t_i_m1___external___trigger___prescaler.html#gaba3c0cdf5e8b3108e84e50b99bbdfff8", [
      [ "TIM1_ExtTRGPSC_OFF", "group___t_i_m1___external___trigger___prescaler.html#ggaba3c0cdf5e8b3108e84e50b99bbdfff8a57ad3a17fec47e5c5826477db97f82e0", null ],
      [ "TIM1_ExtTRGPSC_DIV2", "group___t_i_m1___external___trigger___prescaler.html#ggaba3c0cdf5e8b3108e84e50b99bbdfff8af852c0eca9128fb1d0431f4d9771e64f", null ],
      [ "TIM1_ExtTRGPSC_DIV4", "group___t_i_m1___external___trigger___prescaler.html#ggaba3c0cdf5e8b3108e84e50b99bbdfff8aa9a8b94daed84d0382708011bb066d3b", null ],
      [ "TIM1_ExtTRGPSC_DIV8", "group___t_i_m1___external___trigger___prescaler.html#ggaba3c0cdf5e8b3108e84e50b99bbdfff8a6f80666bfcf36962bb932918afaff6da", null ]
    ] ]
];