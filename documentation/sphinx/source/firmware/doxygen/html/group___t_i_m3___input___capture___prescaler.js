var group___t_i_m3___input___capture___prescaler =
[
    [ "TIM3_ICPSC_TypeDef", "group___t_i_m3___input___capture___prescaler.html#gaeac2a80af678cbeb6915141b2c571a00", [
      [ "TIM3_ICPSC_DIV1", "group___t_i_m3___input___capture___prescaler.html#ggaeac2a80af678cbeb6915141b2c571a00a7f7c228664548991f504a7bc19746716", null ],
      [ "TIM3_ICPSC_DIV2", "group___t_i_m3___input___capture___prescaler.html#ggaeac2a80af678cbeb6915141b2c571a00a9c475872785bac3207e83cbe5c035eb5", null ],
      [ "TIM3_ICPSC_DIV4", "group___t_i_m3___input___capture___prescaler.html#ggaeac2a80af678cbeb6915141b2c571a00aa7f2e1c208783b74cb40da14a61d6bc5", null ],
      [ "TIM3_ICPSC_DIV8", "group___t_i_m3___input___capture___prescaler.html#ggaeac2a80af678cbeb6915141b2c571a00a4cce7b0a10753b09cd13cb35ba927c7f", null ]
    ] ]
];