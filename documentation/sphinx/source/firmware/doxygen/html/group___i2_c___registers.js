var group___i2_c___registers =
[
    [ "I2C_Register_TypeDef", "group___i2_c___registers.html#ga7dfea9973c57ea7b224a5ee116b4f948", [
      [ "I2C_Register_CR1", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a468555a4c6846c39c128c6be036c5487", null ],
      [ "I2C_Register_CR2", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a1d2b5ad33fc4cf7455b61699d245c5f0", null ],
      [ "I2C_Register_FREQR", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948acddd43333903a807c3fb3bcc7e0e8367", null ],
      [ "I2C_Register_OARL", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a831720ab504c614ba18b5a4483d4bc88", null ],
      [ "I2C_Register_OARH", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a1d1b01bfa13985bb33d23bbaad27ab66", null ],
      [ "I2C_Register_DR", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a07bdf9cb9bb8a27a2cb57cc65786045c", null ],
      [ "I2C_Register_SR1", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948ace103b0c2701729c4d4b1ec4882b0370", null ],
      [ "I2C_Register_SR2", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a0bf5649f14adb1db086135e3e745ff32", null ],
      [ "I2C_Register_SR3", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948afad4736d84fb9b2e3411ac22bb53deb1", null ],
      [ "I2C_Register_ITR", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a49159308a6cdebcced08008fb3e7d5f8", null ],
      [ "I2C_Register_CCRL", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a4f1fc54acc9aaef276b2e36249df83ae", null ],
      [ "I2C_Register_CCRH", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a0db3e650c09cbc9be8628061599e6a88", null ],
      [ "I2C_Register_TRISER", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948a9aaeb58d210b4e4b86aae883ad818319", null ],
      [ "I2C_Register_PECR", "group___i2_c___registers.html#gga7dfea9973c57ea7b224a5ee116b4f948acb845738c84fccfb80fbf71e2fd1eb60", null ]
    ] ]
];