var group___t_i_m3___internal___trigger___selection =
[
    [ "TIM3_TRGSelection_TypeDef", "group___t_i_m3___internal___trigger___selection.html#gaadbdb335c2e0724357f716e0edf8e4bc", [
      [ "TIM3_TRGSelection_TIM4", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca0577273064dfa1feb591e8b87baaef2e", null ],
      [ "TIM3_TRGSelection_TIM1", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca12e71cbd05d8050207c51e6f89f798c1", null ],
      [ "TIM3_TRGSelection_TIM5", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bcae64b6cb1b4c7e920bfbb9f73762618b2", null ],
      [ "TIM3_TRGSelection_TIM2", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca0f5d89ea6e7142cf233224e4f25d6981", null ],
      [ "TIM3_TRGSelection_TI1F_ED", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca06e69397141a451ce88329353869aafa", null ],
      [ "TIM3_TRGSelection_TI1FP1", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca0f79cce41cac7225ba62298d91aeee49", null ],
      [ "TIM3_TRGSelection_TI2FP2", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca8b4c6fab9847c7bbdc2ce14455feaab3", null ],
      [ "TIM3_TRGSelection_ETRF", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca96286262bc268024502fe75a2fc58801", null ]
    ] ]
];