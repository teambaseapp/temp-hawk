var group___t_i_m1___d_m_a___source___requests =
[
    [ "TIM1_DMASource_TypeDef", "group___t_i_m1___d_m_a___source___requests.html#ga6f259134457d4c37afb9dbf7325e5779", [
      [ "TIM1_DMASource_Update", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779abc4d2413204799460b68d88902a6668a", null ],
      [ "TIM1_DMASource_CC1", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779ae0b10dccb07b4a99eb6dd564362bf61b", null ],
      [ "TIM1_DMASource_CC2", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779a7d0cfbb957e9e64dacd3a5813291a6ed", null ],
      [ "TIM1_DMASource_CC3", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779a0deb0b7fed8f39b96fd0730f73f561a8", null ],
      [ "TIM1_DMASource_CC4", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779a80577529855a1dd2241be8bc0a0f1295", null ],
      [ "TIM1_DMASource_COM", "group___t_i_m1___d_m_a___source___requests.html#gga6f259134457d4c37afb9dbf7325e5779a00bef918506e9d49945d262b12b2be34", null ]
    ] ]
];