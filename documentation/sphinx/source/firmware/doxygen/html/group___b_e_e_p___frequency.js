var group___b_e_e_p___frequency =
[
    [ "BEEP_Frequency_TypeDef", "group___b_e_e_p___frequency.html#ga0f7c41a259b1d3f6d2b4cc338a70d58b", [
      [ "BEEP_Frequency_1KHz", "group___b_e_e_p___frequency.html#gga0f7c41a259b1d3f6d2b4cc338a70d58ba8092ce42f6a469543b0af4bf4345ea02", null ],
      [ "BEEP_Frequency_2KHz", "group___b_e_e_p___frequency.html#gga0f7c41a259b1d3f6d2b4cc338a70d58bad9dd85e026033368a6b87090edcce0bf", null ],
      [ "BEEP_Frequency_4KHz", "group___b_e_e_p___frequency.html#gga0f7c41a259b1d3f6d2b4cc338a70d58ba5af98cd947658100c34abc3f3196bca6", null ]
    ] ]
];