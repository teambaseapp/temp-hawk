var group___i_t_c =
[
    [ "ITC_Exported_Types", "group___i_t_c___exported___types.html", "group___i_t_c___exported___types" ],
    [ "ITC_Exported_Constants", "group___i_t_c___exported___constants.html", null ],
    [ "ITC_Private_Functions", "group___i_t_c___private___functions.html", "group___i_t_c___private___functions" ],
    [ "ITC_DeInit", "group___i_t_c.html#ga33de86ed566796e4fdce18197188ed1a", null ],
    [ "ITC_GetCPUCC", "group___i_t_c.html#ga6baafae4c11a073d7ef221b89bb757bd", null ],
    [ "ITC_GetSoftIntStatus", "group___i_t_c.html#ga3507955493a20af5edf731c7606c5a7a", null ],
    [ "ITC_GetSoftwarePriority", "group___i_t_c.html#ga1f9946df3f5f8b8614e30fe669e8a963", null ],
    [ "ITC_SetSoftwarePriority", "group___i_t_c.html#ga02a3e474902ec96b0c094b4998c141a9", null ]
];