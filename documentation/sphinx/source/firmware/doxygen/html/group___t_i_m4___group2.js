var group___t_i_m4___group2 =
[
    [ "TIM4_ClearFlag", "group___t_i_m4___group2.html#ga68bf7dba212a67b444cb24c98147cb1a", null ],
    [ "TIM4_ClearITPendingBit", "group___t_i_m4___group2.html#ga1baccba593d47c2c563acd1f7a2d4ca0", null ],
    [ "TIM4_DMACmd", "group___t_i_m4___group2.html#ga998eca93852f289e74330cd713046bb3", null ],
    [ "TIM4_GenerateEvent", "group___t_i_m4___group2.html#gaa58252ecf95374571494922bcb5e73cc", null ],
    [ "TIM4_GetFlagStatus", "group___t_i_m4___group2.html#ga0395b9dd06c2b4d998771a454dc7e33f", null ],
    [ "TIM4_GetITStatus", "group___t_i_m4___group2.html#gacdfd8bbea4ab13ab6f8c73da3109d1bf", null ],
    [ "TIM4_ITConfig", "group___t_i_m4___group2.html#ga9f26f1c1147c5b47c78cbc1348b55fc3", null ]
];