var group___i_w_d_g__prescaler =
[
    [ "IWDG_Prescaler_TypeDef", "group___i_w_d_g__prescaler.html#gae0636527cdb794fce2873f5f29ff665c", [
      [ "IWDG_Prescaler_4", "group___i_w_d_g__prescaler.html#ggae0636527cdb794fce2873f5f29ff665caf489f1cddbc066f3c3bcdc28ae57acc9", null ],
      [ "IWDG_Prescaler_8", "group___i_w_d_g__prescaler.html#ggae0636527cdb794fce2873f5f29ff665caaefb2c5ddcdef62f1a195309e162c680", null ],
      [ "IWDG_Prescaler_16", "group___i_w_d_g__prescaler.html#ggae0636527cdb794fce2873f5f29ff665ca95dee9c8d6d2393ab0d0173c42d23434", null ],
      [ "IWDG_Prescaler_32", "group___i_w_d_g__prescaler.html#ggae0636527cdb794fce2873f5f29ff665ca5812560698ca1e05e75c0d465872fe24", null ],
      [ "IWDG_Prescaler_64", "group___i_w_d_g__prescaler.html#ggae0636527cdb794fce2873f5f29ff665caaee9d63d79741d4168aaf7353b577dec", null ],
      [ "IWDG_Prescaler_128", "group___i_w_d_g__prescaler.html#ggae0636527cdb794fce2873f5f29ff665ca69024032b84d61597508cb920931355d", null ],
      [ "IWDG_Prescaler_256", "group___i_w_d_g__prescaler.html#ggae0636527cdb794fce2873f5f29ff665ca1d281cedc79946c2c3146cbffc8a46cd", null ]
    ] ]
];