var group___t_i_m5___group1 =
[
    [ "TIM5_ARRPreloadConfig", "group___t_i_m5___group1.html#ga028e838bbdf926145f0c0df0a42cbfb2", null ],
    [ "TIM5_Cmd", "group___t_i_m5___group1.html#ga7398d4769dc83ee17dd88477286e8e33", null ],
    [ "TIM5_CounterModeConfig", "group___t_i_m5___group1.html#ga53f10ed4d9ba371fbc4b42f08bfdbe70", null ],
    [ "TIM5_DeInit", "group___t_i_m5___group1.html#gafa10e2573fe38ee52ff48c95960f87a3", null ],
    [ "TIM5_GetCounter", "group___t_i_m5___group1.html#gaaef6e616cd97ca6bfdc4d29b086d2a5f", null ],
    [ "TIM5_GetPrescaler", "group___t_i_m5___group1.html#ga20ee0b9891042ac01c0563b81e98500f", null ],
    [ "TIM5_PrescalerConfig", "group___t_i_m5___group1.html#gab80fdb3c07a11d46b09b268ad13b853d", null ],
    [ "TIM5_SelectOnePulseMode", "group___t_i_m5___group1.html#gabad8083e39d0afd84a0b399607fa4404", null ],
    [ "TIM5_SetAutoreload", "group___t_i_m5___group1.html#gac089b0a3758bc0baabf25d1854117f19", null ],
    [ "TIM5_SetCounter", "group___t_i_m5___group1.html#gab9922abb371c6bd777872d91e135e490", null ],
    [ "TIM5_TimeBaseInit", "group___t_i_m5___group1.html#ga9168c46c087454475d8d6b977650a141", null ],
    [ "TIM5_UpdateDisableConfig", "group___t_i_m5___group1.html#gaba4bf3912e855633a5b3db931edd2a6f", null ],
    [ "TIM5_UpdateRequestConfig", "group___t_i_m5___group1.html#ga27850fca7a4c129a59d1026e024fac4a", null ]
];