var group___i2_c__interrupts__definition =
[
    [ "I2C_IT_TypeDef", "group___i2_c__interrupts__definition.html#gaed6510c550547134afb30f717f17da88", [
      [ "I2C_IT_ERR", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a91e37cab1ef1220a681b5525fd083dfc", null ],
      [ "I2C_IT_EVT", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a33c31ff1b04045bc40ffd4c7c9d95592", null ],
      [ "I2C_IT_BUF", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88ab91030d53b1414d2c0413fd68ab01698", null ],
      [ "I2C_IT_TXE", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88ad56aafec26ded51b4f92e68fcba9dbaf", null ],
      [ "I2C_IT_RXNE", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88acddd7d3b84408f1f157f08175a68df3c", null ],
      [ "I2C_IT_STOPF", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a7a97d691df79f6382196777fe75f0122", null ],
      [ "I2C_IT_ADD10", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88aa46de5e62b4e47f0a892e5fbf7ab2c88", null ],
      [ "I2C_IT_BTF", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a301d613aca7e5ab40ec52750195b8b05", null ],
      [ "I2C_IT_ADDR", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a4d63de1f778d8a4c4a78c6f4521e2809", null ],
      [ "I2C_IT_SB", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a3e70e6198a0cf366b62aab56fb9d8482", null ],
      [ "I2C_IT_SMBALERT", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88ac3bff28dd6dd45d4e497c2205c40b9f5", null ],
      [ "I2C_IT_TIMEOUT", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a14fa6618d44426837e63f327ff41d40c", null ],
      [ "I2C_IT_WUFH", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88aeddb6000558ae467aa92ed00c55ea0a1", null ],
      [ "I2C_IT_PECERR", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88ad55461628ec95730ff249055d8eb94db", null ],
      [ "I2C_IT_OVR", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a741bafcb1fa8a99dbf9de2bb7861caed", null ],
      [ "I2C_IT_AF", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a8709b710349d2df92aca875fa472b30b", null ],
      [ "I2C_IT_ARLO", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a528e922c7b61da105775b3324b0b2e56", null ],
      [ "I2C_IT_BERR", "group___i2_c__interrupts__definition.html#ggaed6510c550547134afb30f717f17da88a06a6ad58d66758f46878778b42bbbec0", null ]
    ] ]
];