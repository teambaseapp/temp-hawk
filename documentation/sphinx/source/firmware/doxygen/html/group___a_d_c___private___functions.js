var group___a_d_c___private___functions =
[
    [ "Initialization and Configuration functions", "group___a_d_c___group1.html", "group___a_d_c___group1" ],
    [ "Analog Watchdog configuration functions", "group___a_d_c___group2.html", "group___a_d_c___group2" ],
    [ "Temperature Sensor & Vrefint (Voltage Reference", "group___a_d_c___group3.html", "group___a_d_c___group3" ],
    [ "Channels Configuration functions", "group___a_d_c___group4.html", "group___a_d_c___group4" ],
    [ "ADC Channels DMA Configuration function", "group___a_d_c___group5.html", "group___a_d_c___group5" ],
    [ "Interrupts and flags management functions", "group___a_d_c___group6.html", "group___a_d_c___group6" ]
];