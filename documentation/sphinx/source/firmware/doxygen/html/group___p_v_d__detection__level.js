var group___p_v_d__detection__level =
[
    [ "PWR_PVDLevel_TypeDef", "group___p_v_d__detection__level.html#gabdea2f5e923f8f1355a34e90ffecb15a", [
      [ "PWR_PVDLevel_1V85", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aa5c2d589e2c99e179be70a8fd1e9e7bc5", null ],
      [ "PWR_PVDLevel_2V05", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aabf6aa729ee87ef0480243491f2a868cb", null ],
      [ "PWR_PVDLevel_2V26", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aafe625229ac9c6397631e8cbb9603684d", null ],
      [ "PWR_PVDLevel_2V45", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aadeca25b4c82dcbdd41688a55fa3ef1c5", null ],
      [ "PWR_PVDLevel_2V65", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aa649c95b7883bebe4fbcb1d39d3530ab9", null ],
      [ "PWR_PVDLevel_2V85", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aaae0431dd00eb78cfb3d2952d74f39340", null ],
      [ "PWR_PVDLevel_3V05", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aac28f7cc806f81d8dd6c618191e9f91f0", null ],
      [ "PWR_PVDLevel_PVDIn", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aabc795b35879afc753f6d767b24c907df", null ]
    ] ]
];