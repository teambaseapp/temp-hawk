var group___t_i_m2___external___trigger___prescaler =
[
    [ "TIM2_ExtTRGPSC_TypeDef", "group___t_i_m2___external___trigger___prescaler.html#gad478800137b2621183eee68623efa21b", [
      [ "TIM2_ExtTRGPSC_OFF", "group___t_i_m2___external___trigger___prescaler.html#ggad478800137b2621183eee68623efa21ba1b316df85b1c9c46804cb9b2c3b47e03", null ],
      [ "TIM2_ExtTRGPSC_DIV2", "group___t_i_m2___external___trigger___prescaler.html#ggad478800137b2621183eee68623efa21ba6ed44eb367341fe2059a81915b41956b", null ],
      [ "TIM2_ExtTRGPSC_DIV4", "group___t_i_m2___external___trigger___prescaler.html#ggad478800137b2621183eee68623efa21baea75e14b406c4d0b9181cde1a9bca6a4", null ],
      [ "TIM2_ExtTRGPSC_DIV8", "group___t_i_m2___external___trigger___prescaler.html#ggad478800137b2621183eee68623efa21bae84474eee11cf9b5161a09dd634be9fa", null ]
    ] ]
];