var stm8l15x__clockutils_8h =
[
    [ "ChangePowerMode", "stm8l15x__clockutils_8h.html#a71dc9c8c25508ab34891a231902cc09c", null ],
    [ "ClockConf", "stm8l15x__clockutils_8h.html#a2ab453aeadf33fe2d2fb0b8eafa7e265", null ],
    [ "EnterLowPowerMode", "stm8l15x__clockutils_8h.html#a169fef07ca6facb52c300178811d52b3", null ],
    [ "LowPowerConf", "stm8l15x__clockutils_8h.html#a67cd29077a733959c8c963e62d184e6e", null ],
    [ "SetUpRTC", "stm8l15x__clockutils_8h.html#ae2633fc6a3c3ca2729da8bc81d67a476", null ]
];