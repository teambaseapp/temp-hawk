var group___t_i_m3___prescaler =
[
    [ "TIM3_Prescaler_TypeDef", "group___t_i_m3___prescaler.html#ga1cbc8497bd7e3b4b5e2410a1187d9c1f", [
      [ "TIM3_Prescaler_1", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa93eee4d3349e25659c221bd960201cea", null ],
      [ "TIM3_Prescaler_2", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fab4918e380c4f625ea6ffcbc32e525282", null ],
      [ "TIM3_Prescaler_4", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa97e8c07033ed548d160820dd885ac944", null ],
      [ "TIM3_Prescaler_8", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa569c25d640fa59654c5f87bc706576f2", null ],
      [ "TIM3_Prescaler_16", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa809b56414206c7a55439b4a96b34b1b9", null ],
      [ "TIM3_Prescaler_32", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fae2a44ac42bb7e3e3937aff092dd5b726", null ],
      [ "TIM3_Prescaler_64", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fac47380ed4e6fccce203b8590bc94d4eb", null ],
      [ "TIM3_Prescaler_128", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa95722614aae8a9900ad33532635f71b4", null ]
    ] ]
];