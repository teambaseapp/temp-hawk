var group___t_i_m5___external___trigger___polarity =
[
    [ "TIM5_ExtTRGPolarity_TypeDef", "group___t_i_m5___external___trigger___polarity.html#gab44ee2c33684fd854a66794fbcab3b4d", [
      [ "TIM5_ExtTRGPolarity_Inverted", "group___t_i_m5___external___trigger___polarity.html#ggab44ee2c33684fd854a66794fbcab3b4da8bd67dce39ea66c4a50f4b66fe8a34b7", null ],
      [ "TIM5_ExtTRGPolarity_NonInverted", "group___t_i_m5___external___trigger___polarity.html#ggab44ee2c33684fd854a66794fbcab3b4dab9203e8eae52464896c95ee9941ce5c1", null ]
    ] ]
];