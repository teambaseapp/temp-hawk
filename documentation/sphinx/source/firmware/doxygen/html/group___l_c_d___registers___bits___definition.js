var group___l_c_d___registers___bits___definition =
[
    [ "LCD_CR1_B2", "group___l_c_d___registers___bits___definition.html#gadebb85505721dc9f9697ebce65c275ad", null ],
    [ "LCD_CR1_BLINK", "group___l_c_d___registers___bits___definition.html#gad62a1458c5e783dab7e87becc85bb4f1", null ],
    [ "LCD_CR1_BLINKF", "group___l_c_d___registers___bits___definition.html#ga3b3da99b5361403fcd5012a4f1cc1af9", null ],
    [ "LCD_CR1_DUTY", "group___l_c_d___registers___bits___definition.html#gaa375359ed6a4d27a2cda95f92e23c8aa", null ],
    [ "LCD_CR2_CC", "group___l_c_d___registers___bits___definition.html#gac8a4647fc8659be2133d346c1c156f16", null ],
    [ "LCD_CR2_HD", "group___l_c_d___registers___bits___definition.html#gad78b6f4f40e0d19f3b8cba4658a2ea81", null ],
    [ "LCD_CR2_PON", "group___l_c_d___registers___bits___definition.html#gae558356520c2ca808491075ac4596892", null ],
    [ "LCD_CR2_VSEL", "group___l_c_d___registers___bits___definition.html#gab324dd147a42aca529601393b1212b30", null ],
    [ "LCD_CR3_DEAD", "group___l_c_d___registers___bits___definition.html#gaff651b839cd39472c589df99f0e24049", null ],
    [ "LCD_CR3_LCDEN", "group___l_c_d___registers___bits___definition.html#gade1766191f4ec97038bb6aacd53897ef", null ],
    [ "LCD_CR3_SOF", "group___l_c_d___registers___bits___definition.html#ga12788ae6647284c563a31b7aa79560c9", null ],
    [ "LCD_CR3_SOFC", "group___l_c_d___registers___bits___definition.html#ga3bece7814343dc0680b5968a3f986ad3", null ],
    [ "LCD_CR3_SOFIE", "group___l_c_d___registers___bits___definition.html#ga0a206a1027d349cba7719fefabfbba0e", null ],
    [ "LCD_CR4_B4", "group___l_c_d___registers___bits___definition.html#gafc6766b8f4fbb360d8f3d4ed3b445d52", null ],
    [ "LCD_CR4_DUTY8", "group___l_c_d___registers___bits___definition.html#ga9f89020f960d052f13c3d5b93bffb209", null ],
    [ "LCD_CR4_MAPCOM", "group___l_c_d___registers___bits___definition.html#ga2cfbd1fc49c458a0c16252ad2e8faf5e", null ],
    [ "LCD_CR4_PAGECOM", "group___l_c_d___registers___bits___definition.html#ga24c82d4643d90f6362d6eb4729896653", null ],
    [ "LCD_FRQ_DIV", "group___l_c_d___registers___bits___definition.html#ga1496f1e57217531d62558304782d265c", null ],
    [ "LCD_FRQ_PS", "group___l_c_d___registers___bits___definition.html#ga5de2664467d6975b4ae061509aa72db5", null ]
];