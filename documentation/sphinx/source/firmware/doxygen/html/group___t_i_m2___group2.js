var group___t_i_m2___group2 =
[
    [ "Input Capture management functions", "group___t_i_m2___group3.html", "group___t_i_m2___group3" ],
    [ "Interrupts DMA and flags management functions", "group___t_i_m2___group4.html", "group___t_i_m2___group4" ],
    [ "Clocks management functions", "group___t_i_m2___group5.html", "group___t_i_m2___group5" ],
    [ "Synchronization management functions", "group___t_i_m2___group6.html", "group___t_i_m2___group6" ],
    [ "Specific interface management functions", "group___t_i_m2___group7.html", "group___t_i_m2___group7" ],
    [ "TIM2_BKRConfig", "group___t_i_m2___group2.html#ga2d4233cd33b9f3ebe4a56ae156b6d3c2", null ],
    [ "TIM2_CCxCmd", "group___t_i_m2___group2.html#gac555ae9048a282e3590221151939d87a", null ],
    [ "TIM2_CtrlPWMOutputs", "group___t_i_m2___group2.html#ga73e4835c83b259ff1f0785d1d51f2d3c", null ],
    [ "TIM2_ForcedOC1Config", "group___t_i_m2___group2.html#ga55263272544958f717863f5d4c7162b6", null ],
    [ "TIM2_ForcedOC2Config", "group___t_i_m2___group2.html#ga54034255c4a202751b45926cfbcaa3c0", null ],
    [ "TIM2_OC1FastConfig", "group___t_i_m2___group2.html#ga4ae06bdaaf0cca6495cf22b06ca15131", null ],
    [ "TIM2_OC1Init", "group___t_i_m2___group2.html#ga6114db04787e78980c5155e79e238a60", null ],
    [ "TIM2_OC1PolarityConfig", "group___t_i_m2___group2.html#gae7f3132748744452683e50355de5612c", null ],
    [ "TIM2_OC1PreloadConfig", "group___t_i_m2___group2.html#ga564e3acbd4b94687578038469047def9", null ],
    [ "TIM2_OC2FastConfig", "group___t_i_m2___group2.html#ga7bfd4d2d96bcc39f13b66ceb1d5a1ad5", null ],
    [ "TIM2_OC2Init", "group___t_i_m2___group2.html#ga67e4c36fca7ea12ea4889d504e26ff5b", null ],
    [ "TIM2_OC2PolarityConfig", "group___t_i_m2___group2.html#gaf0d70699b942af5e4a65189d7636931f", null ],
    [ "TIM2_OC2PreloadConfig", "group___t_i_m2___group2.html#ga75500a204078522ed85a7fa05ed99a1e", null ],
    [ "TIM2_SelectOCxM", "group___t_i_m2___group2.html#ga2a7fa8f8302ac6f24166e05c3ee0937d", null ],
    [ "TIM2_SetCompare1", "group___t_i_m2___group2.html#ga5ecb4d0b7e47e33a4d652099ecea150b", null ],
    [ "TIM2_SetCompare2", "group___t_i_m2___group2.html#ga5e48da7dfd218238e5e567d4917114bd", null ]
];