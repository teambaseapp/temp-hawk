var group___r_t_c___wakeup___clock =
[
    [ "RTC_WakeUpClock_TypeDef", "group___r_t_c___wakeup___clock.html#gaff397e71f1605ca52b50dab0df093f29", [
      [ "RTC_WakeUpClock_RTCCLK_Div16", "group___r_t_c___wakeup___clock.html#ggaff397e71f1605ca52b50dab0df093f29a6bc5fd23e0f309104d14ce9e6c19f3e8", null ],
      [ "RTC_WakeUpClock_RTCCLK_Div8", "group___r_t_c___wakeup___clock.html#ggaff397e71f1605ca52b50dab0df093f29a3a5a1baafa0a64221ce87a5ab29e3d1b", null ],
      [ "RTC_WakeUpClock_RTCCLK_Div4", "group___r_t_c___wakeup___clock.html#ggaff397e71f1605ca52b50dab0df093f29ab6e47985e67194651b8b059ff3c28efa", null ],
      [ "RTC_WakeUpClock_RTCCLK_Div2", "group___r_t_c___wakeup___clock.html#ggaff397e71f1605ca52b50dab0df093f29ae534d7817d2ce6b65f1e91a29b01c9ba", null ],
      [ "RTC_WakeUpClock_CK_SPRE_16bits", "group___r_t_c___wakeup___clock.html#ggaff397e71f1605ca52b50dab0df093f29a923970deb42c0a22dec5afd163efd6bb", null ],
      [ "RTC_WakeUpClock_CK_SPRE_17bits", "group___r_t_c___wakeup___clock.html#ggaff397e71f1605ca52b50dab0df093f29a1fcda0df8990456ef03c692397b2cfdc", null ]
    ] ]
];