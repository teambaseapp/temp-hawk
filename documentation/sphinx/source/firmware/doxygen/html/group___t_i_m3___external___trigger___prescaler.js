var group___t_i_m3___external___trigger___prescaler =
[
    [ "TIM3_ExtTRGPSC_TypeDef", "group___t_i_m3___external___trigger___prescaler.html#ga45f25a66c6eed3c4aa898754fdea9ee9", [
      [ "TIM3_ExtTRGPSC_OFF", "group___t_i_m3___external___trigger___prescaler.html#gga45f25a66c6eed3c4aa898754fdea9ee9a997699af2dfd718935d64811d27d4a6e", null ],
      [ "TIM3_ExtTRGPSC_DIV2", "group___t_i_m3___external___trigger___prescaler.html#gga45f25a66c6eed3c4aa898754fdea9ee9a3484dffb45f80f5428d1ab911d360add", null ],
      [ "TIM3_ExtTRGPSC_DIV4", "group___t_i_m3___external___trigger___prescaler.html#gga45f25a66c6eed3c4aa898754fdea9ee9a8680b66a9801b55fb736697a05fec226", null ],
      [ "TIM3_ExtTRGPSC_DIV8", "group___t_i_m3___external___trigger___prescaler.html#gga45f25a66c6eed3c4aa898754fdea9ee9afeb869a047856650c92aa83807e43c8b", null ]
    ] ]
];