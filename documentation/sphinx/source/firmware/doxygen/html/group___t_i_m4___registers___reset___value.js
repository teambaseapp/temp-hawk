var group___t_i_m4___registers___reset___value =
[
    [ "TIM4_ARR_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#ga2cfbf14fffcb068a34355766be81b1e5", null ],
    [ "TIM4_CNTR_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#gaafdf61ffefb7105d9fbdbca4fb38be28", null ],
    [ "TIM4_CR1_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#ga238d4e69861e5800e5347735900968a2", null ],
    [ "TIM4_CR2_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#ga2c301185838485c196f054cc50ea1a36", null ],
    [ "TIM4_DER_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#ga0feff605e901a847c2441fe635b385ff", null ],
    [ "TIM4_EGR_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#ga0067fffc0ae27ed63c7fda1470b468c5", null ],
    [ "TIM4_IER_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#ga48aead25d540e51abeb3a936d1c1b388", null ],
    [ "TIM4_PSCR_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#gac7b3efb0287c1b7460b4fcb180515f8c", null ],
    [ "TIM4_SMCR_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#ga6a6c49b41330c84e1a9f29bb2b7dd2a5", null ],
    [ "TIM4_SR1_RESET_VALUE", "group___t_i_m4___registers___reset___value.html#ga11408d3b130d705f6d91eeda11f1a0e5", null ]
];