var group___t_i_m1___d_m_a___burst___length =
[
    [ "TIM1_DMABurstLength_TypeDef", "group___t_i_m1___d_m_a___burst___length.html#ga729f239355218cc04d2b1eb30e2d4592", [
      [ "TIM1_DMABurstLength_1Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a698ff25f24e846cd87235d4f4e2ce4c1", null ],
      [ "TIM1_DMABurstLength_2Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a58124e40efd284bbbef9a9b81c8b7d72", null ],
      [ "TIM1_DMABurstLength_3Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a2963af7911b7c6854d3005a3f017bf67", null ],
      [ "TIM1_DMABurstLength_4Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a036da403c40cfdc69e0f1db34d35d72e", null ],
      [ "TIM1_DMABurstLength_5Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a3977883ead045d4a32df80ad4a215c1a", null ],
      [ "TIM1_DMABurstLength_6Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a5624ad5f14abf2022cb560b5ceaa761e", null ],
      [ "TIM1_DMABurstLength_7Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592ad46d63104611e9150dd9a48d7f9a116b", null ],
      [ "TIM1_DMABurstLength_8Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592af8cc052e8f5e980430bfe83c479911ce", null ],
      [ "TIM1_DMABurstLength_9Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592af3abebcb92b247d568d948594437b568", null ],
      [ "TIM1_DMABurstLength_10Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a20e9fb2c2c4bfbb22335cc4ff3fa4244", null ],
      [ "TIM1_DMABurstLength_11Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a150eff12307c20d58502ceb888627049", null ],
      [ "TIM1_DMABurstLength_12Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a4406d700b842113f4df411d3712f5e1a", null ],
      [ "TIM1_DMABurstLength_13Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a0ca250d53aa2a59c8cf0c340f7963c9e", null ],
      [ "TIM1_DMABurstLength_14Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a065760bba26c0662330606b6ec71530d", null ],
      [ "TIM1_DMABurstLength_15Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a34567cb56fa54e01a4308974ba5ac9ba", null ],
      [ "TIM1_DMABurstLength_16Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a6a3aa9a50ab4de98f3f8e004479b2e49", null ],
      [ "TIM1_DMABurstLength_17Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a7b871c039db8d964412f9066baf9a21a", null ],
      [ "TIM1_DMABurstLength_18Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592afc3d35b78ebe9e3e0622397887d8d458", null ],
      [ "TIM1_DMABurstLength_19Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592af7d1af2175cdf0608000711f01c1fd14", null ],
      [ "TIM1_DMABurstLength_20Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a4c486c2d900df0480699c96a36ff93e2", null ],
      [ "TIM1_DMABurstLength_21Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a1e644f26d6b6e3eb58c66b73afc7b7d9", null ],
      [ "TIM1_DMABurstLength_22Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592aa290bb49705db99ad4d395020f265dbd", null ],
      [ "TIM1_DMABurstLength_23Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a05824b337b1e8b23fc6349cbf069db03", null ],
      [ "TIM1_DMABurstLength_24Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a6a2f7d75f2aec304c3e53f8304f77491", null ],
      [ "TIM1_DMABurstLength_25Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a91d77a8d7284a0a4a105dacc772f8a76", null ],
      [ "TIM1_DMABurstLength_26Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a2a4ebe1e04272cddf7bf75d6035284cc", null ],
      [ "TIM1_DMABurstLength_27Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592ad87a6b7423556b437bc6b6e8f5c541a9", null ],
      [ "TIM1_DMABurstLength_28Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a3e45f7a019daf67daee2ae79117129c7", null ],
      [ "TIM1_DMABurstLength_29Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592adac3d0f6ee1859c23bf849de00be0b40", null ],
      [ "TIM1_DMABurstLength_30Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592afed22ba582e929d4dc289076ff2b5105", null ],
      [ "TIM1_DMABurstLength_31Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a0b0459ce79c5d479b7a137c63eb2a9a7", null ],
      [ "TIM1_DMABurstLength_32Byte", "group___t_i_m1___d_m_a___burst___length.html#gga729f239355218cc04d2b1eb30e2d4592a5b0c7a7ed9736b28ec93bb78a621bdae", null ]
    ] ]
];