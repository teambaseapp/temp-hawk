var group___t_i_m4___exported___types =
[
    [ "TIM4_Prescaler", "group___t_i_m4___prescaler.html", "group___t_i_m4___prescaler" ],
    [ "TIM4_One_Pulse_Mode", "group___t_i_m4___one___pulse___mode.html", "group___t_i_m4___one___pulse___mode" ],
    [ "TIM4_Reload_Mode_Prescaler", "group___t_i_m4___reload___mode___prescaler.html", "group___t_i_m4___reload___mode___prescaler" ],
    [ "TIM4_Update_Source", "group___t_i_m4___update___source.html", "group___t_i_m4___update___source" ],
    [ "TIM4_Event_Source", "group___t_i_m4___event___source.html", "group___t_i_m4___event___source" ],
    [ "TIM4_Trigger_Output_Source", "group___t_i_m4___trigger___output___source.html", "group___t_i_m4___trigger___output___source" ],
    [ "TIM4_Salve_Mode", "group___t_i_m4___salve___mode.html", "group___t_i_m4___salve___mode" ],
    [ "TIM4_Flags", "group___t_i_m4___flags.html", "group___t_i_m4___flags" ],
    [ "TIM4_Interrupts", "group___t_i_m4___interrupts.html", "group___t_i_m4___interrupts" ],
    [ "TIM4_Internal_Trigger_Selection", "group___t_i_m4___internal___trigger___selection.html", "group___t_i_m4___internal___trigger___selection" ],
    [ "TIM4_DMA_source_requests", "group___t_i_m4___d_m_a__source__requests.html", "group___t_i_m4___d_m_a__source__requests" ]
];