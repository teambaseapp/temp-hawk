var group___t_i_m2___group3 =
[
    [ "TIM2_GetCapture1", "group___t_i_m2___group3.html#gae74a5de35ed803a2fe7a802f3a3d1e51", null ],
    [ "TIM2_GetCapture2", "group___t_i_m2___group3.html#ga8b5cdcb3a4a8920f05c6c3261e05a219", null ],
    [ "TIM2_ICInit", "group___t_i_m2___group3.html#gab465f24d506292adb5717bf9212c63d9", null ],
    [ "TIM2_PWMIConfig", "group___t_i_m2___group3.html#gafd0b776705f6e3520fbbb4ed0a8fe537", null ],
    [ "TIM2_SetIC1Prescaler", "group___t_i_m2___group3.html#ga41e46848ed6b8234b716bc8f8f1743d7", null ],
    [ "TIM2_SetIC2Prescaler", "group___t_i_m2___group3.html#gafc6a24c744bd2f978995c0516991d4aa", null ]
];