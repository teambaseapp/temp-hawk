var group___c_o_m_p___private___functions =
[
    [ "Initialization and Configuration functions", "group___c_o_m_p___group1.html", "group___c_o_m_p___group1" ],
    [ "Window mode control function", "group___c_o_m_p___group2.html", "group___c_o_m_p___group2" ],
    [ "Internal Reference Voltage output function", "group___c_o_m_p___group3.html", "group___c_o_m_p___group3" ],
    [ "Comparator channels trigger configuration", "group___c_o_m_p___group4.html", "group___c_o_m_p___group4" ],
    [ "Interrupts and flags management functions", "group___c_o_m_p___group5.html", "group___c_o_m_p___group5" ]
];