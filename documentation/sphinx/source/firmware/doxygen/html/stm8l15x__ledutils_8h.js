var stm8l15x__ledutils_8h =
[
    [ "LED_COLOUR", "stm8l15x__ledutils_8h.html#af27c986dbb25dab534e69e5aaf8fc540", [
      [ "LED_YELLOW", "stm8l15x__ledutils_8h.html#af27c986dbb25dab534e69e5aaf8fc540a81b537bb7607998bab91c7f08c5883de", null ],
      [ "LED_GREEN", "stm8l15x__ledutils_8h.html#af27c986dbb25dab534e69e5aaf8fc540a0ad916c7f80666dc88f6b5b22a72e742", null ]
    ] ],
    [ "LED_STATUS", "stm8l15x__ledutils_8h.html#aee112facaa700ed423a16e80c9bba76a", [
      [ "LED_OFF", "stm8l15x__ledutils_8h.html#aee112facaa700ed423a16e80c9bba76aafc0ca8cc6cbe215fd3f1ae6d40255b40", null ],
      [ "LED_ON", "stm8l15x__ledutils_8h.html#aee112facaa700ed423a16e80c9bba76aadd01b80eb93658fb4cf7eb9aceb89a1d", null ]
    ] ],
    [ "GPIOLEDInit", "stm8l15x__ledutils_8h.html#a258e5187088222f47bb9e7ac11cafd41", null ],
    [ "LEDFaultyBlink", "stm8l15x__ledutils_8h.html#a9384cfaca7d760568988afa218c39ff5", null ],
    [ "NotifyGreenLED", "stm8l15x__ledutils_8h.html#a529b629824e4fefb54cd9d68df2f3bef", null ],
    [ "NotifyYellowLED", "stm8l15x__ledutils_8h.html#a54026a4d3091f64d623ab41a189e7098", null ],
    [ "OnOffLED", "stm8l15x__ledutils_8h.html#a53a239eb19c8eb0d7853f2814ab131b9", null ],
    [ "StartUpBlinkLED", "stm8l15x__ledutils_8h.html#af127a13defb4ac6f8c24917cc90ca1b1", null ]
];