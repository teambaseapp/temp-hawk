var group___i2_c___exported___types =
[
    [ "I2C_mode", "group___i2_c__mode.html", "group___i2_c__mode" ],
    [ "I2C_duty_cycle_in_fast_mode", "group___i2_c__duty__cycle__in__fast__mode.html", "group___i2_c__duty__cycle__in__fast__mode" ],
    [ "I2C_acknowledgement", "group___i2_c__acknowledgement.html", "group___i2_c__acknowledgement" ],
    [ "I2C_Position_Acknowledgement", "group___i2_c___position___acknowledgement.html", "group___i2_c___position___acknowledgement" ],
    [ "I2C_acknowledged_address", "group___i2_c__acknowledged__address.html", "group___i2_c__acknowledged__address" ],
    [ "I2C_transfer_direction", "group___i2_c__transfer__direction.html", "group___i2_c__transfer__direction" ],
    [ "I2C_SMBus_alert_pin_level", "group___i2_c___s_m_bus__alert__pin__level.html", "group___i2_c___s_m_bus__alert__pin__level" ],
    [ "I2C_PEC_position", "group___i2_c___p_e_c__position.html", "group___i2_c___p_e_c__position" ],
    [ "I2C_flags_definition", "group___i2_c__flags__definition.html", "group___i2_c__flags__definition" ],
    [ "I2C_interrupts_definition", "group___i2_c__interrupts__definition.html", "group___i2_c__interrupts__definition" ],
    [ "I2C_Events", "group___i2_c___events.html", "group___i2_c___events" ],
    [ "I2C_Registers", "group___i2_c___registers.html", "group___i2_c___registers" ]
];