var group___t_i_m4___trigger___output___source =
[
    [ "TIM4_TRGOSource_TypeDef", "group___t_i_m4___trigger___output___source.html#gadf78b6a17ab3620040fe9b881f007a09", [
      [ "TIM4_TRGOSource_Reset", "group___t_i_m4___trigger___output___source.html#ggadf78b6a17ab3620040fe9b881f007a09aa24c01fb83e9c16b69c3de305a653f7e", null ],
      [ "TIM4_TRGOSource_Enable", "group___t_i_m4___trigger___output___source.html#ggadf78b6a17ab3620040fe9b881f007a09a073788167785921d46f0aa66fda484af", null ],
      [ "TIM4_TRGOSource_Update", "group___t_i_m4___trigger___output___source.html#ggadf78b6a17ab3620040fe9b881f007a09af5353ef31203852402bcc89842a4adbb", null ]
    ] ]
];