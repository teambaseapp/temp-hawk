var group___l_c_d___r_a_m_register =
[
    [ "LCD_RAMRegister_TypeDef", "group___l_c_d___r_a_m_register.html#ga25e5d43a054021e7e53d45116b3c2899", [
      [ "LCD_RAMRegister_0", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a2d44fd0ee2c4aada4a2feefe98bfaad8", null ],
      [ "LCD_RAMRegister_1", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a3ba03d7d17a697864f2e47143e1f6181", null ],
      [ "LCD_RAMRegister_2", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a4f09307dfd896fb086c75edfe3826077", null ],
      [ "LCD_RAMRegister_3", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a6e2518122a6980d3f321c7f55f7d20fb", null ],
      [ "LCD_RAMRegister_4", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899ad7bbf06a1020d2990e7b7960839fd175", null ],
      [ "LCD_RAMRegister_5", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a073c84525aae6b81c71280e5bf9b4822", null ],
      [ "LCD_RAMRegister_6", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a9ad9045bdc619cfdcce9224af3830bb6", null ],
      [ "LCD_RAMRegister_7", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899ab798c0b2c832395a83ca6767943ba876", null ],
      [ "LCD_RAMRegister_8", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a3333d71689c302e44368a86658bda26d", null ],
      [ "LCD_RAMRegister_9", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a22fc73de49999061ab2778036f27c577", null ],
      [ "LCD_RAMRegister_10", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899ae581f40765ebc13960fb923e20c869ab", null ],
      [ "LCD_RAMRegister_11", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a6cbfa7f5d077fdb94743220eccd3c047", null ],
      [ "LCD_RAMRegister_12", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a27c1e2f7409f2db707feb2c8102e4196", null ],
      [ "LCD_RAMRegister_13", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899acd9643f9241981f48c0cf0af6c469e7c", null ],
      [ "LCD_RAMRegister_14", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a191c04af2580157cb6485c5182707be4", null ],
      [ "LCD_RAMRegister_15", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a18ac95898881533def6acbd3d98ec1b8", null ],
      [ "LCD_RAMRegister_16", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899af8cc93faa372136cf8126c5be2765f20", null ],
      [ "LCD_RAMRegister_17", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a6a322e09ae84341f2e123072d7475609", null ],
      [ "LCD_RAMRegister_18", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a5030df091b5c0c1fa259fd5130bd9393", null ],
      [ "LCD_RAMRegister_19", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a00a3b07c1195fcb08eb3837bbe606719", null ],
      [ "LCD_RAMRegister_20", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a483f2cc5d63d1a4b0c2b2a766c1b1224", null ],
      [ "LCD_RAMRegister_21", "group___l_c_d___r_a_m_register.html#gga25e5d43a054021e7e53d45116b3c2899a39d4bf78db869cfd008b98cf6ea9dc30", null ]
    ] ]
];