var group___d_a_c__triangleamplitude =
[
    [ "DAC_TriangleAmplitude_TypeDef", "group___d_a_c__triangleamplitude.html#gabd34f14bff8787d88b561d0a7f09af45", [
      [ "DAC_TriangleAmplitude_1", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a1d3f1b67a4541823885df92dcf8c664d", null ],
      [ "DAC_TriangleAmplitude_3", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a8f09d9a6789d731f6544bf00a268096f", null ],
      [ "DAC_TriangleAmplitude_7", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45aabf742d4b48b53577b4569ad462c1926", null ],
      [ "DAC_TriangleAmplitude_15", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a3d71741e32bf881c7acd3f39d8d98bd0", null ],
      [ "DAC_TriangleAmplitude_31", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a6d1a7aa89fceaa22f64a91a3edfb7768", null ],
      [ "DAC_TriangleAmplitude_63", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a331bdbe6d1bbf91b8f6e7640fad8022c", null ],
      [ "DAC_TriangleAmplitude_127", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a209550fbd459302532105d1aaecda3f2", null ],
      [ "DAC_TriangleAmplitude_255", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45aef048d8e865d690259928155cec55c2f", null ],
      [ "DAC_TriangleAmplitude_511", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a7f5988132d87ae59c476de6268fe9b49", null ],
      [ "DAC_TriangleAmplitude_1023", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a5b27827245f4e2a3b390a23f32cdf58d", null ],
      [ "DAC_TriangleAmplitude_2047", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45afab513622fab893cc9f7a748ab641d10", null ],
      [ "DAC_TriangleAmplitude_4095", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a5bded37d8ecc0526e179d5c6ea133045", null ]
    ] ]
];