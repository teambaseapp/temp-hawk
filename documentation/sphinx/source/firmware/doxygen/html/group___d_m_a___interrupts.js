var group___d_m_a___interrupts =
[
    [ "DMA_IT_TypeDef", "group___d_m_a___interrupts.html#ga3b2a86275cd238b56da61a1ff82e7e6f", [
      [ "DMA1_IT_TC0", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa0ebe40c7640741d732d34a2bd2df4755", null ],
      [ "DMA1_IT_TC1", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa54b9345a276e4ebc28b6138716a702dd", null ],
      [ "DMA1_IT_TC2", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fae5d13676b83ce4f686364fbb011d8b3e", null ],
      [ "DMA1_IT_TC3", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa4543899ab5ce83fb61233c2761525069", null ],
      [ "DMA1_IT_HT0", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fad622e9de61b13945db8c2c5323cbd816", null ],
      [ "DMA1_IT_HT1", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa9552c94e52c361e17962e77d195a840f", null ],
      [ "DMA1_IT_HT2", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa6846364283b262fb0bee5840b9c25eea", null ],
      [ "DMA1_IT_HT3", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa82ed8d00fb6350e941e6f1a1aa74e393", null ]
    ] ]
];