var group___a_d_c___resolution =
[
    [ "ADC_Resolution_TypeDef", "group___a_d_c___resolution.html#gacba2a330d863bc1400670f64ec62f6fa", [
      [ "ADC_Resolution_12Bit", "group___a_d_c___resolution.html#ggacba2a330d863bc1400670f64ec62f6faae3636498a475b8f224d629a1899b5163", null ],
      [ "ADC_Resolution_10Bit", "group___a_d_c___resolution.html#ggacba2a330d863bc1400670f64ec62f6faa856580dbf092e385bfbd96c260101813", null ],
      [ "ADC_Resolution_8Bit", "group___a_d_c___resolution.html#ggacba2a330d863bc1400670f64ec62f6faae268870d243117ec5fa23dbb16654b7d", null ],
      [ "ADC_Resolution_6Bit", "group___a_d_c___resolution.html#ggacba2a330d863bc1400670f64ec62f6faa468bf78d1890a5b0097abf4ba3915d87", null ]
    ] ]
];