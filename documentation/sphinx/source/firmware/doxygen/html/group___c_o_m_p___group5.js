var group___c_o_m_p___group5 =
[
    [ "COMP_ClearFlag", "group___c_o_m_p___group5.html#gae6f1c1ce82575c7507e819248e76b34d", null ],
    [ "COMP_ClearITPendingBit", "group___c_o_m_p___group5.html#ga30f23b9298a45bb664af297493ff366b", null ],
    [ "COMP_GetFlagStatus", "group___c_o_m_p___group5.html#ga07711477a11774cf11a982db191cac9e", null ],
    [ "COMP_GetITStatus", "group___c_o_m_p___group5.html#gaef83885a9947d64b394bd2a12fb16c64", null ],
    [ "COMP_ITConfig", "group___c_o_m_p___group5.html#gab3d794eaa8bc584ae195c044b00aa573", null ]
];