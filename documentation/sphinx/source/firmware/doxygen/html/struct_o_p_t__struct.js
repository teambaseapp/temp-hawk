var struct_o_p_t__struct =
[
    [ "BOR", "struct_o_p_t__struct.html#ac86ba87fe7b0799f92bf0aaf4223912b", null ],
    [ "PCODESIZE", "struct_o_p_t__struct.html#af192c67b4087b3bbe1937cc0e8cd2131", null ],
    [ "RESERVED1", "struct_o_p_t__struct.html#aa7f65e25dc47c919b1fb43fc7331523c", null ],
    [ "RESERVED2", "struct_o_p_t__struct.html#acfd75aa5652ddc0497f2463cd9bb9465", null ],
    [ "RESERVED3", "struct_o_p_t__struct.html#a12d193113a7954ea07aa5a2525352f33", null ],
    [ "RESERVED4", "struct_o_p_t__struct.html#a8a54377901d2a6915ca0ae9cf250857e", null ],
    [ "RESERVED5", "struct_o_p_t__struct.html#a0e7f331dda5701dfa02ad62e62f4ea29", null ],
    [ "ROP", "struct_o_p_t__struct.html#ae23603fd686ae4b445a65e24e4ad93c3", null ],
    [ "UBC", "struct_o_p_t__struct.html#a10c86b6815746073646ccfc40be9af9a", null ],
    [ "WDG", "struct_o_p_t__struct.html#a52e245f509db4df2f7141d3084cf1005", null ],
    [ "XTSTARTUP", "struct_o_p_t__struct.html#a0048e5d57c4d51aeb508da805f45f23a", null ]
];