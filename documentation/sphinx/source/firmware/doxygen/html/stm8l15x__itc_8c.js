var stm8l15x__itc_8c =
[
    [ "ITC_DeInit", "group___i_t_c___group1.html#ga33de86ed566796e4fdce18197188ed1a", null ],
    [ "ITC_GetCPUCC", "group___i_t_c___private___functions.html#ga6baafae4c11a073d7ef221b89bb757bd", null ],
    [ "ITC_GetSoftIntStatus", "group___i_t_c___group1.html#ga3507955493a20af5edf731c7606c5a7a", null ],
    [ "ITC_GetSoftwarePriority", "group___i_t_c___group1.html#ga1f9946df3f5f8b8614e30fe669e8a963", null ],
    [ "ITC_SetSoftwarePriority", "group___i_t_c___group1.html#ga02a3e474902ec96b0c094b4998c141a9", null ]
];