var group___r_t_c___group2 =
[
    [ "RTC_DateStructInit", "group___r_t_c___group2.html#ga6e4e99be910d7759f8910056a2985056", null ],
    [ "RTC_GetDate", "group___r_t_c___group2.html#ga7ccd144b284a3c1d3bc0065323964fae", null ],
    [ "RTC_GetSubSecond", "group___r_t_c___group2.html#ga782a3ea3d545fd4d6b9a35c620a07149", null ],
    [ "RTC_GetTime", "group___r_t_c___group2.html#ga54240825dce8aab0bc8834e555b1ad12", null ],
    [ "RTC_SetDate", "group___r_t_c___group2.html#gafebd1dc116080c04082f70a99f46d5a8", null ],
    [ "RTC_SetTime", "group___r_t_c___group2.html#ga110f4ef4dd959884372e2602d1495c22", null ],
    [ "RTC_TimeStructInit", "group___r_t_c___group2.html#ga0404db6e0c70e5a6bbbe4fa58a577365", null ]
];