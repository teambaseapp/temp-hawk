var group___c_o_m_p___registers___bits___definition =
[
    [ "COMP_CSR1_CMP1", "group___c_o_m_p___registers___bits___definition.html#ga7ffa06851c4700560f97a644bdbc9dff", null ],
    [ "COMP_CSR1_CMP1OUT", "group___c_o_m_p___registers___bits___definition.html#ga63dc54f31af168b2dc6a809cd8dea9b2", null ],
    [ "COMP_CSR1_EF1", "group___c_o_m_p___registers___bits___definition.html#gaf0d7fc76ef31284ab4c81ba07acfc4a1", null ],
    [ "COMP_CSR1_IE1", "group___c_o_m_p___registers___bits___definition.html#ga9b4946e1dc8b69f25c69f4b2c048bc12", null ],
    [ "COMP_CSR1_STE", "group___c_o_m_p___registers___bits___definition.html#ga543df48a6417722e2c97a9249c80345b", null ],
    [ "COMP_CSR2_CMP2", "group___c_o_m_p___registers___bits___definition.html#gab4a87bc0265619bd3c6a010e4ddbd3fb", null ],
    [ "COMP_CSR2_CMP2OUT", "group___c_o_m_p___registers___bits___definition.html#ga043ed4a830c1689e62137c59b390c8d0", null ],
    [ "COMP_CSR2_EF2", "group___c_o_m_p___registers___bits___definition.html#ga4b1e811cea3bff113ebdf8579c0965dd", null ],
    [ "COMP_CSR2_IE2", "group___c_o_m_p___registers___bits___definition.html#ga72c97b8ba37a7acf3c0855cd599a1458", null ],
    [ "COMP_CSR2_SPEED", "group___c_o_m_p___registers___bits___definition.html#gadaa409f4b2675db4ea54bf7ee45dca9c", null ],
    [ "COMP_CSR3_INSEL", "group___c_o_m_p___registers___bits___definition.html#ga14973dce5f88d3ebff3e53a6e4d1833f", null ],
    [ "COMP_CSR3_OUTSEL", "group___c_o_m_p___registers___bits___definition.html#ga53b5eb416689372c107b374821f1ce81", null ],
    [ "COMP_CSR3_VREFEN", "group___c_o_m_p___registers___bits___definition.html#gaee475f5da29bfacab230c4c8d4435be7", null ],
    [ "COMP_CSR3_VREFOUTEN", "group___c_o_m_p___registers___bits___definition.html#gacb944f3ddeec3bb534786cddb20b5d15", null ],
    [ "COMP_CSR3_WNDWE", "group___c_o_m_p___registers___bits___definition.html#gac26f7c69af150c98340b41415014906a", null ],
    [ "COMP_CSR4_INVTRIG", "group___c_o_m_p___registers___bits___definition.html#ga25a28ac72509c73b191f0778576184e3", null ],
    [ "COMP_CSR4_NINVTRIG", "group___c_o_m_p___registers___bits___definition.html#gadcdbdc47f7735272443472022a7fceab", null ],
    [ "COMP_CSR5_DACTRIG", "group___c_o_m_p___registers___bits___definition.html#ga0b38c62f84d6d950916df6c301db03d3", null ],
    [ "COMP_CSR5_VREFTRIG", "group___c_o_m_p___registers___bits___definition.html#ga525219baa7c74237382e0fc72446751f", null ]
];