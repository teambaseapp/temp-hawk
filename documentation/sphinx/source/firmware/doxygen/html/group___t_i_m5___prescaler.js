var group___t_i_m5___prescaler =
[
    [ "TIM5_Prescaler_TypeDef", "group___t_i_m5___prescaler.html#gac77bd9584f9efee7d9a13e55e0c8427b", [
      [ "TIM5_Prescaler_1", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba8fdd99d1b9d5f3ba81b6d4fb2298a2df", null ],
      [ "TIM5_Prescaler_2", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba0534bf5037c442316cd2d4302f60d263", null ],
      [ "TIM5_Prescaler_4", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427baea4433cf8dfbe771ecb8f77d11458faa", null ],
      [ "TIM5_Prescaler_8", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba484263125b7793d50fc1f8d425cdeb3f", null ],
      [ "TIM5_Prescaler_16", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba97895e9ac6ec18cb384b96c5e509787d", null ],
      [ "TIM5_Prescaler_32", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427baa43047fe49934ac61434b782aff17f23", null ],
      [ "TIM5_Prescaler_64", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba6ca21073c23d394fe922c618e68cd345", null ],
      [ "TIM5_Prescaler_128", "group___t_i_m5___prescaler.html#ggac77bd9584f9efee7d9a13e55e0c8427ba45079c3c92efe551253f615da497f694", null ]
    ] ]
];