var group___i_w_d_g___write_access =
[
    [ "IS_IWDG_WRITE_ACCESS_MODE", "group___i_w_d_g___write_access.html#ga982e898b867a40a2c33615dfa5bed264", null ],
    [ "IWDG_WriteAccess_TypeDef", "group___i_w_d_g___write_access.html#ga48dcf1c03fe67bcd52e0e45d739c7e13", [
      [ "IWDG_WriteAccess_Enable", "group___i_w_d_g___write_access.html#gga48dcf1c03fe67bcd52e0e45d739c7e13ac026cc98de8869e1f5f831d2d0a16c71", null ],
      [ "IWDG_WriteAccess_Disable", "group___i_w_d_g___write_access.html#gga48dcf1c03fe67bcd52e0e45d739c7e13ae4e8de0a83e73486c23fc863b48b0773", null ]
    ] ]
];