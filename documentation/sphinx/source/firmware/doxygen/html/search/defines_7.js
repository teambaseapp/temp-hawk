var searchData=
[
  ['i2c',['I2C',['../header_8h.html#a457a9aa93dbb216459873a30bdb4d84a',1,'header.h']]],
  ['i2c_5fdevice_5fenable_5fread_5fmode',['I2C_DEVICE_ENABLE_READ_MODE',['../stm8l15x__i2cutils_8h.html#a661b6a638eb024b33c5e5095ad496208',1,'stm8l15x_i2cutils.h']]],
  ['i2c_5fdevice_5fenable_5fwrite_5fmode',['I2C_DEVICE_ENABLE_WRITE_MODE',['../stm8l15x__i2cutils_8h.html#a92239dfafb1c32e7bcac9edad4f9b5a6',1,'stm8l15x_i2cutils.h']]],
  ['i2c_5fhost_5faddress',['I2C_HOST_ADDRESS',['../header_8h.html#ad21deefd4b3907909b36a816ca38305a',1,'header.h']]],
  ['i2c_5fhtu20d_5faddress',['I2C_HTU20D_ADDRESS',['../htu20dutils_8h.html#ae478b337780ecd9fc3c000a38e8e4be9',1,'htu20dutils.h']]],
  ['i2c_5fport',['I2C_PORT',['../header_8h.html#ad9ecf80e1eac083d16ec47f9d3aeb39f',1,'header.h']]],
  ['i2c_5fscl',['I2C_SCL',['../header_8h.html#a212ca328a6409c98f8c3dfbbe1ba561d',1,'header.h']]],
  ['i2c_5fsda',['I2C_SDA',['../header_8h.html#a18aefd12ad84d4c33dc97923cb821e47',1,'header.h']]]
];
