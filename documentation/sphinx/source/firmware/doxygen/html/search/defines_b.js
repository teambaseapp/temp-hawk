var searchData=
[
  ['sensitivity_5fboost_5fhigh',['SENSITIVITY_BOOST_HIGH',['../rfm69registers_8h.html#a4705a918bc468d89c8701b628025a00f',1,'rfm69registers.h']]],
  ['sensitivity_5fboost_5fnormal',['SENSITIVITY_BOOST_NORMAL',['../rfm69registers_8h.html#a42f28b7265a51de4602b347d3760d631',1,'rfm69registers.h']]],
  ['sensor1',['SENSOR1',['../header_8h.html#a82e45e3e257773335108c48e071867aa',1,'header.h']]],
  ['sensor1_5fport',['SENSOR1_PORT',['../header_8h.html#a1224f1e20ec9d5be816059f341af10f7',1,'header.h']]],
  ['sensor2',['SENSOR2',['../header_8h.html#a8732d399798b580cdffc834d4bfe2d24',1,'header.h']]],
  ['sensor2_5fport',['SENSOR2_PORT',['../header_8h.html#aa40a0953fada8406a071745124908364',1,'header.h']]],
  ['spi',['SPI',['../header_8h.html#aadd93900fc87105fa3ef514675d4133b',1,'header.h']]],
  ['spi_5fcs',['SPI_CS',['../header_8h.html#ade4259fa3cbb71732a4e73c18dcb9b0d',1,'header.h']]],
  ['spi_5fmiso',['SPI_MISO',['../header_8h.html#ab142cc77dfa97010c9d2b616d0992b64',1,'header.h']]],
  ['spi_5fmosi',['SPI_MOSI',['../header_8h.html#a7dbebab5f7dd57885adccf6711b13592',1,'header.h']]],
  ['spi_5fport',['SPI_PORT',['../header_8h.html#a8112c985f7444e82198d7571ce0a9160',1,'header.h']]],
  ['spi_5fscl',['SPI_SCL',['../header_8h.html#a2a4476ae47f5d5a8fc7957242f61710b',1,'header.h']]],
  ['stm8l_5fpower_5fdown_5ftime_5fsec',['STM8L_POWER_DOWN_TIME_SEC',['../config_8h.html#ac19155c1b5b6470a3ccce614b64e3ae4',1,'config.h']]]
];
