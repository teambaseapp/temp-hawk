var searchData=
[
  ['header_2eh',['header.h',['../header_8h.html',1,'']]],
  ['hsicalr',['HSICALR',['../struct_c_l_k__struct.html#a922d903f20e390f380f0257cc8930898',1,'CLK_struct']]],
  ['hsitrimr',['HSITRIMR',['../struct_c_l_k__struct.html#a3e1f2c192f6f44ecca14ae502c2fc511',1,'CLK_struct']]],
  ['hsiunlckr',['HSIUNLCKR',['../struct_c_l_k__struct.html#a4068bd6b82f7ad2ff4a60eb28729ac0f',1,'CLK_struct']]],
  ['htrh',['HTRH',['../struct_a_d_c__struct.html#a8fdd454b6f11f88f736180c52e51c343',1,'ADC_struct']]],
  ['htrl',['HTRL',['../struct_a_d_c__struct.html#a6a7ec312aae63a38070c25abefa4acb8',1,'ADC_struct']]],
  ['htu20d_5fcheckdevice',['HTU20D_CheckDevice',['../htu20dutils_8h.html#a78428a980dc469ee012f961b0efaf2ec',1,'HTU20D_CheckDevice():&#160;htu20dutils.c'],['../htu20dutils_8c.html#a78428a980dc469ee012f961b0efaf2ec',1,'HTU20D_CheckDevice():&#160;htu20dutils.c']]],
  ['htu20dpowerdown',['HTU20DPowerDown',['../htu20dutils_8h.html#a2285b5ba5262bb781ed7f0203446b0dc',1,'HTU20DPowerDown():&#160;htu20dutils.c'],['../htu20dutils_8c.html#a2285b5ba5262bb781ed7f0203446b0dc',1,'HTU20DPowerDown():&#160;htu20dutils.c']]],
  ['htu20dpowerup',['HTU20DPowerUp',['../htu20dutils_8h.html#aeef55e6e08ed8d506d9464c9b6d598d6',1,'HTU20DPowerUp():&#160;htu20dutils.c'],['../htu20dutils_8c.html#aeef55e6e08ed8d506d9464c9b6d598d6',1,'HTU20DPowerUp():&#160;htu20dutils.c']]],
  ['htu20dutils_2ec',['htu20dutils.c',['../htu20dutils_8c.html',1,'']]],
  ['htu20dutils_2eh',['htu20dutils.h',['../htu20dutils_8h.html',1,'']]],
  ['hardware_20crc_20calculation_20functions',['Hardware CRC Calculation functions',['../group___s_p_i___group3.html',1,'']]],
  ['halfduplex_20mode_20function',['Halfduplex mode function',['../group___u_s_a_r_t___group4.html',1,'']]]
];
