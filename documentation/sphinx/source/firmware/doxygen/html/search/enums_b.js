var searchData=
[
  ['spi_5fbaudrateprescaler_5ftypedef',['SPI_BaudRatePrescaler_TypeDef',['../group___s_p_i___baud_rate___prescaler.html#gafd08e5cb7cff2cd9cd8de72b8246961c',1,'stm8l15x_spi.h']]],
  ['spi_5fcpha_5ftypedef',['SPI_CPHA_TypeDef',['../group___s_p_i___clock___phase.html#ga78bd5e9d735e57d303e04e563a37cde4',1,'stm8l15x_spi.h']]],
  ['spi_5fcpol_5ftypedef',['SPI_CPOL_TypeDef',['../group___s_p_i___clock___polarity.html#ga41477365be2d18a10d6a339d231e4fdd',1,'stm8l15x_spi.h']]],
  ['spi_5fcrc_5ftypedef',['SPI_CRC_TypeDef',['../group___s_p_i___c_r_c.html#gaf4ec73111cfc25c10da22353123bfc12',1,'stm8l15x_spi.h']]],
  ['spi_5fdirection_5ftypedef',['SPI_Direction_TypeDef',['../group___s_p_i___direction.html#gac78f6a965850892d9f27db6c2eaa3798',1,'stm8l15x_spi.h']]],
  ['spi_5fdirectionmode_5ftypedef',['SPI_DirectionMode_TypeDef',['../group___s_p_i___direction___mode.html#ga240d445db61962084c2af41635ef6b6c',1,'stm8l15x_spi.h']]],
  ['spi_5fdmareq_5ftypedef',['SPI_DMAReq_TypeDef',['../group___s_p_i___d_m_a__requests.html#ga02f157f2862419fdd1069bf9bb2ca14e',1,'stm8l15x_spi.h']]],
  ['spi_5ffirstbit_5ftypedef',['SPI_FirstBit_TypeDef',['../group___s_p_i___frame___format.html#gadf3ac7d60a4329fdb2bd4058843775a9',1,'stm8l15x_spi.h']]],
  ['spi_5fflag_5ftypedef',['SPI_FLAG_TypeDef',['../group___s_p_i___flags.html#ga4f662a71fef63be8d3a0c7aa23199f71',1,'stm8l15x_spi.h']]],
  ['spi_5fit_5ftypedef',['SPI_IT_TypeDef',['../group___s_p_i___interrupts.html#gab3f320b40f3ccc9ca49baf7e21064c79',1,'stm8l15x_spi.h']]],
  ['spi_5fmode_5ftypedef',['SPI_Mode_TypeDef',['../group___s_p_i___mode.html#ga86cf6253be51ccc45f53b908cb42ba14',1,'stm8l15x_spi.h']]],
  ['spi_5fnss_5ftypedef',['SPI_NSS_TypeDef',['../group___s_p_i___slave_select___management.html#gab933301dd92b8210b299aeac7744b41e',1,'stm8l15x_spi.h']]],
  ['stm8l_5fmode',['STM8L_MODE',['../group___s_t_m8_l___mode.html#gaf71cb79eb7e1a54a7f0e2565b2807690',1,'header.h']]]
];
