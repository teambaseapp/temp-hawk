var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuwx",
  1: "abcdefgiloprstuw",
  2: "chmrs",
  3: "abcdefghilmnoprstuw",
  4: "abcdefghikloprstuwx",
  5: "abcdefgiloprstuw",
  6: "abcdefgilprstuw",
  7: "abcdefgilprstuw",
  8: "abcdefghilmnoprstuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Modules"
};

