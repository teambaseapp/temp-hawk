var group___t_i_m3___trigger___output___source =
[
    [ "TIM3_TRGOSource_TypeDef", "group___t_i_m3___trigger___output___source.html#gaeb510ce730e09210a993d0cac243c1d0", [
      [ "TIM3_TRGOSource_Reset", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0a5a56594ed0814ea8840adfeefdd40e37", null ],
      [ "TIM3_TRGOSource_Enable", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0abeab958b58b95c61356847b060bc5585", null ],
      [ "TIM3_TRGOSource_Update", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0ae4183cc4728ad3c4addcf9607a671f30", null ],
      [ "TIM3_TRGOSource_OC1", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0a11a6c6060e73be3cba0c7b0e7a41964e", null ],
      [ "TIM3_TRGOSource_OC1REF", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0afa2a339d53ad71b0d482669a1add8fda", null ],
      [ "TIM3_TRGOSource_OC2REF", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0adbc9f9e456179a3cd2deddaa4b6561f8", null ]
    ] ]
];