var group___c_o_m_p___inverting___input___selection =
[
    [ "COMP_InvertingInput_Typedef", "group___c_o_m_p___inverting___input___selection.html#gaacd9fca051beb79f6d8bfd9c8ec26ae9", [
      [ "COMP_InvertingInput_IO", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9a9c1f5b5ff61ac52afee457f8a941786c", null ],
      [ "COMP_InvertingInput_VREFINT", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9abd6749d804fe3f9eb8c40910fb011de8", null ],
      [ "COMP_InvertingInput_3_4VREFINT", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9a8cbf9a86e0ae04062043073d99f7665f", null ],
      [ "COMP_InvertingInput_1_2VREFINT", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9a59b6d17fb9c7fe1ea80f5f30fa2eceac", null ],
      [ "COMP_InvertingInput_1_4VREFINT", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9ac51a911dcaab8d16666a4d34366056ef", null ],
      [ "COMP_InvertingInput_DAC1", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9acd18eb133022fe8d6735ed3330db5cbe", null ],
      [ "COMP_InvertingInput_DAC2", "group___c_o_m_p___inverting___input___selection.html#ggaacd9fca051beb79f6d8bfd9c8ec26ae9af968302729dfabdd8b00f316ca00b0c5", null ]
    ] ]
];