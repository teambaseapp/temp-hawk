var group___s_p_i___interrupts =
[
    [ "SPI_IT_TypeDef", "group___s_p_i___interrupts.html#gab3f320b40f3ccc9ca49baf7e21064c79", [
      [ "SPI_IT_WKUP", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a1e36362aa2d402a1bea86703c5e59ac4", null ],
      [ "SPI_IT_OVR", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a8571b40767cc0fbe1ce2f13be17744ba", null ],
      [ "SPI_IT_MODF", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79ace001a946a88e2264624ad6ab4d6f97d", null ],
      [ "SPI_IT_CRCERR", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a8d08dafb78f10d0d91cec8f0554d342d", null ],
      [ "SPI_IT_TXE", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79ac8e8a597da5af13ec80eb9308a8c4c17", null ],
      [ "SPI_IT_RXNE", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a3e5010259a198fbcef23b51f7feffbd6", null ],
      [ "SPI_IT_ERR", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a4a8e5cb185214b84fa6c5b0ff715e0ef", null ]
    ] ]
];