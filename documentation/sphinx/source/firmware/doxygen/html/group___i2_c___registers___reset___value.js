var group___i2_c___registers___reset___value =
[
    [ "I2C_CCRH_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga292bdc25ab2c47f6fd7443fc82b601d5", null ],
    [ "I2C_CCRL_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga0c64b07ebc0bb49b4edf3baa1bd5fbfe", null ],
    [ "I2C_CR1_RESET_VALUE", "group___i2_c___registers___reset___value.html#gad9fb7e2277ce4dc45d658e3466adebf0", null ],
    [ "I2C_CR2_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga998e1c73f5d76260debb6ae551def044", null ],
    [ "I2C_DR_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga2157f6409db17deb7f37f44715a184d4", null ],
    [ "I2C_FREQR_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga85b52a876fe0411e42d07d72eb380179", null ],
    [ "I2C_ITR_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga0b8836158e6e2e96c089cd3c5e83e480", null ],
    [ "I2C_OAR2_RESET_VALUE", "group___i2_c___registers___reset___value.html#gac77247fd1b84f7c25a18793eb78fbe35", null ],
    [ "I2C_OARH_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga957aa843525c7cba90edf3df23dc0246", null ],
    [ "I2C_OARL_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga2002fccc7619b329ceb6ed13352fcb31", null ],
    [ "I2C_PECR_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga1f1824cb73b53345c6c22d0a3650c7c8", null ],
    [ "I2C_SR1_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga401660f4e6658cf2c84d684a40446140", null ],
    [ "I2C_SR2_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga90fb9ba417278aa26e4135e71e91f0c5", null ],
    [ "I2C_SR3_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga6fc6dcc7f3b6c92630a5b6f82b8b685e", null ],
    [ "I2C_TRISER_RESET_VALUE", "group___i2_c___registers___reset___value.html#ga9df83dfdf4b6c4ef978f7e017f7cd298", null ]
];