var group___s_y_s_c_f_g___registers___bits___definition =
[
    [ "SYSCFG_RMPCR1_ADC1DMA_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#ga9e9cf7332d4d3497242e2ed49d74af40", null ],
    [ "SYSCFG_RMPCR1_SPI1_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#ga5319896bb1a6d08ae0cc36b6f2c84206", null ],
    [ "SYSCFG_RMPCR1_TIM4DMA_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#ga943d2d02849ac1f0feff97e324e6606b", null ],
    [ "SYSCFG_RMPCR1_USART1CK_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#gadf0bec651dd3112082362c1c2e98fedd", null ],
    [ "SYSCFG_RMPCR1_USART1TR_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#ga1afebe8ef17607bf7c40ef4e97e9de79", null ],
    [ "SYSCFG_RMPCR2_ADC1TRIG_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#gafdc542812461a3b9d57e65f790ddd404", null ],
    [ "SYSCFG_RMPCR2_SPI2_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#ga90cf19255128c7dc745212a701882fa1", null ],
    [ "SYSCFG_RMPCR2_TIM23BKIN_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#gae53e1fd22416a7f416958fa0a09dff4c", null ],
    [ "SYSCFG_RMPCR2_TIM2TRIG_LSE", "group___s_y_s_c_f_g___registers___bits___definition.html#ga6b2df2a2264ffbb050b97b8a29779ef4", null ],
    [ "SYSCFG_RMPCR2_TIM2TRIG_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#gaf65f7913ee1f815bcaff787ba97e4a09", null ],
    [ "SYSCFG_RMPCR2_TIM3TRIG_LSE", "group___s_y_s_c_f_g___registers___bits___definition.html#ga5ef1b376f069be28c1beda691d95b81b", null ],
    [ "SYSCFG_RMPCR2_TIM3TRIG_REMAP1", "group___s_y_s_c_f_g___registers___bits___definition.html#ga57b2b8cda0505f071f162efca5a8416c", null ],
    [ "SYSCFG_RMPCR2_TIM3TRIG_REMAP2", "group___s_y_s_c_f_g___registers___bits___definition.html#ga1b32ebc40457394f53914aa7f5e1f052", null ],
    [ "SYSCFG_RMPCR3_CCO_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#gab4bc712748f9ebb4e690ef16b93bdd31", null ],
    [ "SYSCFG_RMPCR3_SPI1_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#ga255f1709634c89dbd743e16c7e3e1939", null ],
    [ "SYSCFG_RMPCR3_TIM3CH1_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#gab68f1ac14e2df7a23ada3f1cc4f37662", null ],
    [ "SYSCFG_RMPCR3_TIM3CH2_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#gad9f274e1cc77d378a9f62076009f6add", null ],
    [ "SYSCFG_RMPCR3_USART3CK_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#ga30e9354d7f98e1a5f7ea54511185e875", null ],
    [ "SYSCFG_RMPCR3_USART3TR_REMAP", "group___s_y_s_c_f_g___registers___bits___definition.html#gac83f6916dee290192f6c8201e7d3f78c", null ]
];