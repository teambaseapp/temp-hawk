var stm8l15x__pwr_8h =
[
    [ "PWR_FLAG_TypeDef", "group___p_w_r___flag.html#ga1e89a5abee1d14b30e04dd70d86bc488", [
      [ "PWR_FLAG_PVDOF", "group___p_w_r___flag.html#gga1e89a5abee1d14b30e04dd70d86bc488add51f851905597106b495033ba589f07", null ],
      [ "PWR_FLAG_PVDIF", "group___p_w_r___flag.html#gga1e89a5abee1d14b30e04dd70d86bc488a276c97e6f9677aab7d81e870804fef78", null ],
      [ "PWR_FLAG_VREFINTF", "group___p_w_r___flag.html#gga1e89a5abee1d14b30e04dd70d86bc488a9661b385163edd76a64674e94a83b2c3", null ]
    ] ],
    [ "PWR_PVDLevel_TypeDef", "group___p_v_d__detection__level.html#gabdea2f5e923f8f1355a34e90ffecb15a", [
      [ "PWR_PVDLevel_1V85", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aa5c2d589e2c99e179be70a8fd1e9e7bc5", null ],
      [ "PWR_PVDLevel_2V05", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aabf6aa729ee87ef0480243491f2a868cb", null ],
      [ "PWR_PVDLevel_2V26", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aafe625229ac9c6397631e8cbb9603684d", null ],
      [ "PWR_PVDLevel_2V45", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aadeca25b4c82dcbdd41688a55fa3ef1c5", null ],
      [ "PWR_PVDLevel_2V65", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aa649c95b7883bebe4fbcb1d39d3530ab9", null ],
      [ "PWR_PVDLevel_2V85", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aaae0431dd00eb78cfb3d2952d74f39340", null ],
      [ "PWR_PVDLevel_3V05", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aac28f7cc806f81d8dd6c618191e9f91f0", null ],
      [ "PWR_PVDLevel_PVDIn", "group___p_v_d__detection__level.html#ggabdea2f5e923f8f1355a34e90ffecb15aabc795b35879afc753f6d767b24c907df", null ]
    ] ],
    [ "PWR_DeInit", "group___p_w_r.html#gad03a0aac7bc3bc3a9fd012f3769a6990", null ],
    [ "PWR_FastWakeUpCmd", "group___p_w_r.html#ga548792d51d227ff55f475bcb9d67573f", null ],
    [ "PWR_GetFlagStatus", "group___p_w_r.html#gab84db49950a589c95bbabb62320f25e8", null ],
    [ "PWR_PVDClearFlag", "group___p_w_r.html#ga569fea4abf788c628c79a03e12b18fea", null ],
    [ "PWR_PVDClearITPendingBit", "group___p_w_r.html#ga271e9cdf724871f7133788bbfe870a31", null ],
    [ "PWR_PVDCmd", "group___p_w_r.html#ga42cad476b816e0a33594a933b3ed1acd", null ],
    [ "PWR_PVDGetITStatus", "group___p_w_r.html#ga6015722ce63503e87d7683b0ad0da6b9", null ],
    [ "PWR_PVDITConfig", "group___p_w_r.html#ga0b582081368ee74e570d4e84a3400558", null ],
    [ "PWR_PVDLevelConfig", "group___p_w_r.html#ga8cddc7e5670d82acd64480b604fb1655", null ],
    [ "PWR_UltraLowPowerCmd", "group___p_w_r.html#ga862a54f4cdf1cec028432db1068419d1", null ]
];