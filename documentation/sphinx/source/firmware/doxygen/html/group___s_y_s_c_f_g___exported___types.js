var group___s_y_s_c_f_g___exported___types =
[
    [ "RI_Input_Capture", "group___r_i___input___capture.html", "group___r_i___input___capture" ],
    [ "RI_Input_Capture_Routing", "group___r_i___input___capture___routing.html", "group___r_i___input___capture___routing" ],
    [ "RI_Analog_Switch", "group___r_i___analog___switch.html", "group___r_i___analog___switch" ],
    [ "RI_IO_Switch", "group___r_i___i_o___switch.html", "group___r_i___i_o___switch" ],
    [ "RI_Resistor", "group___r_i___resistor.html", "group___r_i___resistor" ],
    [ "REMAP_Pin", "group___r_e_m_a_p___pin.html", "group___r_e_m_a_p___pin" ],
    [ "REMAP_DMA_Channel", "group___r_e_m_a_p___d_m_a___channel.html", "group___r_e_m_a_p___d_m_a___channel" ]
];