var struct_t_i_m4__struct =
[
    [ "ARR", "struct_t_i_m4__struct.html#a2abfac5755d0ba21977ceb867d84b7dd", null ],
    [ "CNTR", "struct_t_i_m4__struct.html#a0a170cc967beee2277d947e156a98b94", null ],
    [ "CR1", "struct_t_i_m4__struct.html#abdb5e2aed90a3a46151c8bb740665579", null ],
    [ "CR2", "struct_t_i_m4__struct.html#a27d59494343f46e0df9e3d0caf483896", null ],
    [ "DER", "struct_t_i_m4__struct.html#ad0f4e625ce2740b7705bcdf09fc74da5", null ],
    [ "EGR", "struct_t_i_m4__struct.html#a60899af57b319421718b22d2fe9a690a", null ],
    [ "IER", "struct_t_i_m4__struct.html#a1a495ea313f2d5841a0bfd8b0f2ae12a", null ],
    [ "PSCR", "struct_t_i_m4__struct.html#a3056b9700a4fabc0a92df40b100dc51a", null ],
    [ "SMCR", "struct_t_i_m4__struct.html#a76bf4131aa944b6a36ce0935613a1f75", null ],
    [ "SR1", "struct_t_i_m4__struct.html#a590671f863bb6c58ca091b5d9180f8ab", null ]
];