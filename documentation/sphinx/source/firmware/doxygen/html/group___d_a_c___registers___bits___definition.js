var group___d_a_c___registers___bits___definition =
[
    [ "DAC_CR1_BOFF", "group___d_a_c___registers___bits___definition.html#ga62c635571992d977352f6110f94c6ece", null ],
    [ "DAC_CR1_EN", "group___d_a_c___registers___bits___definition.html#ga354dce66eb0756f7bfd96565db3f323e", null ],
    [ "DAC_CR1_TEN", "group___d_a_c___registers___bits___definition.html#gac7cdeeb41d167a1d6db028aa6aa630ed", null ],
    [ "DAC_CR1_TSEL", "group___d_a_c___registers___bits___definition.html#ga90126c47f838ff13ac7f84ae21bf6703", null ],
    [ "DAC_CR1_WAVEN", "group___d_a_c___registers___bits___definition.html#gad291d7442fa353bb191d7d38b666b70b", null ],
    [ "DAC_CR2_DMAEN", "group___d_a_c___registers___bits___definition.html#gaf7ad82a0b1fbe789cb57d253361e6c2f", null ],
    [ "DAC_CR2_DMAUDRIE", "group___d_a_c___registers___bits___definition.html#ga1eb6896a97c16cf7cdcf29c0655cc6c1", null ],
    [ "DAC_CR2_MAMPx", "group___d_a_c___registers___bits___definition.html#ga09a838880d97505d9fc8412bd2fc88a9", null ],
    [ "DAC_DHR8_8DHR", "group___d_a_c___registers___bits___definition.html#ga3e51c908e75ab60fc3f82956b74fdbe5", null ],
    [ "DAC_DORH_DORH", "group___d_a_c___registers___bits___definition.html#gadd9e1beb936add798bd0a6ae5d656d97", null ],
    [ "DAC_DORL_DORL", "group___d_a_c___registers___bits___definition.html#gadf86c88065536b30dc8559e0bd1814ef", null ],
    [ "DAC_LDHRH_LDHRH", "group___d_a_c___registers___bits___definition.html#ga3a0a459e80f940478294f9714fb77364", null ],
    [ "DAC_LDHRL_LDHRL", "group___d_a_c___registers___bits___definition.html#gad01679fd396a4ecd0615d733bb60b3f6", null ],
    [ "DAC_RDHRH_RDHRH", "group___d_a_c___registers___bits___definition.html#gaa4ed3ded141123d79daccc3b4b39c6a2", null ],
    [ "DAC_RDHRL_RDHRL", "group___d_a_c___registers___bits___definition.html#ga9f012690577828cd49fcdfb9ce804c75", null ],
    [ "DAC_SR_DMAUDR1", "group___d_a_c___registers___bits___definition.html#ga7d2048d6b521fb0946dc8c4e577a49c0", null ],
    [ "DAC_SR_DMAUDR2", "group___d_a_c___registers___bits___definition.html#gaf16e48ab85d9261c5b599c56b14aea5d", null ],
    [ "DAC_SWTRIGR_SWTRIG1", "group___d_a_c___registers___bits___definition.html#ga970ef02dffaceb35ff1dd7aceb67acdd", null ],
    [ "DAC_SWTRIGR_SWTRIG2", "group___d_a_c___registers___bits___definition.html#gaf0e53585b505d21f5c457476bd5a18f8", null ]
];