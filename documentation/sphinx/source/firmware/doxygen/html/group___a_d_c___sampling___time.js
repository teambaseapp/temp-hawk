var group___a_d_c___sampling___time =
[
    [ "ADC_SamplingTime_TypeDef", "group___a_d_c___sampling___time.html#ga08e5d23eefe177d2b552d4712ec3d570", [
      [ "ADC_SamplingTime_4Cycles", "group___a_d_c___sampling___time.html#gga08e5d23eefe177d2b552d4712ec3d570ace9d9dd0830b269ec11092ded19a0ac3", null ],
      [ "ADC_SamplingTime_9Cycles", "group___a_d_c___sampling___time.html#gga08e5d23eefe177d2b552d4712ec3d570a28a59d14a2a775e8534984b159acde99", null ],
      [ "ADC_SamplingTime_16Cycles", "group___a_d_c___sampling___time.html#gga08e5d23eefe177d2b552d4712ec3d570a39819195b21194ec2ac0b54837d3ddef", null ],
      [ "ADC_SamplingTime_24Cycles", "group___a_d_c___sampling___time.html#gga08e5d23eefe177d2b552d4712ec3d570ae8103c4c81e2d2306bdebdfefbb6a802", null ],
      [ "ADC_SamplingTime_48Cycles", "group___a_d_c___sampling___time.html#gga08e5d23eefe177d2b552d4712ec3d570ac2be0ab8f423951ab1327910b301174f", null ],
      [ "ADC_SamplingTime_96Cycles", "group___a_d_c___sampling___time.html#gga08e5d23eefe177d2b552d4712ec3d570a4fa7c43f08651f22768c6d3d7aaa281f", null ],
      [ "ADC_SamplingTime_192Cycles", "group___a_d_c___sampling___time.html#gga08e5d23eefe177d2b552d4712ec3d570ae3299705c0feb8a87df615769f431d2b", null ],
      [ "ADC_SamplingTime_384Cycles", "group___a_d_c___sampling___time.html#gga08e5d23eefe177d2b552d4712ec3d570afb300356336f0416d333ab62cc09a5e9", null ]
    ] ]
];