var stm8l15x__spi_8h =
[
    [ "SPI_BaudRatePrescaler_TypeDef", "group___s_p_i___baud_rate___prescaler.html#gafd08e5cb7cff2cd9cd8de72b8246961c", [
      [ "SPI_BaudRatePrescaler_2", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961cae3fa031ff038fcb3c491d28779b1e26b", null ],
      [ "SPI_BaudRatePrescaler_4", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961ca7cf658ad85d48091d3c1fcc050a6efb1", null ],
      [ "SPI_BaudRatePrescaler_8", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961ca276c22abb80cac9f2a074339393e7979", null ],
      [ "SPI_BaudRatePrescaler_16", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961cae2e2d51474cb5863a083c4c8134ed2a3", null ],
      [ "SPI_BaudRatePrescaler_32", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961ca9434623cfd2ea9d3a1147b815a56d412", null ],
      [ "SPI_BaudRatePrescaler_64", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961cacc4485daac112a6e27bc03dd41894e1b", null ],
      [ "SPI_BaudRatePrescaler_128", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961caeb9ded1982accafea3e4729621788872", null ],
      [ "SPI_BaudRatePrescaler_256", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961ca6aad64e4c3a18db5825cbd2612305bd5", null ]
    ] ],
    [ "SPI_CPHA_TypeDef", "group___s_p_i___clock___phase.html#ga78bd5e9d735e57d303e04e563a37cde4", [
      [ "SPI_CPHA_1Edge", "group___s_p_i___clock___phase.html#gga78bd5e9d735e57d303e04e563a37cde4a404d29e5c96e7790ceda4be6156334e4", null ],
      [ "SPI_CPHA_2Edge", "group___s_p_i___clock___phase.html#gga78bd5e9d735e57d303e04e563a37cde4a430fd63456418eb9e90d3c314f2dc658", null ]
    ] ],
    [ "SPI_CPOL_TypeDef", "group___s_p_i___clock___polarity.html#ga41477365be2d18a10d6a339d231e4fdd", [
      [ "SPI_CPOL_Low", "group___s_p_i___clock___polarity.html#gga41477365be2d18a10d6a339d231e4fdda7a59bd7fc76c62dc0bf6e57956dcae4f", null ],
      [ "SPI_CPOL_High", "group___s_p_i___clock___polarity.html#gga41477365be2d18a10d6a339d231e4fdda4bc1f087b8d02c4858efe1163a58d24a", null ]
    ] ],
    [ "SPI_CRC_TypeDef", "group___s_p_i___c_r_c.html#gaf4ec73111cfc25c10da22353123bfc12", [
      [ "SPI_CRC_RX", "group___s_p_i___c_r_c.html#ggaf4ec73111cfc25c10da22353123bfc12a61b4fb023cf87d6638f5d8b2c4f4b4d8", null ],
      [ "SPI_CRC_TX", "group___s_p_i___c_r_c.html#ggaf4ec73111cfc25c10da22353123bfc12adf85212c2961862b294f3295a483454b", null ]
    ] ],
    [ "SPI_Direction_TypeDef", "group___s_p_i___direction.html#gac78f6a965850892d9f27db6c2eaa3798", [
      [ "SPI_Direction_Rx", "group___s_p_i___direction.html#ggac78f6a965850892d9f27db6c2eaa3798aa57c1c9ca239962a5eac67cbeeaee765", null ],
      [ "SPI_Direction_Tx", "group___s_p_i___direction.html#ggac78f6a965850892d9f27db6c2eaa3798af591654a87615fa3315518b709e066c2", null ]
    ] ],
    [ "SPI_DirectionMode_TypeDef", "group___s_p_i___direction___mode.html#ga240d445db61962084c2af41635ef6b6c", [
      [ "SPI_Direction_2Lines_FullDuplex", "group___s_p_i___direction___mode.html#gga240d445db61962084c2af41635ef6b6ca71992216e4296704e7067bd8e933c318", null ],
      [ "SPI_Direction_2Lines_RxOnly", "group___s_p_i___direction___mode.html#gga240d445db61962084c2af41635ef6b6ca18dd82351780eaacb0da2ee356423fce", null ],
      [ "SPI_Direction_1Line_Rx", "group___s_p_i___direction___mode.html#gga240d445db61962084c2af41635ef6b6cabbbb9d8f08ae8c76a078145b0b53b461", null ],
      [ "SPI_Direction_1Line_Tx", "group___s_p_i___direction___mode.html#gga240d445db61962084c2af41635ef6b6ca57c7ec178e7d3951b483cee4e85e1e19", null ]
    ] ],
    [ "SPI_DMAReq_TypeDef", "group___s_p_i___d_m_a__requests.html#ga02f157f2862419fdd1069bf9bb2ca14e", [
      [ "SPI_DMAReq_RX", "group___s_p_i___d_m_a__requests.html#gga02f157f2862419fdd1069bf9bb2ca14ea5c3aabbb4f384abfe23abe2c577a8af0", null ],
      [ "SPI_DMAReq_TX", "group___s_p_i___d_m_a__requests.html#gga02f157f2862419fdd1069bf9bb2ca14eaa903fa36abc93d3309c6fc5d16043440", null ]
    ] ],
    [ "SPI_FirstBit_TypeDef", "group___s_p_i___frame___format.html#gadf3ac7d60a4329fdb2bd4058843775a9", [
      [ "SPI_FirstBit_MSB", "group___s_p_i___frame___format.html#ggadf3ac7d60a4329fdb2bd4058843775a9a1e265070c51e612b1abe2140aa0a301a", null ],
      [ "SPI_FirstBit_LSB", "group___s_p_i___frame___format.html#ggadf3ac7d60a4329fdb2bd4058843775a9a83f89ca551c11568a1a1c6c147e845ec", null ]
    ] ],
    [ "SPI_FLAG_TypeDef", "group___s_p_i___flags.html#ga4f662a71fef63be8d3a0c7aa23199f71", [
      [ "SPI_FLAG_BSY", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a994865b0a4e40851a11e7f8c3361d65f", null ],
      [ "SPI_FLAG_OVR", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71aa42cf678ab63a86e8102daf3b3da1f2f", null ],
      [ "SPI_FLAG_MODF", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a15c1b612cdcd72c5533efef278020f56", null ],
      [ "SPI_FLAG_CRCERR", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a64c97948ca0770650f215d085883bba1", null ],
      [ "SPI_FLAG_WKUP", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a73dd6b0fcc56efe5dba09db395a838dd", null ],
      [ "SPI_FLAG_TXE", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a9bec46ed6c69999c610a7e92fa7782b2", null ],
      [ "SPI_FLAG_RXNE", "group___s_p_i___flags.html#gga4f662a71fef63be8d3a0c7aa23199f71a080bfbf635102da80faacb37a2346f56", null ]
    ] ],
    [ "SPI_IT_TypeDef", "group___s_p_i___interrupts.html#gab3f320b40f3ccc9ca49baf7e21064c79", [
      [ "SPI_IT_WKUP", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a1e36362aa2d402a1bea86703c5e59ac4", null ],
      [ "SPI_IT_OVR", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a8571b40767cc0fbe1ce2f13be17744ba", null ],
      [ "SPI_IT_MODF", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79ace001a946a88e2264624ad6ab4d6f97d", null ],
      [ "SPI_IT_CRCERR", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a8d08dafb78f10d0d91cec8f0554d342d", null ],
      [ "SPI_IT_TXE", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79ac8e8a597da5af13ec80eb9308a8c4c17", null ],
      [ "SPI_IT_RXNE", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a3e5010259a198fbcef23b51f7feffbd6", null ],
      [ "SPI_IT_ERR", "group___s_p_i___interrupts.html#ggab3f320b40f3ccc9ca49baf7e21064c79a4a8e5cb185214b84fa6c5b0ff715e0ef", null ]
    ] ],
    [ "SPI_Mode_TypeDef", "group___s_p_i___mode.html#ga86cf6253be51ccc45f53b908cb42ba14", [
      [ "SPI_Mode_Master", "group___s_p_i___mode.html#gga86cf6253be51ccc45f53b908cb42ba14a27907a819fe089bc6a9ea1dae15bf7ab", null ],
      [ "SPI_Mode_Slave", "group___s_p_i___mode.html#gga86cf6253be51ccc45f53b908cb42ba14abc31e0f5c5fa48a51a30c291d04e0821", null ]
    ] ],
    [ "SPI_NSS_TypeDef", "group___s_p_i___slave_select___management.html#gab933301dd92b8210b299aeac7744b41e", [
      [ "SPI_NSS_Soft", "group___s_p_i___slave_select___management.html#ggab933301dd92b8210b299aeac7744b41ea9157cc888d2b13c6fb5716ac5f46e558", null ],
      [ "SPI_NSS_Hard", "group___s_p_i___slave_select___management.html#ggab933301dd92b8210b299aeac7744b41ea708e21b749ef6c03f5707bd9c0b486f1", null ]
    ] ],
    [ "SPI_BiDirectionalLineConfig", "group___s_p_i.html#ga50646069062aa24309e80de3d2c5f0ea", null ],
    [ "SPI_CalculateCRCCmd", "group___s_p_i.html#gaa936b41c6f1c8010749991a7f1014d9b", null ],
    [ "SPI_ClearFlag", "group___s_p_i.html#ga84cc626656a536a16f5c0c0bbf896686", null ],
    [ "SPI_ClearITPendingBit", "group___s_p_i.html#gacc28dcf2c3ee71bd2181945ef7bf85bc", null ],
    [ "SPI_Cmd", "group___s_p_i.html#gaa31357879a65ee1ed7223f3b9114dcf3", null ],
    [ "SPI_DeInit", "group___s_p_i.html#gae33578348c53fb7140d994563bdf8ae4", null ],
    [ "SPI_DMACmd", "group___s_p_i.html#ga2a486478b63bfaee5502287ed2b494b0", null ],
    [ "SPI_GetCRC", "group___s_p_i.html#gafccc31b40ebfd15731224471594a7669", null ],
    [ "SPI_GetCRCPolynomial", "group___s_p_i.html#ga05cdf3960458dfa79cba51ea58480162", null ],
    [ "SPI_GetFlagStatus", "group___s_p_i.html#gabf038f294dae6cce6bec69363efbbbbe", null ],
    [ "SPI_GetITStatus", "group___s_p_i.html#ga751336d92aedea121635bd9f62fbc57f", null ],
    [ "SPI_Init", "group___s_p_i.html#gacac5219c062caa8f557e47100b56a9ae", null ],
    [ "SPI_ITConfig", "group___s_p_i.html#ga372b5fe70c731c2292c394a0b84434c3", null ],
    [ "SPI_NSSInternalSoftwareCmd", "group___s_p_i.html#gac1f0e025eb134259716595eb958de87d", null ],
    [ "SPI_ReceiveData", "group___s_p_i.html#gae9642bca158c93c67cd25e2df85c66ca", null ],
    [ "SPI_ResetCRC", "group___s_p_i.html#ga27ad061b3ab8e3492bfde4e9d573e1db", null ],
    [ "SPI_SendData", "group___s_p_i.html#gad860e6d7900346070bc7840e12d8a897", null ],
    [ "SPI_TransmitCRC", "group___s_p_i.html#gace8b1058e09bab150b0dbe5978810273", null ]
];