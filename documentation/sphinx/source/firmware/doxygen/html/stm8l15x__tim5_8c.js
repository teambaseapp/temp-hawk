var stm8l15x__tim5_8c =
[
    [ "TIM5_ARRPreloadConfig", "group___t_i_m5___group1.html#ga028e838bbdf926145f0c0df0a42cbfb2", null ],
    [ "TIM5_BKRConfig", "group___t_i_m5___group2.html#ga7175b49cdc94615438db911b3ae67219", null ],
    [ "TIM5_CCxCmd", "group___t_i_m5___group2.html#ga869d6041a28bd998489231dafa195e85", null ],
    [ "TIM5_ClearFlag", "group___t_i_m5___group4.html#ga8bd56e6dcb6ad69042cade7eccaa0312", null ],
    [ "TIM5_ClearITPendingBit", "group___t_i_m5___group4.html#ga662ce45c3bbc00018d160b9df8e65360", null ],
    [ "TIM5_Cmd", "group___t_i_m5___group1.html#ga7398d4769dc83ee17dd88477286e8e33", null ],
    [ "TIM5_CounterModeConfig", "group___t_i_m5___group1.html#ga53f10ed4d9ba371fbc4b42f08bfdbe70", null ],
    [ "TIM5_CtrlPWMOutputs", "group___t_i_m5___group2.html#gae5d298e623fd220441989a7182e43eb2", null ],
    [ "TIM5_DeInit", "group___t_i_m5___group1.html#gafa10e2573fe38ee52ff48c95960f87a3", null ],
    [ "TIM5_DMACmd", "group___t_i_m5___group4.html#ga0d9314194f748725e38ffd3a9e263cb2", null ],
    [ "TIM5_EncoderInterfaceConfig", "group___t_i_m5___group7.html#gad6e0b6e64d89430557442bd1ee371842", null ],
    [ "TIM5_ETRClockMode1Config", "group___t_i_m5___group5.html#ga6688e6d33bd0b5bcf85e86f963163ab5", null ],
    [ "TIM5_ETRClockMode2Config", "group___t_i_m5___group5.html#ga33f527e86554fa2bb8e4bf5278e8c8f5", null ],
    [ "TIM5_ETRConfig", "group___t_i_m5___group6.html#ga0a5784a46375fce22864dc3b021feeaa", null ],
    [ "TIM5_ForcedOC1Config", "group___t_i_m5___group2.html#ga94e625d9ac70bc97a0e8c5b142af9942", null ],
    [ "TIM5_ForcedOC2Config", "group___t_i_m5___group2.html#ga2194c1941550a024daf07af7ce1e0843", null ],
    [ "TIM5_GenerateEvent", "group___t_i_m5___group4.html#gab432a14c278f430e674b7c52a8d13e24", null ],
    [ "TIM5_GetCapture1", "group___t_i_m5___group3.html#gadce87cdf2f02645f64b16012f4536d6a", null ],
    [ "TIM5_GetCapture2", "group___t_i_m5___group3.html#ga37ae416e15ff96bed14126defd6568e4", null ],
    [ "TIM5_GetCounter", "group___t_i_m5___group1.html#gaaef6e616cd97ca6bfdc4d29b086d2a5f", null ],
    [ "TIM5_GetFlagStatus", "group___t_i_m5___group4.html#ga3898fff5e733720fe7fa0467d35b33f9", null ],
    [ "TIM5_GetITStatus", "group___t_i_m5___group4.html#gab879fde33535c32f9c031431a0c328bd", null ],
    [ "TIM5_GetPrescaler", "group___t_i_m5___group1.html#ga20ee0b9891042ac01c0563b81e98500f", null ],
    [ "TIM5_ICInit", "group___t_i_m5___group3.html#gad7295ea4e0d1c758775569ebdb0e9425", null ],
    [ "TIM5_InternalClockConfig", "group___t_i_m5___group5.html#gaa3045d85bc98b193e9adfc2eae4c9ca9", null ],
    [ "TIM5_ITConfig", "group___t_i_m5___group4.html#gaf117cf95222101910eb9513821433871", null ],
    [ "TIM5_OC1FastConfig", "group___t_i_m5___group2.html#ga659db37fcb9eaa35d3b05bb848b7930c", null ],
    [ "TIM5_OC1Init", "group___t_i_m5___group2.html#gac819de5f9b152d46deab5ffd3259808a", null ],
    [ "TIM5_OC1PolarityConfig", "group___t_i_m5___group2.html#ga496cda36432f4a5c920d8514efc308b3", null ],
    [ "TIM5_OC1PreloadConfig", "group___t_i_m5___group2.html#ga90f17f570e5f3ce99855acecd7631df1", null ],
    [ "TIM5_OC2FastConfig", "group___t_i_m5___group2.html#gaf5947b4436a247b09c0d4b6d1efdce90", null ],
    [ "TIM5_OC2Init", "group___t_i_m5___group2.html#ga2d91748bec2748703bcee424647eb4d8", null ],
    [ "TIM5_OC2PolarityConfig", "group___t_i_m5___group2.html#gac71dd04fdb47ef8f2ffe5ed0c96a069d", null ],
    [ "TIM5_OC2PreloadConfig", "group___t_i_m5___group2.html#ga2d6d670f1dbc8b373ff74630f3abe7af", null ],
    [ "TIM5_PrescalerConfig", "group___t_i_m5___group1.html#gab80fdb3c07a11d46b09b268ad13b853d", null ],
    [ "TIM5_PWMIConfig", "group___t_i_m5___group3.html#ga560f717b4a369056cf5e81669d6dbfa9", null ],
    [ "TIM5_SelectCCDMA", "group___t_i_m5___group4.html#ga523d3459687ba79f669522db077f16c4", null ],
    [ "TIM5_SelectHallSensor", "group___t_i_m5___group7.html#gae98d8a10d1685d75304163fbac2eae16", null ],
    [ "TIM5_SelectInputTrigger", "group___t_i_m5___group6.html#ga27f2f833fbe047f14cf8311bccdeb977", null ],
    [ "TIM5_SelectMasterSlaveMode", "group___t_i_m5___group6.html#gaa545b8b6e169a5d6b86eaa11d7fb2f33", null ],
    [ "TIM5_SelectOCxM", "group___t_i_m5___group2.html#ga0b792818264874ef476999fe0e018864", null ],
    [ "TIM5_SelectOnePulseMode", "group___t_i_m5___group1.html#gabad8083e39d0afd84a0b399607fa4404", null ],
    [ "TIM5_SelectOutputTrigger", "group___t_i_m5___group6.html#ga2a51d91552b089566f58e1af8c7923ae", null ],
    [ "TIM5_SelectSlaveMode", "group___t_i_m5___group6.html#ga9dc79c20fdeec4bde35e8ed5d019022b", null ],
    [ "TIM5_SetAutoreload", "group___t_i_m5___group1.html#gac089b0a3758bc0baabf25d1854117f19", null ],
    [ "TIM5_SetCompare1", "group___t_i_m5___group2.html#ga848fbefa25baa2ea26260b8941b7a9bc", null ],
    [ "TIM5_SetCompare2", "group___t_i_m5___group2.html#ga0de74897146ade82ca1bbc7fb2cd55ed", null ],
    [ "TIM5_SetCounter", "group___t_i_m5___group1.html#gab9922abb371c6bd777872d91e135e490", null ],
    [ "TIM5_SetIC1Prescaler", "group___t_i_m5___group3.html#ga578de8b51215192a140453f654441933", null ],
    [ "TIM5_SetIC2Prescaler", "group___t_i_m5___group3.html#gac69828b04314da2328375812c50d5d77", null ],
    [ "TIM5_TimeBaseInit", "group___t_i_m5___group1.html#ga9168c46c087454475d8d6b977650a141", null ],
    [ "TIM5_TIxExternalClockConfig", "group___t_i_m5___group5.html#gaf6a9e78ee41fcdb0d538cb5051a8b665", null ],
    [ "TIM5_UpdateDisableConfig", "group___t_i_m5___group1.html#gaba4bf3912e855633a5b3db931edd2a6f", null ],
    [ "TIM5_UpdateRequestConfig", "group___t_i_m5___group1.html#ga27850fca7a4c129a59d1026e024fac4a", null ]
];