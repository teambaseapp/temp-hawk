var group___d_a_c__trigger__selection =
[
    [ "DAC_Trigger_TypeDef", "group___d_a_c__trigger__selection.html#ga250d061d0f223ae8327008cc0a1b177e", [
      [ "DAC_Trigger_None", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177ea2a84d20905a37456d852b7918229ddd2", null ],
      [ "DAC_Trigger_T4_TRGO", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177eab830042207686889f7cec2277fb6770d", null ],
      [ "DAC_Trigger_T5_TRGO", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177ea83e6d7648b308fe6a3f89ef9bdb75dbd", null ],
      [ "DAC_Trigger_Ext", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177eafa60873154797f8ddfc7c3ea4064132e", null ],
      [ "DAC_Trigger_Software", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177eac78196bbfac35147d46f3ede4254a8a0", null ]
    ] ]
];