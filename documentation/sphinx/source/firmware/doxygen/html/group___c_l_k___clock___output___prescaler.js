var group___c_l_k___clock___output___prescaler =
[
    [ "CLK_CCODiv_TypeDef", "group___c_l_k___clock___output___prescaler.html#ga5e5d368cbb91b209097dae3e2c7e80f5", [
      [ "CLK_CCODiv_1", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a38c825dc0abc601153d4586d4676efd5", null ],
      [ "CLK_CCODiv_2", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5ab0c1debf3576078b5fef825d63c38a71", null ],
      [ "CLK_CCODiv_4", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a5f67fd0fd157a4cbd64cf7a843c3ae2c", null ],
      [ "CLK_CCODiv_8", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a74d6b67b368c43fb35004d75e833a3d5", null ],
      [ "CLK_CCODiv_16", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a96ec091b27ed94caf803ad1bd789ac0d", null ],
      [ "CLK_CCODiv_32", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a429ca54fd0fbc2d31004f33d50369e78", null ],
      [ "CLK_CCODiv_64", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5adc9fc75ad8914cbf1ad6cf8df60e44fa", null ]
    ] ]
];