var stm8l15x__tim3_8h =
[
    [ "TIM3_AutomaticOutput_TypeDef", "group___t_i_m3___automatic___output.html#gaeab9e40c70da17e74fdbabe724d2dcf4", [
      [ "TIM3_AutomaticOutput_Enable", "group___t_i_m3___automatic___output.html#ggaeab9e40c70da17e74fdbabe724d2dcf4aad7b4b6f92dfa78b0ccbe2b8b9b7cd6d", null ],
      [ "TIM3_AutomaticOutput_Disable", "group___t_i_m3___automatic___output.html#ggaeab9e40c70da17e74fdbabe724d2dcf4a0cacfbe6eea58a1ae338fd67c94d8964", null ]
    ] ],
    [ "TIM3_BreakPolarity_TypeDef", "group___t_i_m3___break___polarity.html#ga4d3b74853f0ce627c91a3ad000f87f1d", [
      [ "TIM3_BreakPolarity_High", "group___t_i_m3___break___polarity.html#gga4d3b74853f0ce627c91a3ad000f87f1dacf3647fb8742337cf30b4f375c279fb8", null ],
      [ "TIM3_BreakPolarity_Low", "group___t_i_m3___break___polarity.html#gga4d3b74853f0ce627c91a3ad000f87f1da5b81799965087c073396637c9fd19810", null ]
    ] ],
    [ "TIM3_BreakState_TypeDef", "group___t_i_m3___break___state.html#ga4984b28878ba659e4332b6a4f352fa43", [
      [ "TIM3_BreakState_Disable", "group___t_i_m3___break___state.html#gga4984b28878ba659e4332b6a4f352fa43af2c8ff93e9f59518d69a59e370a98efa", null ],
      [ "TIM3_BreakState_Enable", "group___t_i_m3___break___state.html#gga4984b28878ba659e4332b6a4f352fa43a5d13854b63fa7c9370f2efa56e35062d", null ]
    ] ],
    [ "TIM3_Channel_TypeDef", "group___t_i_m3___channel.html#gafd49f5caf2bdcc8ea5f242e1eb1974f4", [
      [ "TIM3_Channel_1", "group___t_i_m3___channel.html#ggafd49f5caf2bdcc8ea5f242e1eb1974f4a088132dd93bc6e70b3e4ca4fb38ec255", null ],
      [ "TIM3_Channel_2", "group___t_i_m3___channel.html#ggafd49f5caf2bdcc8ea5f242e1eb1974f4a7045162526c0891bf26b41b422d2d57b", null ]
    ] ],
    [ "TIM3_CounterMode_TypeDef", "group___t_i_m3___counter_mode.html#ga39fd7fc87071542d8c2a6b4eb75d4f57", [
      [ "TIM3_CounterMode_Up", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57afc218d81454b1534a1e01ccb535cb803", null ],
      [ "TIM3_CounterMode_Down", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57af104aaf2ce189fafaad8b26a661a8aa7", null ],
      [ "TIM3_CounterMode_CenterAligned1", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57ade3b9ca14d8aa2d285ba32ef61e97827", null ],
      [ "TIM3_CounterMode_CenterAligned2", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57a54e0cf4777a6dcea3eedff27b2502364", null ],
      [ "TIM3_CounterMode_CenterAligned3", "group___t_i_m3___counter_mode.html#gga39fd7fc87071542d8c2a6b4eb75d4f57a0ed3b57649b0b573e8dcfde338d09828", null ]
    ] ],
    [ "TIM3_DMASource_TypeDef", "group___t_i_m3___d_m_a___source___requests.html#ga59d30444548c26e9bd12b45392ea05a4", [
      [ "TIM3_DMASource_Update", "group___t_i_m3___d_m_a___source___requests.html#gga59d30444548c26e9bd12b45392ea05a4aae2a9f9a14c17cb0f91ac5fd0596f231", null ],
      [ "TIM3_DMASource_CC1", "group___t_i_m3___d_m_a___source___requests.html#gga59d30444548c26e9bd12b45392ea05a4a18621f335db14d0128aa7f3e3d648cb3", null ],
      [ "TIM3_DMASource_CC2", "group___t_i_m3___d_m_a___source___requests.html#gga59d30444548c26e9bd12b45392ea05a4acf9b4d90b60ea3f008a15d651aa82d78", null ]
    ] ],
    [ "TIM3_EncoderMode_TypeDef", "group___t_i_m3___encoder___mode.html#ga37042e234737514922a4c0f684e710c5", [
      [ "TIM3_EncoderMode_TI1", "group___t_i_m3___encoder___mode.html#gga37042e234737514922a4c0f684e710c5a6cee55d18b9a8df8fac00cd04d844a67", null ],
      [ "TIM3_EncoderMode_TI2", "group___t_i_m3___encoder___mode.html#gga37042e234737514922a4c0f684e710c5a0e7c3ea69c1d2598b28e8dd64b14cf14", null ],
      [ "TIM3_EncoderMode_TI12", "group___t_i_m3___encoder___mode.html#gga37042e234737514922a4c0f684e710c5a2d81689c3a55e5d274cb7cf0839d10e7", null ]
    ] ],
    [ "TIM3_EventSource_TypeDef", "group___t_i_m3___event___source.html#ga5f977122db893878110980ec1b7439bb", [
      [ "TIM3_EventSource_Update", "group___t_i_m3___event___source.html#gga5f977122db893878110980ec1b7439bba4abbd6c02a6b7c422447f7d2e4c3699a", null ],
      [ "TIM3_EventSource_CC1", "group___t_i_m3___event___source.html#gga5f977122db893878110980ec1b7439bba15f7f0ac2053ffe3b0b8840ad1a94201", null ],
      [ "TIM3_EventSource_CC2", "group___t_i_m3___event___source.html#gga5f977122db893878110980ec1b7439bba32629f742926a9dde18cb1bcc64a1f83", null ],
      [ "TIM3_EventSource_Trigger", "group___t_i_m3___event___source.html#gga5f977122db893878110980ec1b7439bbab7f089867ec320a97b3e43a97469c1b7", null ],
      [ "TIM3_EventSource_Break", "group___t_i_m3___event___source.html#gga5f977122db893878110980ec1b7439bba7ac4b2968becfe2bc2e8e2110bbc6cb1", null ]
    ] ],
    [ "TIM3_ExtTRGPolarity_TypeDef", "group___t_i_m3___external___trigger___polarity.html#gaf328ae7520831d243def11354e0b0bea", [
      [ "TIM3_ExtTRGPolarity_Inverted", "group___t_i_m3___external___trigger___polarity.html#ggaf328ae7520831d243def11354e0b0beaa66d6b03be8fccd3745dd374fed8d9af2", null ],
      [ "TIM3_ExtTRGPolarity_NonInverted", "group___t_i_m3___external___trigger___polarity.html#ggaf328ae7520831d243def11354e0b0beaa7b001dc09772a5f7581e4664d99bcde1", null ]
    ] ],
    [ "TIM3_ExtTRGPSC_TypeDef", "group___t_i_m3___external___trigger___prescaler.html#ga45f25a66c6eed3c4aa898754fdea9ee9", [
      [ "TIM3_ExtTRGPSC_OFF", "group___t_i_m3___external___trigger___prescaler.html#gga45f25a66c6eed3c4aa898754fdea9ee9a997699af2dfd718935d64811d27d4a6e", null ],
      [ "TIM3_ExtTRGPSC_DIV2", "group___t_i_m3___external___trigger___prescaler.html#gga45f25a66c6eed3c4aa898754fdea9ee9a3484dffb45f80f5428d1ab911d360add", null ],
      [ "TIM3_ExtTRGPSC_DIV4", "group___t_i_m3___external___trigger___prescaler.html#gga45f25a66c6eed3c4aa898754fdea9ee9a8680b66a9801b55fb736697a05fec226", null ],
      [ "TIM3_ExtTRGPSC_DIV8", "group___t_i_m3___external___trigger___prescaler.html#gga45f25a66c6eed3c4aa898754fdea9ee9afeb869a047856650c92aa83807e43c8b", null ]
    ] ],
    [ "TIM3_FLAG_TypeDef", "group___t_i_m3___flags.html#ga39eb302dcf8fb7f64d86de53d676866c", [
      [ "TIM3_FLAG_Update", "group___t_i_m3___flags.html#gga39eb302dcf8fb7f64d86de53d676866ca26e0350f230e3d829f7cfa122e38dbb5", null ],
      [ "TIM3_FLAG_CC1", "group___t_i_m3___flags.html#gga39eb302dcf8fb7f64d86de53d676866ca6d3a878847ec7fce7fb5e6e46811fd3f", null ],
      [ "TIM3_FLAG_CC2", "group___t_i_m3___flags.html#gga39eb302dcf8fb7f64d86de53d676866caabcf81b1d80e317b375eb5c8329e05b7", null ],
      [ "TIM3_FLAG_Trigger", "group___t_i_m3___flags.html#gga39eb302dcf8fb7f64d86de53d676866ca20b95833d67f558fe765151a51e45f3e", null ],
      [ "TIM3_FLAG_Break", "group___t_i_m3___flags.html#gga39eb302dcf8fb7f64d86de53d676866caefdad8ee397f9ca4c9727f0e928edfe5", null ],
      [ "TIM3_FLAG_CC1OF", "group___t_i_m3___flags.html#gga39eb302dcf8fb7f64d86de53d676866ca80f7a526842e15f86f74811f81176366", null ],
      [ "TIM3_FLAG_CC2OF", "group___t_i_m3___flags.html#gga39eb302dcf8fb7f64d86de53d676866cad17e726127e75dd7cd6558464ff93286", null ]
    ] ],
    [ "TIM3_ForcedAction_TypeDef", "group___t_i_m3___forced___action.html#ga89600966a87e647b67aa63984e88a07c", [
      [ "TIM3_ForcedAction_Active", "group___t_i_m3___forced___action.html#gga89600966a87e647b67aa63984e88a07ca9e67c0f5b2b582856afe720b44d45a00", null ],
      [ "TIM3_ForcedAction_Inactive", "group___t_i_m3___forced___action.html#gga89600966a87e647b67aa63984e88a07cacc2d5963d35a5ce76e9ec01b897fdc59", null ]
    ] ],
    [ "TIM3_ICPolarity_TypeDef", "group___t_i_m3___input___capture___polarity.html#gab292af5fa80b00d2651a3b62899cfc73", [
      [ "TIM3_ICPolarity_Rising", "group___t_i_m3___input___capture___polarity.html#ggab292af5fa80b00d2651a3b62899cfc73a4b5accd9ca0016c22bb67da65a62eb0c", null ],
      [ "TIM3_ICPolarity_Falling", "group___t_i_m3___input___capture___polarity.html#ggab292af5fa80b00d2651a3b62899cfc73a42af7b86e5b320a7d9085274fe7b233c", null ]
    ] ],
    [ "TIM3_ICPSC_TypeDef", "group___t_i_m3___input___capture___prescaler.html#gaeac2a80af678cbeb6915141b2c571a00", [
      [ "TIM3_ICPSC_DIV1", "group___t_i_m3___input___capture___prescaler.html#ggaeac2a80af678cbeb6915141b2c571a00a7f7c228664548991f504a7bc19746716", null ],
      [ "TIM3_ICPSC_DIV2", "group___t_i_m3___input___capture___prescaler.html#ggaeac2a80af678cbeb6915141b2c571a00a9c475872785bac3207e83cbe5c035eb5", null ],
      [ "TIM3_ICPSC_DIV4", "group___t_i_m3___input___capture___prescaler.html#ggaeac2a80af678cbeb6915141b2c571a00aa7f2e1c208783b74cb40da14a61d6bc5", null ],
      [ "TIM3_ICPSC_DIV8", "group___t_i_m3___input___capture___prescaler.html#ggaeac2a80af678cbeb6915141b2c571a00a4cce7b0a10753b09cd13cb35ba927c7f", null ]
    ] ],
    [ "TIM3_ICSelection_TypeDef", "group___t_i_m3___input___capture___selection.html#ga48e05bae154de5e2728dbe5f7c0f48b8", [
      [ "TIM3_ICSelection_DirectTI", "group___t_i_m3___input___capture___selection.html#gga48e05bae154de5e2728dbe5f7c0f48b8a0a1c6dc0d6fb759d1c09a2fe429d9d3a", null ],
      [ "TIM3_ICSelection_IndirectTI", "group___t_i_m3___input___capture___selection.html#gga48e05bae154de5e2728dbe5f7c0f48b8a714e6e800d9300179679c6fb68c6d0b2", null ],
      [ "TIM3_ICSelection_TRGI", "group___t_i_m3___input___capture___selection.html#gga48e05bae154de5e2728dbe5f7c0f48b8a9ad7037b8c98c6480f4fc8b607292f0e", null ]
    ] ],
    [ "TIM3_IT_TypeDef", "group___t_i_m3___interrupts.html#ga3e62294d76686d7b5c06f6c51161f444", [
      [ "TIM3_IT_Update", "group___t_i_m3___interrupts.html#gga3e62294d76686d7b5c06f6c51161f444a02802cef5ef21032e43107bfcb16675d", null ],
      [ "TIM3_IT_CC1", "group___t_i_m3___interrupts.html#gga3e62294d76686d7b5c06f6c51161f444a1cbe1e8b3513a1a8e157c95e90e13731", null ],
      [ "TIM3_IT_CC2", "group___t_i_m3___interrupts.html#gga3e62294d76686d7b5c06f6c51161f444addee8f4a12faf8e95c7c429f6a0b097a", null ],
      [ "TIM3_IT_Trigger", "group___t_i_m3___interrupts.html#gga3e62294d76686d7b5c06f6c51161f444a65596277c7c94282831fe81c304b6446", null ],
      [ "TIM3_IT_Break", "group___t_i_m3___interrupts.html#gga3e62294d76686d7b5c06f6c51161f444aeb13342320d6f8cd740e562ea92f314d", null ]
    ] ],
    [ "TIM3_LockLevel_TypeDef", "group___t_i_m3___lock___level.html#ga72eb6dea1b18622e6c663588d3cf0f59", [
      [ "TIM3_LockLevel_Off", "group___t_i_m3___lock___level.html#gga72eb6dea1b18622e6c663588d3cf0f59a999a34192880f692f8cc093b871a7eac", null ],
      [ "TIM3_LockLevel_1", "group___t_i_m3___lock___level.html#gga72eb6dea1b18622e6c663588d3cf0f59ad0c62d2e6614de45653a68976a1e9c3f", null ],
      [ "TIM3_LockLevel_2", "group___t_i_m3___lock___level.html#gga72eb6dea1b18622e6c663588d3cf0f59a022e26d76c36a775593a2e0aa161ef70", null ],
      [ "TIM3_LockLevel_3", "group___t_i_m3___lock___level.html#gga72eb6dea1b18622e6c663588d3cf0f59a4a97e81f19d9e28dd75b874a6976b815", null ]
    ] ],
    [ "TIM3_OCIdleState_TypeDef", "group___t_i_m3___output___compare___idle__state.html#gaf2888418a2794d0fe55882d0ff8bb04a", [
      [ "TIM3_OCIdleState_Reset", "group___t_i_m3___output___compare___idle__state.html#ggaf2888418a2794d0fe55882d0ff8bb04aaeaecd090929f08c7609d522e46f0cc02", null ],
      [ "TIM3_OCIdleState_Set", "group___t_i_m3___output___compare___idle__state.html#ggaf2888418a2794d0fe55882d0ff8bb04aa573d3f1cfae702c08871e3b75059e577", null ]
    ] ],
    [ "TIM3_OCMode_TypeDef", "group___t_i_m3___o_c_mode.html#ga9fcb1c714d537d232b9966e942ee7e0d", [
      [ "TIM3_OCMode_Timing", "group___t_i_m3___o_c_mode.html#gga9fcb1c714d537d232b9966e942ee7e0da968f80e4719f46e764f06479049f5451", null ],
      [ "TIM3_OCMode_Active", "group___t_i_m3___o_c_mode.html#gga9fcb1c714d537d232b9966e942ee7e0daa22e03300b74cd8279b6857402c0a291", null ],
      [ "TIM3_OCMode_Inactive", "group___t_i_m3___o_c_mode.html#gga9fcb1c714d537d232b9966e942ee7e0da89f44addc95a417754c2957c6712e358", null ],
      [ "TIM3_OCMode_Toggle", "group___t_i_m3___o_c_mode.html#gga9fcb1c714d537d232b9966e942ee7e0daca578d9caca981263c77f66e13a5b699", null ],
      [ "TIM3_OCMode_PWM1", "group___t_i_m3___o_c_mode.html#gga9fcb1c714d537d232b9966e942ee7e0da8036b05c92f18e924d493ffd7c1c6f5b", null ],
      [ "TIM3_OCMode_PWM2", "group___t_i_m3___o_c_mode.html#gga9fcb1c714d537d232b9966e942ee7e0daba422dfb1080b7b4c6c7db9010012941", null ]
    ] ],
    [ "TIM3_OCPolarity_TypeDef", "group___t_i_m3___output___compare___polarity.html#ga9594322caaf3f226cc8ff7f99cd49811", [
      [ "TIM3_OCPolarity_High", "group___t_i_m3___output___compare___polarity.html#gga9594322caaf3f226cc8ff7f99cd49811a96ab2ddaa2c1037eec8978f06dfd311e", null ],
      [ "TIM3_OCPolarity_Low", "group___t_i_m3___output___compare___polarity.html#gga9594322caaf3f226cc8ff7f99cd49811a1a06e9fd6b9417aa6486cb45e39b1525", null ]
    ] ],
    [ "TIM3_OPMode_TypeDef", "group___t_i_m3___one_pulse_mode.html#ga946903aea6107c6ee919cdb873a73222", [
      [ "TIM3_OPMode_Single", "group___t_i_m3___one_pulse_mode.html#gga946903aea6107c6ee919cdb873a73222a51382dc17dfb0bb83d234f7fcd859d0a", null ],
      [ "TIM3_OPMode_Repetitive", "group___t_i_m3___one_pulse_mode.html#gga946903aea6107c6ee919cdb873a73222ac9e406e19e087f05a4b8c9138e967ff3", null ]
    ] ],
    [ "TIM3_OSSIState_TypeDef", "group___t_i_m3___o_s_s_i___state.html#gaff90148df3e0533cdd838bac2469523b", [
      [ "TIM3_OSSIState_Enable", "group___t_i_m3___o_s_s_i___state.html#ggaff90148df3e0533cdd838bac2469523ba5a37d51de25996c273f68544331ef393", null ],
      [ "TIM3_OSSIState_Disable", "group___t_i_m3___o_s_s_i___state.html#ggaff90148df3e0533cdd838bac2469523bad89a7b86a2a50d7a9a2669d0b4578e86", null ]
    ] ],
    [ "TIM3_OutputState_TypeDef", "group___t_i_m3___output___state.html#gabb5b436d5862bc7d9418847ab7da0c60", [
      [ "TIM3_OutputState_Disable", "group___t_i_m3___output___state.html#ggabb5b436d5862bc7d9418847ab7da0c60a8c000cc41fdb688be348ac13ad40ad6a", null ],
      [ "TIM3_OutputState_Enable", "group___t_i_m3___output___state.html#ggabb5b436d5862bc7d9418847ab7da0c60aacc2fd3628c0f4577bef4bd4e57aa0a7", null ]
    ] ],
    [ "TIM3_Prescaler_TypeDef", "group___t_i_m3___prescaler.html#ga1cbc8497bd7e3b4b5e2410a1187d9c1f", [
      [ "TIM3_Prescaler_1", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa93eee4d3349e25659c221bd960201cea", null ],
      [ "TIM3_Prescaler_2", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fab4918e380c4f625ea6ffcbc32e525282", null ],
      [ "TIM3_Prescaler_4", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa97e8c07033ed548d160820dd885ac944", null ],
      [ "TIM3_Prescaler_8", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa569c25d640fa59654c5f87bc706576f2", null ],
      [ "TIM3_Prescaler_16", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa809b56414206c7a55439b4a96b34b1b9", null ],
      [ "TIM3_Prescaler_32", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fae2a44ac42bb7e3e3937aff092dd5b726", null ],
      [ "TIM3_Prescaler_64", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fac47380ed4e6fccce203b8590bc94d4eb", null ],
      [ "TIM3_Prescaler_128", "group___t_i_m3___prescaler.html#gga1cbc8497bd7e3b4b5e2410a1187d9c1fa95722614aae8a9900ad33532635f71b4", null ]
    ] ],
    [ "TIM3_PSCReloadMode_TypeDef", "group___t_i_m3___prescaler___reload___mode.html#gac7eaef34c3d937a8af7b171b37f3b6b3", [
      [ "TIM3_PSCReloadMode_Update", "group___t_i_m3___prescaler___reload___mode.html#ggac7eaef34c3d937a8af7b171b37f3b6b3aeab5b8e4870281a7c8eb87d8095df8d0", null ],
      [ "TIM3_PSCReloadMode_Immediate", "group___t_i_m3___prescaler___reload___mode.html#ggac7eaef34c3d937a8af7b171b37f3b6b3a31e2be0a5d3c8b05bc6a1e7c4edaf2d1", null ]
    ] ],
    [ "TIM3_SlaveMode_TypeDef", "group___t_i_m3___slave___mode.html#gaf0a169c439c94b689bc3680c54c80725", [
      [ "TIM3_SlaveMode_Reset", "group___t_i_m3___slave___mode.html#ggaf0a169c439c94b689bc3680c54c80725ae83e348e5283f36812b43e8fa861d45a", null ],
      [ "TIM3_SlaveMode_Gated", "group___t_i_m3___slave___mode.html#ggaf0a169c439c94b689bc3680c54c80725a9be0d4aa41bf6812300aa0bb9b396bfa", null ],
      [ "TIM3_SlaveMode_Trigger", "group___t_i_m3___slave___mode.html#ggaf0a169c439c94b689bc3680c54c80725a6e95ce37d637b61ba81c1cd9317468b2", null ],
      [ "TIM3_SlaveMode_External1", "group___t_i_m3___slave___mode.html#ggaf0a169c439c94b689bc3680c54c80725ac1f1dbf992b5780a84a3f0d86ffd905e", null ]
    ] ],
    [ "TIM3_TIxExternalCLK1Source_TypeDef", "group___t_i_m3___t_i___external___clock___source.html#gacf33f993a91e1308c86ffd220622ff62", [
      [ "TIM3_TIxExternalCLK1Source_TI1ED", "group___t_i_m3___t_i___external___clock___source.html#ggacf33f993a91e1308c86ffd220622ff62a2cb6b65049ab913bce77d27da4706779", null ],
      [ "TIM3_TIxExternalCLK1Source_TI1", "group___t_i_m3___t_i___external___clock___source.html#ggacf33f993a91e1308c86ffd220622ff62a4bf69063a0473dcae0dedd1aca9f6243", null ],
      [ "TIM3_TIxExternalCLK1Source_TI2", "group___t_i_m3___t_i___external___clock___source.html#ggacf33f993a91e1308c86ffd220622ff62a982cdacef765ea03390f365bfe57f1ce", null ]
    ] ],
    [ "TIM3_TRGOSource_TypeDef", "group___t_i_m3___trigger___output___source.html#gaeb510ce730e09210a993d0cac243c1d0", [
      [ "TIM3_TRGOSource_Reset", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0a5a56594ed0814ea8840adfeefdd40e37", null ],
      [ "TIM3_TRGOSource_Enable", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0abeab958b58b95c61356847b060bc5585", null ],
      [ "TIM3_TRGOSource_Update", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0ae4183cc4728ad3c4addcf9607a671f30", null ],
      [ "TIM3_TRGOSource_OC1", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0a11a6c6060e73be3cba0c7b0e7a41964e", null ],
      [ "TIM3_TRGOSource_OC1REF", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0afa2a339d53ad71b0d482669a1add8fda", null ],
      [ "TIM3_TRGOSource_OC2REF", "group___t_i_m3___trigger___output___source.html#ggaeb510ce730e09210a993d0cac243c1d0adbc9f9e456179a3cd2deddaa4b6561f8", null ]
    ] ],
    [ "TIM3_TRGSelection_TypeDef", "group___t_i_m3___internal___trigger___selection.html#gaadbdb335c2e0724357f716e0edf8e4bc", [
      [ "TIM3_TRGSelection_TIM4", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca0577273064dfa1feb591e8b87baaef2e", null ],
      [ "TIM3_TRGSelection_TIM1", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca12e71cbd05d8050207c51e6f89f798c1", null ],
      [ "TIM3_TRGSelection_TIM5", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bcae64b6cb1b4c7e920bfbb9f73762618b2", null ],
      [ "TIM3_TRGSelection_TIM2", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca0f5d89ea6e7142cf233224e4f25d6981", null ],
      [ "TIM3_TRGSelection_TI1F_ED", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca06e69397141a451ce88329353869aafa", null ],
      [ "TIM3_TRGSelection_TI1FP1", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca0f79cce41cac7225ba62298d91aeee49", null ],
      [ "TIM3_TRGSelection_TI2FP2", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca8b4c6fab9847c7bbdc2ce14455feaab3", null ],
      [ "TIM3_TRGSelection_ETRF", "group___t_i_m3___internal___trigger___selection.html#ggaadbdb335c2e0724357f716e0edf8e4bca96286262bc268024502fe75a2fc58801", null ]
    ] ],
    [ "TIM3_UpdateSource_TypeDef", "group___t_i_m3___update___source.html#gab170c3ed89b44f7d922971cd2f6e3c9e", [
      [ "TIM3_UpdateSource_Global", "group___t_i_m3___update___source.html#ggab170c3ed89b44f7d922971cd2f6e3c9ea0c9da35748076356a1ccbd4e03c69000", null ],
      [ "TIM3_UpdateSource_Regular", "group___t_i_m3___update___source.html#ggab170c3ed89b44f7d922971cd2f6e3c9ea4ebd64f597c2b50fb6dd3931f9f00a87", null ]
    ] ],
    [ "TIM3_ARRPreloadConfig", "group___t_i_m3.html#ga82d7a8293971f61e397c94adf19b9545", null ],
    [ "TIM3_BKRConfig", "group___t_i_m3.html#gaea3a212c0944283d902b5ebb810297ce", null ],
    [ "TIM3_CCxCmd", "group___t_i_m3.html#ga1ecd2117b16f4f2acde9b854ae7cc8b2", null ],
    [ "TIM3_ClearFlag", "group___t_i_m3.html#gac0c61375d17029e98b275d0749dd60da", null ],
    [ "TIM3_ClearITPendingBit", "group___t_i_m3.html#gad31ab9a35108f28d9a5ecf4c291b5747", null ],
    [ "TIM3_Cmd", "group___t_i_m3.html#ga20e045b0cbe41f2b883507ea7093ed4b", null ],
    [ "TIM3_CounterModeConfig", "group___t_i_m3.html#ga8ad34d501534718482440276d083b682", null ],
    [ "TIM3_CtrlPWMOutputs", "group___t_i_m3.html#ga1b11133b1ae495f4b8c6520a1fb3149b", null ],
    [ "TIM3_DeInit", "group___t_i_m3.html#ga7fd3211510c2c62beb66928c64b723de", null ],
    [ "TIM3_DMACmd", "group___t_i_m3.html#gad414864976ced1876c3a7650c7cf7648", null ],
    [ "TIM3_EncoderInterfaceConfig", "group___t_i_m3.html#gac4419eeae8a1847be106a4c8a02e042e", null ],
    [ "TIM3_ETRClockMode1Config", "group___t_i_m3.html#gade600991120a48d88963b0c722403c4e", null ],
    [ "TIM3_ETRClockMode2Config", "group___t_i_m3.html#ga1f70c8ad3db803a8dd71f5fd119c64c7", null ],
    [ "TIM3_ETRConfig", "group___t_i_m3.html#ga70d0ed0045fb555c0f460cfbf1ea3463", null ],
    [ "TIM3_ForcedOC1Config", "group___t_i_m3.html#gafabf657c65bfc62ce4a17308c73339df", null ],
    [ "TIM3_ForcedOC2Config", "group___t_i_m3.html#gafb7c996ac8c7b6038b1b24f6305d6442", null ],
    [ "TIM3_GenerateEvent", "group___t_i_m3.html#gafb7ff23b5f4ae4ad3fa9458ddf83bfae", null ],
    [ "TIM3_GetCapture1", "group___t_i_m3.html#ga6ce6deb067fd9098e894bdcd5a1304f9", null ],
    [ "TIM3_GetCapture2", "group___t_i_m3.html#ga374bafdb6266c80cce76c6dc7548907d", null ],
    [ "TIM3_GetCounter", "group___t_i_m3.html#gabf8f64df20195071817ab3e3f25fa42a", null ],
    [ "TIM3_GetFlagStatus", "group___t_i_m3.html#gae38a2cf3789462ebd1e2aa85495b413e", null ],
    [ "TIM3_GetITStatus", "group___t_i_m3.html#gab3feb578e4273bbd22c4cc529cf92975", null ],
    [ "TIM3_GetPrescaler", "group___t_i_m3.html#ga5e4c39064908dc7258ccd6d5d1945767", null ],
    [ "TIM3_ICInit", "group___t_i_m3.html#gaa4d7b32da8d5704bbf189d7a976feab7", null ],
    [ "TIM3_InternalClockConfig", "group___t_i_m3.html#gad8ebb69b29b079281e8b7f9af1a682e9", null ],
    [ "TIM3_ITConfig", "group___t_i_m3.html#gaa1972f1d6e2c540154ca3fbe17969cda", null ],
    [ "TIM3_OC1FastConfig", "group___t_i_m3.html#ga27ab5153d07d8fa112ef4f1fdc7fc14e", null ],
    [ "TIM3_OC1Init", "group___t_i_m3.html#ga8a5538e59a43b91af458c1531a6aac04", null ],
    [ "TIM3_OC1PolarityConfig", "group___t_i_m3.html#ga78964654660298146dd1d57c4165242e", null ],
    [ "TIM3_OC1PreloadConfig", "group___t_i_m3.html#ga925ba6a1d5a2fc6257d9f4cd9e813851", null ],
    [ "TIM3_OC2FastConfig", "group___t_i_m3.html#gae5b38893486b2c27b2c4376e201c7dc8", null ],
    [ "TIM3_OC2Init", "group___t_i_m3.html#ga395f01e3048b207748bedf06349c6cc1", null ],
    [ "TIM3_OC2PolarityConfig", "group___t_i_m3.html#gad200a694618c2dde554e4d2d3adbfa63", null ],
    [ "TIM3_OC2PreloadConfig", "group___t_i_m3.html#ga4b5451cd4f17b92501b95f9c767a9e6a", null ],
    [ "TIM3_PrescalerConfig", "group___t_i_m3.html#gaead9a5eae10e682b062a1597a56a696d", null ],
    [ "TIM3_PWMIConfig", "group___t_i_m3.html#gaacce9b8b52d58a71b9fc17d8312a12df", null ],
    [ "TIM3_SelectCCDMA", "group___t_i_m3.html#ga5c6e2aefc97150927b5fe9492f9a54f0", null ],
    [ "TIM3_SelectHallSensor", "group___t_i_m3.html#gafede22e51cd896c02b403c6e7bf3f2e7", null ],
    [ "TIM3_SelectInputTrigger", "group___t_i_m3.html#ga7d40b7d8940d3047d44184d8eee16e8f", null ],
    [ "TIM3_SelectMasterSlaveMode", "group___t_i_m3.html#gad5be843001f08fd607bb049ded91d73b", null ],
    [ "TIM3_SelectOCxM", "group___t_i_m3.html#ga90e5a84cdcf123a8e25bb4b3a6cd0b8d", null ],
    [ "TIM3_SelectOnePulseMode", "group___t_i_m3.html#gae74017b0a5dc3e19cb41c665a57839a5", null ],
    [ "TIM3_SelectOutputTrigger", "group___t_i_m3.html#ga6e04e97a008ed604d3dbf3ad1972f89c", null ],
    [ "TIM3_SelectSlaveMode", "group___t_i_m3.html#ga575954f3b16df85da22afe6db37da5f7", null ],
    [ "TIM3_SetAutoreload", "group___t_i_m3.html#ga36911f2ee2a1fe6c0e45854986373629", null ],
    [ "TIM3_SetCompare1", "group___t_i_m3.html#gad18d4e6ea548a78d0aeb442709a7aff6", null ],
    [ "TIM3_SetCompare2", "group___t_i_m3.html#gad8399482fa0b9d36dffcf76e245a28ac", null ],
    [ "TIM3_SetCounter", "group___t_i_m3.html#gac5ee4fe8e1ecd33e65e1d1fff927d362", null ],
    [ "TIM3_SetIC1Prescaler", "group___t_i_m3.html#ga9484970a2b1f6397d38af7f830fdc95a", null ],
    [ "TIM3_SetIC2Prescaler", "group___t_i_m3.html#ga959d1ed372816a6990b1c95022e1c645", null ],
    [ "TIM3_TimeBaseInit", "group___t_i_m3.html#gac974e817a95e9dbcad021e92a0ff6a88", null ],
    [ "TIM3_TIxExternalClockConfig", "group___t_i_m3.html#ga3f8f430423b6b256a45e7ec8e8ee0144", null ],
    [ "TIM3_UpdateDisableConfig", "group___t_i_m3.html#gab6147cd8eb35ed77992378f920113cc5", null ],
    [ "TIM3_UpdateRequestConfig", "group___t_i_m3.html#ga3f45edf8643e7bcb667956de6183bbf7", null ]
];