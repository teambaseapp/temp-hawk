var stm8l15x__flashutils_8h =
[
    [ "GetDeviceID", "stm8l15x__flashutils_8h.html#adeeb26ebd00db3e7aa3990ca125de55f", null ],
    [ "GetDeviceStatus", "stm8l15x__flashutils_8h.html#a9352c6083a853a67100f44a75d0ff910", null ],
    [ "SetDeviceID", "stm8l15x__flashutils_8h.html#a71ade255ffb70675d054fd232f9251d4", null ],
    [ "SetDeviceStatus", "stm8l15x__flashutils_8h.html#a0ba2fabf39de8dc3440e5c564a6c74d6", null ],
    [ "SetUpEEPROM", "stm8l15x__flashutils_8h.html#ae55fe6eb978ed95b4e8bc2bde680b219", null ]
];