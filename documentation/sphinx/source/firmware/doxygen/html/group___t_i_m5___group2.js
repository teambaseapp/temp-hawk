var group___t_i_m5___group2 =
[
    [ "Input Capture management functions", "group___t_i_m5___group3.html", "group___t_i_m5___group3" ],
    [ "Interrupts DMA and flags management functions", "group___t_i_m5___group4.html", "group___t_i_m5___group4" ],
    [ "Clocks management functions", "group___t_i_m5___group5.html", "group___t_i_m5___group5" ],
    [ "Synchronization management functions", "group___t_i_m5___group6.html", "group___t_i_m5___group6" ],
    [ "Specific interface management functions", "group___t_i_m5___group7.html", "group___t_i_m5___group7" ],
    [ "TIM5_BKRConfig", "group___t_i_m5___group2.html#ga7175b49cdc94615438db911b3ae67219", null ],
    [ "TIM5_CCxCmd", "group___t_i_m5___group2.html#ga869d6041a28bd998489231dafa195e85", null ],
    [ "TIM5_CtrlPWMOutputs", "group___t_i_m5___group2.html#gae5d298e623fd220441989a7182e43eb2", null ],
    [ "TIM5_ForcedOC1Config", "group___t_i_m5___group2.html#ga94e625d9ac70bc97a0e8c5b142af9942", null ],
    [ "TIM5_ForcedOC2Config", "group___t_i_m5___group2.html#ga2194c1941550a024daf07af7ce1e0843", null ],
    [ "TIM5_OC1FastConfig", "group___t_i_m5___group2.html#ga659db37fcb9eaa35d3b05bb848b7930c", null ],
    [ "TIM5_OC1Init", "group___t_i_m5___group2.html#gac819de5f9b152d46deab5ffd3259808a", null ],
    [ "TIM5_OC1PolarityConfig", "group___t_i_m5___group2.html#ga496cda36432f4a5c920d8514efc308b3", null ],
    [ "TIM5_OC1PreloadConfig", "group___t_i_m5___group2.html#ga90f17f570e5f3ce99855acecd7631df1", null ],
    [ "TIM5_OC2FastConfig", "group___t_i_m5___group2.html#gaf5947b4436a247b09c0d4b6d1efdce90", null ],
    [ "TIM5_OC2Init", "group___t_i_m5___group2.html#ga2d91748bec2748703bcee424647eb4d8", null ],
    [ "TIM5_OC2PolarityConfig", "group___t_i_m5___group2.html#gac71dd04fdb47ef8f2ffe5ed0c96a069d", null ],
    [ "TIM5_OC2PreloadConfig", "group___t_i_m5___group2.html#ga2d6d670f1dbc8b373ff74630f3abe7af", null ],
    [ "TIM5_SelectOCxM", "group___t_i_m5___group2.html#ga0b792818264874ef476999fe0e018864", null ],
    [ "TIM5_SetCompare1", "group___t_i_m5___group2.html#ga848fbefa25baa2ea26260b8941b7a9bc", null ],
    [ "TIM5_SetCompare2", "group___t_i_m5___group2.html#ga0de74897146ade82ca1bbc7fb2cd55ed", null ]
];