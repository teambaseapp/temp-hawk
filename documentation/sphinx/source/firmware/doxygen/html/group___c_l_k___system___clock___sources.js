var group___c_l_k___system___clock___sources =
[
    [ "CLK_SYSCLKSource_TypeDef", "group___c_l_k___system___clock___sources.html#ga9b217952eaa4b22b689a9394c29f4e8f", [
      [ "CLK_SYSCLKSource_HSI", "group___c_l_k___system___clock___sources.html#gga9b217952eaa4b22b689a9394c29f4e8fa4b14704598db25a118f752d5728420dc", null ],
      [ "CLK_SYSCLKSource_LSI", "group___c_l_k___system___clock___sources.html#gga9b217952eaa4b22b689a9394c29f4e8fa9fab5c95afb5d5fb9fd77ae434ac28e3", null ],
      [ "CLK_SYSCLKSource_HSE", "group___c_l_k___system___clock___sources.html#gga9b217952eaa4b22b689a9394c29f4e8faf097876736959f332e82d09978fb55d2", null ],
      [ "CLK_SYSCLKSource_LSE", "group___c_l_k___system___clock___sources.html#gga9b217952eaa4b22b689a9394c29f4e8fab949d3cb9c5f98becdb244d639114f03", null ]
    ] ]
];