var stm8l15x__dma_8h =
[
    [ "DMA_DIR_TypeDef", "group___d_m_a___data___transfer___direction.html#ga8b9323d1fd5324f2c3b8b5ec1f718049", [
      [ "DMA_DIR_PeripheralToMemory", "group___d_m_a___data___transfer___direction.html#gga8b9323d1fd5324f2c3b8b5ec1f718049a7ebf8dbdbce18575423953bf1893a0be", null ],
      [ "DMA_DIR_MemoryToPeripheral", "group___d_m_a___data___transfer___direction.html#gga8b9323d1fd5324f2c3b8b5ec1f718049aae59fbef0f5f4f55bbec5dca6293df0c", null ],
      [ "DMA_DIR_Memory0ToMemory1", "group___d_m_a___data___transfer___direction.html#gga8b9323d1fd5324f2c3b8b5ec1f718049a440279ceb694b8af8230777f7791bcfc", null ]
    ] ],
    [ "DMA_FLAG_TypeDef", "group___d_m_a___flags.html#gaa323c1d5393016a590d2353ebc776675", [
      [ "DMA1_FLAG_GB", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a90650133afdaa71857522cca1d4bf9d4", null ],
      [ "DMA1_FLAG_IFC0", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a068ec441a791e80aade13a233ca8e9d6", null ],
      [ "DMA1_FLAG_IFC1", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a38bfa7d26b8c1c8d0149d93d78b851c5", null ],
      [ "DMA1_FLAG_IFC2", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a78f212ed172fbbc12b3b0063be3f46b4", null ],
      [ "DMA1_FLAG_IFC3", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a29fae69c03830bca5604ea93ec0c8b87", null ],
      [ "DMA1_FLAG_TC0", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675abc26c3abf4cf857bd476f2ab1a1de4c3", null ],
      [ "DMA1_FLAG_TC1", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a339e91cb8e37950eaf8b64e980c9a95d", null ],
      [ "DMA1_FLAG_TC2", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a560fcd6d2fb8ab6dd23f4ad89534ccf4", null ],
      [ "DMA1_FLAG_TC3", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675ae21478ecc02e378159ca69d9cd81dcbc", null ],
      [ "DMA1_FLAG_HT0", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a61571a29c103ce8956a2727490be02c3", null ],
      [ "DMA1_FLAG_HT1", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a72395fe7f9669378b1dffb26d0c14003", null ],
      [ "DMA1_FLAG_HT2", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a71ab6d31d7804be64ed29ed1ea2ef2fa", null ],
      [ "DMA1_FLAG_HT3", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675ac5dc2d019e8553712ed750bf0b4fd279", null ],
      [ "DMA1_FLAG_PEND0", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a1da10e5d973e22dedeb6201f7f42ba15", null ],
      [ "DMA1_FLAG_PEND1", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a55336ba6a6ba087b01d9f1197142a891", null ],
      [ "DMA1_FLAG_PEND2", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a54fdfab0420bd051ebf91121f693249d", null ],
      [ "DMA1_FLAG_PEND3", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a64f168a2be576296f458d6e248606d07", null ],
      [ "DMA1_FLAG_BUSY0", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a0cc65147f23ba706c091c95d96a20d43", null ],
      [ "DMA1_FLAG_BUSY1", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a52786b0763d5e7f5f8c327470fe73478", null ],
      [ "DMA1_FLAG_BUSY2", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675a65f8e2fc067a33f1c218f61b3ee924b4", null ],
      [ "DMA1_FLAG_BUSY3", "group___d_m_a___flags.html#ggaa323c1d5393016a590d2353ebc776675aa5b7fc569dd71c0af5f9b6b786caef36", null ]
    ] ],
    [ "DMA_IT_TypeDef", "group___d_m_a___interrupts.html#ga3b2a86275cd238b56da61a1ff82e7e6f", [
      [ "DMA1_IT_TC0", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa0ebe40c7640741d732d34a2bd2df4755", null ],
      [ "DMA1_IT_TC1", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa54b9345a276e4ebc28b6138716a702dd", null ],
      [ "DMA1_IT_TC2", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fae5d13676b83ce4f686364fbb011d8b3e", null ],
      [ "DMA1_IT_TC3", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa4543899ab5ce83fb61233c2761525069", null ],
      [ "DMA1_IT_HT0", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fad622e9de61b13945db8c2c5323cbd816", null ],
      [ "DMA1_IT_HT1", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa9552c94e52c361e17962e77d195a840f", null ],
      [ "DMA1_IT_HT2", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa6846364283b262fb0bee5840b9c25eea", null ],
      [ "DMA1_IT_HT3", "group___d_m_a___interrupts.html#gga3b2a86275cd238b56da61a1ff82e7e6fa82ed8d00fb6350e941e6f1a1aa74e393", null ]
    ] ],
    [ "DMA_ITx_TypeDef", "group___d_m_a___one___channel___interrupts.html#ga4eece40bd14386d595231359b63ec7cb", [
      [ "DMA_ITx_TC", "group___d_m_a___one___channel___interrupts.html#gga4eece40bd14386d595231359b63ec7cba7430b65f1b83d4e2f24a9021f097ded0", null ],
      [ "DMA_ITx_HT", "group___d_m_a___one___channel___interrupts.html#gga4eece40bd14386d595231359b63ec7cba49c6684f345c870de47488631ce9ab51", null ]
    ] ],
    [ "DMA_MemoryDataSize_TypeDef", "group___d_m_a___memory___data___size.html#ga980b99e5ec18db0d919acdc07eb1a46c", [
      [ "DMA_MemoryDataSize_Byte", "group___d_m_a___memory___data___size.html#gga980b99e5ec18db0d919acdc07eb1a46ca77088c61ba0fa65ea2af313e2b9368e9", null ],
      [ "DMA_MemoryDataSize_HalfWord", "group___d_m_a___memory___data___size.html#gga980b99e5ec18db0d919acdc07eb1a46ca99466a5b0db82778ed71cf65819b969d", null ]
    ] ],
    [ "DMA_MemoryIncMode_TypeDef", "group___d_m_a___incremented___mode.html#gab23d613f878b7b141f1c056152cf6474", [
      [ "DMA_MemoryIncMode_Dec", "group___d_m_a___incremented___mode.html#ggab23d613f878b7b141f1c056152cf6474a044d00dae326aa6e71734bc879095b24", null ],
      [ "DMA_MemoryIncMode_Inc", "group___d_m_a___incremented___mode.html#ggab23d613f878b7b141f1c056152cf6474a492132bf0f0afd600caf9298e4098fa6", null ]
    ] ],
    [ "DMA_Mode_TypeDef", "group___d_m_a___mode.html#gaca2ba2c8dd0c5c4a587f3b277f08b62b", [
      [ "DMA_Mode_Normal", "group___d_m_a___mode.html#ggaca2ba2c8dd0c5c4a587f3b277f08b62bad1c06e0539ec34438972602c7e9975f5", null ],
      [ "DMA_Mode_Circular", "group___d_m_a___mode.html#ggaca2ba2c8dd0c5c4a587f3b277f08b62ba7142ef006eefee6ef42061d4a0e546c9", null ]
    ] ],
    [ "DMA_Priority_TypeDef", "group___d_m_a___priority.html#ga8233b35288d68a265b3d78e6d1d98226", [
      [ "DMA_Priority_Low", "group___d_m_a___priority.html#gga8233b35288d68a265b3d78e6d1d98226a9ab65aa66375451f15c909a31bd08372", null ],
      [ "DMA_Priority_Medium", "group___d_m_a___priority.html#gga8233b35288d68a265b3d78e6d1d98226ac8a104fdc8bbd8fc4c6bdf91e3287ae0", null ],
      [ "DMA_Priority_High", "group___d_m_a___priority.html#gga8233b35288d68a265b3d78e6d1d98226a883256c4d65f9f5b2c7beed9a6413474", null ],
      [ "DMA_Priority_VeryHigh", "group___d_m_a___priority.html#gga8233b35288d68a265b3d78e6d1d98226aae2afc431c42aa0366e46d076d64bdc0", null ]
    ] ],
    [ "DMA_ClearFlag", "group___d_m_a.html#ga0063297f18de3167658c463357ab8686", null ],
    [ "DMA_ClearITPendingBit", "group___d_m_a.html#ga2f422ecd7419a3a0aa1438ac96fc3dbd", null ],
    [ "DMA_Cmd", "group___d_m_a.html#ga8ccbd3d4d9474f1deb863764f6cedac8", null ],
    [ "DMA_DeInit", "group___d_m_a.html#ga0349a6c19a891b73b084bd416a4c262a", null ],
    [ "DMA_GetCurrDataCounter", "group___d_m_a.html#ga3f64eeb2a2ec2503495b0cfbfe89c5d2", null ],
    [ "DMA_GetFlagStatus", "group___d_m_a.html#ga387655c480f0bd470236b7b942c34679", null ],
    [ "DMA_GetITStatus", "group___d_m_a.html#ga7322b7d0d82cac72f9e19ac42b01564d", null ],
    [ "DMA_GlobalCmd", "group___d_m_a.html#ga21cf4640a3e3b9dc913492c6058e6478", null ],
    [ "DMA_GlobalDeInit", "group___d_m_a.html#gab652552339b6124b0e87713d738135fc", null ],
    [ "DMA_Init", "group___d_m_a.html#gad4265428d8ee58df39cbddc0d6be60ec", null ],
    [ "DMA_ITConfig", "group___d_m_a.html#ga706009972e7ca3a21c13780481f1866d", null ],
    [ "DMA_SetCurrDataCounter", "group___d_m_a.html#ga505849e107066de90369a7297ba70fb8", null ],
    [ "DMA_SetTimeOut", "group___d_m_a.html#ga2bcc772a1005733c85ecbf37cc806302", null ]
];