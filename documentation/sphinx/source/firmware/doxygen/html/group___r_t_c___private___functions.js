var group___r_t_c___private___functions =
[
    [ "Initialization and Configuration functions", "group___r_t_c___group1.html", "group___r_t_c___group1" ],
    [ "Time and Date configuration functions", "group___r_t_c___group2.html", "group___r_t_c___group2" ],
    [ "Alarms configuration functions", "group___r_t_c___group3.html", "group___r_t_c___group3" ],
    [ "WakeUp Timer configuration functions", "group___r_t_c___group4.html", "group___r_t_c___group4" ],
    [ "Daylight Saving configuration functions", "group___r_t_c___group5.html", "group___r_t_c___group5" ],
    [ "Output pin Configuration function", "group___r_t_c___group6.html", "group___r_t_c___group6" ],
    [ "Output pin Configuration function", "group___r_t_c___group7.html", "group___r_t_c___group7" ],
    [ "Output pin Configuration function", "group___r_t_c___group8.html", "group___r_t_c___group8" ],
    [ "Digital Calibration configuration functions", "group___r_t_c___group9.html", "group___r_t_c___group9" ],
    [ "Tampers configuration functions", "group___r_t_c___group10.html", "group___r_t_c___group10" ],
    [ "Interrupts and flags management functions", "group___r_t_c___group11.html", "group___r_t_c___group11" ]
];