var group___w_f_e___registers___bits___definition =
[
    [ "WFE_CR1_EXTI_EV0", "group___w_f_e___registers___bits___definition.html#gad7b1d36b01266c7f33fc23ec2f3f3e03", null ],
    [ "WFE_CR1_EXTI_EV1", "group___w_f_e___registers___bits___definition.html#ga1e15463d5862158c9af36c46183476f8", null ],
    [ "WFE_CR1_EXTI_EV2", "group___w_f_e___registers___bits___definition.html#ga3acde0e990e7a37a0b3f69e8ff66125a", null ],
    [ "WFE_CR1_EXTI_EV3", "group___w_f_e___registers___bits___definition.html#ga08ebc6233737c5600064d5bcadf0e79f", null ],
    [ "WFE_CR1_TIM1_EV0", "group___w_f_e___registers___bits___definition.html#ga209dc15af63cf62ec23578e2a3e3f260", null ],
    [ "WFE_CR1_TIM1_EV1", "group___w_f_e___registers___bits___definition.html#ga7b91cd957ee0cdcb8e38678ee0e3787c", null ],
    [ "WFE_CR1_TIM2_EV0", "group___w_f_e___registers___bits___definition.html#ga21d360350fba819272784365c870d86e", null ],
    [ "WFE_CR1_TIM2_EV1", "group___w_f_e___registers___bits___definition.html#gad14f160ba82e79da219cf55b35d348dd", null ],
    [ "WFE_CR2_ADC1_COMP_EV", "group___w_f_e___registers___bits___definition.html#ga948550408a940d71e29822a4ba47515c", null ],
    [ "WFE_CR2_EXTI_EV4", "group___w_f_e___registers___bits___definition.html#ga7884fd0a1c89e6f33eb167c1ca194944", null ],
    [ "WFE_CR2_EXTI_EV5", "group___w_f_e___registers___bits___definition.html#gaccfcb931757dc6a623bcc6e02a93157b", null ],
    [ "WFE_CR2_EXTI_EV6", "group___w_f_e___registers___bits___definition.html#gac2fe14462bd5f978bcb3410baf873d5b", null ],
    [ "WFE_CR2_EXTI_EV7", "group___w_f_e___registers___bits___definition.html#ga4c2e29bc8fa07bcec9e87c82cbba6e7d", null ],
    [ "WFE_CR2_EXTI_EVBG", "group___w_f_e___registers___bits___definition.html#ga1ea10c76145ac8ebf13db64f196d4c43", null ],
    [ "WFE_CR2_EXTI_EVDH", "group___w_f_e___registers___bits___definition.html#ga5ba984e52ec97425ca251b4596efa6ad", null ],
    [ "WFE_CR2_EXTI_EVEF", "group___w_f_e___registers___bits___definition.html#gaba1613f66900368d9fdee2b9e1dc5c82", null ],
    [ "WFE_CR3_DMA1CH01_EV", "group___w_f_e___registers___bits___definition.html#ga62bc80ee468e970950ed78dcd22c5057", null ],
    [ "WFE_CR3_DMA1CH23_EV", "group___w_f_e___registers___bits___definition.html#gae43a8bdb65bc066e032897b828144066", null ],
    [ "WFE_CR3_I2C1_EV", "group___w_f_e___registers___bits___definition.html#ga7b02735c7da75d3b697be2ba5ce364a8", null ],
    [ "WFE_CR3_SPI1_EV", "group___w_f_e___registers___bits___definition.html#ga59d3d82c17edee76481502d1319d34aa", null ],
    [ "WFE_CR3_TIM3_EV0", "group___w_f_e___registers___bits___definition.html#ga429772977aad6148b92d37dac89dd17a", null ],
    [ "WFE_CR3_TIM3_EV1", "group___w_f_e___registers___bits___definition.html#gaad2e7acd04591e28eaf9fcb3957c8e08", null ],
    [ "WFE_CR3_TIM4_EV", "group___w_f_e___registers___bits___definition.html#ga16cf01b18d8553a8f2b225caa44adb3e", null ],
    [ "WFE_CR3_USART1_EV", "group___w_f_e___registers___bits___definition.html#ga153d46499d1bef3d05e89e25efd4bf13", null ],
    [ "WFE_CR4_AES_EV", "group___w_f_e___registers___bits___definition.html#gadf4a743897a1a1d6290c3cb0c1097ab2", null ],
    [ "WFE_CR4_RTC_CSS_EV", "group___w_f_e___registers___bits___definition.html#gab4c16ff1feae2241de372962cdc59068", null ],
    [ "WFE_CR4_SPI2_EV", "group___w_f_e___registers___bits___definition.html#gaa80dbc27604b50b8c5ad4e9687f15ef5", null ],
    [ "WFE_CR4_TIM5_EV0", "group___w_f_e___registers___bits___definition.html#ga52ad101ccd06cad7bb8b1dfb34229d0f", null ],
    [ "WFE_CR4_TIM5_EV1", "group___w_f_e___registers___bits___definition.html#gac44957e78949db67027a244629524da6", null ],
    [ "WFE_CR4_USART2_EV", "group___w_f_e___registers___bits___definition.html#ga31668abd460771d799e2a34afc735ee4", null ],
    [ "WFE_CR4_USART3_EV", "group___w_f_e___registers___bits___definition.html#ga04c5915279034522fb2fb88892990d02", null ]
];