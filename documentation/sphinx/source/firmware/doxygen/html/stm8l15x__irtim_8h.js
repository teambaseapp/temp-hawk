var stm8l15x__irtim_8h =
[
    [ "IRTIM_Cmd", "group___i_r_t_i_m.html#ga1bee9c9b3ec40fc0f7c88320a59a2426", null ],
    [ "IRTIM_DeInit", "group___i_r_t_i_m.html#ga3ba54662d703d0c2b070af8f6322de7b", null ],
    [ "IRTIM_GetHighSinkODStatus", "group___i_r_t_i_m.html#gabdc6e7448645a0983e37ce0862255171", null ],
    [ "IRTIM_GetStatus", "group___i_r_t_i_m.html#gac59ae9ffce236b112b7f0e974dc66a35", null ],
    [ "IRTIM_HighSinkODCmd", "group___i_r_t_i_m.html#gae8d7c76bf1a0407a44a93f4e28ecce5f", null ]
];