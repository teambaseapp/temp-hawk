var group___t_i_m1___lock___level =
[
    [ "TIM1_LockLevel_TypeDef", "group___t_i_m1___lock___level.html#ga325a906d797b656263bf549bb6c6e20d", [
      [ "TIM1_LockLevel_Off", "group___t_i_m1___lock___level.html#gga325a906d797b656263bf549bb6c6e20da5ff35cdb47d2a7416b080f33da910ee4", null ],
      [ "TIM1_LockLevel_1", "group___t_i_m1___lock___level.html#gga325a906d797b656263bf549bb6c6e20da8665b4a2a7c1f3878bb1a6c577435644", null ],
      [ "TIM1_LockLevel_2", "group___t_i_m1___lock___level.html#gga325a906d797b656263bf549bb6c6e20da768d695bc8a2a1305810c267d4133c1c", null ],
      [ "TIM1_LockLevel_3", "group___t_i_m1___lock___level.html#gga325a906d797b656263bf549bb6c6e20da503d517b9c76846d433fcb250cb65567", null ]
    ] ]
];