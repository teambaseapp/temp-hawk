var stm8l15x__comp_8c =
[
    [ "COMP_ClearFlag", "group___c_o_m_p___group5.html#gae6f1c1ce82575c7507e819248e76b34d", null ],
    [ "COMP_ClearITPendingBit", "group___c_o_m_p___group5.html#ga30f23b9298a45bb664af297493ff366b", null ],
    [ "COMP_DeInit", "group___c_o_m_p___group1.html#ga6425d7445b304d2dd1bf57c1fb5e70c2", null ],
    [ "COMP_EdgeConfig", "group___c_o_m_p___group1.html#ga2024f9f2ba375b51e49c6a1fbd749584", null ],
    [ "COMP_GetFlagStatus", "group___c_o_m_p___group5.html#ga07711477a11774cf11a982db191cac9e", null ],
    [ "COMP_GetITStatus", "group___c_o_m_p___group5.html#gaef83885a9947d64b394bd2a12fb16c64", null ],
    [ "COMP_GetOutputLevel", "group___c_o_m_p___group1.html#ga85d4acd8f0b09d19f533b1dc3c60fd61", null ],
    [ "COMP_Init", "group___c_o_m_p___group1.html#ga6b0439181e7fe8c1f89a34c8a8c33527", null ],
    [ "COMP_ITConfig", "group___c_o_m_p___group5.html#gab3d794eaa8bc584ae195c044b00aa573", null ],
    [ "COMP_SchmittTriggerCmd", "group___c_o_m_p___group4.html#gaa249e923a4b1b122bf1574218a0da284", null ],
    [ "COMP_TriggerConfig", "group___c_o_m_p___group4.html#ga5f5899c5799a4275aadc5fe2b3f8950b", null ],
    [ "COMP_VrefintOutputCmd", "group___c_o_m_p___group3.html#ga7cc2f05181085dda3c4f0e35351e7cff", null ],
    [ "COMP_VrefintToCOMP1Connect", "group___c_o_m_p___group1.html#gaf51bd5556f4085b68ee156b484200483", null ],
    [ "COMP_WindowCmd", "group___c_o_m_p___group2.html#ga65b8886e4554b35de6211f46c7b55b41", null ]
];