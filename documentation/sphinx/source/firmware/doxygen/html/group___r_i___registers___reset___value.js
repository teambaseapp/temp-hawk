var group___r_i___registers___reset___value =
[
    [ "RI_ASCR1_RESET_VALUE", "group___r_i___registers___reset___value.html#ga728591d9eb9f3f12576d4d150d36d47e", null ],
    [ "RI_ASCR2_RESET_VALUE", "group___r_i___registers___reset___value.html#gaa2844dcec556dbaca32b686139084c28", null ],
    [ "RI_ICR1_RESET_VALUE", "group___r_i___registers___reset___value.html#gab613cfe95418586e0714d406aff796a6", null ],
    [ "RI_ICR2_RESET_VALUE", "group___r_i___registers___reset___value.html#ga10d551ffccbde5b4d4606975ba0be431", null ],
    [ "RI_IOCMR1_RESET_VALUE", "group___r_i___registers___reset___value.html#ga4271ba005089b6cbde7b64c41852ccd9", null ],
    [ "RI_IOCMR2_RESET_VALUE", "group___r_i___registers___reset___value.html#ga21eff9ff0009466c0573c78bc341f3e4", null ],
    [ "RI_IOCMR3_RESET_VALUE", "group___r_i___registers___reset___value.html#ga30186461122d9aada64f7d7fa45e7dd2", null ],
    [ "RI_IOCMR4_RESET_VALUE", "group___r_i___registers___reset___value.html#gad111fc8eecff94876709d6238d6e00a4", null ],
    [ "RI_IOGCR_RESET_VALUE", "group___r_i___registers___reset___value.html#ga9ebd74051c2f7dd2ae3d2b76417d9d97", null ],
    [ "RI_IOSR1_RESET_VALUE", "group___r_i___registers___reset___value.html#ga1b5d3f6379c78ccdd4ad742a404f1dd0", null ],
    [ "RI_IOSR2_RESET_VALUE", "group___r_i___registers___reset___value.html#ga92f1cfd87174869dd2a33328d529eb7e", null ],
    [ "RI_IOSR3_RESET_VALUE", "group___r_i___registers___reset___value.html#ga89676fc4d187fdbc8296e3121d0b1417", null ],
    [ "RI_IOSR4_RESET_VALUE", "group___r_i___registers___reset___value.html#gad7290d00a3c5646db97cc7f2b7371ed8", null ],
    [ "RI_RCR_RESET_VALUE", "group___r_i___registers___reset___value.html#ga714a32965c1edf6fe350a8d8218f1797", null ]
];