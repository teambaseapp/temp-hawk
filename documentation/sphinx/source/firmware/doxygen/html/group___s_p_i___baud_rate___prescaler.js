var group___s_p_i___baud_rate___prescaler =
[
    [ "SPI_BaudRatePrescaler_TypeDef", "group___s_p_i___baud_rate___prescaler.html#gafd08e5cb7cff2cd9cd8de72b8246961c", [
      [ "SPI_BaudRatePrescaler_2", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961cae3fa031ff038fcb3c491d28779b1e26b", null ],
      [ "SPI_BaudRatePrescaler_4", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961ca7cf658ad85d48091d3c1fcc050a6efb1", null ],
      [ "SPI_BaudRatePrescaler_8", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961ca276c22abb80cac9f2a074339393e7979", null ],
      [ "SPI_BaudRatePrescaler_16", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961cae2e2d51474cb5863a083c4c8134ed2a3", null ],
      [ "SPI_BaudRatePrescaler_32", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961ca9434623cfd2ea9d3a1147b815a56d412", null ],
      [ "SPI_BaudRatePrescaler_64", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961cacc4485daac112a6e27bc03dd41894e1b", null ],
      [ "SPI_BaudRatePrescaler_128", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961caeb9ded1982accafea3e4729621788872", null ],
      [ "SPI_BaudRatePrescaler_256", "group___s_p_i___baud_rate___prescaler.html#ggafd08e5cb7cff2cd9cd8de72b8246961ca6aad64e4c3a18db5825cbd2612305bd5", null ]
    ] ]
];