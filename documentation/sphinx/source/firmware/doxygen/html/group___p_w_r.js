var group___p_w_r =
[
    [ "PWR_Exported_Types", "group___p_w_r___exported___types.html", "group___p_w_r___exported___types" ],
    [ "PWR_Private_Functions", "group___p_w_r___private___functions.html", "group___p_w_r___private___functions" ],
    [ "PWR_DeInit", "group___p_w_r.html#gad03a0aac7bc3bc3a9fd012f3769a6990", null ],
    [ "PWR_FastWakeUpCmd", "group___p_w_r.html#ga548792d51d227ff55f475bcb9d67573f", null ],
    [ "PWR_GetFlagStatus", "group___p_w_r.html#gab84db49950a589c95bbabb62320f25e8", null ],
    [ "PWR_PVDClearFlag", "group___p_w_r.html#ga569fea4abf788c628c79a03e12b18fea", null ],
    [ "PWR_PVDClearITPendingBit", "group___p_w_r.html#ga271e9cdf724871f7133788bbfe870a31", null ],
    [ "PWR_PVDCmd", "group___p_w_r.html#ga42cad476b816e0a33594a933b3ed1acd", null ],
    [ "PWR_PVDGetITStatus", "group___p_w_r.html#ga6015722ce63503e87d7683b0ad0da6b9", null ],
    [ "PWR_PVDITConfig", "group___p_w_r.html#ga0b582081368ee74e570d4e84a3400558", null ],
    [ "PWR_PVDLevelConfig", "group___p_w_r.html#ga8cddc7e5670d82acd64480b604fb1655", null ],
    [ "PWR_UltraLowPowerCmd", "group___p_w_r.html#ga862a54f4cdf1cec028432db1068419d1", null ]
];