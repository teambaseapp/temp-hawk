var stm8l15x__itc_8h =
[
    [ "IRQn_TypeDef", "group___i_t_c___interrupt___lines__selection.html#ga349e7e8c18dce6e8cc7fd365e8fc8607", [
      [ "FLASH_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a91b73963ce243a1d031576d49e137fab", null ],
      [ "DMA1_CHANNEL0_1_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607aa168632953a93586059656e9eae83987", null ],
      [ "DMA1_CHANNEL2_3_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607aa3b2e5541e8c5879adf4697df35e047d", null ],
      [ "EXTIE_F_PVD_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a2c7c99966cd5d971dcf7d371367b3a03", null ],
      [ "EXTI0_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607ab6aa6f87d26bbc6cf99b067b8d75c2f7", null ],
      [ "EXTI1_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607ae4badcdecdb94eb10129c4c0577c5e19", null ],
      [ "EXTI2_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a082cb3f7839069a0715fd76c7eacbbc9", null ],
      [ "EXTI3_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607add889c84ba5de466ced209069e05d602", null ],
      [ "EXTI4_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607ab70a40106ca4486770df5d2072d9ac0e", null ],
      [ "EXTI5_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a105bf3a13885747818600452449bc78e", null ],
      [ "EXTI6_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a7b542b7ec9486702510b7f21ed329b6a", null ],
      [ "EXTI7_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a52bc9391a791c7cca1d584fb78e9d9a6", null ],
      [ "ADC1_COMP_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a1f21d9fac09be01d78b835759e21d012", null ],
      [ "TIM4_UPD_OVF_TRG_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a7a787cda98c2fdc5edc1376bbfa51c0d", null ],
      [ "SPI1_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607aacdff1a9c42582ed663214cbe62c1174", null ],
      [ "RTC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607adcc0f2770f7f57f75fac3d8bcac0e858", null ],
      [ "EXTIB_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a555b05f5f89985e8d4f61c1cd4f5e15d", null ],
      [ "EXTID_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a5f2fa6c74b08a1682136610de3200bbd", null ],
      [ "LCD_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a161916b33cc34138d17e57eaa8464568", null ],
      [ "SWITCH_CSS_BREAK_DAC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a1d0c23c80da7da91c30c1fc697266347", null ],
      [ "TIM2_UPD_OVF_TRG_BRK_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a4b77ece6cbe92831121652ae60c13cd0", null ],
      [ "TIM2_CC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a5829f31a3fce75cbbc31ae8eb263691f", null ],
      [ "TIM3_UPD_OVF_TRG_BRK_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a6b424a7d3a62f3d7fb599438f6b75638", null ],
      [ "TIM3_CC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607ae59f47c2d4739914b0a7e381c65e190d", null ],
      [ "TIM1_UPD_OVF_TRG_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607aea106c7be536b397a8e466e1a8a827d8", null ],
      [ "TIM1_CC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a209b287e2753e7a7d662c7cfb392c42f", null ],
      [ "USART1_TX_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a16bf6a84af16c41ef05d15b021c6d5d5", null ],
      [ "USART1_RX_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a99be16bb9ba2411ab60547803d5221b7", null ],
      [ "I2C1_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a5311dc2bd5f163bff06af1f87b0e2d22", null ],
      [ "EXTIB_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a555b05f5f89985e8d4f61c1cd4f5e15d", null ],
      [ "EXTID_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a5f2fa6c74b08a1682136610de3200bbd", null ],
      [ "SWITCH_CSS_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607aadd9d33a5ed816bf0308250cb41e4df8", null ],
      [ "TIM2_UPD_OVF_TRG_BRK_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a4b77ece6cbe92831121652ae60c13cd0", null ],
      [ "TIM2_CC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a5829f31a3fce75cbbc31ae8eb263691f", null ],
      [ "TIM3_UPD_OVF_TRG_BRK_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a6b424a7d3a62f3d7fb599438f6b75638", null ],
      [ "TIM3_CC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607ae59f47c2d4739914b0a7e381c65e190d", null ],
      [ "USART1_TX_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a16bf6a84af16c41ef05d15b021c6d5d5", null ],
      [ "USART1_RX_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a99be16bb9ba2411ab60547803d5221b7", null ],
      [ "I2C1_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a5311dc2bd5f163bff06af1f87b0e2d22", null ],
      [ "EXTIB_G_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a76dec5f64659734e5fd0513fb33939c4", null ],
      [ "EXTID_H_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a37f18ca0d142601158a7e11085750be0", null ],
      [ "LCD_AES_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607ae14c17755beb546328ee39433f535c82", null ],
      [ "SWITCH_CSS_BREAK_DAC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a1d0c23c80da7da91c30c1fc697266347", null ],
      [ "TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607acc4d4b74fd9b54cf30a59772b6f62aa5", null ],
      [ "TIM2_CC_USART2_RX_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607abb4e77f96822b4b35a44a55ab36d1123", null ],
      [ "TIM3_UPD_OVF_TRG_BRK_USART3_TX_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a71ce93e05a5ab82918d3fba84a7411be", null ],
      [ "TIM3_CC_USART3_RX_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a97fd93fdfed0e252899297d277460ccc", null ],
      [ "TIM1_UPD_OVF_TRG_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607aea106c7be536b397a8e466e1a8a827d8", null ],
      [ "TIM1_CC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a209b287e2753e7a7d662c7cfb392c42f", null ],
      [ "USART1_TX_TIM5_UPD_OVF_TRG_BRK_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a390d5e3811a678db6aa710ffabcbeb16", null ],
      [ "USART1_RX_TIM5_CC_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607a9542c49aa127c3c49462487ed7914716", null ],
      [ "I2C1_SPI2_IRQn", "group___i_t_c___interrupt___lines__selection.html#gga349e7e8c18dce6e8cc7fd365e8fc8607adb299afdf3e4ff2b9db7e5693e998d47", null ]
    ] ],
    [ "ITC_PriorityLevel_TypeDef", "group___i_t_c___priority___level__selection.html#gad0fc5e0bd0301dcdb97ebf1b54ea367c", [
      [ "ITC_PriorityLevel_0", "group___i_t_c___priority___level__selection.html#ggad0fc5e0bd0301dcdb97ebf1b54ea367ca96f301338e90e0b07c7e3bdaf2413eae", null ],
      [ "ITC_PriorityLevel_1", "group___i_t_c___priority___level__selection.html#ggad0fc5e0bd0301dcdb97ebf1b54ea367ca9679e6246d6f3bf6bf158a5c6f19fd3a", null ],
      [ "ITC_PriorityLevel_2", "group___i_t_c___priority___level__selection.html#ggad0fc5e0bd0301dcdb97ebf1b54ea367ca6ed944af9758f6ced0118a41566ccd28", null ],
      [ "ITC_PriorityLevel_3", "group___i_t_c___priority___level__selection.html#ggad0fc5e0bd0301dcdb97ebf1b54ea367cae25a325c3a76f1711f7821dc8334f919", null ]
    ] ],
    [ "ITC_DeInit", "group___i_t_c.html#ga33de86ed566796e4fdce18197188ed1a", null ],
    [ "ITC_GetCPUCC", "group___i_t_c.html#ga6baafae4c11a073d7ef221b89bb757bd", null ],
    [ "ITC_GetSoftIntStatus", "group___i_t_c.html#ga3507955493a20af5edf731c7606c5a7a", null ],
    [ "ITC_GetSoftwarePriority", "group___i_t_c.html#ga1f9946df3f5f8b8614e30fe669e8a963", null ],
    [ "ITC_SetSoftwarePriority", "group___i_t_c.html#ga02a3e474902ec96b0c094b4998c141a9", null ]
];