var group___t_i_m3___group2 =
[
    [ "Input Capture management functions", "group___t_i_m3___group3.html", "group___t_i_m3___group3" ],
    [ "Interrupts DMA and flags management functions", "group___t_i_m3___group4.html", "group___t_i_m3___group4" ],
    [ "Clocks management functions", "group___t_i_m3___group5.html", "group___t_i_m3___group5" ],
    [ "Synchronization management functions", "group___t_i_m3___group6.html", "group___t_i_m3___group6" ],
    [ "Specific interface management functions", "group___t_i_m3___group7.html", "group___t_i_m3___group7" ],
    [ "TIM3_BKRConfig", "group___t_i_m3___group2.html#gaea3a212c0944283d902b5ebb810297ce", null ],
    [ "TIM3_CCxCmd", "group___t_i_m3___group2.html#ga1ecd2117b16f4f2acde9b854ae7cc8b2", null ],
    [ "TIM3_CtrlPWMOutputs", "group___t_i_m3___group2.html#ga1b11133b1ae495f4b8c6520a1fb3149b", null ],
    [ "TIM3_ForcedOC1Config", "group___t_i_m3___group2.html#gafabf657c65bfc62ce4a17308c73339df", null ],
    [ "TIM3_ForcedOC2Config", "group___t_i_m3___group2.html#gafb7c996ac8c7b6038b1b24f6305d6442", null ],
    [ "TIM3_OC1FastConfig", "group___t_i_m3___group2.html#ga27ab5153d07d8fa112ef4f1fdc7fc14e", null ],
    [ "TIM3_OC1Init", "group___t_i_m3___group2.html#ga8a5538e59a43b91af458c1531a6aac04", null ],
    [ "TIM3_OC1PolarityConfig", "group___t_i_m3___group2.html#ga78964654660298146dd1d57c4165242e", null ],
    [ "TIM3_OC1PreloadConfig", "group___t_i_m3___group2.html#ga925ba6a1d5a2fc6257d9f4cd9e813851", null ],
    [ "TIM3_OC2FastConfig", "group___t_i_m3___group2.html#gae5b38893486b2c27b2c4376e201c7dc8", null ],
    [ "TIM3_OC2Init", "group___t_i_m3___group2.html#ga395f01e3048b207748bedf06349c6cc1", null ],
    [ "TIM3_OC2PolarityConfig", "group___t_i_m3___group2.html#gad200a694618c2dde554e4d2d3adbfa63", null ],
    [ "TIM3_OC2PreloadConfig", "group___t_i_m3___group2.html#ga4b5451cd4f17b92501b95f9c767a9e6a", null ],
    [ "TIM3_SelectOCxM", "group___t_i_m3___group2.html#ga90e5a84cdcf123a8e25bb4b3a6cd0b8d", null ],
    [ "TIM3_SetCompare1", "group___t_i_m3___group2.html#gad18d4e6ea548a78d0aeb442709a7aff6", null ],
    [ "TIM3_SetCompare2", "group___t_i_m3___group2.html#gad8399482fa0b9d36dffcf76e245a28ac", null ]
];