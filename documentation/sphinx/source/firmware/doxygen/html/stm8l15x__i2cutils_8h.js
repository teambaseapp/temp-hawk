var stm8l15x__i2cutils_8h =
[
    [ "GPIOI2CConf", "stm8l15x__i2cutils_8h.html#af7de495dd6ad9677ba9b33a6df77223d", null ],
    [ "I2CRead", "stm8l15x__i2cutils_8h.html#af97522afc448e629022cb1000c0aa61f", null ],
    [ "I2CSoftReset", "stm8l15x__i2cutils_8h.html#a9d66d87e7af3971415bfd85c65520de2", null ],
    [ "I2CWrite", "stm8l15x__i2cutils_8h.html#a93d1de6b841cd16235a61828a2c33b6b", null ],
    [ "SetUpI2C", "stm8l15x__i2cutils_8h.html#aabd562c85526eb4bca9417c0b11ce391", null ]
];