var group___c_o_m_p___exported___types =
[
    [ "COMP_Selection", "group___c_o_m_p___selection.html", "group___c_o_m_p___selection" ],
    [ "COMP_Edge", "group___c_o_m_p___edge.html", "group___c_o_m_p___edge" ],
    [ "COMP_Inverting_Input_Selection", "group___c_o_m_p___inverting___input___selection.html", "group___c_o_m_p___inverting___input___selection" ],
    [ "COMP2_Output_Selection", "group___c_o_m_p2___output___selection.html", "group___c_o_m_p2___output___selection" ],
    [ "COMP_Speed", "group___c_o_m_p___speed.html", "group___c_o_m_p___speed" ],
    [ "COMP_Trigger_Group", "group___c_o_m_p___trigger___group.html", "group___c_o_m_p___trigger___group" ],
    [ "COMP_Trigger_Pin", "group___c_o_m_p___trigger___pin.html", "group___c_o_m_p___trigger___pin" ],
    [ "COMP_Output_Level", "group___c_o_m_p___output___level.html", "group___c_o_m_p___output___level" ]
];