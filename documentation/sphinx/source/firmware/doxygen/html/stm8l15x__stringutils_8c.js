var stm8l15x__stringutils_8c =
[
    [ "CalculateOffset", "stm8l15x__stringutils_8c.html#a6887c1b8f75074ea0571f2840078d281", null ],
    [ "CheckAcknowledgement", "stm8l15x__stringutils_8c.html#aaaa76cffebb4ac567ec85c64cd8ce239", null ],
    [ "CheckDeviceID", "stm8l15x__stringutils_8c.html#a5212e321e6a9d4949a7d5faeb64ed60f", null ],
    [ "CheckForAcknowledgementRequest", "stm8l15x__stringutils_8c.html#ab17c87efaf601f9b73f8786886d2850f", null ],
    [ "GetAcknowledgementData", "stm8l15x__stringutils_8c.html#a00308d9c93854f00393330b52f77401f", null ],
    [ "GetData", "stm8l15x__stringutils_8c.html#a24d650aac24f9b8e60d54e0bab9ab772", null ],
    [ "GetOffset", "stm8l15x__stringutils_8c.html#a87ecc7afd688df48a539e9d6ac51ad23", null ],
    [ "RFM69_Data", "stm8l15x__stringutils_8c.html#ae944496feae7656102eaf0a29d9b57a3", null ]
];