var stm8l15x__dac_8h =
[
    [ "DAC_Align_TypeDef", "group___d_a_c__data__alignment.html#gaea7b608667dd5438ad8944bb3f7206d4", [
      [ "DAC_Align_12b_R", "group___d_a_c__data__alignment.html#ggaea7b608667dd5438ad8944bb3f7206d4ab0428733486fe0de32e7bbfd1ab1507d", null ],
      [ "DAC_Align_12b_L", "group___d_a_c__data__alignment.html#ggaea7b608667dd5438ad8944bb3f7206d4abc08eab26462c294f8e7f98ed84b187a", null ],
      [ "DAC_Align_8b_R", "group___d_a_c__data__alignment.html#ggaea7b608667dd5438ad8944bb3f7206d4aa4f9866c2dc515048faf5e9d1de418af", null ]
    ] ],
    [ "DAC_Channel_TypeDef", "group___d_a_c___channel__selection.html#gad6c71e7738514cfeae711a9a25186fc5", [
      [ "DAC_Channel_1", "group___d_a_c___channel__selection.html#ggad6c71e7738514cfeae711a9a25186fc5ae1a419af74f34d32349f658d9e3b7657", null ],
      [ "DAC_Channel_2", "group___d_a_c___channel__selection.html#ggad6c71e7738514cfeae711a9a25186fc5a98549f575bf809212a780605930f0cb2", null ]
    ] ],
    [ "DAC_FLAG_TypeDef", "group___d_a_c__flags__definition.html#ga71a2859dac0c7639191c7310604406d0", [
      [ "DAC_FLAG_DMAUDR", "group___d_a_c__flags__definition.html#gga71a2859dac0c7639191c7310604406d0aa074bd037024ac77363f7af9c388850d", null ]
    ] ],
    [ "DAC_IT_TypeDef", "group___d_a_c__interrupts__definition.html#ga6bf1eed6b42caacf243fa3a1aead9720", [
      [ "DAC_IT_DMAUDR", "group___d_a_c__interrupts__definition.html#gga6bf1eed6b42caacf243fa3a1aead9720ae61593db553b309f9b6f0292544aef93", null ]
    ] ],
    [ "DAC_LFSRUnmask_TypeDef", "group___d_a_c__lfsrunmask.html#ga6b3e516413969fc2ce43cb77e2585ede", [
      [ "DAC_LFSRUnmask_Bit0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edeac03c81bdf16179b33e64c2dab9d7e68a", null ],
      [ "DAC_LFSRUnmask_Bits1_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edea06c593b1f7f03c93277ae7385bbd603e", null ],
      [ "DAC_LFSRUnmask_Bits2_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edea6d4bcd0533b02a4d3134fa6d56c3a71a", null ],
      [ "DAC_LFSRUnmask_Bits3_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edea997b5b0af00562aa6dda5b2703befb75", null ],
      [ "DAC_LFSRUnmask_Bits4_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edeaab4005b5bb388e71117672b9420ad2d7", null ],
      [ "DAC_LFSRUnmask_Bits5_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edeac482065e43e4bb808505f33b08534feb", null ],
      [ "DAC_LFSRUnmask_Bits6_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edeacb4a31ec4bb173231b8f2b4330ece7f5", null ],
      [ "DAC_LFSRUnmask_Bits7_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edea3e0499e477ced048262463e32b704ad9", null ],
      [ "DAC_LFSRUnmask_Bits8_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edea375963a61bb47ec8ee6ce5f6c5fd6c8b", null ],
      [ "DAC_LFSRUnmask_Bits9_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edea5b2cdee25b0fe39236fffdd8bb2c68b0", null ],
      [ "DAC_LFSRUnmask_Bits10_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edead64733012c27b0641ddc15f3d525b3f5", null ],
      [ "DAC_LFSRUnmask_Bits11_0", "group___d_a_c__lfsrunmask.html#gga6b3e516413969fc2ce43cb77e2585edea1881709128eb88961fd9643e8c189e8a", null ]
    ] ],
    [ "DAC_OutputBuffer_TypeDef", "group___d_a_c__output__buffer.html#ga5459cd9c367ab56b0a7d15d37597b40e", [
      [ "DAC_OutputBuffer_Enable", "group___d_a_c__output__buffer.html#gga5459cd9c367ab56b0a7d15d37597b40ea70b619c8ff2f6c71a7a26d672c18541e", null ],
      [ "DAC_OutputBuffer_Disable", "group___d_a_c__output__buffer.html#gga5459cd9c367ab56b0a7d15d37597b40ea0eadcf0224bbb3c14e3b23b2d6064a3b", null ]
    ] ],
    [ "DAC_TriangleAmplitude_TypeDef", "group___d_a_c__triangleamplitude.html#gabd34f14bff8787d88b561d0a7f09af45", [
      [ "DAC_TriangleAmplitude_1", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a1d3f1b67a4541823885df92dcf8c664d", null ],
      [ "DAC_TriangleAmplitude_3", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a8f09d9a6789d731f6544bf00a268096f", null ],
      [ "DAC_TriangleAmplitude_7", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45aabf742d4b48b53577b4569ad462c1926", null ],
      [ "DAC_TriangleAmplitude_15", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a3d71741e32bf881c7acd3f39d8d98bd0", null ],
      [ "DAC_TriangleAmplitude_31", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a6d1a7aa89fceaa22f64a91a3edfb7768", null ],
      [ "DAC_TriangleAmplitude_63", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a331bdbe6d1bbf91b8f6e7640fad8022c", null ],
      [ "DAC_TriangleAmplitude_127", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a209550fbd459302532105d1aaecda3f2", null ],
      [ "DAC_TriangleAmplitude_255", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45aef048d8e865d690259928155cec55c2f", null ],
      [ "DAC_TriangleAmplitude_511", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a7f5988132d87ae59c476de6268fe9b49", null ],
      [ "DAC_TriangleAmplitude_1023", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a5b27827245f4e2a3b390a23f32cdf58d", null ],
      [ "DAC_TriangleAmplitude_2047", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45afab513622fab893cc9f7a748ab641d10", null ],
      [ "DAC_TriangleAmplitude_4095", "group___d_a_c__triangleamplitude.html#ggabd34f14bff8787d88b561d0a7f09af45a5bded37d8ecc0526e179d5c6ea133045", null ]
    ] ],
    [ "DAC_Trigger_TypeDef", "group___d_a_c__trigger__selection.html#ga250d061d0f223ae8327008cc0a1b177e", [
      [ "DAC_Trigger_None", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177ea2a84d20905a37456d852b7918229ddd2", null ],
      [ "DAC_Trigger_T4_TRGO", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177eab830042207686889f7cec2277fb6770d", null ],
      [ "DAC_Trigger_T5_TRGO", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177ea83e6d7648b308fe6a3f89ef9bdb75dbd", null ],
      [ "DAC_Trigger_Ext", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177eafa60873154797f8ddfc7c3ea4064132e", null ],
      [ "DAC_Trigger_Software", "group___d_a_c__trigger__selection.html#gga250d061d0f223ae8327008cc0a1b177eac78196bbfac35147d46f3ede4254a8a0", null ]
    ] ],
    [ "DAC_Wave_TypeDef", "group___d_a_c__wave__generation.html#gad51c3f744c1b7628fbf94dd46e05b194", [
      [ "DAC_Wave_Noise", "group___d_a_c__wave__generation.html#ggad51c3f744c1b7628fbf94dd46e05b194ac6350d1613a8dd3c98556ffeaf76f55f", null ],
      [ "DAC_Wave_Triangle", "group___d_a_c__wave__generation.html#ggad51c3f744c1b7628fbf94dd46e05b194aeeadf87c440f12c9fd50c74ba84f8b07", null ]
    ] ],
    [ "DAC_ClearFlag", "group___d_a_c.html#gacf8277882eb81c1f157d84d5ea0714c5", null ],
    [ "DAC_ClearITPendingBit", "group___d_a_c.html#ga7a45fa7b420605403db0f5e606f08e37", null ],
    [ "DAC_Cmd", "group___d_a_c.html#gad2fa4067680c58119e4520a3d4a9c2aa", null ],
    [ "DAC_DeInit", "group___d_a_c.html#ga1fae225204e1e049d6795319e99ba8bc", null ],
    [ "DAC_DMACmd", "group___d_a_c.html#ga31a200a6c9263162a90728da53c8cc5f", null ],
    [ "DAC_DualSoftwareTriggerCmd", "group___d_a_c.html#gab4d3b364a6b184dcd65f3b294ebf56dc", null ],
    [ "DAC_GetDataOutputValue", "group___d_a_c.html#gae7ccf57321c2adeae4c8e0f6f8383be7", null ],
    [ "DAC_GetFlagStatus", "group___d_a_c.html#ga5d6f065020cc7c29bb971174a53cf288", null ],
    [ "DAC_GetITStatus", "group___d_a_c.html#gae497a37de02f31295d0fb73369b1762c", null ],
    [ "DAC_Init", "group___d_a_c.html#gab622f15aacaa0dbaf86371aa023ae3ef", null ],
    [ "DAC_ITConfig", "group___d_a_c.html#ga1da89b84200d4b54ffc1060400d2cb88", null ],
    [ "DAC_SetChannel1Data", "group___d_a_c.html#ga1a473940a91ef4402ece55a543de3b85", null ],
    [ "DAC_SetChannel2Data", "group___d_a_c.html#ga8a893e4126c65a2406dfef3ca00469e9", null ],
    [ "DAC_SetDualChannelData", "group___d_a_c.html#gac670ae80de1812520fded8d710722dd1", null ],
    [ "DAC_SetNoiseWaveLFSR", "group___d_a_c.html#gafa6684f7e0b64f41bd765a789de113a1", null ],
    [ "DAC_SetTriangleWaveAmplitude", "group___d_a_c.html#ga2fb7989e03b2e7d75ede80638af754a7", null ],
    [ "DAC_SoftwareTriggerCmd", "group___d_a_c.html#ga066d876248dea2a09cd092817784e67e", null ],
    [ "DAC_WaveGenerationCmd", "group___d_a_c.html#ga84d280cad96abdfe54c626a100304207", null ]
];