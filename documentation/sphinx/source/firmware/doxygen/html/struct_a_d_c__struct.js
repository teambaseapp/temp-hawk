var struct_a_d_c__struct =
[
    [ "CR1", "struct_a_d_c__struct.html#abdb5e2aed90a3a46151c8bb740665579", null ],
    [ "CR2", "struct_a_d_c__struct.html#a27d59494343f46e0df9e3d0caf483896", null ],
    [ "CR3", "struct_a_d_c__struct.html#ac24825a4ec134141fe7ab87eb4a7e996", null ],
    [ "DRH", "struct_a_d_c__struct.html#a63050bd150b4738ee7cc8a73dc058160", null ],
    [ "DRL", "struct_a_d_c__struct.html#a8294715cb35b6f096f6a4cf3f1d7dbc7", null ],
    [ "HTRH", "struct_a_d_c__struct.html#a8fdd454b6f11f88f736180c52e51c343", null ],
    [ "HTRL", "struct_a_d_c__struct.html#a6a7ec312aae63a38070c25abefa4acb8", null ],
    [ "LTRH", "struct_a_d_c__struct.html#a9edb2a64c3636244528e147ce2a2916f", null ],
    [ "LTRL", "struct_a_d_c__struct.html#a7b052678b34d6093ed11ab372dcc5089", null ],
    [ "SQR", "struct_a_d_c__struct.html#abdb49dd753c627c61b0aef3cf5aadf39", null ],
    [ "SR", "struct_a_d_c__struct.html#a5634132d0d636b9eac05627fe9e2b2f9", null ],
    [ "TRIGR", "struct_a_d_c__struct.html#ab219cbca68566cac0e007c6bee969979", null ]
];