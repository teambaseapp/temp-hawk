var stm8l15x__tim2_8h =
[
    [ "TIM2_AutomaticOutput_TypeDef", "group___t_i_m2___automatic___output.html#ga31d31d1a4df8c4c047c58babd8714e2c", [
      [ "TIM2_AutomaticOutput_Enable", "group___t_i_m2___automatic___output.html#gga31d31d1a4df8c4c047c58babd8714e2ca0e4f1abd8c6f45620e009a6b322a7da8", null ],
      [ "TIM2_AutomaticOutput_Disable", "group___t_i_m2___automatic___output.html#gga31d31d1a4df8c4c047c58babd8714e2ca5f201ba109b43e9ccff67cbf37cec15f", null ]
    ] ],
    [ "TIM2_BreakPolarity_TypeDef", "group___t_i_m2___break___polarity.html#gabbd781ed0e0a77f978874c5d2240e77c", [
      [ "TIM2_BreakPolarity_High", "group___t_i_m2___break___polarity.html#ggabbd781ed0e0a77f978874c5d2240e77ca6bdc00dd77b5a7fd0a41eed330dfd945", null ],
      [ "TIM2_BreakPolarity_Low", "group___t_i_m2___break___polarity.html#ggabbd781ed0e0a77f978874c5d2240e77ca0e573e162aa61d80ffc80f60a4814ec2", null ]
    ] ],
    [ "TIM2_BreakState_TypeDef", "group___t_i_m2___break___state.html#gaf346dfa8e316a43384cc9447c37a9e92", [
      [ "TIM2_BreakState_Disable", "group___t_i_m2___break___state.html#ggaf346dfa8e316a43384cc9447c37a9e92aa37c433fad7cf8d13f6aff627499c88a", null ],
      [ "TIM2_BreakState_Enable", "group___t_i_m2___break___state.html#ggaf346dfa8e316a43384cc9447c37a9e92a281c59bcf8e497c9efc5f77cad2cbaf9", null ]
    ] ],
    [ "TIM2_Channel_TypeDef", "group___t_i_m2___channel.html#gaec0a07cde66cfda20f77487d03a5bd22", [
      [ "TIM2_Channel_1", "group___t_i_m2___channel.html#ggaec0a07cde66cfda20f77487d03a5bd22a5f280f9e8fe51dfa545a570ddcb65006", null ],
      [ "TIM2_Channel_2", "group___t_i_m2___channel.html#ggaec0a07cde66cfda20f77487d03a5bd22acfb9637778b5f6bd819b85cf74c290d7", null ]
    ] ],
    [ "TIM2_CounterMode_TypeDef", "group___t_i_m2___counter_mode.html#ga4a2d16a202cd58bbdf7c5f497118426c", [
      [ "TIM2_CounterMode_Up", "group___t_i_m2___counter_mode.html#gga4a2d16a202cd58bbdf7c5f497118426ca271a2e3a83e8723368210ea78286d169", null ],
      [ "TIM2_CounterMode_Down", "group___t_i_m2___counter_mode.html#gga4a2d16a202cd58bbdf7c5f497118426caaeaa20bd03746273708a0a3abaf9586e", null ],
      [ "TIM2_CounterMode_CenterAligned1", "group___t_i_m2___counter_mode.html#gga4a2d16a202cd58bbdf7c5f497118426ca4840cccaa9df7c5b8f5c63f70e2e321f", null ],
      [ "TIM2_CounterMode_CenterAligned2", "group___t_i_m2___counter_mode.html#gga4a2d16a202cd58bbdf7c5f497118426caf5186339140c9c10f2c6092c5c481986", null ],
      [ "TIM2_CounterMode_CenterAligned3", "group___t_i_m2___counter_mode.html#gga4a2d16a202cd58bbdf7c5f497118426caa2e07d059b55e6dad40f06671bb43f00", null ]
    ] ],
    [ "TIM2_DMASource_TypeDef", "group___t_i_m2___d_m_a___source___requests.html#gae1649c0174dcecf89619b5d1886639ca", [
      [ "TIM2_DMASource_Update", "group___t_i_m2___d_m_a___source___requests.html#ggae1649c0174dcecf89619b5d1886639caaa27fa444df8a6070051f890ed2bc3009", null ],
      [ "TIM2_DMASource_CC1", "group___t_i_m2___d_m_a___source___requests.html#ggae1649c0174dcecf89619b5d1886639caa505ff3d623316447a9cc582c89444e3e", null ],
      [ "TIM2_DMASource_CC2", "group___t_i_m2___d_m_a___source___requests.html#ggae1649c0174dcecf89619b5d1886639caa3f63117c3540e459b387b58351bbc912", null ]
    ] ],
    [ "TIM2_EncoderMode_TypeDef", "group___t_i_m2___encoder___mode.html#ga2b68ca6553cf32b48e30d37a1f4620df", [
      [ "TIM2_EncoderMode_TI1", "group___t_i_m2___encoder___mode.html#gga2b68ca6553cf32b48e30d37a1f4620dfa3cd41bb178efc6b653ce9f6f4f63e3fa", null ],
      [ "TIM2_EncoderMode_TI2", "group___t_i_m2___encoder___mode.html#gga2b68ca6553cf32b48e30d37a1f4620dfaa1e72c08137e10b4813ac1096bdb0eba", null ],
      [ "TIM2_EncoderMode_TI12", "group___t_i_m2___encoder___mode.html#gga2b68ca6553cf32b48e30d37a1f4620dfab504ba2da0315342db5aed3e3ad56ad9", null ]
    ] ],
    [ "TIM2_EventSource_TypeDef", "group___t_i_m2___event___source.html#ga3570dc8eebffc84e41a6eeb8016a7fbc", [
      [ "TIM2_EventSource_Update", "group___t_i_m2___event___source.html#gga3570dc8eebffc84e41a6eeb8016a7fbca47208c75e561b9e1d0f1ebf32857b63c", null ],
      [ "TIM2_EventSource_CC1", "group___t_i_m2___event___source.html#gga3570dc8eebffc84e41a6eeb8016a7fbcae632e6a751ad3b165051e8aaf7d645c6", null ],
      [ "TIM2_EventSource_CC2", "group___t_i_m2___event___source.html#gga3570dc8eebffc84e41a6eeb8016a7fbcadaa939ce917454821d88c5195a16a83b", null ],
      [ "TIM2_EventSource_Trigger", "group___t_i_m2___event___source.html#gga3570dc8eebffc84e41a6eeb8016a7fbca2e88c8b4d667b50f712f44f70e1724f1", null ],
      [ "TIM2_EventSource_Break", "group___t_i_m2___event___source.html#gga3570dc8eebffc84e41a6eeb8016a7fbca7356d3943092a57d78e6e6387d4a5b5a", null ]
    ] ],
    [ "TIM2_ExtTRGPolarity_TypeDef", "group___t_i_m2___external___trigger___polarity.html#ga0383416ca3c54be1446adb819e6fcdf7", [
      [ "TIM2_ExtTRGPolarity_Inverted", "group___t_i_m2___external___trigger___polarity.html#gga0383416ca3c54be1446adb819e6fcdf7acbd4f6e2a2f6e797e4a4fb2c21a9ca2f", null ],
      [ "TIM2_ExtTRGPolarity_NonInverted", "group___t_i_m2___external___trigger___polarity.html#gga0383416ca3c54be1446adb819e6fcdf7a34cca30cdba799a0e3e3817043c0b6ca", null ]
    ] ],
    [ "TIM2_ExtTRGPSC_TypeDef", "group___t_i_m2___external___trigger___prescaler.html#gad478800137b2621183eee68623efa21b", [
      [ "TIM2_ExtTRGPSC_OFF", "group___t_i_m2___external___trigger___prescaler.html#ggad478800137b2621183eee68623efa21ba1b316df85b1c9c46804cb9b2c3b47e03", null ],
      [ "TIM2_ExtTRGPSC_DIV2", "group___t_i_m2___external___trigger___prescaler.html#ggad478800137b2621183eee68623efa21ba6ed44eb367341fe2059a81915b41956b", null ],
      [ "TIM2_ExtTRGPSC_DIV4", "group___t_i_m2___external___trigger___prescaler.html#ggad478800137b2621183eee68623efa21baea75e14b406c4d0b9181cde1a9bca6a4", null ],
      [ "TIM2_ExtTRGPSC_DIV8", "group___t_i_m2___external___trigger___prescaler.html#ggad478800137b2621183eee68623efa21bae84474eee11cf9b5161a09dd634be9fa", null ]
    ] ],
    [ "TIM2_FLAG_TypeDef", "group___t_i_m2___flags.html#gaeb67e614e6d8b1b42d7476c490c154f6", [
      [ "TIM2_FLAG_Update", "group___t_i_m2___flags.html#ggaeb67e614e6d8b1b42d7476c490c154f6abcf49c85e146337fc708f24c789358f1", null ],
      [ "TIM2_FLAG_CC1", "group___t_i_m2___flags.html#ggaeb67e614e6d8b1b42d7476c490c154f6a4872bf4cbc89f6ee41380c51a8f7d79b", null ],
      [ "TIM2_FLAG_CC2", "group___t_i_m2___flags.html#ggaeb67e614e6d8b1b42d7476c490c154f6a1ed1d568d266d6062feb6ef25882fa27", null ],
      [ "TIM2_FLAG_Trigger", "group___t_i_m2___flags.html#ggaeb67e614e6d8b1b42d7476c490c154f6a7ee527289181f6b3444d674075af1d0a", null ],
      [ "TIM2_FLAG_Break", "group___t_i_m2___flags.html#ggaeb67e614e6d8b1b42d7476c490c154f6a1fdfd33bca3dfe919c1b8a65ba1d678d", null ],
      [ "TIM2_FLAG_CC1OF", "group___t_i_m2___flags.html#ggaeb67e614e6d8b1b42d7476c490c154f6a2398cef6799e7988deaa1b128427f669", null ],
      [ "TIM2_FLAG_CC2OF", "group___t_i_m2___flags.html#ggaeb67e614e6d8b1b42d7476c490c154f6afa55e03a57fc1e1a7c549b74da5186d9", null ]
    ] ],
    [ "TIM2_ForcedAction_TypeDef", "group___t_i_m2___forced___action.html#ga17567836b9129a63f49e40a5e29f1a08", [
      [ "TIM2_ForcedAction_Active", "group___t_i_m2___forced___action.html#gga17567836b9129a63f49e40a5e29f1a08aabd54d7cf18911c4a1113c7569957bec", null ],
      [ "TIM2_ForcedAction_Inactive", "group___t_i_m2___forced___action.html#gga17567836b9129a63f49e40a5e29f1a08a1d6f9bc0212726b9bc9272ae5ba107d3", null ]
    ] ],
    [ "TIM2_ICPolarity_TypeDef", "group___t_i_m2___input___capture___polarity.html#ga3a5747ad2a34ceb8d102fc1935ce12f6", [
      [ "TIM2_ICPolarity_Rising", "group___t_i_m2___input___capture___polarity.html#gga3a5747ad2a34ceb8d102fc1935ce12f6a1aa485e6fee71bd0f8e05366f7d81737", null ],
      [ "TIM2_ICPolarity_Falling", "group___t_i_m2___input___capture___polarity.html#gga3a5747ad2a34ceb8d102fc1935ce12f6a8b620cd28fd81802eb8934a4f0e26452", null ]
    ] ],
    [ "TIM2_ICPSC_TypeDef", "group___t_i_m2___input___capture___prescaler.html#ga0f15999e6dcf204eac258ab3ad871d37", [
      [ "TIM2_ICPSC_DIV1", "group___t_i_m2___input___capture___prescaler.html#gga0f15999e6dcf204eac258ab3ad871d37ad92db63fc27cc1e4c547f7a94df769f5", null ],
      [ "TIM2_ICPSC_DIV2", "group___t_i_m2___input___capture___prescaler.html#gga0f15999e6dcf204eac258ab3ad871d37a8276209193f70bc9dcad52ad357a8949", null ],
      [ "TIM2_ICPSC_DIV4", "group___t_i_m2___input___capture___prescaler.html#gga0f15999e6dcf204eac258ab3ad871d37a4fe960c276764853d1cb8efe0353b9f9", null ],
      [ "TIM2_ICPSC_DIV8", "group___t_i_m2___input___capture___prescaler.html#gga0f15999e6dcf204eac258ab3ad871d37ad64f46ce16effbdf1c953b985d86f087", null ]
    ] ],
    [ "TIM2_ICSelection_TypeDef", "group___t_i_m2___input___capture___selection.html#ga04bfef1fe71ef840bacca17a4f831f14", [
      [ "TIM2_ICSelection_DirectTI", "group___t_i_m2___input___capture___selection.html#gga04bfef1fe71ef840bacca17a4f831f14a9a7fc51b0fca91604b682316cb3ac02f", null ],
      [ "TIM2_ICSelection_IndirectTI", "group___t_i_m2___input___capture___selection.html#gga04bfef1fe71ef840bacca17a4f831f14a9f85f5c4f6b2882bdce732ea6d4b7aa6", null ],
      [ "TIM2_ICSelection_TRGI", "group___t_i_m2___input___capture___selection.html#gga04bfef1fe71ef840bacca17a4f831f14a21570908a5dd3f56890f673fc10ac8a7", null ]
    ] ],
    [ "TIM2_IT_TypeDef", "group___t_i_m2___interrupts.html#gafce8eb89db0de24168b8b2d66c8b912f", [
      [ "TIM2_IT_Update", "group___t_i_m2___interrupts.html#ggafce8eb89db0de24168b8b2d66c8b912fa0b00319f44d055227d8cd3a785fe7001", null ],
      [ "TIM2_IT_CC1", "group___t_i_m2___interrupts.html#ggafce8eb89db0de24168b8b2d66c8b912fa8e5fa2d4146465e38c23f4a2235ef0db", null ],
      [ "TIM2_IT_CC2", "group___t_i_m2___interrupts.html#ggafce8eb89db0de24168b8b2d66c8b912faa63b64359536c112384440fb2f9a584f", null ],
      [ "TIM2_IT_Trigger", "group___t_i_m2___interrupts.html#ggafce8eb89db0de24168b8b2d66c8b912fa60cb5707f735bc028f2275ed7e50e343", null ],
      [ "TIM2_IT_Break", "group___t_i_m2___interrupts.html#ggafce8eb89db0de24168b8b2d66c8b912fa08ad8c5d5fd739dc61b295125d39aed2", null ]
    ] ],
    [ "TIM2_LockLevel_TypeDef", "group___t_i_m2___lock___level.html#ga3598684c6cf5140609d6ded3e5da0d99", [
      [ "TIM2_LockLevel_Off", "group___t_i_m2___lock___level.html#gga3598684c6cf5140609d6ded3e5da0d99a39b16c8f42f24f5976d1d7381a9c9bdb", null ],
      [ "TIM2_LockLevel_1", "group___t_i_m2___lock___level.html#gga3598684c6cf5140609d6ded3e5da0d99a0f9ac91aab827d64fca37f5b4c3c2b09", null ],
      [ "TIM2_LockLevel_2", "group___t_i_m2___lock___level.html#gga3598684c6cf5140609d6ded3e5da0d99a3a7155d7c5004a17b712f2cb176106ec", null ],
      [ "TIM2_LockLevel_3", "group___t_i_m2___lock___level.html#gga3598684c6cf5140609d6ded3e5da0d99a7da1f76d25f1040fd67efdae4e11f36d", null ]
    ] ],
    [ "TIM2_OCIdleState_TypeDef", "group___t_i_m2___output___compare___idle__state.html#ga3fe39ddfc7a533e2b6fafca91afc8dbb", [
      [ "TIM2_OCIdleState_Reset", "group___t_i_m2___output___compare___idle__state.html#gga3fe39ddfc7a533e2b6fafca91afc8dbba0e1b48ba6e642573cf82ba3c9700a505", null ],
      [ "TIM2_OCIdleState_Set", "group___t_i_m2___output___compare___idle__state.html#gga3fe39ddfc7a533e2b6fafca91afc8dbba1bff07d17021b4b6b7603fd5e78dc956", null ]
    ] ],
    [ "TIM2_OCMode_TypeDef", "group___t_i_m2___o_c_mode.html#ga2495beeef015d03d6768d7a569558884", [
      [ "TIM2_OCMode_Timing", "group___t_i_m2___o_c_mode.html#gga2495beeef015d03d6768d7a569558884a13424c115c00d1835cd2035a84909b65", null ],
      [ "TIM2_OCMode_Active", "group___t_i_m2___o_c_mode.html#gga2495beeef015d03d6768d7a569558884a17ff9fbd2670396fc53844d3f7c3037f", null ],
      [ "TIM2_OCMode_Inactive", "group___t_i_m2___o_c_mode.html#gga2495beeef015d03d6768d7a569558884ad36bc292a8cfc91e3371f670dddf547a", null ],
      [ "TIM2_OCMode_Toggle", "group___t_i_m2___o_c_mode.html#gga2495beeef015d03d6768d7a569558884a7bbd437c1c59cc264caa51a7c0f47a8e", null ],
      [ "TIM2_OCMode_PWM1", "group___t_i_m2___o_c_mode.html#gga2495beeef015d03d6768d7a569558884aea2325b841a3f04d9848b697a08e69e5", null ],
      [ "TIM2_OCMode_PWM2", "group___t_i_m2___o_c_mode.html#gga2495beeef015d03d6768d7a569558884a37ec8aaf5b85256e69fb09e5c7a9ba54", null ]
    ] ],
    [ "TIM2_OCPolarity_TypeDef", "group___t_i_m2___output___compare___polarity.html#ga2d5bb3e74912abde0f41143b6ae6df70", [
      [ "TIM2_OCPolarity_High", "group___t_i_m2___output___compare___polarity.html#gga2d5bb3e74912abde0f41143b6ae6df70a27c507a3c1f9aee4441b27dbd4ec2e01", null ],
      [ "TIM2_OCPolarity_Low", "group___t_i_m2___output___compare___polarity.html#gga2d5bb3e74912abde0f41143b6ae6df70aaffe00abe6aa815592ededd70a599524", null ]
    ] ],
    [ "TIM2_OPMode_TypeDef", "group___t_i_m2___one_pulse_mode.html#ga7bbaf7ba229cc78c5155e1c31cfffdad", [
      [ "TIM2_OPMode_Single", "group___t_i_m2___one_pulse_mode.html#gga7bbaf7ba229cc78c5155e1c31cfffdada50b884f341774737795e348a17cdbab2", null ],
      [ "TIM2_OPMode_Repetitive", "group___t_i_m2___one_pulse_mode.html#gga7bbaf7ba229cc78c5155e1c31cfffdada4395b21defb7e67c8879c840cc9682bd", null ]
    ] ],
    [ "TIM2_OSSIState_TypeDef", "group___t_i_m2___o_s_s_i___state.html#ga06bfb1d081c9667dbfbf8eb8478396d0", [
      [ "TIM2_OSSIState_Enable", "group___t_i_m2___o_s_s_i___state.html#gga06bfb1d081c9667dbfbf8eb8478396d0adbb33fb4bd10feb1198d3b76580fb147", null ],
      [ "TIM2_OSSIState_Disable", "group___t_i_m2___o_s_s_i___state.html#gga06bfb1d081c9667dbfbf8eb8478396d0a4d83f7203e878e52801066ece6ccf310", null ]
    ] ],
    [ "TIM2_OutputState_TypeDef", "group___t_i_m2___output___state.html#gad5c864227305a816ad18e9c89c5b7ceb", [
      [ "TIM2_OutputState_Disable", "group___t_i_m2___output___state.html#ggad5c864227305a816ad18e9c89c5b7ceba42b6b13b587c4c4031021c9709271b3f", null ],
      [ "TIM2_OutputState_Enable", "group___t_i_m2___output___state.html#ggad5c864227305a816ad18e9c89c5b7ceba56bffd3d2e936a0f0d7313ba90c4c787", null ]
    ] ],
    [ "TIM2_Prescaler_TypeDef", "group___t_i_m2___prescaler.html#ga6e423660d6269f78d8d36a5c7c432c21", [
      [ "TIM2_Prescaler_1", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21af43057e167fbb7be8e9e10ebf284e919", null ],
      [ "TIM2_Prescaler_2", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a025f9529c218e26ac71af0300e1d9e04", null ],
      [ "TIM2_Prescaler_4", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a6b1e4a77590709c8e279ca30104e90eb", null ],
      [ "TIM2_Prescaler_8", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21acc85dea82c69cfba403fe97f0474af4f", null ],
      [ "TIM2_Prescaler_16", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a07bf3c13654d704c9ffe504964f51134", null ],
      [ "TIM2_Prescaler_32", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a20ad02de589c85026c7e675d0e00bedb", null ],
      [ "TIM2_Prescaler_64", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a2b106b7736eaad6326fe5290799ab53b", null ],
      [ "TIM2_Prescaler_128", "group___t_i_m2___prescaler.html#gga6e423660d6269f78d8d36a5c7c432c21a08e00fd2e38ab568b7355aeb4ef6d1f3", null ]
    ] ],
    [ "TIM2_PSCReloadMode_TypeDef", "group___t_i_m2___prescaler___reload___mode.html#ga7e5e991912f9050beee4fcfbf42c47ae", [
      [ "TIM2_PSCReloadMode_Update", "group___t_i_m2___prescaler___reload___mode.html#gga7e5e991912f9050beee4fcfbf42c47aeaef4652b98ee511ed6e030fc43d36ee26", null ],
      [ "TIM2_PSCReloadMode_Immediate", "group___t_i_m2___prescaler___reload___mode.html#gga7e5e991912f9050beee4fcfbf42c47aea77837f94d0fbd342f5b105c63c460a1c", null ]
    ] ],
    [ "TIM2_SlaveMode_TypeDef", "group___t_i_m2___slave___mode.html#gacd425ff12d3298a7b1ef226469c0652a", [
      [ "TIM2_SlaveMode_Reset", "group___t_i_m2___slave___mode.html#ggacd425ff12d3298a7b1ef226469c0652aad1df7d40285241dc7729908588e25a75", null ],
      [ "TIM2_SlaveMode_Gated", "group___t_i_m2___slave___mode.html#ggacd425ff12d3298a7b1ef226469c0652aae4a28c2fbee759f92d921e99c5d88a64", null ],
      [ "TIM2_SlaveMode_Trigger", "group___t_i_m2___slave___mode.html#ggacd425ff12d3298a7b1ef226469c0652aa8025cef8eec65658131f0d81a661f080", null ],
      [ "TIM2_SlaveMode_External1", "group___t_i_m2___slave___mode.html#ggacd425ff12d3298a7b1ef226469c0652aa7f8f92fed84c56afd82970dd8a4b55e4", null ]
    ] ],
    [ "TIM2_TIxExternalCLK1Source_TypeDef", "group___t_i_m2___t_i___external___clock___source.html#ga9bd7ef2ae447e185a6e0d8d33d5035b3", [
      [ "TIM2_TIxExternalCLK1Source_TI1ED", "group___t_i_m2___t_i___external___clock___source.html#gga9bd7ef2ae447e185a6e0d8d33d5035b3a2085d138e26bc490f230d3557a45b7b4", null ],
      [ "TIM2_TIxExternalCLK1Source_TI1", "group___t_i_m2___t_i___external___clock___source.html#gga9bd7ef2ae447e185a6e0d8d33d5035b3a50f98e94588f369d307af5ea743d6ddd", null ],
      [ "TIM2_TIxExternalCLK1Source_TI2", "group___t_i_m2___t_i___external___clock___source.html#gga9bd7ef2ae447e185a6e0d8d33d5035b3acc705ce063467c405346a47fa26d4cd4", null ]
    ] ],
    [ "TIM2_TRGOSource_TypeDef", "group___t_i_m2___trigger___output___source.html#gaae700dc77b6f41ce666c4bd35f947479", [
      [ "TIM2_TRGOSource_Reset", "group___t_i_m2___trigger___output___source.html#ggaae700dc77b6f41ce666c4bd35f947479adbcd933852783ba0718a8720c041cb23", null ],
      [ "TIM2_TRGOSource_Enable", "group___t_i_m2___trigger___output___source.html#ggaae700dc77b6f41ce666c4bd35f947479a99f1a42f7f17d673066df88d12409dfe", null ],
      [ "TIM2_TRGOSource_Update", "group___t_i_m2___trigger___output___source.html#ggaae700dc77b6f41ce666c4bd35f947479ab66b4e9b320b4d07c082760d7b66d24b", null ],
      [ "TIM2_TRGOSource_OC1", "group___t_i_m2___trigger___output___source.html#ggaae700dc77b6f41ce666c4bd35f947479a162f113dd26302bbae16bd550e7b7e96", null ],
      [ "TIM2_TRGOSource_OC1REF", "group___t_i_m2___trigger___output___source.html#ggaae700dc77b6f41ce666c4bd35f947479adc0f563ea7c30c6488c4bc01f6af6f91", null ],
      [ "TIM2_TRGOSource_OC2REF", "group___t_i_m2___trigger___output___source.html#ggaae700dc77b6f41ce666c4bd35f947479a0612a73f54507d0ba4f5c99309453a29", null ]
    ] ],
    [ "TIM2_TRGSelection_TypeDef", "group___t_i_m2___internal___trigger___selection.html#ga49555b8d38713e562ff6d53d9bfde806", [
      [ "TIM2_TRGSelection_TIM4", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806ac542215c4ab5cadbcd4933fcad3277cc", null ],
      [ "TIM2_TRGSelection_TIM1", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806a93ac770f86ada9d76dd0b696a7e97b8e", null ],
      [ "TIM2_TRGSelection_TIM3", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806a61431540b1044ac075c3c2f774aa2e2e", null ],
      [ "TIM2_TRGSelection_TIM5", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806ab0240419d4b8a34a4b85d43a67533c76", null ],
      [ "TIM2_TRGSelection_TI1F_ED", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806ad07f5a562b2023acfa5a8de1c5982b2a", null ],
      [ "TIM2_TRGSelection_TI1FP1", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806afbbfebcf493b97d61fd00368cacd7cd6", null ],
      [ "TIM2_TRGSelection_TI2FP2", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806a6af0ac49428df70fa655e8913b253356", null ],
      [ "TIM2_TRGSelection_ETRF", "group___t_i_m2___internal___trigger___selection.html#gga49555b8d38713e562ff6d53d9bfde806ad3c66edabbbd6c12c51ac9798320647c", null ]
    ] ],
    [ "TIM2_UpdateSource_TypeDef", "group___t_i_m2___update___source.html#ga37da496f2db289138b917129fd3ad497", [
      [ "TIM2_UpdateSource_Global", "group___t_i_m2___update___source.html#gga37da496f2db289138b917129fd3ad497ad2d4fa72d37087ec0ca80ee2200af683", null ],
      [ "TIM2_UpdateSource_Regular", "group___t_i_m2___update___source.html#gga37da496f2db289138b917129fd3ad497aad475824f8eb326c0f71cb18b1577377", null ]
    ] ],
    [ "TIM2_ARRPreloadConfig", "group___t_i_m2.html#ga1f303d9722035d338d429020a3a9d98d", null ],
    [ "TIM2_BKRConfig", "group___t_i_m2.html#ga2d4233cd33b9f3ebe4a56ae156b6d3c2", null ],
    [ "TIM2_CCxCmd", "group___t_i_m2.html#gac555ae9048a282e3590221151939d87a", null ],
    [ "TIM2_ClearFlag", "group___t_i_m2.html#ga38429731a6ac4981e17a0c7f392b9d65", null ],
    [ "TIM2_ClearITPendingBit", "group___t_i_m2.html#gafcb0e1930cb4d62b3cf85f2d596c50c6", null ],
    [ "TIM2_Cmd", "group___t_i_m2.html#ga3c6b7abdb64798c68fa03d835d5e97d7", null ],
    [ "TIM2_CounterModeConfig", "group___t_i_m2.html#ga5c3e19c931374eab35018d7f11022eb8", null ],
    [ "TIM2_CtrlPWMOutputs", "group___t_i_m2.html#ga73e4835c83b259ff1f0785d1d51f2d3c", null ],
    [ "TIM2_DeInit", "group___t_i_m2.html#ga46dd9c25765d47dba181c8ed05319007", null ],
    [ "TIM2_DMACmd", "group___t_i_m2.html#gad3fe9607a06f134ff487dcdad65a380d", null ],
    [ "TIM2_EncoderInterfaceConfig", "group___t_i_m2.html#ga3a6f2637962af35640fb60c6c1fce4ae", null ],
    [ "TIM2_ETRClockMode1Config", "group___t_i_m2.html#ga32ea0053c23d73e10dfe6f53ceecfe05", null ],
    [ "TIM2_ETRClockMode2Config", "group___t_i_m2.html#gaa3c5839da3c2eb9160503ef0fe75fee1", null ],
    [ "TIM2_ETRConfig", "group___t_i_m2.html#ga4a59067fe81a81c12babd7d6deac887b", null ],
    [ "TIM2_ForcedOC1Config", "group___t_i_m2.html#ga55263272544958f717863f5d4c7162b6", null ],
    [ "TIM2_ForcedOC2Config", "group___t_i_m2.html#ga54034255c4a202751b45926cfbcaa3c0", null ],
    [ "TIM2_GenerateEvent", "group___t_i_m2.html#ga18f4c7333c1a43ffc7e552e4445f7362", null ],
    [ "TIM2_GetCapture1", "group___t_i_m2.html#gae74a5de35ed803a2fe7a802f3a3d1e51", null ],
    [ "TIM2_GetCapture2", "group___t_i_m2.html#ga8b5cdcb3a4a8920f05c6c3261e05a219", null ],
    [ "TIM2_GetCounter", "group___t_i_m2.html#ga5aa9620a1356cf205d49264180418300", null ],
    [ "TIM2_GetFlagStatus", "group___t_i_m2.html#ga1790a0a7c37c0e18db4d4cdee4d903fa", null ],
    [ "TIM2_GetITStatus", "group___t_i_m2.html#ga4eebf80f53affb22b34aa8c39e4d49d3", null ],
    [ "TIM2_GetPrescaler", "group___t_i_m2.html#ga65065d561f414bc6b81f9bbdb0c775aa", null ],
    [ "TIM2_ICInit", "group___t_i_m2.html#gab465f24d506292adb5717bf9212c63d9", null ],
    [ "TIM2_InternalClockConfig", "group___t_i_m2.html#gae7730c7fca611d3e51801d2db6fb9d08", null ],
    [ "TIM2_ITConfig", "group___t_i_m2.html#ga6c4556382f4c91aed62dfd0113097985", null ],
    [ "TIM2_OC1FastConfig", "group___t_i_m2.html#ga4ae06bdaaf0cca6495cf22b06ca15131", null ],
    [ "TIM2_OC1Init", "group___t_i_m2.html#ga6114db04787e78980c5155e79e238a60", null ],
    [ "TIM2_OC1PolarityConfig", "group___t_i_m2.html#gae7f3132748744452683e50355de5612c", null ],
    [ "TIM2_OC1PreloadConfig", "group___t_i_m2.html#ga564e3acbd4b94687578038469047def9", null ],
    [ "TIM2_OC2FastConfig", "group___t_i_m2.html#ga7bfd4d2d96bcc39f13b66ceb1d5a1ad5", null ],
    [ "TIM2_OC2Init", "group___t_i_m2.html#ga67e4c36fca7ea12ea4889d504e26ff5b", null ],
    [ "TIM2_OC2PolarityConfig", "group___t_i_m2.html#gaf0d70699b942af5e4a65189d7636931f", null ],
    [ "TIM2_OC2PreloadConfig", "group___t_i_m2.html#ga75500a204078522ed85a7fa05ed99a1e", null ],
    [ "TIM2_PrescalerConfig", "group___t_i_m2.html#gad71e6e9facb6802c7f9d6b56de1205b8", null ],
    [ "TIM2_PWMIConfig", "group___t_i_m2.html#gafd0b776705f6e3520fbbb4ed0a8fe537", null ],
    [ "TIM2_SelectCCDMA", "group___t_i_m2.html#ga6db9940c58b619a42f46a596f5211d21", null ],
    [ "TIM2_SelectHallSensor", "group___t_i_m2.html#ga72fddb1cfd3c4e8bf1ac4a00ffc5b8ac", null ],
    [ "TIM2_SelectInputTrigger", "group___t_i_m2.html#gaaf06b354ebafd31afa320330415d7a79", null ],
    [ "TIM2_SelectMasterSlaveMode", "group___t_i_m2.html#gad5a3ef763186ff88c6cab6d1215924f4", null ],
    [ "TIM2_SelectOCxM", "group___t_i_m2.html#ga2a7fa8f8302ac6f24166e05c3ee0937d", null ],
    [ "TIM2_SelectOnePulseMode", "group___t_i_m2.html#gae8a75637978adac6a17d6082f0c86748", null ],
    [ "TIM2_SelectOutputTrigger", "group___t_i_m2.html#ga6ba49676a589d8ef30845e99c6b8548c", null ],
    [ "TIM2_SelectSlaveMode", "group___t_i_m2.html#ga0af4dc138283377e9d80ab0995692715", null ],
    [ "TIM2_SetAutoreload", "group___t_i_m2.html#gacb202e2d99e46ba44fe1f443547c5d8c", null ],
    [ "TIM2_SetCompare1", "group___t_i_m2.html#ga5ecb4d0b7e47e33a4d652099ecea150b", null ],
    [ "TIM2_SetCompare2", "group___t_i_m2.html#ga5e48da7dfd218238e5e567d4917114bd", null ],
    [ "TIM2_SetCounter", "group___t_i_m2.html#ga986110ec7ae166ad5dd56d62983c2722", null ],
    [ "TIM2_SetIC1Prescaler", "group___t_i_m2.html#ga41e46848ed6b8234b716bc8f8f1743d7", null ],
    [ "TIM2_SetIC2Prescaler", "group___t_i_m2.html#gafc6a24c744bd2f978995c0516991d4aa", null ],
    [ "TIM2_TimeBaseInit", "group___t_i_m2.html#ga290ac15b1a23c91b754c3f5766b2436f", null ],
    [ "TIM2_TIxExternalClockConfig", "group___t_i_m2.html#ga6eb1e01f3fe27835c4abcc6da89b834e", null ],
    [ "TIM2_UpdateDisableConfig", "group___t_i_m2.html#ga81181cdaf2edbcd6a0320e70da341502", null ],
    [ "TIM2_UpdateRequestConfig", "group___t_i_m2.html#gaa38508fedd3e0929e9f0691def092017", null ]
];