var stm8l15x__beep_8c =
[
    [ "BEEP_Cmd", "group___b_e_e_p___group1.html#gaa83f09563e4d520e949b061b7f98e8c5", null ],
    [ "BEEP_DeInit", "group___b_e_e_p___group1.html#gaa51205c91b68013ff6c5bb1fa724bccc", null ],
    [ "BEEP_Init", "group___b_e_e_p___group1.html#gae29084588d06e1aa2872ed306daed265", null ],
    [ "BEEP_LSClockToTIMConnectCmd", "group___b_e_e_p___group2.html#ga6d9a5615d6ee8133b6b6e3939c38ffe6", null ],
    [ "BEEP_LSICalibrationConfig", "group___b_e_e_p___group2.html#gaa84c5df6579ebee737a8d4192a01dc58", null ]
];