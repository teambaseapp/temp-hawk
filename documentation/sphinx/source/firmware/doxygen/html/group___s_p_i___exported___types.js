var group___s_p_i___exported___types =
[
    [ "SPI_Direction_Mode", "group___s_p_i___direction___mode.html", "group___s_p_i___direction___mode" ],
    [ "SPI_SlaveSelect_Management", "group___s_p_i___slave_select___management.html", "group___s_p_i___slave_select___management" ],
    [ "SPI_Direction", "group___s_p_i___direction.html", "group___s_p_i___direction" ],
    [ "SPI_Mode", "group___s_p_i___mode.html", "group___s_p_i___mode" ],
    [ "SPI_BaudRate_Prescaler", "group___s_p_i___baud_rate___prescaler.html", "group___s_p_i___baud_rate___prescaler" ],
    [ "SPI_Clock_Polarity", "group___s_p_i___clock___polarity.html", "group___s_p_i___clock___polarity" ],
    [ "SPI_Clock_Phase", "group___s_p_i___clock___phase.html", "group___s_p_i___clock___phase" ],
    [ "SPI_Frame_Format", "group___s_p_i___frame___format.html", "group___s_p_i___frame___format" ],
    [ "SPI_DMA_requests", "group___s_p_i___d_m_a__requests.html", "group___s_p_i___d_m_a__requests" ],
    [ "SPI_CRC", "group___s_p_i___c_r_c.html", "group___s_p_i___c_r_c" ],
    [ "SPI_Flags", "group___s_p_i___flags.html", "group___s_p_i___flags" ],
    [ "SPI_Interrupts", "group___s_p_i___interrupts.html", "group___s_p_i___interrupts" ]
];