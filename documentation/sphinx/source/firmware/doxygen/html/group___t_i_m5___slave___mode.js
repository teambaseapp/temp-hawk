var group___t_i_m5___slave___mode =
[
    [ "TIM5_SlaveMode_TypeDef", "group___t_i_m5___slave___mode.html#ga14fbbfc108c8430840978c90cdfb0102", [
      [ "TIM5_SlaveMode_Reset", "group___t_i_m5___slave___mode.html#gga14fbbfc108c8430840978c90cdfb0102aed2466cc10c6f26ac8b40902a9563a01", null ],
      [ "TIM5_SlaveMode_Gated", "group___t_i_m5___slave___mode.html#gga14fbbfc108c8430840978c90cdfb0102a416a0cfb4bb6890302703704dd860b56", null ],
      [ "TIM5_SlaveMode_Trigger", "group___t_i_m5___slave___mode.html#gga14fbbfc108c8430840978c90cdfb0102ac60e1d73f1aaf115b4ae47c845f36c03", null ],
      [ "TIM5_SlaveMode_External1", "group___t_i_m5___slave___mode.html#gga14fbbfc108c8430840978c90cdfb0102a7c25040481f1fbdb74d346e4e9943482", null ]
    ] ]
];