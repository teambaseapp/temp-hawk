var struct_c_l_k__struct =
[
    [ "CBEEPR", "struct_c_l_k__struct.html#ab3ac74ad2d657697f565c1575e4a6e44", null ],
    [ "CCOR", "struct_c_l_k__struct.html#a2db9167ce7cd4b84a1377d33b7987652", null ],
    [ "CKDIVR", "struct_c_l_k__struct.html#ac18ef1830848ee5d1fc0a514392310fc", null ],
    [ "CRTCR", "struct_c_l_k__struct.html#a1587e942aa5bb2ed935c088c343d3873", null ],
    [ "CSSR", "struct_c_l_k__struct.html#afa865360242900c554b74a28d7cd5f31", null ],
    [ "ECKCR", "struct_c_l_k__struct.html#abe12152cd9c835edc05cd90aab67b498", null ],
    [ "HSICALR", "struct_c_l_k__struct.html#a922d903f20e390f380f0257cc8930898", null ],
    [ "HSITRIMR", "struct_c_l_k__struct.html#a3e1f2c192f6f44ecca14ae502c2fc511", null ],
    [ "HSIUNLCKR", "struct_c_l_k__struct.html#a4068bd6b82f7ad2ff4a60eb28729ac0f", null ],
    [ "ICKCR", "struct_c_l_k__struct.html#aee9b1c0f8248376286b49f2429e95a26", null ],
    [ "PCKENR1", "struct_c_l_k__struct.html#a194d0e9faffec5628cde24c5da0b6687", null ],
    [ "PCKENR2", "struct_c_l_k__struct.html#a7f569d3f403643116232589f9c9b5009", null ],
    [ "PCKENR3", "struct_c_l_k__struct.html#aad908540b611dbbd602b0627cb5080a1", null ],
    [ "REGCSR", "struct_c_l_k__struct.html#a1bfb99d94a910cf4bdd8a846cbec0864", null ],
    [ "SCSR", "struct_c_l_k__struct.html#aad173559d2ba8671ee94a6fa46e2ffa6", null ],
    [ "SWCR", "struct_c_l_k__struct.html#a5fe9355879fb6ec78196c3afa35cbc84", null ],
    [ "SWR", "struct_c_l_k__struct.html#ac2522877f67aef2f212fb3975730ec7c", null ]
];