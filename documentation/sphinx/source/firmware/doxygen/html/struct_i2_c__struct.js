var struct_i2_c__struct =
[
    [ "CCRH", "struct_i2_c__struct.html#a23636cbb6f964dd2f34dfad33e380a9c", null ],
    [ "CCRL", "struct_i2_c__struct.html#a49d174a317994e8d0585e2d519442b99", null ],
    [ "CR1", "struct_i2_c__struct.html#abdb5e2aed90a3a46151c8bb740665579", null ],
    [ "CR2", "struct_i2_c__struct.html#a27d59494343f46e0df9e3d0caf483896", null ],
    [ "DR", "struct_i2_c__struct.html#a35794dd520e00381839cf8307c6dc4b3", null ],
    [ "FREQR", "struct_i2_c__struct.html#a303d726a1e99ec009b94c7d502ba8f2d", null ],
    [ "ITR", "struct_i2_c__struct.html#aab5d553aa11b4ce109d54cbec5727996", null ],
    [ "OAR2", "struct_i2_c__struct.html#aa47e56ff77735148cf11c3cda9048783", null ],
    [ "OARH", "struct_i2_c__struct.html#a768a593a02f29a3e5e0472cc8cb72341", null ],
    [ "OARL", "struct_i2_c__struct.html#a6f07f06bd4bc1d4e9e7aec46dc34e643", null ],
    [ "PECR", "struct_i2_c__struct.html#a3b84787898bc78c1d7c1979166d29665", null ],
    [ "SR1", "struct_i2_c__struct.html#a590671f863bb6c58ca091b5d9180f8ab", null ],
    [ "SR2", "struct_i2_c__struct.html#af66f8aca67a3a27312316a39805a96ef", null ],
    [ "SR3", "struct_i2_c__struct.html#ab0d1cf46ce943f11c6df3eb49ba5c3bf", null ],
    [ "TRISER", "struct_i2_c__struct.html#a791d17d2b2a59bf373a60626d1ba6a1e", null ]
];