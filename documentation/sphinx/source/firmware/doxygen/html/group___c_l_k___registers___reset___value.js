var group___c_l_k___registers___reset___value =
[
    [ "CLK_CBEEPR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#gabd9ee4b7f215fb9e79932f82b0387233", null ],
    [ "CLK_CCOR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga65851d3d2aad9c8e8c6592ca318d5e12", null ],
    [ "CLK_CKDIVR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga601647b8c7c464aa40b86a4cb91edc09", null ],
    [ "CLK_CRTCR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga009d2420897ffc97649832f8f0c575ca", null ],
    [ "CLK_CSSR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga7616a9641bffb25d0ab79593143ffd96", null ],
    [ "CLK_ECKCR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#gabd8e928b0a36fa955d49758b5365abb0", null ],
    [ "CLK_HSICALR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga06378898c731646eb1206bff5b919ece", null ],
    [ "CLK_HSITRIMR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga4226bde535be2997d1a7435a2a45cfbf", null ],
    [ "CLK_HSIUNLCKR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#gaa6c15956928d4f83f2be10ac1756550c", null ],
    [ "CLK_ICKCR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#gaaabad8457f1508f3da8bba684b9f7d0e", null ],
    [ "CLK_PCKENR1_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga9c01c518a73b15fd6e8e1b3f853c1e65", null ],
    [ "CLK_PCKENR2_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga7115f3b2205986018dfcfcc88ac92857", null ],
    [ "CLK_PCKENR3_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga8095c36a9b59d8954d204ed0ad610488", null ],
    [ "CLK_REGCSR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#gaf686b050f36d5754c4db30d70bd5de67", null ],
    [ "CLK_SCSR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#gaa3c1b4a55651996d9ba1119a3f0b4798", null ],
    [ "CLK_SWCR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#gaf0af1e2b8c727386250ae6c4391e654a", null ],
    [ "CLK_SWR_RESET_VALUE", "group___c_l_k___registers___reset___value.html#ga0584e4231a75d11f3928873bf615fb94", null ]
];