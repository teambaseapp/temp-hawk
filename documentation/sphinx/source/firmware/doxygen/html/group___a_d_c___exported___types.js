var group___a_d_c___exported___types =
[
    [ "ADC_Channels", "group___a_d_c___channels.html", "group___a_d_c___channels" ],
    [ "ADC_Conversion_Mode", "group___a_d_c___conversion___mode.html", "group___a_d_c___conversion___mode" ],
    [ "ADC_Resolution", "group___a_d_c___resolution.html", "group___a_d_c___resolution" ],
    [ "ADC_Clock_Prescaler", "group___a_d_c___clock___prescaler.html", "group___a_d_c___clock___prescaler" ],
    [ "ADC_External_Trigger_Sensitivity", "group___a_d_c___external___trigger___sensitivity.html", "group___a_d_c___external___trigger___sensitivity" ],
    [ "ADC_External_Event_Source_Selection", "group___a_d_c___external___event___source___selection.html", "group___a_d_c___external___event___source___selection" ],
    [ "ADC_Group_Channel_Definition", "group___a_d_c___group___channel___definition.html", "group___a_d_c___group___channel___definition" ],
    [ "ADC_Sampling_Time", "group___a_d_c___sampling___time.html", "group___a_d_c___sampling___time" ],
    [ "ADC_Analog_Watchdog_Channel_selection", "group___a_d_c___analog___watchdog___channel__selection.html", "group___a_d_c___analog___watchdog___channel__selection" ],
    [ "ADC_Interrupts", "group___a_d_c___interrupts.html", "group___a_d_c___interrupts" ],
    [ "ADC_Flags", "group___a_d_c___flags.html", "group___a_d_c___flags" ]
];