var struct_d_m_a___channel__struct =
[
    [ "CCR", "struct_d_m_a___channel__struct.html#af959ddb88caef28108c2926e310a72bd", null ],
    [ "CM0ARH", "struct_d_m_a___channel__struct.html#a54f0c9024afce896847a62ba9b4030f4", null ],
    [ "CM0ARL", "struct_d_m_a___channel__struct.html#a62066f413523239f193121f4bae34838", null ],
    [ "CM0EAR", "struct_d_m_a___channel__struct.html#a2892e697ba8abb0b75bc1e59caa96599", null ],
    [ "CNBTR", "struct_d_m_a___channel__struct.html#aeed24b7df0a62c1eae120a74c651a98c", null ],
    [ "CPARH", "struct_d_m_a___channel__struct.html#aeaf3a9a7a5a636140fc881e8a86a54e2", null ],
    [ "CPARL", "struct_d_m_a___channel__struct.html#ae8f2edbb3ab2c787f3fe1adb08c2a3f8", null ],
    [ "CSPR", "struct_d_m_a___channel__struct.html#a7a34f62aa218710f8a7de1dd5c6b55a6", null ]
];