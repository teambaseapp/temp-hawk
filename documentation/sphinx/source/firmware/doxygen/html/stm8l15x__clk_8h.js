var stm8l15x__clk_8h =
[
    [ "CLK_BEEPCLKSource_TypeDef", "group___c_l_k___beep___selection.html#ga50bc4f897a723341e12b957f04802683", [
      [ "CLK_BEEPCLKSource_Off", "group___c_l_k___beep___selection.html#gga50bc4f897a723341e12b957f04802683abdacb0e2da8a2e24ae727ed03a0a2a99", null ],
      [ "CLK_BEEPCLKSource_LSI", "group___c_l_k___beep___selection.html#gga50bc4f897a723341e12b957f04802683ab9d091cfe0e57cf52616e58f3c4830d6", null ],
      [ "CLK_BEEPCLKSource_LSE", "group___c_l_k___beep___selection.html#gga50bc4f897a723341e12b957f04802683a4d7748ae6297ffb338113efcb55edda9", null ]
    ] ],
    [ "CLK_CCODiv_TypeDef", "group___c_l_k___clock___output___prescaler.html#ga5e5d368cbb91b209097dae3e2c7e80f5", [
      [ "CLK_CCODiv_1", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a38c825dc0abc601153d4586d4676efd5", null ],
      [ "CLK_CCODiv_2", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5ab0c1debf3576078b5fef825d63c38a71", null ],
      [ "CLK_CCODiv_4", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a5f67fd0fd157a4cbd64cf7a843c3ae2c", null ],
      [ "CLK_CCODiv_8", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a74d6b67b368c43fb35004d75e833a3d5", null ],
      [ "CLK_CCODiv_16", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a96ec091b27ed94caf803ad1bd789ac0d", null ],
      [ "CLK_CCODiv_32", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5a429ca54fd0fbc2d31004f33d50369e78", null ],
      [ "CLK_CCODiv_64", "group___c_l_k___clock___output___prescaler.html#gga5e5d368cbb91b209097dae3e2c7e80f5adc9fc75ad8914cbf1ad6cf8df60e44fa", null ]
    ] ],
    [ "CLK_CCOSource_TypeDef", "group___c_l_k___clock___output___selection.html#ga9ea6b2d16e2fc2f768172eb5b957aae1", [
      [ "CLK_CCOSource_Off", "group___c_l_k___clock___output___selection.html#gga9ea6b2d16e2fc2f768172eb5b957aae1a4d8a32ab354eed61715ff28a50c35a78", null ],
      [ "CLK_CCOSource_HSI", "group___c_l_k___clock___output___selection.html#gga9ea6b2d16e2fc2f768172eb5b957aae1aef215b2efef3dbc3db8131101968de99", null ],
      [ "CLK_CCOSource_LSI", "group___c_l_k___clock___output___selection.html#gga9ea6b2d16e2fc2f768172eb5b957aae1ac877b52654f8e943c7004f8c3bb0f94b", null ],
      [ "CLK_CCOSource_HSE", "group___c_l_k___clock___output___selection.html#gga9ea6b2d16e2fc2f768172eb5b957aae1a59470be88b6b3a0bcd049dc202bceec7", null ],
      [ "CLK_CCOSource_LSE", "group___c_l_k___clock___output___selection.html#gga9ea6b2d16e2fc2f768172eb5b957aae1a8c37ce2db4afb8c6f4ce134fcb7578ea", null ]
    ] ],
    [ "CLK_FLAG_TypeDef", "group___c_l_k___flags.html#gaf6f522610a5bce51814c92eee06aa399", [
      [ "CLK_FLAG_RTCSWBSY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a053ef355b41e72b010098e602a341954", null ],
      [ "CLK_FLAG_HSIRDY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a15df4ebe76cc5422ab939073aaa20c43", null ],
      [ "CLK_FLAG_LSIRDY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a6061c04bbd891785c8ee94e8c01b1a75", null ],
      [ "CLK_FLAG_CCOBSY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a01d982c7e7095e880b442424768dce38", null ],
      [ "CLK_FLAG_HSERDY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399acfd36997ee2e4998252b8dc2b95c1f42", null ],
      [ "CLK_FLAG_LSERDY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399ae50378577c9f6fd0be4c191864625c9c", null ],
      [ "CLK_FLAG_SWBSY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a336b821544b0a64cf141446e82b3e4b7", null ],
      [ "CLK_FLAG_AUX", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a37f7fa6cec7c13998b117bccd1960aa1", null ],
      [ "CLK_FLAG_CSSD", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399ae8245a50e4b761c13d724f3b2598cd86", null ],
      [ "CLK_FLAG_BEEPSWBSY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399ababdd1c00b032e4e24634a4395803db8", null ],
      [ "CLK_FLAG_EEREADY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a6b50bb95292f19b847894a5a0b6cae7e", null ],
      [ "CLK_FLAG_EEBUSY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a97261f8b6a31e69681b66c4c1ac91fb9", null ],
      [ "CLK_FLAG_LSEPD", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399aa2fcd29e9b742ed13f990203d2d3e478", null ],
      [ "CLK_FLAG_HSEPD", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a70a80e031ec8501eb32abbb1e301e35e", null ],
      [ "CLK_FLAG_LSIPD", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399ac39ef3a5bf656dfee1110758dfd162f0", null ],
      [ "CLK_FLAG_HSIPD", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a2e15718832392411e72083d6b0dfbd13", null ],
      [ "CLK_FLAG_REGREADY", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399ad42702a3a38bd7762bb7f6712e776d26", null ],
      [ "CLK_FLAG_LSECSSF", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399a3a472b479f07ec543389a504f116a0db", null ],
      [ "CLK_FLAG_RTCCLKSWF", "group___c_l_k___flags.html#ggaf6f522610a5bce51814c92eee06aa399ae70e4023da259760f8b694c27136a117", null ]
    ] ],
    [ "CLK_Halt_TypeDef", "group___c_l_k___halt___configuration.html#ga4bf6cd3a577dd72a06a370751f987a6d", [
      [ "CLK_Halt_BEEPRunning", "group___c_l_k___halt___configuration.html#gga4bf6cd3a577dd72a06a370751f987a6da73a4beda096fc859bdd657c65a7f7957", null ],
      [ "CLK_Halt_FastWakeup", "group___c_l_k___halt___configuration.html#gga4bf6cd3a577dd72a06a370751f987a6da4633b3a5db271ac6bda6a577b4eaa0c9", null ],
      [ "CLK_Halt_SlowWakeup", "group___c_l_k___halt___configuration.html#gga4bf6cd3a577dd72a06a370751f987a6da41ce4f492744dfb7bcd53b1a9866bd80", null ]
    ] ],
    [ "CLK_HSE_TypeDef", "group___c_l_k___h_s_e___configuration.html#ga951515846e0f9aed5ba42be70bb859be", [
      [ "CLK_HSE_OFF", "group___c_l_k___h_s_e___configuration.html#gga951515846e0f9aed5ba42be70bb859bea6883a0645dd4330572afaaa1fb81f8cb", null ],
      [ "CLK_HSE_ON", "group___c_l_k___h_s_e___configuration.html#gga951515846e0f9aed5ba42be70bb859bea41d32df15b92ac1b3676e910d37aef1b", null ],
      [ "CLK_HSE_Bypass", "group___c_l_k___h_s_e___configuration.html#gga951515846e0f9aed5ba42be70bb859beac2c0aee89595bc282bcac3a1a4eadd4f", null ]
    ] ],
    [ "CLK_IT_TypeDef", "group___c_l_k___interrupts.html#ga1989a3fb4060ebaffaaebed2f951a66f", [
      [ "CLK_IT_CSSD", "group___c_l_k___interrupts.html#gga1989a3fb4060ebaffaaebed2f951a66fa3899cd1b76a746ad5e731532da9235bf", null ],
      [ "CLK_IT_SWIF", "group___c_l_k___interrupts.html#gga1989a3fb4060ebaffaaebed2f951a66fa645008a06ee153cf30276bd5d1981950", null ],
      [ "CLK_IT_LSECSSF", "group___c_l_k___interrupts.html#gga1989a3fb4060ebaffaaebed2f951a66fa1d7d89c47ca759154e8ef0df1abebc7a", null ]
    ] ],
    [ "CLK_LSE_TypeDef", "group___c_l_k___l_s_e___configuration.html#ga012b54467ddf79101652845f11de93ce", [
      [ "CLK_LSE_OFF", "group___c_l_k___l_s_e___configuration.html#gga012b54467ddf79101652845f11de93cea3abe3f70479d8045c8705fd2c3661d6e", null ],
      [ "CLK_LSE_ON", "group___c_l_k___l_s_e___configuration.html#gga012b54467ddf79101652845f11de93cea7125df795958d3acb18e7c457f587044", null ],
      [ "CLK_LSE_Bypass", "group___c_l_k___l_s_e___configuration.html#gga012b54467ddf79101652845f11de93ceacee7e21e6a80556a1cfaaa129608c85b", null ]
    ] ],
    [ "CLK_Peripheral_TypeDef", "group___c_l_k___peripherals.html#ga7e317dbac5504fca247dcf82e86c0451", [
      [ "CLK_Peripheral_TIM2", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451af25802a33b551199d920a72cb3185e77", null ],
      [ "CLK_Peripheral_TIM3", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451aa4a746a54f4e39111a090848d13e53b4", null ],
      [ "CLK_Peripheral_TIM4", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a108e2d328f14b851fd53362353223bf5", null ],
      [ "CLK_Peripheral_I2C1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a9516cb719378955fe4701104e413f760", null ],
      [ "CLK_Peripheral_SPI1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a4f390369e27daa77fe786e18d5527f82", null ],
      [ "CLK_Peripheral_USART1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a1d81f794cf66c957956ee3634be4d834", null ],
      [ "CLK_Peripheral_BEEP", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a1e1d181b93e7fb1793f01921702d0db2", null ],
      [ "CLK_Peripheral_DAC", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a9e727edda9178694f4db4b3011671757", null ],
      [ "CLK_Peripheral_ADC1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a17d67982762c71204c21c266215bccba", null ],
      [ "CLK_Peripheral_TIM1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a15569ab862e9e58758109868d5e227c9", null ],
      [ "CLK_Peripheral_RTC", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a9c0557ad4460b46bc5e957e64be0fbf3", null ],
      [ "CLK_Peripheral_LCD", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a6708b7e8b59e9877a6e18faccd6425c2", null ],
      [ "CLK_Peripheral_DMA1", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451ab35bb173ece4d4f9d7e95c8a6744ebed", null ],
      [ "CLK_Peripheral_COMP", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451ae4eca68e5b1a4c0c707a31ac72c85dc3", null ],
      [ "CLK_Peripheral_BOOTROM", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451aacfb4d1960496e69d3011766ececfbe4", null ],
      [ "CLK_Peripheral_AES", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451acd5c01a25db5117a154891c8871f16cb", null ],
      [ "CLK_Peripheral_TIM5", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a9cbed31c9a9ff4f0647668c24b7b32a2", null ],
      [ "CLK_Peripheral_SPI2", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a10c8602227404ae69faf7047e1086264", null ],
      [ "CLK_Peripheral_USART2", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451ae8c8f83cb854c151403a6c6d1ee4d968", null ],
      [ "CLK_Peripheral_USART3", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a7897e32bac76e4c7ad23ff8b3d18debf", null ],
      [ "CLK_Peripheral_CSSLSE", "group___c_l_k___peripherals.html#gga7e317dbac5504fca247dcf82e86c0451a7fbb37131a41f138798f46bb8f5330db", null ]
    ] ],
    [ "CLK_RTCCLKDiv_TypeDef", "group___c_l_k___r_t_c___prescaler.html#ga1acdb2452689e6994900557f0b5c16c5", [
      [ "CLK_RTCCLKDiv_1", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a45f6c44b3a1931cf94656146f2dae08c", null ],
      [ "CLK_RTCCLKDiv_2", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a8ac62419c9df796d8ab07319d22b0297", null ],
      [ "CLK_RTCCLKDiv_4", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a98e876a824667e5a4fa63d02ef20e446", null ],
      [ "CLK_RTCCLKDiv_8", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5acb3b7a75837a19bb2333343cb17ca6ef", null ],
      [ "CLK_RTCCLKDiv_16", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a43493b075f516cd6efab4994e70acdb4", null ],
      [ "CLK_RTCCLKDiv_32", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a6c2ad0797f094669a1565a70e2bc3618", null ],
      [ "CLK_RTCCLKDiv_64", "group___c_l_k___r_t_c___prescaler.html#gga1acdb2452689e6994900557f0b5c16c5a4c6eeca3f44768a6e34d1ed4568e56da", null ]
    ] ],
    [ "CLK_RTCCLKSource_TypeDef", "group___c_l_k___r_t_c___selection.html#ga5c75b763f690d8faaad8c0b8df6b0b23", [
      [ "CLK_RTCCLKSource_Off", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23a036e973d78e658b78ca44a8b3d8a8881", null ],
      [ "CLK_RTCCLKSource_HSI", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23ad3a345f9f21fa25b5c93de1cfbd879e0", null ],
      [ "CLK_RTCCLKSource_LSI", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23a9754923a68ddbd16c949589535d8ab05", null ],
      [ "CLK_RTCCLKSource_HSE", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23a0331f69c466def532e1f7638da1705bf", null ],
      [ "CLK_RTCCLKSource_LSE", "group___c_l_k___r_t_c___selection.html#gga5c75b763f690d8faaad8c0b8df6b0b23ae783ecea4fc242af5e97307f292bc300", null ]
    ] ],
    [ "CLK_SYSCLKDiv_TypeDef", "group___c_l_k___system___clock___divider.html#gabbba644738a39aa9345e6e7412de1323", [
      [ "CLK_SYSCLKDiv_1", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323aa9a13655163781dc0325bebba137cac3", null ],
      [ "CLK_SYSCLKDiv_2", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323a61601741fece994c7449a40cd58827aa", null ],
      [ "CLK_SYSCLKDiv_4", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323a2d7ce7257bb97f707c98653b4bc7796c", null ],
      [ "CLK_SYSCLKDiv_8", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323a6fda4c47578a4b603a2767ac5a1c7efa", null ],
      [ "CLK_SYSCLKDiv_16", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323ad3a5ff09f1de0aada554d1b824c861e3", null ],
      [ "CLK_SYSCLKDiv_32", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323ace1c7ff6d9177afc26394f7204ec64db", null ],
      [ "CLK_SYSCLKDiv_64", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323aa84d6f9fa295e0ad400a46778550fe2b", null ],
      [ "CLK_SYSCLKDiv_128", "group___c_l_k___system___clock___divider.html#ggabbba644738a39aa9345e6e7412de1323ab3a8a9d2543956ff7629ce726e61c6d2", null ]
    ] ],
    [ "CLK_SYSCLKSource_TypeDef", "group___c_l_k___system___clock___sources.html#ga9b217952eaa4b22b689a9394c29f4e8f", [
      [ "CLK_SYSCLKSource_HSI", "group___c_l_k___system___clock___sources.html#gga9b217952eaa4b22b689a9394c29f4e8fa4b14704598db25a118f752d5728420dc", null ],
      [ "CLK_SYSCLKSource_LSI", "group___c_l_k___system___clock___sources.html#gga9b217952eaa4b22b689a9394c29f4e8fa9fab5c95afb5d5fb9fd77ae434ac28e3", null ],
      [ "CLK_SYSCLKSource_HSE", "group___c_l_k___system___clock___sources.html#gga9b217952eaa4b22b689a9394c29f4e8faf097876736959f332e82d09978fb55d2", null ],
      [ "CLK_SYSCLKSource_LSE", "group___c_l_k___system___clock___sources.html#gga9b217952eaa4b22b689a9394c29f4e8fab949d3cb9c5f98becdb244d639114f03", null ]
    ] ],
    [ "CLK_AdjustHSICalibrationValue", "group___c_l_k.html#ga101d260433a971af8dfe3592238db9de", null ],
    [ "CLK_BEEPClockConfig", "group___c_l_k.html#ga75458d6fafff10523399387a235ee852", null ],
    [ "CLK_CCOConfig", "group___c_l_k.html#ga256e0766ff6cdeb3033e818dc333361e", null ],
    [ "CLK_ClearFlag", "group___c_l_k.html#ga217b0b488f420a4d0e573fc484b276e2", null ],
    [ "CLK_ClearITPendingBit", "group___c_l_k.html#ga278f71c8d3f77f67e3e2aa35e6587474", null ],
    [ "CLK_ClockSecuritySystemEnable", "group___c_l_k.html#gad1bb45d35ea2a30c731f7f4882837825", null ],
    [ "CLK_ClockSecuritySytemDeglitchCmd", "group___c_l_k.html#gaf06e09d2318e0b00b1d47a5c93a35b06", null ],
    [ "CLK_DeInit", "group___c_l_k.html#ga7aa3910abf18b164ca4a05c7312da863", null ],
    [ "CLK_GetClockFreq", "group___c_l_k.html#ga2d27cbf7ffb73aa74f9cb03e3afdbf8f", null ],
    [ "CLK_GetFlagStatus", "group___c_l_k.html#ga4101f1153830484cac5c6042a01c1235", null ],
    [ "CLK_GetITStatus", "group___c_l_k.html#gabc7d33f466770b3e3dc5254b0ece47c6", null ],
    [ "CLK_GetSYSCLKSource", "group___c_l_k.html#gaec9e2c58fa0e3c0d4f446ce818f5c832", null ],
    [ "CLK_HaltConfig", "group___c_l_k.html#ga078ffefb979b4fb229090fafee88e1ea", null ],
    [ "CLK_HSEConfig", "group___c_l_k.html#ga5275444102d1adf5032800ea598e4958", null ],
    [ "CLK_HSICmd", "group___c_l_k.html#ga1d9e1ea1623f87ad8355dadc77b90b7b", null ],
    [ "CLK_ITConfig", "group___c_l_k.html#ga48308478205c6b628ddbecb54aa09257", null ],
    [ "CLK_LSEClockSecuritySystemEnable", "group___c_l_k.html#ga4f1ac58b0d13944a0348bd90139e23d0", null ],
    [ "CLK_LSEConfig", "group___c_l_k.html#ga0786abc1aa08bdd2859a900086b9e985", null ],
    [ "CLK_LSICmd", "group___c_l_k.html#ga8e0c429dc148bc34557b855a524262c6", null ],
    [ "CLK_MainRegulatorCmd", "group___c_l_k.html#gacd2e08157ab38f587a294229281d43dd", null ],
    [ "CLK_PeripheralClockConfig", "group___c_l_k.html#gabbb45bdfac7093a30d73b714322322b2", null ],
    [ "CLK_RTCCLKSwitchOnLSEFailureEnable", "group___c_l_k.html#ga3faa3a80338b0a15995052e0077074f1", null ],
    [ "CLK_RTCClockConfig", "group___c_l_k.html#ga8ede9a18e01856dcd9e983e1bb156d1a", null ],
    [ "CLK_SYSCLKDivConfig", "group___c_l_k.html#ga0b4a3579f860960e31de0a42cc909ac8", null ],
    [ "CLK_SYSCLKSourceConfig", "group___c_l_k.html#ga3e981572b0df73b902eab6f67c9d1120", null ],
    [ "CLK_SYSCLKSourceSwitchCmd", "group___c_l_k.html#gaa0506d258dce10dd4b7cd4a823db8caa", null ]
];