var group___a_e_s___private___functions =
[
    [ "Configuration", "group___a_e_s___group1.html", "group___a_e_s___group1" ],
    [ "AES Read and Write", "group___a_e_s___group2.html", "group___a_e_s___group2" ],
    [ "DMA transfers management functions", "group___a_e_s___group3.html", "group___a_e_s___group3" ],
    [ "Interrupts and flags management functions", "group___a_e_s___group4.html", "group___a_e_s___group4" ]
];