------------------------
How To Program: Method 2
------------------------

For a person who doesn't have access to the source code of the project, this can be done with the help of hex files. These are Intel extended HEX files generated by IAR Embedded Workbench. 

`ST Visual Programmer Download Link`_   

.. _ST Visual Programmer Download Link: http://www.st.com/en/development-tools/stvp-stm8.html

ST Visual Programmer  
^^^^^^^^^^^^^^^^^^^^

   1. Open ST Visual Programmer. 

    .. image:: ./pics/stVisualProgrammer/stvp1.jpg
       :align: center

   2. Go to *Configure > Configure ST Visual Programmer*     

    .. image:: ./pics/stVisualProgrammer/stvp2.jpg
       :align: center

   3. Select 
      
      * *ST-LINK* from *Hardware*, 
      
      * *USB* from *Port*, 

      * *SWIM* from *Programming mode* and

      * *STM8L051x3* from *Device* 

        .. image:: ./pics/stVisualProgrammer/stvp3.jpg
           :align: center
      
   4. Open the required hex file for the appropriate device.  

    .. image:: ./pics/stVisualProgrammer/stvp4.jpg
       :align: center
   
   5. Program the current tab by:
   
      * Click on to the *Program current tab or active sectors* icon in the toolbar, or
       
      * Go to *Program > Current tab*, or
      
      * Shortcut *Ctrl + P*     

        .. image:: ./pics/stVisualProgrammer/stvp5.jpg
           :align: center

   6. Device ID: (only applicable for temperature-humidity sensor and Irms sensor) Inorder to program the device ID:
   
      * Go to *Data Momeory* tab. Right click on the memory area inside the window and click onto *Fill Area...* option. 

        .. image:: ./pics/stVisualProgrammer/stvp6.jpg
           :align: center
   
      * Make sure you change the option to *Subrange* and fill the values of *Start @* as **1002** and *End @* as **1003**.
       
      * Fill the device ID instead of "XX XX" in the *Fill with* option. 
       
      * Program the current tab.  

        .. image:: ./pics/stVisualProgrammer/stvp7.jpg
           :align: center

   7. Option Byte: (only applicable for temperature-humidity sensor and Irms sensor) Inorder to program the option byte:
   
      * Go to *Option Byte* tab. Click on the options for *IWDG_HALT*. 
      
      * Change the option from *Independant watchdogtimer continues running in Halt/Active-halt mode* to *Independant watchdogtimer stopped in Halt/Active-halt mode*. 
      
      * Program the current tab.

        .. image:: ./pics/stVisualProgrammer/stvp8.jpg
           :align: center 

   8. Close the window. 

Now the board is good to go. Enjoy!