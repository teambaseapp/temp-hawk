-----------------
Power Consumption
-----------------

For a module working on a battery, power consumtion is one of the most crucial aspects to be considered. Thus the Sensor module has been tuned to use the least amount of power leaving behind enough juice in the battery to run for much more than an year.

How is it possible?
^^^^^^^^^^^^^^^^^^^

Every part of the Sensor module was designed taking into consideration the aspect of less power consumption. Thus, our audition for micro controller listed STM8L as number one. The next important power consumption unit in the module was the transceiver. RFM69HW was again the logical selection. A seperate sensor, HTU20D, is employed to fetch temperatue and humidity which also takes minimal power not only to acquire data but also while in idle (sleep).

Let's break it down. The power consumption break down (as in data sheet):

	* STM8L
		* Run mode with all the necessary peripherals for the sensor takes 
		* Active-Halt mode with RTC on LSI takes 0.8 µA at 25°C
	* RFM69HW
		* Transmit mode: 45mA
		* Receiver mode: 16 mA
		* Sleep mode: typically 0.1 µA
	* HTU20D
		* Measuring mode: 450 µA
		* Sleep mode: 0.02 µA

STM8L
-----

The micro controller uses active-halt mode to reduce the use of battery power. In active-halt mode, all the peripherals are gated off and the high speed internal (HSI) clock switched off whereby decreasing a lot of power consumption. The LSI is kept on to facilitate the funcationality of wake up timer interrupt. Even with the LSI on and two of the GPIO pins to act as interrupt (switch interrupt and door sensor interrupt), the micro controller takes only very less power.

Before going to active-halt mode, the micro controller puts the on board devices to sleep. RFM69HW is switched to sleep mode and power to the device is cut-off. HTU20D will be in idle mode after a measurement where in it consumes very little power (typically 0.02 µA current) and thus it is in a desired state for power down mode.

RFM69HW
-------

The RFM69HW transceiver is switched to sleep mode before gating off power. Thus it is guaranteed to use least power (typically 0.1 µA).

HTU20D
------

The HTU20D, temperatue-humidity sensor will be in idle (sleep) mode with very little power consumption (typically 0.02 µA) and thus it is already in a desired state for power down mode.

Calculations
^^^^^^^^^^^^

Let's do some maths!

.. figure:: ./pics/IdleStateCurrentReadings.jpg
   :align: center

This is the ideal current measurement of the Sensor board while in idle state.

current consumption during active-halt mode,
 | I = 0.001 mA.

.. image:: ./pics/OutputVoltageOfResistorBoard.jpg
   :align: center

This is the peak to peak voltage measurement across resistor board while the Sensor board is transmitting.

peak to peak voltage, 
 | Vpp = 130 mV
 
resistance across which the voltage is measured, 
 | R = 2.2 Ohm
 
current through the resistor board, 
 | I = 130/2.2 = 59.09 mA
 
as it is in series, current through the Sensor board = current through the resistor board, 
 | I = 59.09 mA

duration of wake up,
 | t = 35 ms

The cell that we use for Sensor module is CR123A with a capacity of 1500 mAh. We would be taking into consideration an useful power of 1000 mAh for our calculations.

We will take the help of an external website to calculate the life time of our battery.

 | website: http://oregonembedded.com/batterycalc.htm

Parameters for the battery life calculation are (the recorded values):

+-----------------------------------------------------+-------+-------+
| Field                                               | Value | Unit  |
+=====================================================+=======+=======+
| Capacity rating of the battery                      | 1000  | mAh   |
+-----------------------------------------------------+-------+-------+
| Current consumption of device during sleep          | 0.001 | mA    |
+-----------------------------------------------------+-------+-------+
| Current consumption of device during wake           | 59.09 | mA    |
+-----------------------------------------------------+-------+-------+
| Number of wakeups per hour                          | 60    |       |
+-----------------------------------------------------+-------+-------+
| Duration of wake time                               | 35    | mA    |
+-----------------------------------------------------+-------+-------+

.. image:: ./pics/oregonembedded.jpg
   :align: center

This data would calculate to 998.54 days, which is 2.74 years. [This calculation does not consider the power taken for the debug mode where in the module takes higher power for a longer period of time to send a signal and wait for its acknowledgement. This is not taken into consideration as the debug process will be carried out only at the time of installation.] Let me be modest, I want the Sensor module to last around for one and a half year ;-).