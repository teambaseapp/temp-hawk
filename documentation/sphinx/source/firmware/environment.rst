-------------------
Working Environment
-------------------

Operating System: 
 | ``Windows 7``

Integrated Development Environment: 
 | ``IAR Embedded Workbench IDE``

Debugging Tools:
 | ``Saleae Logic Analyser``
 | ``Software-Defined Radio (SDR)``

 
Operating System
----------------

Operating System used for the development of firmware is ``Windows 7 Home Basic Service Pack 1.``
Windows 7 was the logical selection as it supports a variety of software packages and IDEs. 
On top of that, Saleae Logic Analyser proved to be hard to work with in Ubuntu 14.04.1 LTS	(Trusty Tahr) as it crashes both the software and the OS.


Integrated Development Environment (IDE)
-----------------------------------------

IAR Embedded Workbench is a development environment that includes a C/C++ compiler and debugger that supports 30 different processor families. The development tools support the following targets: 78K, 8051, ARM, AVR, AVR32, CR16C, Coldfire, H8, HCS12, M16C, M32C, MSP430, Maxim MAXQ, R32C, R8C, RH850, RL78, RX, S08, SAM8, STM8, SuperH, V850.[2] The supported ARM core families are: ARM7 / ARM9 / ARM10 / ARM11, Cortex M0 / M0+ / M1 / M3 / M4 / M7, Cortex R4 / R5 / R7, Cortex A5 / A7 / A8 / A9 / A15.

The ISO/IEC 9899:1999 (known as C99) ISO/ANSI C/C++ Compliance has been used to program the STM8L micocontroller.


Saleae Logic Analyser
---------------------

From the company's product lineup, we used the Logic 8 Logic Analyser which has 8 (all analog/digital) channels. 
Software Version: Saleae Logic 1.2.10


Software-Defined Radio (SDR)
----------------------------

Software Version: SDR# v1.0.0.1488