------------------------
How To Program: Method 1
------------------------

The board can be programmed in different ways. For a developer, who has access to the source code, method 1 would be convenient as he will be able to edit the code according to his convenience.   

`ST Visual Programmer Download Link`_   

.. _ST Visual Programmer Download Link: http://www.st.com/en/development-tools/stvp-stm8.html

STEP 1: IAR Embedded Workbench
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Open the IAR Embedded Workbench which corresponds to the hardware version. Please refer to Setting Up section for more information on how to set up the IDE.

   1. Version 1.0

      a. Open IAR Embedded Workbench
		
      b. Navigate to *config.h* file.

      c. The code can be compiled in three different ways:

         * Sensor (Transmitter)

         * Gateway (Receiver)

         Choose the required oprions from the config.h file.

      d. Some suble changes can be made to the code by introducing debug functionality for the transmitter board through LEDs. This can be changed by defining the appropriate macros.

      e. Rebuild the project.

      f. Download and debug the project.

	
   2. Version 2.0

      a. Version 2.0 has the same instructions to be followed, except that the macro *TEMPHAWK_VERSION_2_0* needs to be defined in *config.h*.
      Add ``#define TEMPHAWK_VERSION_2_0`` in ``config.h`` file.
	
   3. Version 3.0

      a. Open IAR Embedded Workbench
		
      b. Navigate to *config.h* file.

      c. The code can be compiled in three different ways:

         * Sensor board (Transmitter) as normal temperature and humidity sensing application.

         * Sensor board as ASK (OOK) relay board.

         * Sensor board as current measurement board.

         * Gateway (Receiver)

         Choose the required oprions from the config.h file.

      d. Some suble changes can be made to the code by introducing debug functionality for the transmitter board through LEDs. This can be changed by defining the appropriate macros.

      e. Rebuild the project.

      f. Download and debug the project.

      g. If the device programmed is Sensor (transmitter), please program the appropriate device ID into the data section of the controller. Please refer to ``stm8l15x_flashutils.h`` for the exact memory locations to be programmed.

      h. Download and debug the project.

STEP 2: ST Visual Programmer
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If the device programmed is Sensor (transmitter), there are two things to be done. 

   * Device ID: Program the appropriate device ID into the data section of the controller. Please refer to ``stm8l15x_flashutils.h`` for the exact memory locations to be programmed. For all versions (upto Version3.0), these steps are to be followed.

   * Option Byte: Option byte has to be changed for the independant watchdog timer from *Independant watchdogtimer continues running in Halt/Active-halt mode* to *Independant watchdogtimer stopped in Halt/Active-halt mode*

   1. Open ST Visual Programmer.

    .. image:: ./pics/stVisualProgrammer/stvp1.jpg
       :align: center

   2. Go to *Configure > Configure ST Visual Programmer*

   3. Select 
      
      * *ST-LINK* from *Hardware*, 
      
      * *USB* from *Port*, 

      * *SWIM* from *Programming mode* and

      * *STM8L051x3* from *Device*    

    .. image:: ./pics/stVisualProgrammer/stvp2.jpg
       :align: center

   4. Device ID: (only applicable for temperature-humidity sensor and Irms sensor) Inorder to program the device ID:
   
      * Go to *Data Momeory* tab. Right click on the memory area inside the window and click onto *Fill Area...* option. 

        .. image:: ./pics/stVisualProgrammer/stvp6.jpg
           :align: center
   
      * Make sure you change the option to *Subrange* and fill the values of *Start @* as **1002** and *End @* as **1003**.
       
      * Fill the device ID instead of "XX XX" in the *Fill with* option. 
       
      * Program the current tab.  

        .. image:: ./pics/stVisualProgrammer/stvp7.jpg
           :align: center

   5. Option Byte: (only applicable for temperature-humidity sensor and Irms sensor) Inorder to program the device ID:
   
      * Go to *Option Byte* tab. Click on the options for *IWDG_HALT*. 
      
      * Change the option from *Independant watchdogtimer continues running in Halt/Active-halt mode* to *Independant watchdogtimer stopped in Halt/Active-halt mode*. 
      
      * Program the current tab.

        .. image:: ./pics/stVisualProgrammer/stvp8.jpg
           :align: center 

   6. Close the window.

Now the board is good to go. Enjoy!