----------------
SummaryInNumbers
----------------

Operating Range
^^^^^^^^^^^^^^^

   * STM8L015F3: 

   * RFM69HW: 

   * HTU20D Supply Voltage (VDD): 1.5V (Min), 3.0V (Typical) 3.6V (Maximum)

   * Temperature Measurement: -40°C to 125°C

   * Humidity Measurement: 0%RH to 100%RH

   * Battery Voltage Measurement: 

   * Power Consumption Measurement: 
      
Percentage Error For Measurements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

   * Temperature Measurement: +/- 0.3°C

   * Humidity Measurement: +/- 5%RH max tolerance @55% RH

   * Battery Voltage Measurement: 

   * Power Consumption Measurement: 