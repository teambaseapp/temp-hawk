EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:LM2596T-5.0
LIBS:STM8S003F3P6
LIBS:MMBT2222ALT1
LIBS:symbols
LIBS:LM2596SX-12
LIBS:temphawk charger circuit-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LM358 U3
U 1 1 59AD184F
P 7500 3250
F 0 "U3" H 7500 3450 50  0000 L CNN
F 1 "LM358" H 7500 3050 50  0000 L CNN
F 2 "MY_LIB:LM358DT" H 7500 3250 50  0001 C CNN
F 3 "" H 7500 3250 50  0001 C CNN
	1    7500 3250
	-1   0    0    1   
$EndComp
$Comp
L LM358 U3
U 2 1 59AD195A
P 7400 4600
F 0 "U3" H 7400 4800 50  0000 L CNN
F 1 "LM358" H 7400 4400 50  0000 L CNN
F 2 "MY_LIB:LM358DT" H 7400 4600 50  0001 C CNN
F 3 "" H 7400 4600 50  0001 C CNN
	2    7400 4600
	-1   0    0    1   
$EndComp
$Comp
L R R3
U 1 1 59AD19FB
P 4800 3500
F 0 "R3" V 4880 3500 50  0000 C CNN
F 1 "1K" V 4800 3500 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 4730 3500 50  0001 C CNN
F 3 "" H 4800 3500 50  0001 C CNN
	1    4800 3500
	1    0    0    -1  
$EndComp
$Comp
L D_Schottky D2
U 1 1 59AD2060
P 6800 2300
F 0 "D2" H 6800 2400 50  0000 C CNN
F 1 "SS54" H 6800 2200 50  0000 C CNN
F 2 "Diodes_SMD:D_SMC" H 6800 2300 50  0001 C CNN
F 3 "" H 6800 2300 50  0001 C CNN
	1    6800 2300
	0    1    1    0   
$EndComp
$Comp
L L L1
U 1 1 59AD20C8
P 7000 1800
F 0 "L1" V 6950 1800 50  0000 C CNN
F 1 "33uH" V 7075 1800 50  0000 C CNN
F 2 "FIXED IND 33UH 5A 75.4 MOHM SMD:33uH" H 7000 1800 50  0001 C CNN
F 3 "" H 7000 1800 50  0001 C CNN
	1    7000 1800
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 59AD220B
P 7400 2450
F 0 "R1" V 7480 2450 50  0000 C CNN
F 1 "470R" V 7400 2450 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 7330 2450 50  0001 C CNN
F 3 "" H 7400 2450 50  0001 C CNN
	1    7400 2450
	1    0    0    -1  
$EndComp
$Comp
L R R_current1
U 1 1 59AD24EC
P 8000 2650
F 0 "R_current1" V 8080 2650 50  0000 C CNN
F 1 "PCB TRACE" V 8000 2650 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 7930 2650 50  0001 C CNN
F 3 "" H 8000 2650 50  0001 C CNN
	1    8000 2650
	0    1    1    0   
$EndComp
$Comp
L C C3
U 1 1 59AD25E3
P 4400 3500
F 0 "C3" H 4425 3600 50  0000 L CNN
F 1 "0.1uF" H 4425 3400 50  0000 L CNN
F 2 "MY_LIB:Capacitor_0603_Hand_Solder" H 4438 3350 50  0001 C CNN
F 3 "" H 4400 3500 50  0001 C CNN
	1    4400 3500
	1    0    0    -1  
$EndComp
$Comp
L LED LED3
U 1 1 59AD2869
P 4400 4250
F 0 "LED3" H 4400 4350 50  0000 C CNN
F 1 "OK" H 4400 4150 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 4400 4250 50  0001 C CNN
F 3 "" H 4400 4250 50  0001 C CNN
	1    4400 4250
	0    -1   -1   0   
$EndComp
$Comp
L LED LED2
U 1 1 59AD28DC
P 4800 4250
F 0 "LED2" H 4800 4350 50  0000 C CNN
F 1 "CH" H 4800 4150 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 4800 4250 50  0001 C CNN
F 3 "" H 4800 4250 50  0001 C CNN
	1    4800 4250
	0    -1   -1   0   
$EndComp
$Comp
L R R4
U 1 1 59AD29D7
P 4400 4700
F 0 "R4" V 4480 4700 50  0000 C CNN
F 1 "1K" V 4400 4700 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 4330 4700 50  0001 C CNN
F 3 "" H 4400 4700 50  0001 C CNN
	1    4400 4700
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 59AD2B15
P 5500 3700
F 0 "R2" V 5580 3700 50  0000 C CNN
F 1 "470K" V 5500 3700 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 5430 3700 50  0001 C CNN
F 3 "" H 5500 3700 50  0001 C CNN
	1    5500 3700
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR01
U 1 1 59AD2BD5
P 4800 3200
F 0 "#PWR01" H 4800 3050 50  0001 C CNN
F 1 "+5V" H 4800 3340 50  0000 C CNN
F 2 "" H 4800 3200 50  0001 C CNN
F 3 "" H 4800 3200 50  0001 C CNN
	1    4800 3200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 59AD2C0F
P 5500 4500
F 0 "#PWR02" H 5500 4250 50  0001 C CNN
F 1 "GND" H 5500 4350 50  0000 C CNN
F 2 "" H 5500 4500 50  0001 C CNN
F 3 "" H 5500 4500 50  0001 C CNN
	1    5500 4500
	1    0    0    -1  
$EndComp
$Comp
L D D1
U 1 1 59AD36A8
P 3300 2100
F 0 "D1" H 3300 2200 50  0000 C CNN
F 1 "D" H 3300 2000 50  0000 C CNN
F 2 "Diodes_SMD:D_SMC" H 3300 2100 50  0001 C CNN
F 3 "" H 3300 2100 50  0001 C CNN
	1    3300 2100
	0    1    1    0   
$EndComp
$Comp
L GND #PWR03
U 1 1 59AD3FF5
P 2900 2500
F 0 "#PWR03" H 2900 2250 50  0001 C CNN
F 1 "GND" H 2900 2350 50  0000 C CNN
F 2 "" H 2900 2500 50  0001 C CNN
F 3 "" H 2900 2500 50  0001 C CNN
	1    2900 2500
	1    0    0    -1  
$EndComp
$Comp
L LED LED6
U 1 1 59AD4925
P 7000 2950
F 0 "LED6" H 7000 3050 50  0000 C CNN
F 1 "CC/CV" H 7000 2850 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 7000 2950 50  0001 C CNN
F 3 "" H 7000 2950 50  0001 C CNN
	1    7000 2950
	0    1    1    0   
$EndComp
$Comp
L POT VAdj.1
U 1 1 59AD5360
P 7400 2050
F 0 "VAdj.1" V 7225 2050 50  0000 C CNN
F 1 "10K" V 7300 2050 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer-EVM3E" H 7400 2050 50  0001 C CNN
F 3 "" H 7400 2050 50  0001 C CNN
	1    7400 2050
	1    0    0    -1  
$EndComp
$Comp
L POT CC_ADJ1
U 1 1 59AD57B5
P 5500 4200
F 0 "CC_ADJ1" V 5325 4200 50  0000 C CNN
F 1 "10K" V 5400 4200 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer-EVM3E" H 5500 4200 50  0001 C CNN
F 3 "" H 5500 4200 50  0001 C CNN
	1    5500 4200
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 59AD5BFF
P 4400 3750
F 0 "#PWR04" H 4400 3500 50  0001 C CNN
F 1 "GND" H 4400 3600 50  0000 C CNN
F 2 "" H 4400 3750 50  0001 C CNN
F 3 "" H 4400 3750 50  0001 C CNN
	1    4400 3750
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 59AD5F41
P 4400 4950
F 0 "#PWR05" H 4400 4700 50  0001 C CNN
F 1 "GND" H 4400 4800 50  0000 C CNN
F 2 "" H 4400 4950 50  0001 C CNN
F 3 "" H 4400 4950 50  0001 C CNN
	1    4400 4950
	1    0    0    -1  
$EndComp
$Comp
L POT CH_LED_ADJ1
U 1 1 59AD60F5
P 7000 4000
F 0 "CH_LED_ADJ1" V 6825 4000 50  0000 C CNN
F 1 "100K" V 6900 4000 50  0000 C CNN
F 2 "Potentiometers:Potentiometer_Trimmer-EVM3E" H 7000 4000 50  0001 C CNN
F 3 "" H 7000 4000 50  0001 C CNN
	1    7000 4000
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 59AD6375
P 7000 3500
F 0 "C4" H 7025 3600 50  0000 L CNN
F 1 "0.01uF" H 7025 3400 50  0000 L CNN
F 2 "MY_LIB:Capacitor_0603_Hand_Solder" H 7038 3350 50  0001 C CNN
F 3 "" H 7000 3500 50  0001 C CNN
	1    7000 3500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 59AD6573
P 7000 4400
F 0 "#PWR06" H 7000 4150 50  0001 C CNN
F 1 "GND" H 7000 4250 50  0000 C CNN
F 2 "" H 7000 4400 50  0001 C CNN
F 3 "" H 7000 4400 50  0001 C CNN
	1    7000 4400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 59AD6760
P 7600 4150
F 0 "#PWR07" H 7600 3900 50  0001 C CNN
F 1 "GND" H 7600 4000 50  0000 C CNN
F 2 "" H 7600 4150 50  0001 C CNN
F 3 "" H 7600 4150 50  0001 C CNN
	1    7600 4150
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR08
U 1 1 59AD6959
P 7650 4900
F 0 "#PWR08" H 7650 4750 50  0001 C CNN
F 1 "+5V" H 7650 5040 50  0000 C CNN
F 2 "" H 7650 4900 50  0001 C CNN
F 3 "" H 7650 4900 50  0001 C CNN
	1    7650 4900
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR09
U 1 1 59AD6A40
P 7700 3550
F 0 "#PWR09" H 7700 3400 50  0001 C CNN
F 1 "+5V" H 7700 3690 50  0000 C CNN
F 2 "" H 7700 3550 50  0001 C CNN
F 3 "" H 7700 3550 50  0001 C CNN
	1    7700 3550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR010
U 1 1 59AD6B6D
P 7850 2950
F 0 "#PWR010" H 7850 2700 50  0001 C CNN
F 1 "GND" H 7850 2800 50  0000 C CNN
F 2 "" H 7850 2950 50  0001 C CNN
F 3 "" H 7850 2950 50  0001 C CNN
	1    7850 2950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR011
U 1 1 59AD6E13
P 3800 3700
F 0 "#PWR011" H 3800 3450 50  0001 C CNN
F 1 "GND" H 3800 3550 50  0000 C CNN
F 2 "" H 3800 3700 50  0001 C CNN
F 3 "" H 3800 3700 50  0001 C CNN
	1    3800 3700
	1    0    0    -1  
$EndComp
$Comp
L D D3
U 1 1 59ADAACD
P 8050 1800
F 0 "D3" H 8050 1900 50  0000 C CNN
F 1 "D" H 8050 1700 50  0000 C CNN
F 2 "Diodes_SMD:D_SMC" H 8050 1800 50  0001 C CNN
F 3 "" H 8050 1800 50  0001 C CNN
	1    8050 1800
	-1   0    0    1   
$EndComp
$Comp
L Screw_Terminal_01x02 J2
U 1 1 59ADECA4
P 10450 2900
F 0 "J2" H 10450 3000 50  0000 C CNN
F 1 "BATT" H 10450 2700 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_MPT-2.54mm_2pol" H 10450 2900 50  0001 C CNN
F 3 "" H 10450 2900 50  0001 C CNN
	1    10450 2900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR012
U 1 1 59ADEF39
P 10100 3150
F 0 "#PWR012" H 10100 2900 50  0001 C CNN
F 1 "GND" H 10100 3000 50  0000 C CNN
F 2 "" H 10100 3150 50  0001 C CNN
F 3 "" H 10100 3150 50  0001 C CNN
	1    10100 3150
	1    0    0    -1  
$EndComp
Text Label 9850 2450 0    60   ~ 0
V_BATT
$Comp
L Screw_Terminal_01x02 J3
U 1 1 59AE0B68
P 10500 1650
F 0 "J3" H 10500 1750 50  0000 C CNN
F 1 "VOUT" H 10500 1450 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_MPT-2.54mm_2pol" H 10500 1650 50  0001 C CNN
F 3 "" H 10500 1650 50  0001 C CNN
	1    10500 1650
	1    0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 59AE1648
P 9950 1100
F 0 "R9" V 10030 1100 50  0000 C CNN
F 1 "10K" V 9950 1100 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 9880 1100 50  0001 C CNN
F 3 "" H 9950 1100 50  0001 C CNN
	1    9950 1100
	-1   0    0    1   
$EndComp
Text Label 8900 1600 0    60   ~ 0
CTRL
$Comp
L GND #PWR013
U 1 1 59AE1ADC
P 9800 2000
F 0 "#PWR013" H 9800 1750 50  0001 C CNN
F 1 "GND" H 9800 1850 50  0000 C CNN
F 2 "" H 9800 2000 50  0001 C CNN
F 3 "" H 9800 2000 50  0001 C CNN
	1    9800 2000
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 59AE26D5
P 9250 1600
F 0 "R12" V 9330 1600 50  0000 C CNN
F 1 "1K" V 9250 1600 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 9180 1600 50  0001 C CNN
F 3 "" H 9250 1600 50  0001 C CNN
	1    9250 1600
	0    -1   -1   0   
$EndComp
Text Label 9700 950  0    60   ~ 0
V_BATT
$Comp
L GND #PWR014
U 1 1 59AE2F8D
P 10250 1850
F 0 "#PWR014" H 10250 1600 50  0001 C CNN
F 1 "GND" H 10250 1700 50  0000 C CNN
F 2 "" H 10250 1850 50  0001 C CNN
F 3 "" H 10250 1850 50  0001 C CNN
	1    10250 1850
	1    0    0    -1  
$EndComp
$Comp
L LED LED4
U 1 1 59AE3C42
P 2550 6100
F 0 "LED4" H 2550 6200 50  0000 C CNN
F 1 "OK" H 2550 6000 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 2550 6100 50  0001 C CNN
F 3 "" H 2550 6100 50  0001 C CNN
	1    2550 6100
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 59AE3E20
P 2550 6450
F 0 "R7" V 2630 6450 50  0000 C CNN
F 1 "1K" V 2550 6450 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 2480 6450 50  0001 C CNN
F 3 "" H 2550 6450 50  0001 C CNN
	1    2550 6450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR015
U 1 1 59AE3EE8
P 2750 5800
F 0 "#PWR015" H 2750 5550 50  0001 C CNN
F 1 "GND" H 2750 5650 50  0000 C CNN
F 2 "" H 2750 5800 50  0001 C CNN
F 3 "" H 2750 5800 50  0001 C CNN
	1    2750 5800
	1    0    0    -1  
$EndComp
Text Label 1350 6750 0    60   ~ 0
Greenled
Text Label 2100 6750 0    60   ~ 0
LOW_V
$Comp
L R R5
U 1 1 59AE5649
P 1350 6950
F 0 "R5" V 1430 6950 50  0000 C CNN
F 1 "1K" V 1350 6950 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 1280 6950 50  0001 C CNN
F 3 "" H 1350 6950 50  0001 C CNN
	1    1350 6950
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 59AE5C5F
P 2100 6950
F 0 "R8" V 2180 6950 50  0000 C CNN
F 1 "1K" V 2100 6950 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 2030 6950 50  0001 C CNN
F 3 "" H 2100 6950 50  0001 C CNN
	1    2100 6950
	1    0    0    -1  
$EndComp
$Comp
L LED LED1
U 1 1 59AE6021
P 1350 7300
F 0 "LED1" H 1350 7400 50  0000 C CNN
F 1 "OK" H 1350 7200 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 1350 7300 50  0001 C CNN
F 3 "" H 1350 7300 50  0001 C CNN
	1    1350 7300
	0    -1   -1   0   
$EndComp
$Comp
L LED LED5
U 1 1 59AE656B
P 2100 7300
F 0 "LED5" H 2100 7400 50  0000 C CNN
F 1 "OK" H 2100 7200 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 2100 7300 50  0001 C CNN
F 3 "" H 2100 7300 50  0001 C CNN
	1    2100 7300
	0    -1   -1   0   
$EndComp
$Comp
L R R10
U 1 1 59AE4696
P 9750 4000
F 0 "R10" V 9830 4000 50  0000 C CNN
F 1 "10K" V 9750 4000 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 9680 4000 50  0001 C CNN
F 3 "" H 9750 4000 50  0001 C CNN
	1    9750 4000
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 59AE481F
P 9750 4600
F 0 "R11" V 9830 4600 50  0000 C CNN
F 1 "5.6K" V 9750 4600 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 9680 4600 50  0001 C CNN
F 3 "" H 9750 4600 50  0001 C CNN
	1    9750 4600
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR016
U 1 1 59AE49A3
P 9750 5000
F 0 "#PWR016" H 9750 4750 50  0001 C CNN
F 1 "GND" H 9750 4850 50  0000 C CNN
F 2 "" H 9750 5000 50  0001 C CNN
F 3 "" H 9750 5000 50  0001 C CNN
	1    9750 5000
	1    0    0    -1  
$EndComp
Text Label 9750 3700 0    60   ~ 0
V_BATT
Text Label 10100 4300 0    60   ~ 0
BATT_MONITOR
$Comp
L Conn_01x04 J1
U 1 1 59AE7ED6
P 1250 1050
F 0 "J1" H 1250 1250 50  0000 C CNN
F 1 "PROG" H 1250 750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04_Pitch2.54mm" H 1250 1050 50  0001 C CNN
F 3 "" H 1250 1050 50  0001 C CNN
	1    1250 1050
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR017
U 1 1 59AE822C
P 900 950
F 0 "#PWR017" H 900 800 50  0001 C CNN
F 1 "+5V" H 900 1090 50  0000 C CNN
F 2 "" H 900 950 50  0001 C CNN
F 3 "" H 900 950 50  0001 C CNN
	1    900  950 
	1    0    0    -1  
$EndComp
Text Label 900  1050 0    60   ~ 0
SWIM
Text Label 900  1250 0    60   ~ 0
NRST
$Comp
L GND #PWR018
U 1 1 59AE8878
P 850 1150
F 0 "#PWR018" H 850 900 50  0001 C CNN
F 1 "GND" H 850 1000 50  0000 C CNN
F 2 "" H 850 1150 50  0001 C CNN
F 3 "" H 850 1150 50  0001 C CNN
	1    850  1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2800 1900 3800 1900
Wire Wire Line
	3800 1100 3800 1950
Wire Wire Line
	3300 1950 3300 1900
Connection ~ 3800 1900
Connection ~ 3300 1900
Wire Wire Line
	3300 2250 3300 2400
Wire Wire Line
	3800 2400 3800 2250
Connection ~ 3300 2400
Wire Wire Line
	2900 2150 2900 2500
Connection ~ 2900 2400
Connection ~ 3800 2400
Wire Wire Line
	5950 1800 6850 1800
Wire Wire Line
	6800 2150 6800 1800
Connection ~ 6800 1800
Wire Wire Line
	7400 1800 7400 1900
Wire Wire Line
	7700 1800 7700 1900
Connection ~ 7400 1800
Wire Wire Line
	7400 2200 7400 2300
Wire Wire Line
	7700 2650 7700 2200
Wire Wire Line
	4500 2650 7850 2650
Wire Wire Line
	7400 2650 7400 2600
Connection ~ 7700 2650
Wire Wire Line
	8150 2650 8550 2650
Connection ~ 7700 1800
Connection ~ 7400 2650
Wire Wire Line
	6800 2450 6800 2650
Connection ~ 6800 2650
Wire Wire Line
	7000 2000 7000 2800
Wire Wire Line
	7000 2250 7400 2250
Connection ~ 7400 2250
Connection ~ 7000 2250
Wire Wire Line
	7200 3250 7000 3250
Wire Wire Line
	7000 3100 7000 3350
Wire Wire Line
	8300 2650 8300 4500
Connection ~ 8300 2650
Wire Wire Line
	8300 3350 7800 3350
Wire Wire Line
	8300 4500 7700 4500
Connection ~ 8300 3350
Wire Wire Line
	3500 3300 3100 3300
Wire Wire Line
	3100 3300 3100 1900
Connection ~ 3100 1900
Wire Wire Line
	4100 3300 5500 3300
Wire Wire Line
	4400 3300 4400 3350
Wire Wire Line
	4800 3200 4800 3350
Connection ~ 4400 3300
Connection ~ 4800 3300
Wire Wire Line
	5500 3300 5500 3550
Wire Wire Line
	7550 1800 7550 2050
Connection ~ 7550 1800
Wire Wire Line
	5500 4050 5500 3850
Wire Wire Line
	5500 4350 5500 4500
Wire Wire Line
	4400 3650 4400 3750
Wire Wire Line
	4800 3650 4800 4100
Wire Wire Line
	4400 4100 4400 4050
Wire Wire Line
	4400 4050 4800 4050
Connection ~ 4800 4050
Wire Wire Line
	4400 4400 4400 4550
Wire Wire Line
	4400 4850 4400 4950
Wire Wire Line
	4800 4400 4800 4600
Wire Wire Line
	4800 4600 7100 4600
Connection ~ 7000 3250
Wire Wire Line
	7000 3650 7000 3850
Wire Wire Line
	7150 4000 7800 4000
Wire Wire Line
	7800 4000 7800 4700
Wire Wire Line
	7800 4700 7700 4700
Wire Wire Line
	7000 4150 7000 4400
Wire Wire Line
	7800 3150 7900 3150
Wire Wire Line
	7900 3150 7900 3700
Wire Wire Line
	7900 3700 6750 3700
Connection ~ 7000 3700
Wire Wire Line
	5650 4200 6750 4200
Wire Wire Line
	6750 4200 6750 3700
Wire Wire Line
	7500 4300 7500 4150
Wire Wire Line
	7500 4150 7600 4150
Wire Wire Line
	7650 4900 7500 4900
Wire Wire Line
	7600 3550 7700 3550
Wire Wire Line
	7600 2950 7850 2950
Wire Wire Line
	3800 3600 3800 3700
Wire Wire Line
	7150 1800 7900 1800
Wire Wire Line
	8750 2750 10250 2750
Wire Wire Line
	8550 3000 10250 3000
Wire Wire Line
	10100 3000 10100 3150
Wire Wire Line
	10250 2750 10250 2900
Wire Wire Line
	9850 2450 9850 2750
Connection ~ 9850 2750
Wire Wire Line
	8750 1800 8750 2750
Wire Wire Line
	8200 1800 8750 1800
Wire Wire Line
	9400 1600 9500 1600
Wire Wire Line
	8900 1600 9100 1600
Wire Wire Line
	9800 1800 9800 2000
Wire Wire Line
	9800 1300 10150 1300
Wire Wire Line
	9700 950  10300 950 
Wire Wire Line
	10300 1750 10250 1750
Wire Wire Line
	10250 1750 10250 1850
Wire Wire Line
	2550 5950 2550 5800
Wire Wire Line
	2550 5800 2750 5800
Wire Wire Line
	2550 6300 2550 6250
Wire Wire Line
	2550 6700 2550 6600
Wire Wire Line
	1350 6750 1350 6800
Wire Wire Line
	2100 6750 2100 6800
Wire Wire Line
	1350 7100 1350 7150
Wire Wire Line
	2100 7100 2100 7150
Wire Wire Line
	1350 7450 2100 7450
Connection ~ 1750 7450
Wire Wire Line
	9750 4150 9750 4450
Wire Wire Line
	9750 4750 9750 5000
Wire Wire Line
	9750 3850 9750 3700
Wire Wire Line
	10100 4300 9750 4300
Connection ~ 9750 4300
Wire Wire Line
	900  950  1050 950 
Wire Wire Line
	1050 1050 900  1050
Wire Wire Line
	1050 1150 850  1150
Wire Wire Line
	1050 1250 900  1250
Connection ~ 9950 950 
Wire Wire Line
	9950 1250 9950 1300
Connection ~ 9950 1300
Wire Wire Line
	2900 2400 4500 2400
$Comp
L Screw_Terminal_01x02 J4
U 1 1 59AF1832
P 2300 2150
F 0 "J4" H 2300 2250 50  0000 C CNN
F 1 "MAINS" H 2300 1950 50  0000 C CNN
F 2 "Connectors_Terminal_Blocks:TerminalBlock_Pheonix_MPT-2.54mm_2pol" H 2300 2150 50  0001 C CNN
F 3 "" H 2300 2150 50  0001 C CNN
	1    2300 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	2800 1900 2800 2050
Wire Wire Line
	2800 2050 2500 2050
Wire Wire Line
	2500 2150 2900 2150
Wire Wire Line
	8550 2650 8550 3000
Connection ~ 10100 3000
$Comp
L DMG2307L-7 U5
U 1 1 59AE4FA8
P 10550 1050
F 0 "U5" H 11000 900 60  0000 C CNN
F 1 "DMG2307L-7" H 10950 750 60  0000 C CNN
F 2 "MY_LIB2:DMG2307L-7" H 11450 1290 60  0001 C CNN
F 3 "" H 10550 1050 60  0000 C CNN
	1    10550 1050
	1    0    0    1   
$EndComp
Wire Wire Line
	10300 950  10300 600 
Wire Wire Line
	10300 600  10650 600 
Wire Wire Line
	10150 1300 10150 1050
Wire Wire Line
	10650 1500 10300 1500
Wire Wire Line
	10300 1500 10300 1650
$Comp
L STM8S003F3P U6
U 1 1 59AE9807
P 4300 6600
F 0 "U6" H 3150 7400 50  0000 L CNN
F 1 "STM8S003F3P" H 3150 7300 50  0001 L CNN
F 2 "STM8S003F3P6:STM8S003F3P6" H 3150 5800 50  0001 L CIN
F 3 "" H 4250 6200 50  0001 C CNN
	1    4300 6600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 6700 3000 6700
$Comp
L LM1117-5.0 U4
U 1 1 59AEC8A3
P 1150 5750
F 0 "U4" H 1000 5875 50  0000 C CNN
F 1 "LM1117-5.0" H 1150 5875 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 1150 5750 50  0001 C CNN
F 3 "" H 1150 5750 50  0001 C CNN
	1    1150 5750
	1    0    0    -1  
$EndComp
Text Label 550  5750 0    60   ~ 0
V_BATT
Wire Wire Line
	550  5750 850  5750
$Comp
L R R6
U 1 1 59AEE5EB
P 2950 5900
F 0 "R6" V 3030 5900 50  0000 C CNN
F 1 "1K" V 2950 5900 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 2880 5900 50  0001 C CNN
F 3 "" H 2950 5900 50  0001 C CNN
	1    2950 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1450 5750 2950 5750
Wire Wire Line
	2950 6050 2950 6100
Wire Wire Line
	2950 6100 3000 6100
$Comp
L GND #PWR019
U 1 1 59AEEE48
P 1150 6250
F 0 "#PWR019" H 1150 6000 50  0001 C CNN
F 1 "GND" H 1150 6100 50  0000 C CNN
F 2 "" H 1150 6250 50  0001 C CNN
F 3 "" H 1150 6250 50  0001 C CNN
	1    1150 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1150 6050 1150 6250
$Comp
L C C6
U 1 1 59AEF65D
P 3750 5600
F 0 "C6" H 3775 5700 50  0000 L CNN
F 1 "0.1uF" H 3775 5500 50  0000 L CNN
F 2 "MY_LIB:Capacitor_0603_Hand_Solder" H 3788 5450 50  0001 C CNN
F 3 "" H 3750 5600 50  0001 C CNN
	1    3750 5600
	1    0    0    -1  
$EndComp
$Comp
L C C5
U 1 1 59AEFD4F
P 2850 7350
F 0 "C5" H 2875 7450 50  0000 L CNN
F 1 "0.1uF" H 2875 7250 50  0000 L CNN
F 2 "MY_LIB:Capacitor_0603_Hand_Solder" H 2888 7200 50  0001 C CNN
F 3 "" H 2850 7350 50  0001 C CNN
	1    2850 7350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 5750 2950 5450
Wire Wire Line
	2950 5450 4300 5450
Wire Wire Line
	4300 5450 4300 5800
Connection ~ 3750 5450
$Comp
L GND #PWR020
U 1 1 59AF0DEA
P 3650 5750
F 0 "#PWR020" H 3650 5500 50  0001 C CNN
F 1 "GND" H 3650 5600 50  0000 C CNN
F 2 "" H 3650 5750 50  0001 C CNN
F 3 "" H 3650 5750 50  0001 C CNN
	1    3650 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 5750 3650 5750
$Comp
L GND #PWR021
U 1 1 59AF1548
P 2850 7550
F 0 "#PWR021" H 2850 7300 50  0001 C CNN
F 1 "GND" H 2850 7400 50  0000 C CNN
F 2 "" H 2850 7550 50  0001 C CNN
F 3 "" H 2850 7550 50  0001 C CNN
	1    2850 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 7200 2850 7200
Wire Wire Line
	2850 7500 2850 7550
Text Label 3000 6100 0    60   ~ 0
NRST
Text Label 2650 6800 0    60   ~ 0
LOW_V
Text Label 2650 6900 0    60   ~ 0
Greenled
Wire Wire Line
	2650 6800 3000 6800
Wire Wire Line
	2650 6900 3000 6900
Text Label 2750 6600 0    60   ~ 0
CTRL
Text Label 2650 6500 0    60   ~ 0
BATT_MONITOR
Text Label 2750 6400 0    60   ~ 0
SWIM
Wire Wire Line
	2750 6400 3000 6400
Wire Wire Line
	2650 6500 3000 6500
Wire Wire Line
	2750 6600 3000 6600
Wire Wire Line
	4300 7500 2850 7500
Connection ~ 2850 7500
Wire Wire Line
	4500 2100 4500 2650
Connection ~ 4500 2400
Wire Wire Line
	5050 2000 5050 2650
Connection ~ 5050 2650
$Comp
L LM1117-5.0 U2
U 1 1 59AFE06E
P 3800 3300
F 0 "U2" H 3650 3425 50  0000 C CNN
F 1 "LM1117-5.0" H 3800 3425 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-223" H 3800 3300 50  0001 C CNN
F 3 "" H 3800 3300 50  0001 C CNN
	1    3800 3300
	1    0    0    -1  
$EndComp
$Comp
L CP1 C2
U 1 1 59AFADA7
P 7700 2050
F 0 "C2" H 7725 2150 50  0000 L CNN
F 1 "220uF/35V" H 7725 1950 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10.5" H 7700 2050 50  0001 C CNN
F 3 "" H 7700 2050 50  0001 C CNN
	1    7700 2050
	1    0    0    -1  
$EndComp
$Comp
L CP1 C1
U 1 1 59AFAEC1
P 3800 2100
F 0 "C1" H 3825 2200 50  0000 L CNN
F 1 "220uF/35V" H 3825 2000 50  0000 L CNN
F 2 "Capacitors_SMD:CP_Elec_10x10.5" H 3800 2100 50  0001 C CNN
F 3 "" H 3800 2100 50  0001 C CNN
	1    3800 2100
	1    0    0    -1  
$EndComp
$Comp
L LM2596SX-12 U1
U 1 1 5A031727
P 5100 1200
F 0 "U1" H 4904 1537 50  0000 L BNN
F 1 "LM2596SX-12" H 4915 500 50  0000 L BNN
F 2 "MY_LIB:lm2596sxheatpad" H 5100 1200 50  0001 L BNN
F 3 "Unavailable" H 5100 1200 50  0001 L BNN
F 4 "None" H 5100 1200 50  0001 L BNN "Package"
F 5 "2.01 USD" H 5100 1200 50  0001 L BNN "Price"
F 6 "LM2596SX-12" H 5100 1200 50  0001 L BNN "MP"
F 7 "SIMPLE SWITCHER Power Converter 150 KHz 3A Step-Down Voltage Regulator 5-DDPAK/TO-263" H 5100 1200 50  0001 L BNN "Description"
F 8 "Texas Instruments" H 5100 1200 50  0001 L BNN "MF"
	1    5100 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 1100 4400 1100
Wire Wire Line
	4500 2100 4050 2100
Wire Wire Line
	4050 2100 4050 1300
Wire Wire Line
	4050 1300 4400 1300
Wire Wire Line
	4400 1500 4400 2000
Wire Wire Line
	4400 2000 5050 2000
Connection ~ 4400 1600
Wire Wire Line
	5950 1800 5950 1100
Wire Wire Line
	5950 1100 5800 1100
Wire Wire Line
	7000 2000 5800 2000
Wire Wire Line
	5800 2000 5800 1300
$Comp
L GND #PWR022
U 1 1 5A035BF6
P 1750 7550
F 0 "#PWR022" H 1750 7300 50  0001 C CNN
F 1 "GND" H 1750 7400 50  0000 C CNN
F 2 "" H 1750 7550 50  0001 C CNN
F 3 "" H 1750 7550 50  0001 C CNN
	1    1750 7550
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 7450 1750 7550
Text Notes 9550 2600 0    60   ~ 0
13.8V
Text Notes 10050 1650 0    60   ~ 0
13.8V
Text Notes 10800 4300 0    60   ~ 0
5V
$Comp
L R R13
U 1 1 5A040F66
P 900 4150
F 0 "R13" V 980 4150 50  0000 C CNN
F 1 "10K" V 900 4150 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 830 4150 50  0001 C CNN
F 3 "" H 900 4150 50  0001 C CNN
	1    900  4150
	1    0    0    -1  
$EndComp
$Comp
L R R14
U 1 1 5A041284
P 900 4550
F 0 "R14" V 980 4550 50  0000 C CNN
F 1 "4.7K" V 900 4550 50  0000 C CNN
F 2 "MY_LIB:R_0603_Hand_Solder" V 830 4550 50  0001 C CNN
F 3 "" H 900 4550 50  0001 C CNN
	1    900  4550
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR023
U 1 1 5A0418EB
P 2900 1750
F 0 "#PWR023" H 2900 1600 50  0001 C CNN
F 1 "VDD" H 2900 1900 50  0000 C CNN
F 2 "" H 2900 1750 50  0001 C CNN
F 3 "" H 2900 1750 50  0001 C CNN
	1    2900 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1750 2900 1900
Connection ~ 2900 1900
$Comp
L VDD #PWR024
U 1 1 5A041F00
P 900 3800
F 0 "#PWR024" H 900 3650 50  0001 C CNN
F 1 "VDD" H 900 3950 50  0000 C CNN
F 2 "" H 900 3800 50  0001 C CNN
F 3 "" H 900 3800 50  0001 C CNN
	1    900  3800
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR025
U 1 1 5A042373
P 900 4850
F 0 "#PWR025" H 900 4600 50  0001 C CNN
F 1 "GND" H 900 4700 50  0000 C CNN
F 2 "" H 900 4850 50  0001 C CNN
F 3 "" H 900 4850 50  0001 C CNN
	1    900  4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  3800 900  4000
Wire Wire Line
	900  4300 900  4400
Wire Wire Line
	900  4700 900  4850
Text Label 1050 4350 0    60   ~ 0
IN_V_Monit
Wire Wire Line
	1050 4350 900  4350
Connection ~ 900  4350
Text Label 5750 7100 0    60   ~ 0
IN_V_Monit
Wire Wire Line
	5750 7100 5600 7100
Text Notes 1000 3900 0    60   ~ 0
16V\n
Text Notes 1600 4350 0    60   ~ 0
5V\n
$Comp
L Q_NPN_BEC Q1
U 1 1 5A215B6C
P 9700 1600
F 0 "Q1" H 9900 1650 50  0000 L CNN
F 1 "Q_NPN_BEC" H 9900 1550 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 9900 1700 50  0001 C CNN
F 3 "" H 9700 1600 50  0001 C CNN
	1    9700 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 1300 9800 1400
$EndSCHEMATC
