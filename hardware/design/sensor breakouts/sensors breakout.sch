<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BaseApp">
<packages>
<package name="SOT23-W">
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.3984" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.3984" width="0.1524" layer="21"/>
<wire x1="0.2954" y1="-0.6604" x2="-0.3094" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.3" dx="2.8" dy="1.4" layer="1"/>
<smd name="2" x="1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<text x="2.032" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="2.032" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="C0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1.905" y="0.835" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
</package>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.508" x2="1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.508" x2="-1.27" y2="0.508" width="0.127" layer="21"/>
</package>
<package name="FILM-CAPACITOR-0.47U/400V">
<wire x1="-9.25" y1="3.9" x2="9.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="3.9" x2="9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="-3.9" x2="-9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-9.25" y1="-3.9" x2="-9.25" y2="3.9" width="0.127" layer="21"/>
<pad name="P$1" x="-7.5" y="0" drill="0.9"/>
<pad name="P$2" x="7.5" y="0" drill="0.9"/>
<text x="-2.54" y="4.17" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.17" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="51"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="51"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
</package>
<package name="20TSSOP">
<wire x1="-3.28" y1="2.2" x2="3.28" y2="2.2" width="0.2032" layer="51"/>
<wire x1="3.28" y1="2.2" x2="3.28" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="3.28" y1="-2.2" x2="-3.28" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3.28" y1="-2.2" x2="-3.28" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.28" y1="-2" x2="-3.28" y2="2" width="0.2032" layer="21"/>
<wire x1="3.28" y1="2" x2="3.28" y2="-2" width="0.2032" layer="21"/>
<circle x="-2.536" y="-1.378" radius="0.2231" width="0.2032" layer="21"/>
<smd name="20" x="-2.955" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="19" x="-2.305" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="18" x="-1.655" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="16" x="-0.355" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="17" x="-1.005" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="15" x="0.295" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="14" x="0.945" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="13" x="1.595" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="1" x="-2.955" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="2" x="-2.305" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="3" x="-1.655" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="4" x="-1.005" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="5" x="-0.355" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="6" x="0.295" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="7" x="0.945" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="8" x="1.595" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<text x="-3.55" y="-2.01" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="2.68" y="-2.01" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.82" y1="-3.2" x2="1.07" y2="-2.2" layer="51"/>
<rectangle x1="1.47" y1="-3.2" x2="1.72" y2="-2.2" layer="51"/>
<rectangle x1="0.17" y1="-3.2" x2="0.42" y2="-2.2" layer="51"/>
<rectangle x1="-0.48" y1="-3.2" x2="-0.23" y2="-2.2" layer="51"/>
<rectangle x1="-1.13" y1="-3.2" x2="-0.88" y2="-2.2" layer="51"/>
<rectangle x1="-1.78" y1="-3.2" x2="-1.53" y2="-2.2" layer="51"/>
<rectangle x1="-2.43" y1="-3.2" x2="-2.18" y2="-2.2" layer="51"/>
<rectangle x1="-3.08" y1="-3.2" x2="-2.83" y2="-2.2" layer="51"/>
<rectangle x1="0.82" y1="2.2" x2="1.07" y2="3.2" layer="51"/>
<rectangle x1="1.47" y1="2.2" x2="1.72" y2="3.2" layer="51"/>
<rectangle x1="0.17" y1="2.2" x2="0.42" y2="3.2" layer="51"/>
<rectangle x1="-0.48" y1="2.2" x2="-0.23" y2="3.2" layer="51"/>
<rectangle x1="-1.13" y1="2.2" x2="-0.88" y2="3.2" layer="51"/>
<rectangle x1="-1.78" y1="2.2" x2="-1.53" y2="3.2" layer="51"/>
<rectangle x1="-2.43" y1="2.2" x2="-2.18" y2="3.2" layer="51"/>
<rectangle x1="-3.08" y1="2.2" x2="-2.83" y2="3.2" layer="51"/>
<smd name="9" x="2.245" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="10" x="2.895" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="11" x="2.895" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="12" x="2.245" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<rectangle x1="2.12" y1="-3.2" x2="2.37" y2="-2.2" layer="51"/>
<rectangle x1="2.77" y1="-3.2" x2="3.02" y2="-2.2" layer="51"/>
<rectangle x1="2.12" y1="2.2" x2="2.37" y2="3.2" layer="51"/>
<rectangle x1="2.77" y1="2.2" x2="3.02" y2="3.2" layer="51"/>
</package>
<package name="R0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-2.032" y="0.635" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.032" y="-2.159" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.63" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.63" width="0.127" layer="21"/>
</package>
<package name="R-0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.3" y1="0.473" x2="-1.3" y2="-0.473" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-0.473" x2="1.2" y2="-0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.473" x2="1.2" y2="0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.473" x2="-1.3" y2="0.473" width="0.127" layer="21"/>
</package>
<package name="10555-METRIC">
<smd name="P$1" x="-4.85" y="0" dx="3.3" dy="2" layer="1"/>
<smd name="P$2" x="4.35" y="0" dx="3.3" dy="2" layer="1"/>
<wire x1="-6.75" y1="3" x2="-6.75" y2="-3" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-3" x2="6.25" y2="-3" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3" x2="6.25" y2="3" width="0.127" layer="21"/>
<wire x1="6.25" y1="3" x2="-6.75" y2="3" width="0.127" layer="21"/>
<text x="-2.75" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805-METRIC">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="R0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="-0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="-2" y2="0.75" width="0.127" layer="21"/>
</package>
<package name="B2,54">
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.0024" layer="37"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="37"/>
<smd name="TP" x="0" y="0" dx="2.54" dy="2.54" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.397" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="B5.08">
<wire x1="-0.635" y1="0" x2="0.635" y2="0" width="0.0024" layer="37"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.0024" layer="37"/>
<circle x="0" y="0" radius="0.635" width="0.254" layer="37"/>
<smd name="TP" x="0" y="0" dx="5.08" dy="5.08" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.397" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.175" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
</package>
<package name="SO08">
<wire x1="-2.362" y1="-1.803" x2="2.362" y2="-1.803" width="0.1524" layer="51"/>
<wire x1="2.362" y1="-1.803" x2="2.362" y2="1.803" width="0.1524" layer="21"/>
<wire x1="2.362" y1="1.803" x2="-2.362" y2="1.803" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="1.803" x2="-2.362" y2="-1.803" width="0.1524" layer="21"/>
<circle x="-1.8034" y="-0.9906" radius="0.1436" width="0.2032" layer="21"/>
<smd name="1" x="-1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="2" x="-0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="3" x="0.635" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="4" x="1.905" y="-2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="5" x="1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="6" x="0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="7" x="-0.635" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="8" x="-1.905" y="2.6162" dx="0.6096" dy="2.2098" layer="1"/>
<text x="-1.27" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.27" y="0" size="0.4064" layer="25">&gt;NAME</text>
<rectangle x1="-2.0828" y1="-2.8702" x2="-1.7272" y2="-1.8542" layer="51"/>
<rectangle x1="-0.8128" y1="-2.8702" x2="-0.4572" y2="-1.8542" layer="51"/>
<rectangle x1="0.4572" y1="-2.8702" x2="0.8128" y2="-1.8542" layer="51"/>
<rectangle x1="1.7272" y1="-2.8702" x2="2.0828" y2="-1.8542" layer="51"/>
<rectangle x1="-2.0828" y1="1.8542" x2="-1.7272" y2="2.8702" layer="51"/>
<rectangle x1="-0.8128" y1="1.8542" x2="-0.4572" y2="2.8702" layer="51"/>
<rectangle x1="0.4572" y1="1.8542" x2="0.8128" y2="2.8702" layer="51"/>
<rectangle x1="1.7272" y1="1.8542" x2="2.0828" y2="2.8702" layer="51"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="-2.4" y1="1" x2="2.4" y2="1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1" x2="-2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-2.4" y2="1" width="0.127" layer="21"/>
</package>
<package name="C1210">
<smd name="P$1" x="-1.35" y="0" dx="1" dy="3" layer="1"/>
<smd name="P$2" x="1.35" y="0" dx="1" dy="3" layer="1"/>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.127" layer="21"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<text x="-1.5" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R2512">
<smd name="P$1" x="0" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<smd name="P$2" x="6.5024" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<wire x1="-1.27" y1="1.9304" x2="-1.27" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.9304" x2="7.7724" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="-1.9304" x2="7.7724" y2="1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="1.9304" x2="-1.27" y2="1.9304" width="0.127" layer="21"/>
<text x="1.016" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0.508" y="-3.556" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="5.334" y1="1.524" x2="1.016" y2="1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="-1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="-1.524" x2="5.334" y2="-1.524" width="0.127" layer="51"/>
<wire x1="5.334" y1="-1.524" x2="5.334" y2="1.524" width="0.127" layer="51"/>
</package>
<package name="MELF0204">
<smd name="P$1" x="-0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.8" x2="1.2" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.8" x2="1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.8" x2="-1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.8" x2="-1.2" y2="0.8" width="0.127" layer="21"/>
<text x="-0.8" y="1.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.8" y="-2.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="HTU21D">
<description>&lt;b&gt;Description:&lt;/b&gt; HTU21D is a very small, low cost, I2C digital humidity and temperature sensor.</description>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.2032" layer="21"/>
<smd name="NC@6" x="1.5" y="1" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="GND" x="1.5" y="0" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="DATA" x="1.5" y="-1" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="SCK" x="-1.5" y="-1" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="VDD" x="-1.5" y="0" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="NC@1" x="-1.5" y="1" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="7" x="0" y="0" dx="0.2" dy="0.2" layer="1"/>
<polygon width="0.127" layer="1">
<vertex x="-0.7" y="1.25"/>
<vertex x="0.75" y="1.25"/>
<vertex x="0.75" y="-0.8"/>
<vertex x="0.3" y="-1.25"/>
<vertex x="-0.7" y="-1.25"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-0.55" y="1.05"/>
<vertex x="0.55" y="1.05"/>
<vertex x="0.55" y="-0.65"/>
<vertex x="0.1" y="-1.1"/>
<vertex x="-0.55" y="-1.1"/>
</polygon>
<wire x1="-0.7" y1="-1.5" x2="0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="1.2" x2="0.5" y2="1.2" width="0.127" layer="51"/>
<wire x1="0.5" y1="1.2" x2="0.5" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.5" y1="0.5" x2="-0.5" y2="0.5" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0.5" x2="-0.5" y2="1.2" width="0.127" layer="51"/>
<polygon width="0.127" layer="29">
<vertex x="-0.775" y="1.325"/>
<vertex x="-0.775" y="-1.3"/>
<vertex x="0.375" y="-1.3"/>
<vertex x="0.825" y="-0.85"/>
<vertex x="0.825" y="1.325"/>
</polygon>
<text x="-1.075" y="1.775" size="0.5" layer="25">&gt;Name</text>
<text x="-1.325" y="-2.2" size="0.5" layer="27">&gt;Value</text>
<wire x1="-0.508" y1="0.508" x2="0.508" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.508" x2="0.508" y2="1.1938" width="0.127" layer="21"/>
<wire x1="0.508" y1="1.1938" x2="-0.508" y2="1.1938" width="0.127" layer="21"/>
<wire x1="-0.508" y1="1.1938" x2="-0.508" y2="0.508" width="0.127" layer="21"/>
<circle x="1.778" y="-1.7272" radius="0.160640625" width="0.127" layer="21"/>
</package>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
</package>
<package name="LS_A67K">
<smd name="C" x="-1.27" y="0" dx="3.5" dy="1.5" layer="1" rot="R90"/>
<smd name="A" x="1.27" y="0" dx="3.5" dy="1.5" layer="1" rot="R90"/>
<wire x1="-2.5" y1="2" x2="-2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2" x2="2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2" x2="2.5" y2="2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2" x2="-2.5" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<circle x="0" y="1.524" radius="0.254" width="0.127" layer="21"/>
</package>
<package name="LS_Y876">
<smd name="A" x="1.5" y="0" dx="1" dy="0.7" layer="1" rot="R270"/>
<smd name="C" x="-1.5" y="0" dx="1" dy="0.7" layer="1" rot="R270"/>
<wire x1="-1.016" y1="-0.762" x2="1.016" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.762" x2="1.016" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.762" x2="-1.016" y2="0.762" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.762" x2="-1.016" y2="-0.762" width="0.127" layer="21"/>
<circle x="0" y="-0.254" radius="0.254" width="0.127" layer="21"/>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="LED0603">
<wire x1="0.45" y1="0.4" x2="-0.45" y2="0.4" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-0.4" x2="-0.45" y2="-0.4" width="0.1016" layer="51"/>
<smd name="C" x="0.85" y="0" dx="1" dy="1" layer="1" rot="R270"/>
<smd name="A" x="-0.85" y="0" dx="1" dy="1" layer="1" rot="R270"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.2" y1="-0.2" x2="1.1" y2="0.2" layer="51" rot="R270"/>
<rectangle x1="-1.1" y1="-0.2" x2="-0.2" y2="0.2" layer="51" rot="R270"/>
<rectangle x1="0.075" y1="0.225" x2="0.225" y2="0.525" layer="21" rot="R270"/>
<rectangle x1="0.075" y1="-0.525" x2="0.225" y2="-0.225" layer="21" rot="R270"/>
<rectangle x1="0" y1="-0.15" x2="0.3" y2="0.15" layer="21" rot="R270"/>
<wire x1="1.5" y1="0.7" x2="-1.6" y2="0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.7" x2="1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="1.5" y2="0.7" width="0.127" layer="21"/>
</package>
<package name="LED-5MM">
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IR-LED">
<circle x="0" y="0" radius="1.5" width="0.127" layer="51"/>
<pad name="+" x="-1.27" y="0" drill="0.5"/>
<pad name="-" x="1.27" y="0" drill="0.5"/>
<wire x1="1.5" y1="1.57" x2="1.5" y2="-1.57" width="0.127" layer="21"/>
<wire x1="-2.2" y1="0" x2="0" y2="2.2" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.2" y1="0" x2="0" y2="-2.2" width="0.127" layer="21" curve="90"/>
<wire x1="0" y1="2.2" x2="1.5" y2="1.6" width="0.127" layer="21" curve="-43.602819"/>
<wire x1="0" y1="-2.2" x2="1.5" y2="-1.6" width="0.127" layer="21" curve="43.602819"/>
</package>
<package name="LED-3MM">
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="-2.5908" y1="1.7272" x2="-1.8542" y2="1.7272" width="0.127" layer="21"/>
<wire x1="-2.2352" y1="1.3208" x2="-2.2352" y2="2.1082" width="0.127" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<pin name="GND" x="0" y="0" visible="pad" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VDD">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.635" x2="0" y2="3.81" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VDD" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="MCP17XX">
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="2.54" width="0.4064" layer="94"/>
<wire x1="7.62" y1="2.54" x2="-7.62" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<text x="-7.62" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.032" y="-4.318" size="1.524" layer="95">GND</text>
<pin name="VI" x="-10.16" y="0" length="short" direction="in"/>
<pin name="GND" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="VO" x="10.16" y="0" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CAPACITOR">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
</symbol>
<symbol name="STM8L051F3P6">
<pin name="PC5" x="-20.32" y="12.7" length="middle"/>
<pin name="PC6" x="-20.32" y="10.16" length="middle"/>
<pin name="PA0" x="-20.32" y="7.62" length="middle"/>
<pin name="PA1/NRST" x="-20.32" y="5.08" length="middle"/>
<pin name="PA2" x="-20.32" y="2.54" length="middle"/>
<pin name="PA3" x="-20.32" y="0" length="middle"/>
<pin name="VSS/VREF-/VSSA" x="-20.32" y="-5.08" length="middle"/>
<pin name="VDD/VREF+/VDDA" x="-20.32" y="-2.54" length="middle"/>
<pin name="PD0" x="-20.32" y="-7.62" length="middle"/>
<pin name="PB0" x="-20.32" y="-10.16" length="middle"/>
<pin name="PC4" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="PC1" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="PC0" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="PB7" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="PB6" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="PB5" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="PB4" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="PB3" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="PB2" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="PB1" x="20.32" y="-10.16" length="middle" rot="R180"/>
<wire x1="-15.24" y1="15.24" x2="-15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-12.7" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<text x="-2.54" y="15.24" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="RESISTOR">
<text x="-2.54" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-2.54" y="-3.81" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="TESTPAD">
<wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<text x="-1.27" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-1.27" size="1.778" layer="96">&gt;VALUE</text>
<pin name="TP" x="0" y="-2.54" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="RS485">
<wire x1="-7.62" y1="7.62" x2="12.7" y2="7.62" width="0.254" layer="94"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="0" width="0.254" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="-2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="-2.54" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="3.556" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.556" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="0.762" y2="-3.302" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.302" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="1.27" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="5.08" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-1.524" x2="0.762" y2="-1.524" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.524" x2="0.762" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-3.556" x2="3.81" y2="-3.556" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-3.556" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="1.27" width="0.1524" layer="94"/>
<wire x1="3.81" y1="1.27" x2="3.048" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="3.556" width="0.1524" layer="94"/>
<wire x1="5.08" y1="3.556" x2="2.54" y2="3.556" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.096" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-6.096" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-6.096" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="7.874" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="7.874" y2="0" width="0.1524" layer="94"/>
<wire x1="12.7" y1="0" x2="11.176" y2="0" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-2.54" x2="11.176" y2="-2.54" width="0.1524" layer="94"/>
<circle x="1.27" y="0.762" radius="0.254" width="0.254" layer="94"/>
<circle x="2.794" y="1.27" radius="0.254" width="0.254" layer="94"/>
<circle x="1.524" y="-3.556" radius="0.254" width="0.254" layer="94"/>
<circle x="3.81" y="0" radius="0.127" width="0.1524" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.127" width="0.1524" layer="94"/>
<text x="-7.62" y="8.128" size="1.778" layer="95">&gt;Name</text>
<text x="-7.62" y="-12.7" size="1.778" layer="96">&gt;Value</text>
<pin name="RO" x="-10.16" y="2.54" visible="pin" length="short"/>
<pin name="/RE" x="-10.16" y="0" visible="pin" length="short"/>
<pin name="DE" x="-10.16" y="-2.54" visible="pin" length="short"/>
<pin name="DI" x="-10.16" y="-5.08" visible="pin" length="short"/>
<pin name="VCC" x="15.24" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="B" x="15.24" y="0" visible="pin" length="short" rot="R180"/>
<pin name="A" x="15.24" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="GND" x="15.24" y="-5.08" visible="pin" length="short" rot="R180"/>
</symbol>
<symbol name="HTU21D">
<description>&lt;b&gt;Description:&lt;/b&gt; The HTU21D is a very small, low cost, I2D digitally controlled humidity and temperature sensor.</description>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="5.588" size="1.778" layer="95">&gt;Name</text>
<text x="-7.62" y="-7.62" size="1.778" layer="95">&gt;Value</text>
<pin name="SCK" x="10.16" y="0" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="DATA" x="10.16" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="GND@2" x="-10.16" y="0" visible="pin" length="short" direction="pwr"/>
<pin name="VDD" x="-10.16" y="2.54" visible="pin" length="short" direction="pwr"/>
<pin name="GND@7" x="-10.16" y="-2.54" visible="pin" length="short" direction="pwr"/>
</symbol>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC">
<description>&lt;b&gt;SUPPLY SYMBOL</description>
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0" addlevel="request"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VDD">
<description>&lt;b&gt;SUPPLY SYMBOL</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0" addlevel="request"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP1700">
<description>Digikey No :- __MCP1700T3302ETTCT-ND__</description>
<gates>
<gate name="G$1" symbol="MCP17XX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-W">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VI" pad="3"/>
<connect gate="G$1" pin="VO" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCP1700T3302ETTCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.37" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>CAPACITOR</description>
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0.1UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1095-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FILM_AC-CAP" package="FILM-CAPACITOR-0.47U/400V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1275-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1079-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.23"/>
</technology>
</technologies>
</device>
<device name="-33PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1055-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-18PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1052-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-DNP/NI" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2087-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3022-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-3386-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.11"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1821-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.12"/>
</technology>
</technologies>
</device>
<device name="-0.01UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1132-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4700PF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2062-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1524-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3012-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.16" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/25V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3335-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7NF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9092-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-18PF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="77-VJ0805A180GXQCBC" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.06" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/10V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1356-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.24" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-9730-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1024-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/25V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1353-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3303-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1532-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22PF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9031-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-5043-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10NF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1926-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1993-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.47" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V." package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-4112-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V-0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3340-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1494-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2200PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1085-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1281-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.022UF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1063-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1296-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/50V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1909-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0367" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/10V-0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1863-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.031" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/16V" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-5514-2-ND " constant="no"/>
<attribute name="MANUFACTURER" value="C1210C476M4PACTU" constant="no"/>
<attribute name="MOUSER" value="80-C1210C476M4P" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.54" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.01" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2143-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.017" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5.6PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-7944-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0193" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12PF" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1254-1-ND  " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM8L051F3" prefix="STM8L">
<gates>
<gate name="G$1" symbol="STM8L051F3P6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="20TSSOP">
<connects>
<connect gate="G$1" pin="PA0" pad="3"/>
<connect gate="G$1" pin="PA1/NRST" pad="4"/>
<connect gate="G$1" pin="PA2" pad="5"/>
<connect gate="G$1" pin="PA3" pad="6"/>
<connect gate="G$1" pin="PB0" pad="10"/>
<connect gate="G$1" pin="PB1" pad="11"/>
<connect gate="G$1" pin="PB2" pad="12"/>
<connect gate="G$1" pin="PB3" pad="13"/>
<connect gate="G$1" pin="PB4" pad="14"/>
<connect gate="G$1" pin="PB5" pad="15"/>
<connect gate="G$1" pin="PB6" pad="16"/>
<connect gate="G$1" pin="PB7" pad="17"/>
<connect gate="G$1" pin="PC0" pad="18"/>
<connect gate="G$1" pin="PC1" pad="19"/>
<connect gate="G$1" pin="PC4" pad="20"/>
<connect gate="G$1" pin="PC5" pad="1"/>
<connect gate="G$1" pin="PC6" pad="2"/>
<connect gate="G$1" pin="PD0" pad="9"/>
<connect gate="G$1" pin="VDD/VREF+/VDDA" pad="8"/>
<connect gate="G$1" pin="VSS/VREF-/VSSA" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="497-13042-5-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.32" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;B&gt;Resistance&lt;/B&gt; &lt;p&gt; 
Mount :-Surface mount</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-50R_1/8W_RF" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0603-50-E3 "/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-50R_1/20W_RF" package="R-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="FC0402-50BWCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.52"/>
</technology>
</technologies>
</device>
<device name="-POWER_RESISTOR" package="10555-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-100K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-100K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-1M_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00M-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-12K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-12.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-330R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF0603JT330RCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-30R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P30.0HCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0R" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P0.0GCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-4.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-24K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM24KCFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-560R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-560GRDKR-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.7K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-2.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-33R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM33.0CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-6.2K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM6.2KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-10.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-10R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P10.0HCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5K_1/8W" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0805-5K" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300K_/1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM300KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-33K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-33.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-110K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-110KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-20K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-20.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-140K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-140KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-84.5K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-84.5KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4823-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15K_1/8W" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-15.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-47.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_0.125W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-10KARCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/4W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM1.0KDCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-698R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-698HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.5K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.50K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-3.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.58K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RC0603FR-071K58L" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-51.1R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-51.1HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.37K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.37KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-150GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-82GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-130R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-130GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12.1K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-12.1KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-270KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-100HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM470AZCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.13" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.0003R/3W-CURRENTSENSE" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="HCS2512FTL300CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.9625" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6.8R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-6.80ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.102" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-470ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-270KACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-680GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0048" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="267K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-267KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-191K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4862-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9.53K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-9.53KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-49.9K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-49.9KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-470GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.55K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.55KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.2K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.2KGRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-348K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-348KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="27R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-27GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TESTPAD" prefix="TP">
<gates>
<gate name="G$1" symbol="TESTPAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="B2,54">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part. Plain copper pads"/>
<attribute name="PRICE_PER_UNIT" value="0"/>
</technology>
</technologies>
</device>
<device name="'" package="B5.08">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RS485">
<description>&lt;b&gt;Digikey No :- &lt;/b&gt; __ISL83485IBZ-ND__&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="RS485" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO08">
<connects>
<connect gate="G$1" pin="/RE" pad="2"/>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3.3V" package="SO08">
<connects>
<connect gate="G$1" pin="/RE" pad="2"/>
<connect gate="G$1" pin="A" pad="6"/>
<connect gate="G$1" pin="B" pad="7"/>
<connect gate="G$1" pin="DE" pad="3"/>
<connect gate="G$1" pin="DI" pad="4"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="RO" pad="1"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="ISL83485IBZ-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.48" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HTU20D" prefix="U">
<gates>
<gate name="U$1" symbol="HTU21D" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="HTU21D">
<connects>
<connect gate="U$1" pin="DATA" pad="DATA"/>
<connect gate="U$1" pin="GND@2" pad="GND"/>
<connect gate="U$1" pin="GND@7" pad="7"/>
<connect gate="U$1" pin="SCK" pad="SCK"/>
<connect gate="U$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="223-1592-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="4.38" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" prefix="FID" uservalue="yes">
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No_Part. Plain Copper Pads."/>
<attribute name="PRICE_PER_UNIT" value="0"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LS_A67K">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-" package="LS_Y876">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-RED" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-2512-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.09"/>
</technology>
</technologies>
</device>
<device name="-GREEN" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-3118-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17" constant="no"/>
</technology>
</technologies>
</device>
<device name="-YELLOW" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-1196-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17"/>
</technology>
</technologies>
</device>
<device name="-BLUE" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-2816-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17"/>
</technology>
</technologies>
</device>
<device name="-5MM" package="LED-5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="C503B-RAN-CY0B0AA1-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-IR" package="IR-LED">
<connects>
<connect gate="G$1" pin="A" pad="+"/>
<connect gate="G$1" pin="C" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1080-1076-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.1628" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.39" constant="no"/>
</technology>
</technologies>
</device>
<device name="RED" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="732-5005-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.224" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
<device name="-GRN" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1125-1186-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.125" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.33" constant="no"/>
</technology>
</technologies>
</device>
<device name="-YLW" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="732-5010-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.224" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
</package>
<package name="2X03/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-2.54" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-2.54" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="-2.921" y1="-5.461" x2="-2.159" y2="-4.699" layer="21"/>
<rectangle x1="-2.921" y1="-4.699" x2="-2.159" y2="-2.921" layer="51"/>
<rectangle x1="-0.381" y1="-4.699" x2="0.381" y2="-2.921" layer="51"/>
<rectangle x1="-0.381" y1="-5.461" x2="0.381" y2="-4.699" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-5.461" x2="2.921" y2="-4.699" layer="21"/>
<rectangle x1="2.159" y1="-4.699" x2="2.921" y2="-2.921" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINH2X3">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A5L-LOC">
<wire x1="85.09" y1="3.81" x2="85.09" y2="24.13" width="0.1016" layer="94"/>
<wire x1="85.09" y1="24.13" x2="139.065" y2="24.13" width="0.1016" layer="94"/>
<wire x1="139.065" y1="24.13" x2="180.34" y2="24.13" width="0.1016" layer="94"/>
<wire x1="170.18" y1="3.81" x2="170.18" y2="8.89" width="0.1016" layer="94"/>
<wire x1="170.18" y1="8.89" x2="180.34" y2="8.89" width="0.1016" layer="94"/>
<wire x1="170.18" y1="8.89" x2="139.065" y2="8.89" width="0.1016" layer="94"/>
<wire x1="139.065" y1="8.89" x2="139.065" y2="3.81" width="0.1016" layer="94"/>
<wire x1="139.065" y1="8.89" x2="139.065" y2="13.97" width="0.1016" layer="94"/>
<wire x1="139.065" y1="13.97" x2="180.34" y2="13.97" width="0.1016" layer="94"/>
<wire x1="139.065" y1="13.97" x2="139.065" y2="19.05" width="0.1016" layer="94"/>
<wire x1="139.065" y1="19.05" x2="180.34" y2="19.05" width="0.1016" layer="94"/>
<wire x1="139.065" y1="19.05" x2="139.065" y2="24.13" width="0.1016" layer="94"/>
<text x="140.97" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="140.97" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="154.305" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="140.716" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="184.15" y2="133.35" columns="4" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA5_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A5, landscape with doc field</description>
<gates>
<gate name="G$1" symbol="A5L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="1X01">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.9304" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="1X01-CLEANBIG">
<pad name="1" x="0" y="0" drill="1.016" diameter="1.778"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="PINHD1">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X1" prefix="JP" uservalue="yes">
<description>&lt;b&gt;Pin header 1x1 for 0.1" spacing&lt;/b&gt;
&lt;p&gt;
With round pins</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CB" package="1X01-CLEANBIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND10" library="BaseApp" deviceset="GND" device=""/>
<part name="U$14" library="BaseApp" deviceset="VDD" device=""/>
<part name="U$1" library="BaseApp" deviceset="MCP1700" device=""/>
<part name="C1" library="BaseApp" deviceset="CAPACITOR" device="-1UF/6.3V" value="1UF"/>
<part name="C2" library="BaseApp" deviceset="CAPACITOR" device="-1UF/6.3V" value="1UF"/>
<part name="STM8L" library="BaseApp" deviceset="STM8L051F3" device=""/>
<part name="U$3" library="BaseApp" deviceset="VDD" device=""/>
<part name="GND1" library="BaseApp" deviceset="GND" device=""/>
<part name="R3" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="C3" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value=".1uF"/>
<part name="U$4" library="BaseApp" deviceset="VCC" device=""/>
<part name="U$6" library="BaseApp" deviceset="VDD" device=""/>
<part name="GND3" library="BaseApp" deviceset="GND" device=""/>
<part name="VDD" library="BaseApp" deviceset="TESTPAD" device="" value="TESTPAD"/>
<part name="SWDIO" library="BaseApp" deviceset="TESTPAD" device="" value="TESTPAD"/>
<part name="GND" library="BaseApp" deviceset="TESTPAD" device="" value="TESTPAD"/>
<part name="NRST" library="BaseApp" deviceset="TESTPAD" device="" value="TESTPAD"/>
<part name="JP1" library="pinhead" deviceset="PINHD-2X3" device=""/>
<part name="U$2" library="BaseApp" deviceset="RS485" device="-3.3V"/>
<part name="GND4" library="BaseApp" deviceset="GND" device=""/>
<part name="U1" library="BaseApp" deviceset="HTU20D" device="SMD"/>
<part name="C4" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value=".1uF"/>
<part name="GND5" library="BaseApp" deviceset="GND" device=""/>
<part name="C5" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value=".1uF"/>
<part name="U$7" library="BaseApp" deviceset="VCC" device=""/>
<part name="GND2" library="BaseApp" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" deviceset="DINA5_L" device=""/>
<part name="FID1" library="BaseApp" deviceset="FIDUCIAL" device=""/>
<part name="FID2" library="BaseApp" deviceset="FIDUCIAL" device=""/>
<part name="FID3" library="BaseApp" deviceset="FIDUCIAL" device=""/>
<part name="FID4" library="BaseApp" deviceset="FIDUCIAL" device=""/>
<part name="LED" library="BaseApp" deviceset="LED" device="-YELLOW"/>
<part name="R1" library="BaseApp" deviceset="RESISTOR" device="-330R_1/10W"/>
<part name="U$5" library="BaseApp" deviceset="VDD" device=""/>
<part name="R2" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="W" library="adafruit" deviceset="PINHD-1X1" device=""/>
<part name="JP3" library="adafruit" deviceset="PINHD-1X1" device=""/>
<part name="GND6" library="BaseApp" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="2.54" y1="55.88" x2="83.82" y2="55.88" width="1.016" layer="94" style="shortdash"/>
<wire x1="83.82" y1="55.88" x2="109.22" y2="55.88" width="1.016" layer="94" style="shortdash"/>
<wire x1="109.22" y1="55.88" x2="109.22" y2="93.98" width="1.016" layer="94" style="shortdash"/>
<wire x1="109.22" y1="93.98" x2="109.22" y2="129.54" width="1.016" layer="94" style="shortdash"/>
<wire x1="109.22" y1="129.54" x2="2.54" y2="129.54" width="1.016" layer="94" style="shortdash"/>
<wire x1="2.54" y1="129.54" x2="2.54" y2="55.88" width="1.016" layer="94" style="shortdash"/>
<wire x1="2.54" y1="55.88" x2="2.54" y2="5.08" width="1.016" layer="94" style="shortdash"/>
<wire x1="2.54" y1="5.08" x2="83.82" y2="5.08" width="1.016" layer="94" style="shortdash"/>
<wire x1="83.82" y1="5.08" x2="83.82" y2="22.86" width="1.016" layer="94" style="shortdash"/>
<wire x1="83.82" y1="22.86" x2="83.82" y2="55.88" width="1.016" layer="94" style="shortdash"/>
<wire x1="109.22" y1="93.98" x2="180.34" y2="93.98" width="1.016" layer="94" style="shortdash"/>
<wire x1="180.34" y1="93.98" x2="180.34" y2="129.54" width="1.016" layer="94" style="shortdash"/>
<wire x1="180.34" y1="129.54" x2="109.22" y2="129.54" width="1.016" layer="94" style="shortdash"/>
<wire x1="111.76" y1="55.88" x2="127" y2="55.88" width="1.016" layer="94" style="shortdash"/>
<wire x1="127" y1="55.88" x2="180.34" y2="55.88" width="1.016" layer="94" style="shortdash"/>
<wire x1="180.34" y1="55.88" x2="180.34" y2="93.98" width="1.016" layer="94" style="shortdash"/>
<wire x1="83.82" y1="22.86" x2="127" y2="22.86" width="1.016" layer="94" style="shortdash"/>
<wire x1="127" y1="22.86" x2="127" y2="55.88" width="1.016" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="GND10" gate="G$1" x="58.42" y="71.12"/>
<instance part="U$14" gate="G$1" x="30.48" y="81.28"/>
<instance part="U$1" gate="G$1" x="35.56" y="30.48"/>
<instance part="C1" gate="G$1" x="17.78" y="22.86"/>
<instance part="C2" gate="G$1" x="53.34" y="22.86"/>
<instance part="STM8L" gate="G$1" x="60.96" y="104.14"/>
<instance part="U$3" gate="G$1" x="15.24" y="111.76" smashed="yes">
<attribute name="VALUE" x="12.7" y="111.76" size="1.016" layer="96" rot="R90"/>
</instance>
<instance part="GND1" gate="G$1" x="15.24" y="76.2" smashed="yes"/>
<instance part="R3" gate="G$1" x="25.4" y="109.22"/>
<instance part="C3" gate="G$1" x="5.08" y="99.06"/>
<instance part="U$4" gate="G$1" x="17.78" y="35.56" rot="MR0"/>
<instance part="U$6" gate="G$1" x="53.34" y="35.56" smashed="yes">
<attribute name="VALUE" x="50.8" y="35.56" size="1.016" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="G$1" x="35.56" y="15.24"/>
<instance part="VDD" gate="G$1" x="30.48" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="31.75" y="67.31" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="SWDIO" gate="G$1" x="45.72" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="46.99" y="67.31" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND" gate="G$1" x="53.34" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="54.61" y="67.31" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="NRST" gate="G$1" x="68.58" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="69.85" y="67.31" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="JP1" gate="A" x="101.6" y="35.56"/>
<instance part="U$2" gate="G$1" x="134.62" y="73.66"/>
<instance part="GND4" gate="G$1" x="154.94" y="60.96"/>
<instance part="U1" gate="U$1" x="147.32" y="109.22"/>
<instance part="C4" gate="G$1" x="127" y="106.68"/>
<instance part="GND5" gate="G$1" x="134.62" y="99.06"/>
<instance part="C5" gate="G$1" x="170.18" y="71.12"/>
<instance part="U$7" gate="G$1" x="91.44" y="45.72" rot="MR0"/>
<instance part="GND2" gate="G$1" x="121.92" y="35.56"/>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FID1" gate="G$1" x="137.16" y="48.26"/>
<instance part="FID2" gate="G$1" x="149.86" y="48.26"/>
<instance part="FID3" gate="G$1" x="137.16" y="35.56"/>
<instance part="FID4" gate="G$1" x="149.86" y="35.56"/>
<instance part="LED" gate="G$1" x="99.06" y="93.98" rot="R270"/>
<instance part="R1" gate="G$1" x="88.9" y="93.98"/>
<instance part="U$5" gate="G$1" x="104.14" y="101.6" smashed="yes">
<attribute name="VALUE" x="101.6" y="101.6" size="1.016" layer="96" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="96.52" y="99.06"/>
<instance part="W" gate="G$1" x="86.36" y="76.2" rot="R270"/>
<instance part="JP3" gate="G$1" x="93.98" y="76.2" rot="R270"/>
<instance part="GND6" gate="G$1" x="99.06" y="78.74"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="53.34" y1="71.12" x2="58.42" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND10" gate="G$1" pin="GND"/>
<pinref part="GND" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="VSS/VREF-/VSSA"/>
<wire x1="40.64" y1="99.06" x2="15.24" y2="99.06" width="0.1524" layer="91"/>
<wire x1="15.24" y1="99.06" x2="15.24" y2="96.52" width="0.1524" layer="91"/>
<pinref part="GND1" gate="G$1" pin="GND"/>
<wire x1="15.24" y1="96.52" x2="15.24" y2="76.2" width="0.1524" layer="91"/>
<junction x="15.24" y="96.52"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="15.24" y1="96.52" x2="5.08" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="17.78" y1="20.32" x2="17.78" y2="15.24" width="0.1524" layer="91"/>
<wire x1="17.78" y1="15.24" x2="35.56" y2="15.24" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="35.56" y1="15.24" x2="35.56" y2="22.86" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="35.56" y1="15.24" x2="53.34" y2="15.24" width="0.1524" layer="91"/>
<wire x1="53.34" y1="15.24" x2="53.34" y2="20.32" width="0.1524" layer="91"/>
<junction x="35.56" y="15.24"/>
<pinref part="GND3" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="149.86" y1="68.58" x2="154.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="154.94" y1="68.58" x2="154.94" y2="60.96" width="0.1524" layer="91"/>
<pinref part="GND4" gate="G$1" pin="GND"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="154.94" y1="68.58" x2="170.18" y2="68.58" width="0.1524" layer="91"/>
<junction x="154.94" y="68.58"/>
</segment>
<segment>
<pinref part="U1" gate="U$1" pin="GND@2"/>
<wire x1="137.16" y1="109.22" x2="134.62" y2="109.22" width="0.1524" layer="91"/>
<wire x1="134.62" y1="109.22" x2="134.62" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U1" gate="U$1" pin="GND@7"/>
<wire x1="134.62" y1="106.68" x2="134.62" y2="99.06" width="0.1524" layer="91"/>
<wire x1="137.16" y1="106.68" x2="134.62" y2="106.68" width="0.1524" layer="91"/>
<junction x="134.62" y="106.68"/>
<pinref part="GND5" gate="G$1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="127" y1="104.14" x2="127" y2="99.06" width="0.1524" layer="91"/>
<wire x1="127" y1="99.06" x2="134.62" y2="99.06" width="0.1524" layer="91"/>
<junction x="134.62" y="99.06"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<pinref part="GND2" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="38.1" x2="121.92" y2="38.1" width="0.1524" layer="91"/>
<wire x1="121.92" y1="38.1" x2="121.92" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="1"/>
<pinref part="GND6" gate="G$1" pin="GND"/>
<wire x1="93.98" y1="78.74" x2="99.06" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="VI"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="25.4" y1="30.48" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<wire x1="17.78" y1="30.48" x2="17.78" y2="27.94" width="0.1524" layer="91"/>
<wire x1="17.78" y1="30.48" x2="17.78" y2="35.56" width="0.1524" layer="91"/>
<junction x="17.78" y="30.48"/>
<pinref part="U$4" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<pinref part="U$7" gate="G$1" pin="VCC"/>
<wire x1="99.06" y1="38.1" x2="91.44" y2="38.1" width="0.1524" layer="91"/>
<wire x1="91.44" y1="38.1" x2="91.44" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<wire x1="30.48" y1="71.12" x2="30.48" y2="81.28" width="0.1524" layer="91"/>
<pinref part="U$14" gate="G$1" pin="VDD"/>
<pinref part="VDD" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="VDD/VREF+/VDDA"/>
<wire x1="40.64" y1="101.6" x2="15.24" y2="101.6" width="0.1524" layer="91"/>
<wire x1="15.24" y1="101.6" x2="15.24" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="VDD"/>
<wire x1="15.24" y1="104.14" x2="15.24" y2="109.22" width="0.1524" layer="91"/>
<wire x1="15.24" y1="109.22" x2="15.24" y2="111.76" width="0.1524" layer="91"/>
<junction x="15.24" y="104.14"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="20.32" y1="109.22" x2="15.24" y2="109.22" width="0.1524" layer="91"/>
<junction x="15.24" y="109.22"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="15.24" y1="104.14" x2="5.08" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VO"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="45.72" y1="30.48" x2="53.34" y2="30.48" width="0.1524" layer="91"/>
<wire x1="53.34" y1="30.48" x2="53.34" y2="27.94" width="0.1524" layer="91"/>
<wire x1="53.34" y1="30.48" x2="53.34" y2="35.56" width="0.1524" layer="91"/>
<junction x="53.34" y="30.48"/>
<pinref part="U$6" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="VDD"/>
<pinref part="LED" gate="G$1" pin="A"/>
<wire x1="104.14" y1="101.6" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="104.14" y1="99.06" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<wire x1="101.6" y1="99.06" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<junction x="104.14" y="99.06"/>
</segment>
</net>
<net name="NRST" class="0">
<segment>
<wire x1="68.58" y1="71.12" x2="68.58" y2="81.28" width="0.1524" layer="91"/>
<label x="68.58" y="73.66" size="1.778" layer="95" rot="R90"/>
<pinref part="NRST" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PA1/NRST"/>
<wire x1="40.64" y1="109.22" x2="30.48" y2="109.22" width="0.1524" layer="91"/>
<label x="30.48" y="109.22" size="1.778" layer="95"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="SWIM" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PA0"/>
<wire x1="40.64" y1="111.76" x2="30.48" y2="111.76" width="0.1524" layer="91"/>
<label x="30.48" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="45.72" y1="81.28" x2="45.72" y2="71.12" width="0.1524" layer="91"/>
<label x="45.72" y="71.12" size="1.778" layer="95" rot="R90"/>
<pinref part="SWDIO" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PC0"/>
<wire x1="81.28" y1="111.76" x2="93.98" y2="111.76" width="0.1524" layer="91"/>
<label x="86.36" y="111.76" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="U$1" pin="DATA"/>
<wire x1="157.48" y1="111.76" x2="165.1" y2="111.76" width="0.1524" layer="91"/>
<label x="157.48" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PC1"/>
<wire x1="81.28" y1="114.3" x2="99.06" y2="114.3" width="0.1524" layer="91"/>
<label x="86.36" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="U$1" pin="SCK"/>
<wire x1="157.48" y1="109.22" x2="165.1" y2="109.22" width="0.1524" layer="91"/>
<label x="157.48" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="B"/>
<wire x1="149.86" y1="73.66" x2="165.1" y2="73.66" width="0.1524" layer="91"/>
<label x="154.94" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="6"/>
<wire x1="106.68" y1="33.02" x2="116.84" y2="33.02" width="0.1524" layer="91"/>
<label x="111.76" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="A" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="A"/>
<wire x1="149.86" y1="71.12" x2="165.1" y2="71.12" width="0.1524" layer="91"/>
<label x="154.94" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="99.06" y1="33.02" x2="88.9" y2="33.02" width="0.1524" layer="91"/>
<label x="88.9" y="33.02" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="5"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PA2"/>
<wire x1="40.64" y1="106.68" x2="30.48" y2="106.68" width="0.1524" layer="91"/>
<label x="30.48" y="106.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="DI"/>
<wire x1="124.46" y1="68.58" x2="111.76" y2="68.58" width="0.1524" layer="91"/>
<label x="114.3" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PA3"/>
<wire x1="40.64" y1="104.14" x2="30.48" y2="104.14" width="0.1524" layer="91"/>
<label x="30.48" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="RO"/>
<wire x1="124.46" y1="76.2" x2="111.76" y2="76.2" width="0.1524" layer="91"/>
<label x="114.3" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="485_EN" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="/RE"/>
<wire x1="124.46" y1="73.66" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<label x="111.76" y="73.66" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="DE"/>
<wire x1="119.38" y1="73.66" x2="111.76" y2="73.66" width="0.1524" layer="91"/>
<wire x1="124.46" y1="71.12" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
<wire x1="119.38" y1="71.12" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<junction x="119.38" y="73.66"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PC5"/>
<wire x1="40.64" y1="116.84" x2="30.48" y2="116.84" width="0.1524" layer="91"/>
<label x="30.48" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="485_PWR" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<wire x1="149.86" y1="76.2" x2="154.94" y2="76.2" width="0.1524" layer="91"/>
<wire x1="154.94" y1="76.2" x2="154.94" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="154.94" y1="76.2" x2="170.18" y2="76.2" width="0.1524" layer="91"/>
<junction x="154.94" y="76.2"/>
<label x="154.94" y="86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PC6"/>
<wire x1="40.64" y1="114.3" x2="30.48" y2="114.3" width="0.1524" layer="91"/>
<label x="30.48" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="HTU_PWR" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="VDD"/>
<wire x1="137.16" y1="111.76" x2="132.08" y2="111.76" width="0.1524" layer="91"/>
<wire x1="132.08" y1="111.76" x2="132.08" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="127" y1="111.76" x2="132.08" y2="111.76" width="0.1524" layer="91"/>
<junction x="132.08" y="111.76"/>
<label x="132.08" y="121.92" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PC4"/>
<wire x1="81.28" y1="116.84" x2="93.98" y2="116.84" width="0.1524" layer="91"/>
<label x="93.98" y="116.84" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="IO1" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PD0"/>
<wire x1="40.64" y1="96.52" x2="30.48" y2="96.52" width="0.1524" layer="91"/>
<label x="33.02" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="99.06" y1="35.56" x2="88.9" y2="35.56" width="0.1524" layer="91"/>
<label x="88.9" y="35.56" size="1.778" layer="95"/>
<pinref part="JP1" gate="A" pin="3"/>
</segment>
</net>
<net name="IO2" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PB0"/>
<wire x1="40.64" y1="93.98" x2="30.48" y2="93.98" width="0.1524" layer="91"/>
<label x="33.02" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="106.68" y1="35.56" x2="116.84" y2="35.56" width="0.1524" layer="91"/>
<label x="111.76" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="LED" gate="G$1" pin="C"/>
<wire x1="96.52" y1="93.98" x2="93.98" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PB1"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="83.82" y1="93.98" x2="81.28" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IO3" class="0">
<segment>
<pinref part="W" gate="G$1" pin="1"/>
<wire x1="86.36" y1="78.74" x2="86.36" y2="86.36" width="0.1524" layer="91"/>
<label x="86.36" y="83.82" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PB3"/>
<wire x1="81.28" y1="99.06" x2="91.44" y2="99.06" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<label x="86.36" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
