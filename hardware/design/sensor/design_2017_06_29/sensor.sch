<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.4.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="14" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="120" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BaseApp">
<packages>
<package name="C0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1.905" y="0.835" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
</package>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.508" x2="1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.508" x2="-1.27" y2="0.508" width="0.127" layer="21"/>
</package>
<package name="FILM-CAPACITOR-0.47U/400V">
<wire x1="-9.25" y1="3.9" x2="9.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="3.9" x2="9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="-3.9" x2="-9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-9.25" y1="-3.9" x2="-9.25" y2="3.9" width="0.127" layer="21"/>
<pad name="P$1" x="-7.5" y="0" drill="0.9"/>
<pad name="P$2" x="7.5" y="0" drill="0.9"/>
<text x="-2.54" y="4.17" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.17" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="51"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="51"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="-2.4" y1="1" x2="2.4" y2="1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1" x2="-2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-2.4" y2="1" width="0.127" layer="21"/>
</package>
<package name="C1210">
<smd name="P$1" x="-1.35" y="0" dx="1" dy="3" layer="1"/>
<smd name="P$2" x="1.35" y="0" dx="1" dy="3" layer="1"/>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.127" layer="21"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<text x="-1.5" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="20TSSOP">
<wire x1="-3.28" y1="2.2" x2="3.28" y2="2.2" width="0.2032" layer="51"/>
<wire x1="3.28" y1="2.2" x2="3.28" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="3.28" y1="-2.2" x2="-3.28" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3.28" y1="-2.2" x2="-3.28" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.28" y1="-2" x2="-3.28" y2="2" width="0.2032" layer="21"/>
<wire x1="3.28" y1="2" x2="3.28" y2="-2" width="0.2032" layer="21"/>
<circle x="-2.536" y="-1.378" radius="0.2231" width="0.2032" layer="21"/>
<smd name="20" x="-2.955" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="19" x="-2.305" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="18" x="-1.655" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="16" x="-0.355" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="17" x="-1.005" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="15" x="0.295" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="14" x="0.945" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="13" x="1.595" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="1" x="-2.955" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="2" x="-2.305" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="3" x="-1.655" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="4" x="-1.005" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="5" x="-0.355" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="6" x="0.295" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="7" x="0.945" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="8" x="1.595" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<text x="-3.55" y="-2.01" size="0.4064" layer="25" rot="R90">&gt;NAME</text>
<text x="2.68" y="-2.01" size="0.4064" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.82" y1="-3.2" x2="1.07" y2="-2.2" layer="51"/>
<rectangle x1="1.47" y1="-3.2" x2="1.72" y2="-2.2" layer="51"/>
<rectangle x1="0.17" y1="-3.2" x2="0.42" y2="-2.2" layer="51"/>
<rectangle x1="-0.48" y1="-3.2" x2="-0.23" y2="-2.2" layer="51"/>
<rectangle x1="-1.13" y1="-3.2" x2="-0.88" y2="-2.2" layer="51"/>
<rectangle x1="-1.78" y1="-3.2" x2="-1.53" y2="-2.2" layer="51"/>
<rectangle x1="-2.43" y1="-3.2" x2="-2.18" y2="-2.2" layer="51"/>
<rectangle x1="-3.08" y1="-3.2" x2="-2.83" y2="-2.2" layer="51"/>
<rectangle x1="0.82" y1="2.2" x2="1.07" y2="3.2" layer="51"/>
<rectangle x1="1.47" y1="2.2" x2="1.72" y2="3.2" layer="51"/>
<rectangle x1="0.17" y1="2.2" x2="0.42" y2="3.2" layer="51"/>
<rectangle x1="-0.48" y1="2.2" x2="-0.23" y2="3.2" layer="51"/>
<rectangle x1="-1.13" y1="2.2" x2="-0.88" y2="3.2" layer="51"/>
<rectangle x1="-1.78" y1="2.2" x2="-1.53" y2="3.2" layer="51"/>
<rectangle x1="-2.43" y1="2.2" x2="-2.18" y2="3.2" layer="51"/>
<rectangle x1="-3.08" y1="2.2" x2="-2.83" y2="3.2" layer="51"/>
<smd name="9" x="2.245" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="10" x="2.895" y="-3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="11" x="2.895" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<smd name="12" x="2.245" y="3.05" dx="0.4" dy="1.65" layer="1"/>
<rectangle x1="2.12" y1="-3.2" x2="2.37" y2="-2.2" layer="51"/>
<rectangle x1="2.77" y1="-3.2" x2="3.02" y2="-2.2" layer="51"/>
<rectangle x1="2.12" y1="2.2" x2="2.37" y2="3.2" layer="51"/>
<rectangle x1="2.77" y1="2.2" x2="3.02" y2="3.2" layer="51"/>
</package>
<package name="R0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-2.032" y="0.635" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.032" y="-2.159" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.63" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.63" width="0.127" layer="21"/>
</package>
<package name="R-0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.3" y1="0.473" x2="-1.3" y2="-0.473" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-0.473" x2="1.2" y2="-0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.473" x2="1.2" y2="0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.473" x2="-1.3" y2="0.473" width="0.127" layer="21"/>
</package>
<package name="10555-METRIC">
<smd name="P$1" x="-4.85" y="0" dx="3.3" dy="2" layer="1"/>
<smd name="P$2" x="4.35" y="0" dx="3.3" dy="2" layer="1"/>
<wire x1="-6.75" y1="3" x2="-6.75" y2="-3" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-3" x2="6.25" y2="-3" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3" x2="6.25" y2="3" width="0.127" layer="21"/>
<wire x1="6.25" y1="3" x2="-6.75" y2="3" width="0.127" layer="21"/>
<text x="-2.75" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805-METRIC">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="R0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="-0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="-2" y2="0.75" width="0.127" layer="21"/>
</package>
<package name="R2512">
<smd name="P$1" x="0" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<smd name="P$2" x="6.5024" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<wire x1="-1.27" y1="1.9304" x2="-1.27" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.9304" x2="7.7724" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="-1.9304" x2="7.7724" y2="1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="1.9304" x2="-1.27" y2="1.9304" width="0.127" layer="21"/>
<text x="1.016" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0.508" y="-3.556" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="5.334" y1="1.524" x2="1.016" y2="1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="-1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="-1.524" x2="5.334" y2="-1.524" width="0.127" layer="51"/>
<wire x1="5.334" y1="-1.524" x2="5.334" y2="1.524" width="0.127" layer="51"/>
</package>
<package name="MELF0204">
<smd name="P$1" x="-0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.8" x2="1.2" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.8" x2="1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.8" x2="-1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.8" x2="-1.2" y2="0.8" width="0.127" layer="21"/>
<text x="-0.8" y="1.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.8" y="-2.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="PINHEAD-1X4-2MM">
<wire x1="-4.27" y1="1.27" x2="4.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="4.27" y1="-1.27" x2="-4.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-4.27" y1="-1.27" x2="-4.27" y2="1.27" width="0.127" layer="21"/>
<pad name="1" x="-3" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="2" x="-1" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="-3.254" y1="-0.254" x2="-2.746" y2="0.254" layer="51"/>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<wire x1="4.27" y1="1.27" x2="4.27" y2="-1.27" width="0.127" layer="21"/>
<text x="-3.73" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<pad name="3" x="1" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<pad name="4" x="3" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="2.746" y1="-0.254" x2="3.254" y2="0.254" layer="51"/>
</package>
<package name="PINHEAD-1X4-2.54MM">
<wire x1="-5.08" y1="1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="-5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.127" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-5.1562" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
<package name="PINHEAD-1X4-2MM-DIRECTIONAL">
<wire x1="-5" y1="3.43" x2="5" y2="3.43" width="0.127" layer="21"/>
<pad name="1" x="-3" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="2" x="-1" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="-3.254" y1="-0.254" x2="-2.746" y2="0.254" layer="51"/>
<rectangle x1="-1.254" y1="-0.254" x2="-0.746" y2="0.254" layer="51"/>
<wire x1="-5" y1="-2" x2="-5" y2="3.43" width="0.127" layer="21"/>
<wire x1="5" y1="3.43" x2="5" y2="-1" width="0.127" layer="21"/>
<text x="-3.73" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<pad name="3" x="1" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="0.746" y1="-0.254" x2="1.254" y2="0.254" layer="51"/>
<pad name="4" x="3" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="2.746" y1="-0.254" x2="3.254" y2="0.254" layer="51"/>
<wire x1="5" y1="-1" x2="5" y2="-1.2" width="0.127" layer="21"/>
<wire x1="-5" y1="-2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="-2" y2="-3" width="0.127" layer="21"/>
<wire x1="-2" y1="-3" x2="2" y2="-3" width="0.127" layer="21"/>
<wire x1="2" y1="-3" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="5" y2="-2" width="0.127" layer="21"/>
<wire x1="5" y1="-2" x2="5" y2="-1.2" width="0.127" layer="21"/>
</package>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
</package>
<package name="DUOLED-C-5MM">
<wire x1="-2.794" y1="0.8636" x2="-2.794" y2="-0.8636" width="0.254" layer="51" curve="34.351807"/>
<wire x1="2.3876" y1="-0.8636" x2="2.3876" y2="0.8636" width="0.1524" layer="51" curve="39.77033"/>
<wire x1="2.54" y1="1.4478" x2="2.54" y2="0.889" width="0.254" layer="21"/>
<wire x1="-2.3876" y1="-0.8636" x2="2.3876" y2="-0.8636" width="0.1524" layer="21" curve="140.22967"/>
<wire x1="-2.7933" y1="0.8634" x2="2.54" y2="1.4478" width="0.254" layer="21" curve="-133.151599"/>
<wire x1="-2.3876" y1="0.8636" x2="-2.3876" y2="-0.8636" width="0.1524" layer="51" curve="39.77033"/>
<wire x1="-2.794" y1="-0.8636" x2="2.5407" y2="-1.4482" width="0.254" layer="21" curve="133.133633"/>
<wire x1="-2.3876" y1="0.8636" x2="2.3876" y2="0.8636" width="0.1524" layer="21" curve="-140.22967"/>
<wire x1="2.54" y1="0.9398" x2="2.54" y2="-0.889" width="0.254" layer="51"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="-1.4478" width="0.254" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="AX" x="-2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="C" x="0" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="AR" x="2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.81" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="DUOLED-C-3MM">
<wire x1="1.27" y1="1.5748" x2="-1.27" y2="1.5748" width="0.254" layer="21"/>
<wire x1="0" y1="-1.524" x2="-0.9756" y2="-1.1708" width="0.1524" layer="21" curve="-39.806332"/>
<wire x1="0" y1="-1.524" x2="1.0125" y2="-1.1391" width="0.1524" layer="21" curve="41.638926"/>
<wire x1="-0.9918" y1="1.1571" x2="0" y2="1.524" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="0.9756" y1="1.1708" x2="0" y2="1.524" width="0.1524" layer="51" curve="39.806332"/>
<wire x1="-1.524" y1="0" x2="-0.8858" y2="1.2401" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-0.9144" y1="-1.2192" x2="-1.524" y2="0" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="1.524" y1="0" x2="0.9356" y2="1.203" width="0.1524" layer="21" curve="52.130554"/>
<wire x1="0.9356" y1="-1.203" x2="1.524" y2="0" width="0.1524" layer="21" curve="52.130554"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="0" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.016" x2="-1.016" y2="0" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="90"/>
<wire x1="1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.032" y1="0" x2="-1.3009" y2="1.561" width="0.254" layer="51" curve="-50.193108"/>
<wire x1="-0.9562" y1="-1.7929" x2="-2.032" y2="0" width="0.254" layer="51" curve="-61.930333"/>
<wire x1="2.032" y1="0" x2="1.3126" y2="1.5512" width="0.254" layer="51" curve="49.763022"/>
<wire x1="1.0082" y1="-1.7643" x2="2.032" y2="0" width="0.254" layer="51" curve="60.265035"/>
<wire x1="0" y1="-2.032" x2="-0.9634" y2="-1.7891" width="0.254" layer="21" curve="-28.301701"/>
<wire x1="0" y1="-2.032" x2="1.065" y2="-1.7306" width="0.254" layer="21" curve="31.609816"/>
<pad name="AX" x="-2.54" y="0" drill="0.8" diameter="1.4" rot="R90"/>
<pad name="CX" x="0" y="0" drill="0.8" diameter="1.4" rot="R90"/>
<pad name="AX@1" x="2.54" y="0" drill="0.8" diameter="1.4" rot="R90"/>
<text x="3.81" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="HTU21D">
<description>&lt;b&gt;Description:&lt;/b&gt; HTU21D is a very small, low cost, I2C digital humidity and temperature sensor.</description>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="51"/>
<wire x1="1.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.2032" layer="21"/>
<smd name="NC@6" x="1.5" y="1" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="GND" x="1.5" y="0" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="DATA" x="1.5" y="-1" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="SCK" x="-1.5" y="-1" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="VDD" x="-1.5" y="0" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="NC@1" x="-1.5" y="1" dx="0.5" dy="0.8" layer="1" rot="R270"/>
<smd name="7" x="0" y="0" dx="0.2" dy="0.2" layer="1"/>
<polygon width="0.127" layer="1">
<vertex x="-0.7" y="1.25"/>
<vertex x="0.75" y="1.25"/>
<vertex x="0.75" y="-0.8"/>
<vertex x="0.3" y="-1.25"/>
<vertex x="-0.7" y="-1.25"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-0.55" y="1.05"/>
<vertex x="0.55" y="1.05"/>
<vertex x="0.55" y="-0.65"/>
<vertex x="0.1" y="-1.1"/>
<vertex x="-0.55" y="-1.1"/>
</polygon>
<wire x1="-0.7" y1="-1.5" x2="0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="1.2" x2="0.5" y2="1.2" width="0.127" layer="51"/>
<wire x1="0.5" y1="1.2" x2="0.5" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.5" y1="0.5" x2="-0.5" y2="0.5" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0.5" x2="-0.5" y2="1.2" width="0.127" layer="51"/>
<polygon width="0.127" layer="29">
<vertex x="-0.775" y="1.325"/>
<vertex x="-0.775" y="-1.3"/>
<vertex x="0.375" y="-1.3"/>
<vertex x="0.825" y="-0.85"/>
<vertex x="0.825" y="1.325"/>
</polygon>
<text x="-1.075" y="1.775" size="0.5" layer="25">&gt;Name</text>
<text x="-1.325" y="-2.2" size="0.5" layer="27">&gt;Value</text>
<wire x1="-0.508" y1="0.508" x2="0.508" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.508" x2="0.508" y2="1.1938" width="0.127" layer="21"/>
<wire x1="0.508" y1="1.1938" x2="-0.508" y2="1.1938" width="0.127" layer="21"/>
<wire x1="-0.508" y1="1.1938" x2="-0.508" y2="0.508" width="0.127" layer="21"/>
<circle x="1.778" y="-1.7272" radius="0.160640625" width="0.127" layer="21"/>
</package>
<package name="TACTILE">
<smd name="1" x="2.24" y="4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="2.24" y="-4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="4" x="-2.24" y="-4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="-2.24" y="4.55" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<text x="3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="TACTILE-PTH">
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="-2.54" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="TACTILE-SMD-4.5X4.5">
<smd name="1" x="1.5" y="3.28" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="1.5" y="-3.28" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="4" x="-1.5" y="-3.28" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="-1.5" y="3.28" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.127" layer="21"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.127" layer="21"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.127" layer="21"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<text x="3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="TAC-SMD-12X12">
<smd name="1" x="2.5" y="6.75" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="2" x="2.5" y="-6.75" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="4" x="-2.5" y="-6.75" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<smd name="3" x="-2.5" y="6.75" dx="2.1" dy="1.4" layer="1" rot="R90"/>
<wire x1="-6" y1="6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-6" y2="6" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<text x="3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="TACTILE-SMD-6X6">
<smd name="1" x="2.3" y="4.35" dx="2.1" dy="1" layer="1" rot="R90"/>
<smd name="2" x="2.3" y="-4.35" dx="2.1" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-2.3" y="-4.35" dx="2.1" dy="1" layer="1" rot="R90"/>
<smd name="3" x="-2.3" y="4.35" dx="2.1" dy="1" layer="1" rot="R90"/>
<wire x1="-3" y1="3" x2="3" y2="3" width="0.127" layer="21"/>
<wire x1="3" y1="3" x2="3" y2="-3" width="0.127" layer="21"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.127" layer="21"/>
<wire x1="-3" y1="-3" x2="-3" y2="3" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<text x="3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="PINHEAD-1X3">
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="-2.804" y1="-0.254" x2="-2.296" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-2.73" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.4224" shape="octagon" rot="R90"/>
<rectangle x1="2.296" y1="-0.254" x2="2.804" y2="0.254" layer="51"/>
<wire x1="-3.81" y1="1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
</package>
<package name="R2616">
<smd name="P$1" x="0" y="0" dx="3" dy="3" layer="1" rot="R90"/>
<smd name="P$2" x="5.85" y="0" dx="3" dy="3" layer="1" rot="R90"/>
<wire x1="-1.5" y1="2" x2="7.5" y2="2" width="0.127" layer="21"/>
<wire x1="7.5" y1="2" x2="7.5" y2="-2" width="0.127" layer="21"/>
<wire x1="7.5" y1="-2" x2="-1.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="2" width="0.127" layer="21"/>
</package>
<package name="SOT23-W">
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.3984" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.3984" width="0.1524" layer="21"/>
<wire x1="0.2954" y1="-0.6604" x2="-0.3094" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.3" dx="2.8" dy="1.4" layer="1"/>
<smd name="2" x="1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<smd name="1" x="-1.1" y="-1.3" dx="1.2" dy="1.4" layer="1"/>
<text x="2.032" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="2.032" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
<package name="POWERPACK1212">
<smd name="S3" x="-1.4224" y="-0.3302" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="S2" x="-1.4224" y="0.3302" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="S1" x="-1.4224" y="0.9906" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="G" x="-1.4224" y="-0.9906" dx="0.9906" dy="0.4064" layer="1"/>
<smd name="D2" x="1.5494" y="0.3302" dx="0.762" dy="0.4064" layer="1"/>
<smd name="D1" x="1.5494" y="0.9906" dx="0.762" dy="0.4064" layer="1"/>
<smd name="D3" x="1.5494" y="-0.3302" dx="0.762" dy="0.4064" layer="1"/>
<smd name="D4" x="1.5494" y="-0.9906" dx="0.762" dy="0.4064" layer="1"/>
<smd name="DPAD" x="0.5842" y="0" dx="1.7272" dy="2.2352" layer="1"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="1.4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.5" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.35" width="0.127" layer="21"/>
<text x="-1.143" y="0.508" size="0.4064" layer="27">&gt;VALUE</text>
<text x="-1.143" y="-0.381" size="0.4064" layer="25">&gt;NAME</text>
</package>
<package name="SOT23-R">
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.5724" y1="-0.6604" x2="-1.5724" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6604" x2="-1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.5724" y1="-0.6524" x2="-1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.5724" y1="0.6604" x2="-0.5136" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.5724" y1="0.6604" x2="1.5724" y2="-0.6524" width="0.1524" layer="21"/>
<wire x1="0.5636" y1="0.6604" x2="1.5724" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="0.4224" y1="-0.6604" x2="-0.4364" y2="-0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="1.778" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="1.778" y="-0.508" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<pin name="GND" x="0" y="0" visible="pad" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="CAPACITOR">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
</symbol>
<symbol name="STM8L051F3P6">
<pin name="PC5" x="-20.32" y="12.7" length="middle"/>
<pin name="PC6" x="-20.32" y="10.16" length="middle"/>
<pin name="PA0" x="-20.32" y="7.62" length="middle"/>
<pin name="PA1/NRST" x="-20.32" y="5.08" length="middle"/>
<pin name="PA2" x="-20.32" y="2.54" length="middle"/>
<pin name="PA3" x="-20.32" y="0" length="middle"/>
<pin name="VSS/VREF-/VSSA" x="-20.32" y="-5.08" length="middle"/>
<pin name="VDD/VREF+/VDDA" x="-20.32" y="-2.54" length="middle"/>
<pin name="PD0" x="-20.32" y="-7.62" length="middle"/>
<pin name="PB0" x="-20.32" y="-10.16" length="middle"/>
<pin name="PC4" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="PC1" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="PC0" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="PB7" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="PB6" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="PB5" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="PB4" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="PB3" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="PB2" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="PB1" x="20.32" y="-10.16" length="middle" rot="R180"/>
<wire x1="-15.24" y1="15.24" x2="-15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-12.7" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<text x="-2.54" y="15.24" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="VDD">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.635" x2="0" y2="3.81" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VDD" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="RESISTOR">
<text x="-2.54" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-2.54" y="-3.81" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="PINHEAD-1X4">
<wire x1="1.27" y1="-7.62" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<wire x1="1.27" y1="-7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="-10.16" size="1.27" layer="96">&gt;VALUE</text>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="DUOLED-GY-C">
<wire x1="1.27" y1="5.08" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="5.08" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-1.27" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="4.318" x2="-3.429" y2="2.921" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="3.175" x2="-3.302" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.032" y1="-4.318" x2="3.429" y2="-2.921" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-3.175" x2="3.302" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.254" width="0.4064" layer="94"/>
<text x="6.35" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="8.89" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="2.54" y="3.81" size="1.016" layer="94" ratio="10" rot="R90">green</text>
<text x="-2.54" y="-5.08" size="1.016" layer="94" ratio="10" rot="R90">yellow</text>
<pin name="C" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="AG" x="0" y="7.62" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="AY" x="0" y="-7.62" visible="off" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="2.921"/>
<vertex x="-3.048" y="3.81"/>
<vertex x="-2.54" y="3.302"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="1.778"/>
<vertex x="-2.921" y="2.667"/>
<vertex x="-2.413" y="2.159"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="2.413" y="-2.159"/>
<vertex x="3.302" y="-1.778"/>
<vertex x="2.921" y="-2.667"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="-3.302"/>
<vertex x="3.429" y="-2.921"/>
<vertex x="3.048" y="-3.81"/>
</polygon>
</symbol>
<symbol name="HTU21D">
<description>&lt;b&gt;Description:&lt;/b&gt; The HTU21D is a very small, low cost, I2D digitally controlled humidity and temperature sensor.</description>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<text x="-7.62" y="5.588" size="1.778" layer="95">&gt;Name</text>
<text x="-7.62" y="-7.62" size="1.778" layer="95">&gt;Value</text>
<pin name="SCK" x="10.16" y="0" visible="pin" length="short" direction="in" rot="R180"/>
<pin name="DATA" x="10.16" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="GND@2" x="-10.16" y="0" visible="pin" length="short" direction="pwr"/>
<pin name="VDD" x="-10.16" y="2.54" visible="pin" length="short" direction="pwr"/>
<pin name="GND@7" x="-10.16" y="-2.54" visible="pin" length="short" direction="pwr"/>
</symbol>
<symbol name="TACTILE">
<wire x1="-2.54" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="2.54" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="0.635" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="0" y2="0.635" width="0.254" layer="94"/>
<pin name="1" x="-7.62" y="0" visible="pin" length="short"/>
<pin name="2" x="-7.62" y="-2.54" visible="pin" length="short"/>
<pin name="3" x="7.62" y="0" visible="pin" length="short" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" visible="pin" length="short" rot="R180"/>
<wire x1="0" y1="0.635" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<text x="7.62" y="3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="7.62" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PINHEAD-1X3">
<wire x1="1.27" y1="-5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<wire x1="1.27" y1="-5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="MOSFET-PCHANNEL">
<wire x1="-3.6576" y1="4.953" x2="-3.6576" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="4.445" x2="-2.0066" y2="4.445" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-4.445" x2="0" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="4.445" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="2.54" y2="4.445" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-4.445" x2="0" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="0" y1="-4.445" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-4.445" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="4.445" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="3.175" y2="0.635" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0.635" x2="1.905" y2="0.635" width="0.1524" layer="94"/>
<wire x1="1.905" y1="0.635" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.762" x2="2.54" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-0.762" x2="1.905" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="1.905" y1="-0.762" x2="1.651" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-0.762" x2="3.429" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="0" x2="-2.032" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.143" y1="-0.254" x2="-0.254" y2="0" width="0.3048" layer="94"/>
<wire x1="-0.254" y1="0" x2="-1.143" y2="0.254" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0" width="0.3048" layer="94"/>
<wire x1="-1.143" y1="0" x2="-0.889" y2="0" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-5.08" x2="-5.08" y2="-5.08" width="0.1524" layer="94"/>
<circle x="0" y="-4.445" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="4.445" radius="0.127" width="0.4064" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.08" size="0.8128" layer="93">D</text>
<text x="-1.27" y="-6.096" size="0.8128" layer="93">S</text>
<text x="-5.08" y="-3.81" size="0.8128" layer="93">G</text>
<rectangle x1="-2.794" y1="-5.08" x2="-2.032" y2="-3.81" layer="94"/>
<rectangle x1="-2.794" y1="3.81" x2="-2.032" y2="5.08" layer="94"/>
<rectangle x1="-2.794" y1="-0.889" x2="-2.032" y2="0.889" layer="94"/>
<pin name="G" x="-7.62" y="-5.08" visible="pin" length="short" direction="pas"/>
<pin name="D" x="0" y="7.62" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="S" x="0" y="-7.62" visible="pin" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>CAPACITOR</description>
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0.1UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1095-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FILM_AC-CAP" package="FILM-CAPACITOR-0.47U/400V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1275-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1079-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.23"/>
</technology>
</technologies>
</device>
<device name="-33PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1055-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-18PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1052-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-DNP/NI" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2087-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3022-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-3386-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.11"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1821-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.12"/>
</technology>
</technologies>
</device>
<device name="-0.01UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1132-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4700PF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2062-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1524-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3012-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.16" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/25V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3335-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7NF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9092-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-18PF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="77-VJ0805A180GXQCBC" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.06" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/10V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1356-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.24" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-9730-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1024-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/25V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1353-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3303-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1532-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22PF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9031-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-5043-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10NF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1926-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1993-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.47" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V." package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-4112-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V-0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3340-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1494-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2200PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1085-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1281-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.022UF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1063-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1296-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/50V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1909-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0367" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/10V-0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1863-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.031" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/16V" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-5514-2-ND " constant="no"/>
<attribute name="MANUFACTURER" value="C1210C476M4PACTU" constant="no"/>
<attribute name="MOUSER" value="80-C1210C476M4P" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.54" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.01" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2143-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.017" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5.6PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-7944-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0193" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12PF" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1254-1-ND  " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM8L051F3" prefix="STM8L">
<gates>
<gate name="G$1" symbol="STM8L051F3P6" x="0" y="0"/>
</gates>
<devices>
<device name="" package="20TSSOP">
<connects>
<connect gate="G$1" pin="PA0" pad="3"/>
<connect gate="G$1" pin="PA1/NRST" pad="4"/>
<connect gate="G$1" pin="PA2" pad="5"/>
<connect gate="G$1" pin="PA3" pad="6"/>
<connect gate="G$1" pin="PB0" pad="10"/>
<connect gate="G$1" pin="PB1" pad="11"/>
<connect gate="G$1" pin="PB2" pad="12"/>
<connect gate="G$1" pin="PB3" pad="13"/>
<connect gate="G$1" pin="PB4" pad="14"/>
<connect gate="G$1" pin="PB5" pad="15"/>
<connect gate="G$1" pin="PB6" pad="16"/>
<connect gate="G$1" pin="PB7" pad="17"/>
<connect gate="G$1" pin="PC0" pad="18"/>
<connect gate="G$1" pin="PC1" pad="19"/>
<connect gate="G$1" pin="PC4" pad="20"/>
<connect gate="G$1" pin="PC5" pad="1"/>
<connect gate="G$1" pin="PC6" pad="2"/>
<connect gate="G$1" pin="PD0" pad="9"/>
<connect gate="G$1" pin="VDD/VREF+/VDDA" pad="8"/>
<connect gate="G$1" pin="VSS/VREF-/VSSA" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="497-13042-5-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.32" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VDD">
<description>&lt;b&gt;SUPPLY SYMBOL</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0" addlevel="request"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;B&gt;Resistance&lt;/B&gt; &lt;p&gt; 
Mount :-Surface mount</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-50R_1/8W_RF" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-49.9HRCT-ND  " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-50R_1/20W_RF" package="R-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="FC0402-50BWCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.52"/>
</technology>
</technologies>
</device>
<device name="-POWER_RESISTOR" package="10555-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-100K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-100K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-1M_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00M-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-12K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-12.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-330R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF0603JT330RCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-30R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P30.0HCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0R" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P0.0GCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-4.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-24K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM24KCFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-560R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-560GRDKR-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.7K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-2.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-33R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM33.0CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-6.2K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM6.2KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-10.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-10R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P10.0HCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5K_1/8W" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0805-5K" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300K_/1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM300KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-33K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-33.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-110K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-110KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-20K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-20.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-140K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-140KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-84.5K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-84.5KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4823-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15K_1/8W" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-15.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-47.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_0.125W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-10KARCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/4W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM1.0KDCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-698R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-698HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.5K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.50K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-3.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.58K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RC0603FR-071K58L" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-51.1R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-51.1HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.37K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.37KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-150GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-82GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-130R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-130GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12.1K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-12.1KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-270KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-100HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM470AZCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.13" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.0003R/3W-CURRENTSENSE" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="HCS2512FTL300CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.9625" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6.8R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-6.80ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.102" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-470ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-270KACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-680GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0048" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="267K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-267KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-191K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4862-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9.53K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-9.53KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-49.9K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-49.9KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-470GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.55K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.55KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.2K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.2KGRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-348K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-348KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="27R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-27GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8.2R_2W" package="R2616">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="A103603CT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.56" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.05K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.05KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-13K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-13KGRCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3.3K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-3.3KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82K/1W" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF2512JT82K0CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.29" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD-1X4">
<gates>
<gate name="G$1" symbol="PINHEAD-1X4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PINHEAD-1X4-2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2.54MM" package="PINHEAD-1X4-2.54MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="952-2265-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17" constant="no"/>
</technology>
</technologies>
</device>
<device name="-DIR" package="PINHEAD-1X4-2MM-DIRECTIONAL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="A100098-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" prefix="FID" uservalue="yes">
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No_Part. Plain Copper Pads."/>
<attribute name="PRICE_PER_UNIT" value="0"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DUOLED-GY-C" prefix="LD" uservalue="yes">
<description>Digikey Number :- __754-1234-ND__</description>
<gates>
<gate name="G$1" symbol="DUOLED-GY-C" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DUOLED-C-5MM">
<connects>
<connect gate="G$1" pin="AG" pad="AR"/>
<connect gate="G$1" pin="AY" pad="AX"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="754-1234-ND  " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.44" constant="no"/>
</technology>
</technologies>
</device>
<device name="'" package="DUOLED-C-3MM">
<connects>
<connect gate="G$1" pin="AG" pad="AX"/>
<connect gate="G$1" pin="AY" pad="AX@1"/>
<connect gate="G$1" pin="C" pad="CX"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HTU20D" prefix="U">
<gates>
<gate name="U$1" symbol="HTU21D" x="0" y="0"/>
</gates>
<devices>
<device name="SMD" package="HTU21D">
<connects>
<connect gate="U$1" pin="DATA" pad="DATA"/>
<connect gate="U$1" pin="GND@2" pad="GND"/>
<connect gate="U$1" pin="GND@7" pad="7"/>
<connect gate="U$1" pin="SCK" pad="SCK"/>
<connect gate="U$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="223-1592-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="4.38" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TACTILE" prefix="TAC">
<description>http://www.digikey.com/product-detail/en/FSM4JSMA/450-1129-ND/525821</description>
<gates>
<gate name="G$1" symbol="TACTILE" x="0" y="0"/>
</gates>
<devices>
<device name="-SMT" package="TACTILE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="450-1129-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.20" constant="no"/>
</technology>
</technologies>
</device>
<device name="-DIP" package="TACTILE-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="679-2428-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="TACTILE-SMD-4.5X4.5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="EG5353CT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.1823" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12X12" package="TAC-SMD-12X12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="EG4904DKR-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.4937" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.63" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6X6" package="TACTILE-SMD-6X6">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="CKN9112CT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD-1X3">
<gates>
<gate name="G$1" symbol="PINHEAD-1X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PINHEAD-1X3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="952-2264-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.14" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PMOS" prefix="Q">
<gates>
<gate name="G$1" symbol="MOSFET-PCHANNEL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-W">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="IRLML5203TRPBFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.41" constant="no"/>
</technology>
</technologies>
</device>
<device name="-FDN340" package="SOT23-W">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="FDN340PCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.44" constant="no"/>
</technology>
</technologies>
</device>
<device name="-SI7615ADN" package="POWERPACK1212">
<connects>
<connect gate="G$1" pin="D" pad="D1 D2 D3 D4 DPAD"/>
<connect gate="G$1" pin="G" pad="G"/>
<connect gate="G$1" pin="S" pad="S1 S2 S3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="SI7615ADN-T1-GE3CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.78" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12V/4.3A" package="SOT23-R">
<connects>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="G" pad="1"/>
<connect gate="G$1" pin="S" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="IRLML6401PBFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.51" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="DINA4_L">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connectors &amp; holders">
<packages>
<package name="BATTERY_HOLDER">
<smd name="+" x="-14.65" y="0" dx="5" dy="3.5" layer="1" rot="R90"/>
<smd name="ADH" x="0" y="0" dx="11" dy="7" layer="1" rot="R180"/>
<smd name="-" x="14.65" y="0" dx="5" dy="3.5" layer="1" rot="R90"/>
<text x="-3.22" y="7.62" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.22" y="-7.62" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-13.38" y1="2.54" x2="-13.38" y2="3.81" width="0.127" layer="21"/>
<wire x1="-13.38" y1="3.81" x2="-8.3" y2="8" width="0.127" layer="21" curve="-90"/>
<wire x1="-13.38" y1="-2.54" x2="-13.38" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-13.38" y1="-3.81" x2="-8.3" y2="-8" width="0.127" layer="21" curve="90"/>
<wire x1="13.29" y1="2.54" x2="13.29" y2="3.81" width="0.127" layer="21"/>
<wire x1="13.29" y1="3.81" x2="9.48" y2="8" width="0.127" layer="21" curve="90"/>
<wire x1="13.47" y1="-2.54" x2="13.47" y2="-3.81" width="0.127" layer="21"/>
<wire x1="13.47" y1="-3.81" x2="8.39" y2="-8" width="0.127" layer="21" curve="-90"/>
<wire x1="-8.21" y1="8" x2="9.57" y2="8" width="0.127" layer="21"/>
<wire x1="-8.21" y1="-8" x2="8.3" y2="-8" width="0.127" layer="21"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="-12" y="-1" size="1.27" layer="21">+</text>
<text x="11" y="-1" size="1.27" layer="21">-</text>
</package>
<package name="2*AA_BATTERY_HOLDER">
<wire x1="-29.755" y1="16.45" x2="29.755" y2="16.45" width="0.127" layer="21"/>
<wire x1="-29.755" y1="-16.45" x2="29.755" y2="-16.45" width="0.127" layer="21"/>
<wire x1="-29.755" y1="16.45" x2="-29.755" y2="-16.45" width="0.127" layer="21"/>
<wire x1="29.755" y1="16.45" x2="29.755" y2="-16.45" width="0.127" layer="21"/>
<pad name="+" x="27.18" y="-7.5" drill="1.02"/>
<pad name="-" x="27.18" y="7.49" drill="1.02"/>
<text x="8.965" y="10.43" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="8.965" y="-7.5" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="24" y="-7.5" size="2.54" layer="21">+</text>
<text x="-20" y="8.5" size="2.54" layer="21">+</text>
<text x="24" y="8.5" size="2.54" layer="21">-</text>
<text x="-20" y="-7.5" size="2.54" layer="21">-</text>
</package>
<package name="AA-BATTERY-HOLDER-DIMENSION">
<wire x1="-29.6" y1="16.5" x2="29.6" y2="16.5" width="0.127" layer="20"/>
<wire x1="-29.6" y1="-16.5" x2="29.6" y2="-16.5" width="0.127" layer="20"/>
<wire x1="-30" y1="16.1" x2="-30" y2="-16.1" width="0.127" layer="20"/>
<pad name="+" x="27.18" y="-7.5" drill="1.02"/>
<pad name="-" x="27.18" y="7.49" drill="1.02"/>
<text x="8.965" y="10.43" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="8.965" y="-7.5" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="24" y="-7.5" size="2.54" layer="21">+</text>
<text x="-20" y="8.5" size="2.54" layer="21">+</text>
<text x="24" y="8.5" size="2.54" layer="21">-</text>
<text x="-20" y="-7.5" size="2.54" layer="21">-</text>
<wire x1="30" y1="16.1" x2="30" y2="-16.1" width="0.127" layer="20"/>
<wire x1="-29.6" y1="16.5" x2="-30" y2="16.1" width="0.127" layer="20"/>
<wire x1="29.6" y1="16.5" x2="30" y2="16.1" width="0.127" layer="20"/>
<wire x1="-29.6" y1="-16.5" x2="-30" y2="-16.1" width="0.127" layer="20"/>
<wire x1="30" y1="-16.1" x2="29.6" y2="-16.5" width="0.127" layer="20"/>
</package>
<package name="LI-BATTERY-HOLDER">
<wire x1="10.45" y1="-38.85" x2="-10.45" y2="-38.85" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="-38.85" x2="-10.45" y2="38.85" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="38.85" x2="10.45" y2="38.85" width="0.4064" layer="21"/>
<wire x1="10.45" y1="38.85" x2="10.45" y2="-38.85" width="0.4064" layer="21"/>
<hole x="0" y="27.805" drill="3.2"/>
<hole x="0" y="-27.805" drill="3.2"/>
<text x="-1.45" y="20.17" size="5.08" layer="21" font="vector" ratio="15">+</text>
<text x="-1.45" y="-25.83" size="5.08" layer="21" font="vector" ratio="15">-</text>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<pad name="+" x="0" y="36.45" drill="2.794" diameter="3.81" shape="long"/>
<pad name="-" x="0" y="-36.45" drill="2.794" diameter="3.81" shape="long"/>
</package>
<package name="COINCELL-SMT">
<smd name="P$1" x="0" y="10.7" dx="3" dy="3.5" layer="1" rot="R90"/>
<smd name="P$2" x="0" y="-10.7" dx="3.5" dy="4" layer="1"/>
<wire x1="-7.18" y1="8.4" x2="7.18" y2="8.4" width="0.127" layer="51" curve="-126.869898"/>
<wire x1="7.18" y1="-3.8" x2="-7.18" y2="-3.8" width="0.127" layer="51" curve="-126.869898"/>
<wire x1="-7.2" y1="8.4" x2="-7.2" y2="-8.4" width="0.127" layer="21"/>
<wire x1="7.2" y1="8.4" x2="7.2" y2="-8.4" width="0.127" layer="21"/>
<wire x1="-7.2" y1="-8.4" x2="-2" y2="-11.9" width="0.127" layer="21"/>
<wire x1="7.2" y1="-8.4" x2="2" y2="-11.9" width="0.127" layer="21"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
</package>
<package name="COINCELL-DIP">
<pad name="+" x="-13.45" y="1.25" drill="1.1"/>
<pad name="+@1" x="-13.45" y="-1.25" drill="1.1"/>
<pad name="-" x="13.45" y="0" drill="1.1"/>
<wire x1="-15" y1="3.5" x2="-15" y2="-3.5" width="0.127" layer="21"/>
<wire x1="15" y1="3.5" x2="15" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-15" y1="3.5" x2="-11" y2="3.5" width="0.127" layer="21"/>
<wire x1="-15" y1="-3.5" x2="-11" y2="-3.5" width="0.127" layer="21"/>
<wire x1="11" y1="3.5" x2="15" y2="3.5" width="0.127" layer="21"/>
<wire x1="15" y1="-3.5" x2="11" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="10" x2="5.5" y2="10" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-10" x2="5.5" y2="-10" width="0.127" layer="21"/>
<wire x1="-11" y1="3.5" x2="-11" y2="4.5" width="0.127" layer="21"/>
<wire x1="-11" y1="4.5" x2="-5.5" y2="10" width="0.127" layer="21" curve="-90"/>
<wire x1="11" y1="3.5" x2="11" y2="4.5" width="0.127" layer="21"/>
<wire x1="11" y1="4.5" x2="5.5" y2="10" width="0.127" layer="21" curve="90"/>
<wire x1="-5.5" y1="-10" x2="-11" y2="-3.5" width="0.127" layer="21" curve="-99.527296"/>
<wire x1="5.5" y1="-10" x2="11" y2="-3.5" width="0.127" layer="21" curve="99.527296"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="-12" y="-1" size="1.778" layer="21" ratio="15">+</text>
<text x="11" y="-0.5" size="1.778" layer="21" ratio="15">-</text>
<text x="-4" y="4" size="1.27" layer="21" ratio="15">&gt;NAME</text>
<text x="-4" y="-4.5" size="1.27" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="BC-2400-ND">
<pad name="+@1" x="-10.475" y="0" drill="2.03"/>
<pad name="+@2" x="10.475" y="0" drill="2.03"/>
<wire x1="-12.475" y1="3.5" x2="12.525" y2="3.5" width="0.127" layer="21"/>
<wire x1="12.525" y1="3.5" x2="12.525" y2="-3.5" width="0.127" layer="21"/>
<wire x1="12.525" y1="-3.5" x2="-12.475" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-12.475" y1="-3.5" x2="-12.475" y2="3.5" width="0.127" layer="21"/>
<smd name="GND" x="0" y="0" dx="15" dy="15" layer="1" roundness="100"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="0" y="10" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-10" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CR2032-THRU">
<wire x1="10.34" y1="3.8" x2="13.32" y2="3.8" width="0.2032" layer="21"/>
<wire x1="13.32" y1="3.8" x2="13.32" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="13.32" y1="-3.8" x2="10.34" y2="-3.8" width="0.2032" layer="21"/>
<circle x="0.06" y="0.1" radius="10" width="0.2032" layer="51"/>
<pad name="2" x="-8.15" y="0" drill="1.3" rot="R90"/>
<pad name="1" x="11.85" y="0" drill="1.3" rot="R90"/>
<text x="-15.985" y="0.635" size="0.4064" layer="25">&gt;NAME</text>
<text x="-15.985" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
<text x="8.6" y="-0.7" size="1.27" layer="51">+</text>
<text x="-6.4" y="-0.7" size="1.27" layer="51">-</text>
<wire x1="-10.54" y1="3.8" x2="-10.54" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="10.34" y1="3.8" x2="-10.54" y2="3.8" width="0.2032" layer="21" curve="139.856795"/>
<wire x1="10.34" y1="-3.8" x2="-10.54" y2="-3.8" width="0.2032" layer="21" curve="-139.856795"/>
</package>
<package name="3-AA-BATTERY-HOLDER">
<wire x1="-30" y1="25" x2="30" y2="25" width="0.127" layer="21"/>
<wire x1="-30" y1="-25" x2="30" y2="-25" width="0.127" layer="21"/>
<wire x1="-30" y1="-25" x2="-30" y2="25" width="0.127" layer="21"/>
<wire x1="30" y1="-25" x2="30" y2="25" width="0.127" layer="21"/>
<pad name="-" x="26.07" y="-0.7" drill="1.2" shape="square"/>
<pad name="+" x="26.07" y="-16.5" drill="1.2"/>
<text x="21" y="-18.27" size="3.81" layer="51">+</text>
<text x="21" y="-2.27" size="3.81" layer="51">-</text>
<text x="-2" y="21" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-21" size="1.27" layer="25">&gt;VALUE</text>
</package>
<package name="CR2032-VERT">
<pad name="-" x="0" y="0" drill="0.8" shape="square"/>
<pad name="+@1" x="-5" y="-3.81" drill="0.8"/>
<pad name="+@2" x="5" y="-3.81" drill="0.8"/>
<wire x1="-11.5" y1="1.27" x2="-11.5" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-11.5" y1="-5.08" x2="11.5" y2="-5.08" width="0.127" layer="21"/>
<wire x1="11.5" y1="-5.08" x2="11.5" y2="1.27" width="0.127" layer="21"/>
<wire x1="11.5" y1="1.27" x2="-11.5" y2="1.27" width="0.127" layer="21"/>
<text x="1.27" y="0" size="1.27" layer="51">-ve</text>
<text x="-8.89" y="-3.81" size="1.27" layer="51">+ve</text>
<text x="6.35" y="-3.81" size="1.27" layer="51">+ve</text>
</package>
<package name="BC-2003">
<wire x1="-13.97" y1="3.5" x2="13.97" y2="3.5" width="0.127" layer="21"/>
<wire x1="13.97" y1="3.5" x2="13.97" y2="-3.5" width="0.127" layer="21"/>
<wire x1="13.97" y1="-3.5" x2="-13.97" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-3.5" x2="-13.97" y2="3.5" width="0.127" layer="21"/>
<smd name="GND" x="0" y="0" dx="10" dy="5" layer="1"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="0" y="10" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-10" size="1.27" layer="27">&gt;VALUE</text>
<smd name="+@0" x="-11.905" y="0" dx="6" dy="3" layer="1" rot="R90"/>
<smd name="+@1" x="11.905" y="0" dx="6" dy="3" layer="1" rot="R90"/>
</package>
<package name="BK-912">
<smd name="GND" x="0" y="0" dx="16" dy="16" layer="1" roundness="100"/>
<circle x="0" y="0" radius="10" width="0.127" layer="51"/>
<text x="0" y="10" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-10" size="1.27" layer="27">&gt;VALUE</text>
<smd name="+@0" x="-11" y="0" dx="5.5" dy="3" layer="1" rot="R90"/>
<smd name="+@1" x="11" y="0" dx="5.5" dy="3" layer="1" rot="R90"/>
<wire x1="-9.525" y1="10.16" x2="9.525" y2="10.16" width="0.127" layer="21"/>
<wire x1="-9.525" y1="10.16" x2="-10.795" y2="8.89" width="0.127" layer="21"/>
<wire x1="-10.795" y1="8.89" x2="-10.795" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-5.08" x2="-5.08" y2="-8.89" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-8.89" x2="-3.175" y2="-6.985" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-6.985" x2="3.81" y2="-6.985" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.985" x2="5.08" y2="-8.89" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="-8.89" x2="10.795" y2="-5.08" width="0.127" layer="21"/>
<wire x1="10.795" y1="-5.08" x2="10.795" y2="8.89" width="0.127" layer="21"/>
<wire x1="10.795" y1="8.89" x2="9.525" y2="10.16" width="0.127" layer="21"/>
</package>
<package name="1*AA_BATTERY_HOLDER">
<pad name="-" x="-26" y="0" drill="1.1"/>
<pad name="+" x="26" y="0" drill="1.1"/>
<hole x="-23.24" y="6" drill="2.7"/>
<hole x="23.24" y="-6" drill="2.7"/>
<wire x1="-29" y1="8.5" x2="29" y2="8.5" width="0.127" layer="21"/>
<wire x1="29" y1="8.5" x2="29" y2="-8.5" width="0.127" layer="21"/>
<wire x1="29" y1="-8.5" x2="-29" y2="-8.5" width="0.127" layer="21"/>
<wire x1="-29" y1="-8.5" x2="-29" y2="8.5" width="0.127" layer="21"/>
<text x="22" y="-2" size="2.54" layer="21">+</text>
<text x="-25" y="-3" size="2.54" layer="21">-</text>
<text x="-7" y="4" size="2.54" layer="25">&gt;NAME</text>
<text x="-7" y="-7" size="2.54" layer="27">&gt;VALUE</text>
</package>
<package name="BK-18650-PC2">
<wire x1="10.45" y1="-38.48" x2="-10.45" y2="-38.48" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="-38.48" x2="-10.45" y2="38.48" width="0.4064" layer="21"/>
<wire x1="-10.45" y1="38.48" x2="10.45" y2="38.48" width="0.4064" layer="21"/>
<wire x1="10.45" y1="38.48" x2="10.45" y2="-38.48" width="0.4064" layer="21"/>
<hole x="0" y="27.805" drill="3.3"/>
<hole x="0" y="-27.805" drill="3.3"/>
<text x="-1.45" y="20.17" size="5.08" layer="21" font="vector" ratio="15">+</text>
<text x="-1.45" y="-25.83" size="5.08" layer="21" font="vector" ratio="15">-</text>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<pad name="+" x="0" y="36.45" drill="2.794" diameter="3.81" shape="long"/>
<pad name="-" x="0" y="-36.45" drill="2.794" diameter="3.81" shape="long"/>
</package>
<package name="BH123A">
<wire x1="-22.5" y1="9" x2="22.5" y2="9" width="0.127" layer="21"/>
<wire x1="22.5" y1="9" x2="22.5" y2="-9" width="0.127" layer="21"/>
<wire x1="22.5" y1="-9" x2="-22.5" y2="-9" width="0.127" layer="21"/>
<wire x1="-22.5" y1="-9" x2="-22.5" y2="9" width="0.127" layer="21"/>
<text x="0" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<pad name="+" x="-19" y="0" drill="1.5"/>
<pad name="-" x="19" y="0" drill="1.5"/>
<text x="21" y="-5" size="5.08" layer="21" rot="R90">-</text>
<text x="-17" y="-6" size="5.08" layer="21" rot="R90">+</text>
<hole x="-19.58" y="6.37" drill="1.3"/>
</package>
<package name="PIHEAD-1X2">
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="PINHEAD-1X2-3.96MM">
<wire x1="-3.9" y1="1.27" x2="3.9" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.9" y1="-1.27" x2="-3.9" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-3.9" y1="-1.27" x2="-3.9" y2="1.27" width="0.127" layer="21"/>
<pad name="1" x="-1.98" y="0" drill="1.75" diameter="3" shape="octagon" rot="R90"/>
<pad name="2" x="1.98" y="0" drill="1.75" diameter="3" shape="octagon" rot="R90"/>
<wire x1="3.9" y1="1.27" x2="3.9" y2="-1.27" width="0.127" layer="21"/>
<text x="-2.54" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-3.302" y1="0" x2="-3.302" y2="5.41" width="0.127" layer="51"/>
<wire x1="-3.302" y1="5.41" x2="3.302" y2="5.41" width="0.127" layer="51"/>
<wire x1="3.302" y1="5.41" x2="3.302" y2="0" width="0.127" layer="51"/>
<wire x1="-3.302" y1="5.41" x2="-3.302" y2="8.71" width="0.127" layer="21"/>
<wire x1="-3.302" y1="8.71" x2="3.302" y2="8.71" width="0.127" layer="21"/>
<wire x1="3.302" y1="8.71" x2="3.302" y2="5.41" width="0.127" layer="21"/>
<wire x1="-3.302" y1="8.71" x2="-3.302" y2="20.14" width="0.127" layer="51"/>
<wire x1="-3.302" y1="20.14" x2="3.302" y2="20.14" width="0.127" layer="51"/>
<wire x1="3.302" y1="20.14" x2="3.302" y2="8.71" width="0.127" layer="51"/>
<wire x1="-3.302" y1="0" x2="3.302" y2="0" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="BATTERY">
<pin name="-" x="-5.08" y="0" visible="off" length="short"/>
<pin name="+" x="2.54" y="0" visible="off" length="short" rot="R180"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<text x="5.08" y="5.08" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PINHEAD-1X2">
<wire x1="1.27" y1="-2.54" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.27" layer="95">&gt;NAME</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<wire x1="1.27" y1="-2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="BATTERY_HOLDER" prefix="VBAT" uservalue="yes">
<description>&lt;b&gt;Coin Cell Holder&lt;/b&gt; :-  __BU2032SM-HD-G__  __BC-2400-ND__&lt;p&gt;
&lt;b&gt;AA  Battery Holder&lt;/b&gt; :- __2462K-ND__&lt;p&gt;
&lt;b&gt;LI-ION Holder &lt;/b&gt;:- __BH-18650-PC__</description>
<gates>
<gate name="G$1" symbol="BATTERY" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="BATTERY_HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AA" package="2*AA_BATTERY_HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="2462K-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.63" constant="no"/>
</technology>
</technologies>
</device>
<device name="-DIM" package="AA-BATTERY-HOLDER-DIMENSION">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LI-ION" package="LI-BATTERY-HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BH-18650-PC " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="2.81" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2032" package="COINCELL-SMT">
<connects>
<connect gate="G$1" pin="+" pad="P$2"/>
<connect gate="G$1" pin="-" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4MM_PINS" package="COINCELL-DIP">
<connects>
<connect gate="G$1" pin="+" pad="+ +@1"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CLIP" package="BC-2400-ND">
<connects>
<connect gate="G$1" pin="+" pad="+@1 +@2"/>
<connect gate="G$1" pin="-" pad="GND"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-20MM_DIP" package="CR2032-THRU">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BC2032-E2-ND &amp; N189-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.04" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3*AA" package="3-AA-BATTERY-HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BC3AAPC-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.5" constant="no"/>
</technology>
</technologies>
</device>
<device name="CR2032-VERT" package="CR2032-VERT">
<connects>
<connect gate="G$1" pin="+" pad="+@1 +@2"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BS-5-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.71" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.85" constant="no"/>
</technology>
</technologies>
</device>
<device name="-CLIP_SMT" package="BC-2003">
<connects>
<connect gate="G$1" pin="+" pad="+@0 +@1"/>
<connect gate="G$1" pin="-" pad="GND"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BC-2003-ND " constant="no"/>
<attribute name="MOUSER" value="" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="RETAINER_SMT" package="BK-912">
<connects>
<connect gate="G$1" pin="+" pad="+@0 +@1"/>
<connect gate="G$1" pin="-" pad="GND"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="BK-912-ND " constant="no"/>
<attribute name="MOUSER" value="712-BAT-HLD-001 " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.29" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.34" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1XAA" package="1*AA_BATTERY_HOLDER">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="36-2460-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.8649" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.57" constant="no"/>
</technology>
</technologies>
</device>
<device name="-PC2" package="BK-18650-PC2">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CR123" package="BH123A">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHEAD-1X2" prefix="HEADER">
<gates>
<gate name="G$1" symbol="PINHEAD-1X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PIHEAD-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="952-2262-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="-MOLEX_1X2-3.96MM" package="PINHEAD-1X2-3.96MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="WM4640-ND &amp; WM2122-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.52" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="modules">
<packages>
<package name="RFM69HW">
<smd name="RESET" x="-9" y="7.5" dx="3" dy="1" layer="1"/>
<smd name="DIO0" x="-9" y="5.5" dx="3" dy="1" layer="1"/>
<smd name="3.3V" x="-9" y="-6.5" dx="3" dy="1" layer="1"/>
<smd name="GND@0" x="8.7" y="-6.5" dx="3" dy="1" layer="1"/>
<smd name="ANA" x="8.7" y="-4.5" dx="3" dy="1" layer="1"/>
<smd name="GND@1" x="8.7" y="-2.5" dx="3" dy="1" layer="1"/>
<smd name="SCK" x="8.7" y="-0.5" dx="3" dy="1" layer="1"/>
<smd name="MISO" x="8.7" y="1.5" dx="3" dy="1" layer="1"/>
<smd name="MOSI" x="8.7" y="3.5" dx="3" dy="1" layer="1"/>
<smd name="NSS" x="8.7" y="5.5" dx="3" dy="1" layer="1"/>
<smd name="NC" x="8.7" y="7.5" dx="3" dy="1" layer="1"/>
<wire x1="-11" y1="9.5" x2="10.7" y2="9.5" width="0.127" layer="21"/>
<wire x1="10.7" y1="9.5" x2="10.7" y2="-8.5" width="0.127" layer="21"/>
<wire x1="10.7" y1="-8.5" x2="-11" y2="-8.5" width="0.127" layer="21"/>
<wire x1="-11" y1="-8.5" x2="-11" y2="9.5" width="0.127" layer="21"/>
<text x="-4.5" y="7.5" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<circle x="-6.35" y="7.62" radius="0.359209375" width="0.127" layer="21"/>
<circle x="-6.35" y="7.62" radius="0.254" width="0.127" layer="21"/>
<circle x="-6.35" y="7.62" radius="0.254" width="0.127" layer="21"/>
<smd name="DIO2" x="-9" y="1.5" dx="3" dy="1" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="RFM69HW">
<pin name="RESET" x="-20.32" y="12.7" length="middle"/>
<pin name="DIO0" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="3.3V" x="-2.54" y="20.32" length="middle" rot="R270"/>
<pin name="GND" x="2.54" y="20.32" length="middle" rot="R270"/>
<pin name="ANA" x="0" y="-20.32" length="middle" rot="R90"/>
<pin name="GND@1" x="5.08" y="20.32" length="middle" rot="R270"/>
<pin name="SCK" x="-20.32" y="2.54" length="middle"/>
<pin name="MISO" x="-20.32" y="7.62" length="middle"/>
<pin name="MOSI" x="-20.32" y="5.08" length="middle"/>
<pin name="NSS" x="-20.32" y="-5.08" length="middle"/>
<pin name="NC" x="-20.32" y="-10.16" length="middle"/>
<wire x1="-15.24" y1="15.24" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="-15.24" y2="15.24" width="0.254" layer="94"/>
<text x="-2.54" y="2.54" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
<pin name="DIO2" x="20.32" y="5.08" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RFM69HW">
<gates>
<gate name="G$1" symbol="RFM69HW" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RFM69HW">
<connects>
<connect gate="G$1" pin="3.3V" pad="3.3V"/>
<connect gate="G$1" pin="ANA" pad="ANA"/>
<connect gate="G$1" pin="DIO0" pad="DIO0"/>
<connect gate="G$1" pin="DIO2" pad="DIO2"/>
<connect gate="G$1" pin="GND" pad="GND@0"/>
<connect gate="G$1" pin="GND@1" pad="GND@1"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="NC" pad="NC"/>
<connect gate="G$1" pin="NSS" pad="NSS"/>
<connect gate="G$1" pin="RESET" pad="RESET"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RFM69HW-433S2-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="10.34" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X01">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
<package name="2X03">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
</package>
<package name="2X03/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-2.54" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-2.54" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-4.445" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.715" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="-2.921" y1="-5.461" x2="-2.159" y2="-4.699" layer="21"/>
<rectangle x1="-2.921" y1="-4.699" x2="-2.159" y2="-2.921" layer="51"/>
<rectangle x1="-0.381" y1="-4.699" x2="0.381" y2="-2.921" layer="51"/>
<rectangle x1="-0.381" y1="-5.461" x2="0.381" y2="-4.699" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-5.461" x2="2.921" y2="-4.699" layer="21"/>
<rectangle x1="2.159" y1="-4.699" x2="2.921" y2="-2.921" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINHD1">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="-6.35" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINH2X3">
<wire x1="-6.35" y1="-5.08" x2="8.89" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="8.89" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="-5.08" width="0.4064" layer="94"/>
<text x="-6.35" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X1" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X3" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X3" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X03">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X03/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="C0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1.905" y="0.835" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
</package>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.508" x2="1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.508" x2="-1.27" y2="0.508" width="0.127" layer="21"/>
</package>
<package name="FILM-CAPACITOR-0.47U/400V">
<wire x1="-9.25" y1="3.9" x2="9.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="3.9" x2="9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="-3.9" x2="-9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-9.25" y1="-3.9" x2="-9.25" y2="3.9" width="0.127" layer="21"/>
<pad name="P$1" x="-7.5" y="0" drill="0.9"/>
<pad name="P$2" x="7.5" y="0" drill="0.9"/>
<text x="-2.54" y="4.17" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.17" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="51"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="51"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="-2.4" y1="1" x2="2.4" y2="1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1" x2="-2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-2.4" y2="1" width="0.127" layer="21"/>
</package>
<package name="C1210">
<smd name="P$1" x="-1.35" y="0" dx="1" dy="3" layer="1"/>
<smd name="P$2" x="1.35" y="0" dx="1" dy="3" layer="1"/>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.127" layer="21"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<text x="-1.5" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C2312">
<wire x1="-3" y1="1.6" x2="3" y2="1.6" width="0.127" layer="21"/>
<wire x1="3" y1="-1.6" x2="-3" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-4.2" y1="2" x2="4.2" y2="2" width="0.127" layer="39"/>
<wire x1="4.2" y1="2" x2="4.2" y2="-2" width="0.127" layer="39"/>
<wire x1="4.2" y1="-2" x2="-4.2" y2="-2" width="0.127" layer="39"/>
<wire x1="-4.2" y1="-2" x2="-4.2" y2="2" width="0.127" layer="39"/>
<wire x1="1.25" y1="-1.5" x2="1.25" y2="1.5" width="0.127" layer="21"/>
<circle x="-4.5" y="0" radius="0.2" width="0.3" layer="21"/>
<smd name="1" x="-2.7" y="0" dx="2.4" dy="2.7" layer="1"/>
<smd name="2" x="2.7" y="0" dx="2.4" dy="2.7" layer="1"/>
</package>
<package name="R0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-2.032" y="0.635" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.032" y="-2.159" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.63" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.63" width="0.127" layer="21"/>
</package>
<package name="R-0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.3" y1="0.473" x2="-1.3" y2="-0.473" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-0.473" x2="1.2" y2="-0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.473" x2="1.2" y2="0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.473" x2="-1.3" y2="0.473" width="0.127" layer="21"/>
</package>
<package name="10555-METRIC">
<smd name="P$1" x="-4.85" y="0" dx="3.3" dy="2" layer="1"/>
<smd name="P$2" x="4.35" y="0" dx="3.3" dy="2" layer="1"/>
<wire x1="-6.75" y1="3" x2="-6.75" y2="-3" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-3" x2="6.25" y2="-3" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3" x2="6.25" y2="3" width="0.127" layer="21"/>
<wire x1="6.25" y1="3" x2="-6.75" y2="3" width="0.127" layer="21"/>
<text x="-2.75" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805-METRIC">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="R0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="-0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="-2" y2="0.75" width="0.127" layer="21"/>
</package>
<package name="R2512">
<smd name="P$1" x="0" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<smd name="P$2" x="6.5024" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<wire x1="-1.27" y1="1.9304" x2="-1.27" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.9304" x2="7.7724" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="-1.9304" x2="7.7724" y2="1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="1.9304" x2="-1.27" y2="1.9304" width="0.127" layer="21"/>
<text x="1.016" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0.508" y="-3.556" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="5.334" y1="1.524" x2="1.016" y2="1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="-1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="-1.524" x2="5.334" y2="-1.524" width="0.127" layer="51"/>
<wire x1="5.334" y1="-1.524" x2="5.334" y2="1.524" width="0.127" layer="51"/>
</package>
<package name="MELF0204">
<smd name="P$1" x="-0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.8" x2="1.2" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.8" x2="1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.8" x2="-1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.8" x2="-1.2" y2="0.8" width="0.127" layer="21"/>
<text x="-0.8" y="1.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.8" y="-2.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R2616">
<smd name="P$1" x="0" y="0" dx="3" dy="3" layer="1" rot="R90"/>
<smd name="P$2" x="5.85" y="0" dx="3" dy="3" layer="1" rot="R90"/>
<wire x1="-1.5" y1="2" x2="7.5" y2="2" width="0.127" layer="21"/>
<wire x1="7.5" y1="2" x2="7.5" y2="-2" width="0.127" layer="21"/>
<wire x1="7.5" y1="-2" x2="-1.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="2" width="0.127" layer="21"/>
</package>
<package name="R-1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="-2.4" y1="1" x2="2.4" y2="1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1" x2="-2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-2.4" y2="1" width="0.127" layer="21"/>
</package>
<package name="R1210">
<smd name="P$1" x="-1.35" y="0" dx="1" dy="3" layer="1"/>
<smd name="P$2" x="1.35" y="0" dx="1" dy="3" layer="1"/>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.127" layer="21"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<text x="-1.5" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="CAPACITOR">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
</symbol>
<symbol name="RESISTOR">
<text x="-2.54" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-2.54" y="-3.81" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>CAPACITOR</description>
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0.1UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1095-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FILM_AC-CAP" package="FILM-CAPACITOR-0.47U/400V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1275-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1079-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.23"/>
</technology>
</technologies>
</device>
<device name="-33PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1055-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-18PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1052-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-DNP/NI" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2087-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3022-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-3386-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.11"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1821-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.12"/>
</technology>
</technologies>
</device>
<device name="-0.01UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1132-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4700PF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2062-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1524-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3012-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.16" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/25V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3335-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7NF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9092-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-18PF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="77-VJ0805A180GXQCBC" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.06" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/10V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1356-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.24" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-9730-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1024-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/25V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1353-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3303-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1532-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22PF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9031-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-5043-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10NF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1926-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1993-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.47" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V." package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-4112-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V-0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3340-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1494-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2200PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1085-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1281-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.022UF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1063-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1296-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/50V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1909-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0367" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/10V-0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1863-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.031" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/16V" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-5514-2-ND " constant="no"/>
<attribute name="MANUFACTURER" value="C1210C476M4PACTU" constant="no"/>
<attribute name="MOUSER" value="80-C1210C476M4P" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.54" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.01" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2143-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.017" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5.6PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-7944-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0193" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12PF" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1254-1-ND  " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.47UF" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2377-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.8PF/100V" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="478-9758-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.85" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100UF/6.3V" package="C2312">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="478-8157-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value=".81" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/10V." package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-10777-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10PF/50V" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1139-1-ND" constant="no"/>
<attribute name="PRICE_PE_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12PF/50V" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1178-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt; &lt;p&gt; 
Mount :-Surface mount</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-50R_1/8W_RF" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-49.9HRCT-ND  " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-50R_1/20W_RF" package="R-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="FC0402-50BWCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.52"/>
</technology>
</technologies>
</device>
<device name="-POWER_RESISTOR" package="10555-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-100K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-100K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-1M_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00M-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-12K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-12.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-330R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF0603JT330RCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-30R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P30.0HCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0R" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P0.0GCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-4.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-24K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM24KCFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-560R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-560GRDKR-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.7K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-2.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-33R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM33.0CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-6.2K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM6.2KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-10.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-10R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P10.0HCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5K_1/8W" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0805-5K" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300K_/1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM300KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-33K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-33.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-110K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-110KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-20K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-20.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-140K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-140KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-84.5K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-84.5KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4823-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15K_1/8W" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-15.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-47.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_0.125W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-10KARCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/4W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM1.0KDCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-698R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-698HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.5K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.50K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-3.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.58K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RC0603FR-071K58L" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-51.1R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-51.1HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.37K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.37KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-150GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-82GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-130R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-130GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12.1K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-12.1KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-270KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-100HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM470AZCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.13" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.0003R/3W-CURRENTSENSE" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="HCS2512FTL300CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.9625" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6.8R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-6.80ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.102" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-470ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-270KACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-680GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0048" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="267K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-267KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-191K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4862-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9.53K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-9.53KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-49.9K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-49.9KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-470GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.55K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.55KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.2K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.2KGRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-348K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-348KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="27R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-27GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8.2R_2W" package="R2616">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="A103603CT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.56" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.05K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.05KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-13K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-13KGRCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3.3K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-3.3KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82K/1W" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF2512JT82K0CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.29" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-300GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value=".01" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/4W" package="R-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM22CJCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-120R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-120HRCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-120R_1/4W" package="R-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF1206JT120RCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68R_1/2W" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="541-68VCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6.8K_1/2W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P6.8KADCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.12" constant="no"/>
</technology>
</technologies>
</device>
<device name="-39K_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM39KKCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-330K_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM330KKCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="470K_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM470KKCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3.3M_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM3.5MKCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680K_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM680KKCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/2W" package="R-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RNCP1206FTD1K00CT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM22KCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5.1R_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM5.1KCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.5R_1/4W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="A109810CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.2" constant="no"/>
</technology>
</technologies>
</device>
<device name="-52.3K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-52.3KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-22.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.17" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="GND1" library="BaseApp" deviceset="GND" device=""/>
<part name="STM8L" library="BaseApp" deviceset="STM8L051F3" device=""/>
<part name="U$5" library="BaseApp" deviceset="VDD" device=""/>
<part name="GND3" library="BaseApp" deviceset="GND" device=""/>
<part name="R2" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="C2" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value=".1uF"/>
<part name="J100" library="BaseApp" deviceset="PINHEAD-1X4" device="-2.54MM" value="PINHEAD-1X4-2.54MM"/>
<part name="R3" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="GND6" library="BaseApp" deviceset="GND" device=""/>
<part name="C1" library="BaseApp" deviceset="CAPACITOR" device="-1UF/6.3V" value="1UF/6.3V "/>
<part name="GND7" library="BaseApp" deviceset="GND" device=""/>
<part name="R4" library="BaseApp" deviceset="RESISTOR" device="-330R_1/10W" value="330R"/>
<part name="R5" library="BaseApp" deviceset="RESISTOR" device="-330R_1/10W" value="330R"/>
<part name="R1" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="U$14" library="BaseApp" deviceset="VDD" device=""/>
<part name="GND9" library="BaseApp" deviceset="GND" device=""/>
<part name="FRAME1" library="frames" deviceset="DINA4_L" device="" value="DNP"/>
<part name="FID1" library="BaseApp" deviceset="FIDUCIAL" device="" value="DNP"/>
<part name="FID4" library="BaseApp" deviceset="FIDUCIAL" device="" value="DNP"/>
<part name="FID3" library="BaseApp" deviceset="FIDUCIAL" device="" value="DNP"/>
<part name="FID2" library="BaseApp" deviceset="FIDUCIAL" device="" value="DNP"/>
<part name="GND11" library="BaseApp" deviceset="GND" device=""/>
<part name="U1" library="BaseApp" deviceset="HTU20D" device="SMD"/>
<part name="C5" library="BaseApp" deviceset="CAPACITOR" device="-0.1UF/6.3V" value=".1uF"/>
<part name="GND5" library="BaseApp" deviceset="GND" device=""/>
<part name="U$1" library="BaseApp" deviceset="VDD" device=""/>
<part name="TAC" library="BaseApp" deviceset="TACTILE" device="-DIP"/>
<part name="R9" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="R8" library="BaseApp" deviceset="RESISTOR" device="-10K_1/8W" value="10K"/>
<part name="U$6" library="BaseApp" deviceset="PINHEAD-1X3" device=""/>
<part name="GND8" library="BaseApp" deviceset="GND" device=""/>
<part name="R6" library="BaseApp" deviceset="RESISTOR" device="-47K_1/8W" value="47K"/>
<part name="Q1" library="BaseApp" deviceset="PMOS" device="-FDN340"/>
<part name="U$4" library="BaseApp" deviceset="VDD" device=""/>
<part name="VBAT2" library="connectors &amp; holders" deviceset="BATTERY_HOLDER" device="AA" value="AA"/>
<part name="U$2" library="modules" deviceset="RFM69HW" device=""/>
<part name="JP2" library="pinhead" deviceset="PINHD-1X1" device="" value="ANT"/>
<part name="LED1" library="BaseApp" deviceset="DUOLED-GY-C" device="" value="DUOLED"/>
<part name="C4" library="passives" deviceset="CAPACITOR" device="-22UF/6.3V" value="22uF"/>
<part name="C3" library="passives" deviceset="CAPACITOR" device="-22UF/6.3V" value="22uF"/>
<part name="JP3" library="pinhead" deviceset="PINHD-2X3" device="" value="CON"/>
<part name="GND2" library="BaseApp" deviceset="GND" device=""/>
<part name="GND4" library="BaseApp" deviceset="GND" device=""/>
<part name="C6" library="BaseApp" deviceset="CAPACITOR" device="-1UF/6.3V" value="1UF/6.3V "/>
<part name="GND10" library="BaseApp" deviceset="GND" device=""/>
<part name="U$3" library="BaseApp" deviceset="VDD" device=""/>
<part name="R7" library="passives" deviceset="RESISTOR" device="-0R" value="DNP"/>
<part name="R10" library="passives" deviceset="RESISTOR" device="-0R" value="DNP"/>
<part name="R12" library="passives" deviceset="RESISTOR" device="-0R" value="DNP"/>
<part name="R14" library="passives" deviceset="RESISTOR" device="-0R" value="DNP"/>
<part name="R11" library="passives" deviceset="RESISTOR" device="-0R" value="DNP"/>
<part name="R13" library="passives" deviceset="RESISTOR" device="-0R" value="DNP"/>
<part name="IN" library="connectors &amp; holders" deviceset="PINHEAD-1X2" device=""/>
<part name="THCONN" library="BaseApp" deviceset="PINHEAD-1X4" device="-2.54MM" value="PINHEAD-1X4-2.54MM"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="167.64" y1="-38.1" x2="223.52" y2="-38.1" width="1.27" layer="94" style="shortdash"/>
<wire x1="223.52" y1="-38.1" x2="307.34" y2="-38.1" width="1.27" layer="94" style="shortdash"/>
<wire x1="307.34" y1="-38.1" x2="307.34" y2="-27.94" width="1.27" layer="94" style="shortdash"/>
<wire x1="307.34" y1="-27.94" x2="307.34" y2="55.88" width="1.27" layer="94" style="shortdash"/>
<wire x1="307.34" y1="55.88" x2="167.64" y2="55.88" width="1.27" layer="94" style="shortdash"/>
<wire x1="167.64" y1="55.88" x2="167.64" y2="-38.1" width="1.27" layer="94" style="shortdash"/>
<wire x1="167.64" y1="-38.1" x2="167.64" y2="-124.46" width="1.27" layer="94" style="shortdash"/>
<wire x1="167.64" y1="-124.46" x2="223.52" y2="-124.46" width="1.27" layer="94" style="shortdash"/>
<wire x1="223.52" y1="-124.46" x2="223.52" y2="-38.1" width="1.27" layer="94" style="shortdash"/>
<wire x1="307.34" y1="-38.1" x2="317.5" y2="-38.1" width="1.27" layer="94" style="shortdash"/>
<wire x1="317.5" y1="-38.1" x2="317.5" y2="-63.5" width="1.27" layer="94" style="shortdash"/>
<wire x1="317.5" y1="-63.5" x2="317.5" y2="-86.36" width="1.27" layer="94" style="shortdash"/>
<wire x1="317.5" y1="-86.36" x2="317.5" y2="-124.46" width="1.27" layer="94" style="shortdash"/>
<wire x1="317.5" y1="-124.46" x2="223.52" y2="-124.46" width="1.27" layer="94" style="shortdash"/>
<wire x1="317.5" y1="-63.5" x2="431.8" y2="-63.5" width="1.27" layer="94" style="shortdash"/>
<wire x1="431.8" y1="-63.5" x2="431.8" y2="-27.94" width="1.27" layer="94" style="shortdash"/>
<wire x1="431.8" y1="-27.94" x2="307.34" y2="-27.94" width="1.27" layer="94" style="shortdash"/>
<wire x1="307.34" y1="55.88" x2="431.8" y2="55.88" width="1.27" layer="94" style="shortdash"/>
<wire x1="431.8" y1="55.88" x2="431.8" y2="-27.94" width="1.27" layer="94" style="shortdash"/>
<wire x1="317.5" y1="-86.36" x2="431.8" y2="-86.36" width="1.27" layer="94" style="shortdash"/>
<wire x1="431.8" y1="-86.36" x2="431.8" y2="-63.5" width="1.27" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="GND1" gate="G$1" x="195.58" y="-99.06"/>
<instance part="STM8L" gate="G$1" x="223.52" y="30.48"/>
<instance part="U$5" gate="G$1" x="180.34" y="38.1" smashed="yes">
<attribute name="VALUE" x="177.8" y="38.1" size="1.016" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="G$1" x="180.34" y="2.54" smashed="yes"/>
<instance part="R2" gate="G$1" x="187.96" y="35.56"/>
<instance part="C2" gate="G$1" x="170.18" y="25.4"/>
<instance part="J100" gate="G$1" x="220.98" y="-10.16" rot="R270"/>
<instance part="R3" gate="G$1" x="337.82" y="17.78" rot="R180"/>
<instance part="GND6" gate="G$1" x="332.74" y="12.7" smashed="yes"/>
<instance part="C1" gate="G$1" x="375.92" y="30.48" smashed="yes">
<attribute name="NAME" x="374.904" y="33.401" size="1.016" layer="95"/>
<attribute name="VALUE" x="374.904" y="28.321" size="1.016" layer="96"/>
</instance>
<instance part="GND7" gate="G$1" x="378.46" y="25.4" smashed="yes"/>
<instance part="R4" gate="G$1" x="276.86" y="-5.08"/>
<instance part="R5" gate="G$1" x="246.38" y="-5.08"/>
<instance part="R1" gate="G$1" x="264.16" y="12.7" rot="R90"/>
<instance part="U$14" gate="G$1" x="264.16" y="20.32" smashed="yes">
<attribute name="VALUE" x="261.62" y="20.32" size="1.016" layer="96" rot="R90"/>
</instance>
<instance part="GND9" gate="G$1" x="292.1" y="2.54" smashed="yes"/>
<instance part="FRAME1" gate="G$1" x="167.64" y="-124.46"/>
<instance part="FRAME1" gate="G$2" x="330.2" y="-124.46"/>
<instance part="FID1" gate="G$1" x="345.44" y="-71.12"/>
<instance part="FID4" gate="G$1" x="355.6" y="-71.12"/>
<instance part="FID3" gate="G$1" x="365.76" y="-71.12"/>
<instance part="FID2" gate="G$1" x="375.92" y="-71.12"/>
<instance part="GND11" gate="G$1" x="261.62" y="-17.78" smashed="yes"/>
<instance part="U1" gate="U$1" x="365.76" y="-45.72"/>
<instance part="C5" gate="G$1" x="345.44" y="-48.26"/>
<instance part="GND5" gate="G$1" x="353.06" y="-55.88"/>
<instance part="U$1" gate="G$1" x="180.34" y="-83.82" smashed="yes">
<attribute name="VALUE" x="180.34" y="-83.82" size="1.016" layer="96" rot="R90"/>
</instance>
<instance part="TAC" gate="G$1" x="276.86" y="10.16"/>
<instance part="R9" gate="G$1" x="264.16" y="45.72" rot="R90"/>
<instance part="R8" gate="G$1" x="271.78" y="45.72" rot="R90"/>
<instance part="U$6" gate="G$1" x="187.96" y="-25.4" rot="R270"/>
<instance part="GND8" gate="G$1" x="195.58" y="-27.94" smashed="yes"/>
<instance part="R6" gate="G$1" x="332.74" y="43.18" rot="R90"/>
<instance part="Q1" gate="G$1" x="342.9" y="33.02" rot="MR180"/>
<instance part="U$4" gate="G$1" x="342.9" y="48.26" smashed="yes">
<attribute name="VALUE" x="340.36" y="48.26" size="1.016" layer="96" rot="R90"/>
</instance>
<instance part="VBAT2" gate="G$1" x="180.34" y="-91.44" rot="R90"/>
<instance part="U$2" gate="G$1" x="368.3" y="5.08"/>
<instance part="JP2" gate="G$1" x="360.68" y="-20.32" rot="R270"/>
<instance part="LED1" gate="G$1" x="261.62" y="-5.08" rot="R90"/>
<instance part="C4" gate="G$1" x="195.58" y="-93.98"/>
<instance part="C3" gate="G$1" x="187.96" y="-93.98"/>
<instance part="JP3" gate="A" x="269.24" y="-53.34"/>
<instance part="GND2" gate="G$1" x="289.56" y="-50.8"/>
<instance part="GND4" gate="G$1" x="276.86" y="-99.06"/>
<instance part="C6" gate="G$1" x="284.48" y="-91.44" smashed="yes">
<attribute name="NAME" x="283.464" y="-88.519" size="1.016" layer="95"/>
<attribute name="VALUE" x="283.464" y="-93.599" size="1.016" layer="96"/>
</instance>
<instance part="GND10" gate="G$1" x="254" y="-104.14"/>
<instance part="U$3" gate="G$1" x="261.62" y="-66.04" smashed="yes">
<attribute name="VALUE" x="259.08" y="-66.04" size="1.016" layer="96" rot="R90"/>
</instance>
<instance part="R7" gate="G$1" x="276.86" y="-76.2" rot="R90"/>
<instance part="R10" gate="G$1" x="261.62" y="-76.2" rot="R90"/>
<instance part="R12" gate="G$1" x="276.86" y="-91.44" rot="R90"/>
<instance part="R14" gate="G$1" x="266.7" y="-86.36" rot="R90"/>
<instance part="R11" gate="G$1" x="259.08" y="-91.44" rot="R180"/>
<instance part="R13" gate="G$1" x="254" y="-96.52" rot="R90"/>
<instance part="IN" gate="G$1" x="246.38" y="-91.44" rot="MR0"/>
<instance part="THCONN" gate="G$1" x="243.84" y="-22.86"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="G$1" pin="GND"/>
<wire x1="187.96" y1="-99.06" x2="195.58" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-96.52" x2="187.96" y2="-99.06" width="0.1524" layer="91"/>
<junction x="187.96" y="-99.06"/>
<wire x1="195.58" y1="-96.52" x2="195.58" y2="-99.06" width="0.1524" layer="91"/>
<junction x="195.58" y="-99.06"/>
<wire x1="180.34" y1="-99.06" x2="187.96" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-96.52" x2="180.34" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="VBAT2" gate="G$1" pin="-"/>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="C3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="VSS/VREF-/VSSA"/>
<wire x1="203.2" y1="25.4" x2="180.34" y2="25.4" width="0.1524" layer="91"/>
<wire x1="180.34" y1="25.4" x2="180.34" y2="22.86" width="0.1524" layer="91"/>
<pinref part="GND3" gate="G$1" pin="GND"/>
<wire x1="180.34" y1="22.86" x2="180.34" y2="2.54" width="0.1524" layer="91"/>
<junction x="180.34" y="22.86"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="180.34" y1="22.86" x2="170.18" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J100" gate="G$1" pin="3"/>
<wire x1="218.44" y1="-7.62" x2="218.44" y2="7.62" width="0.1524" layer="91"/>
<label x="218.44" y="2.54" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="332.74" y1="17.78" x2="332.74" y2="12.7" width="0.1524" layer="91"/>
<pinref part="GND6" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="373.38" y1="27.94" x2="375.92" y2="27.94" width="0.1524" layer="91"/>
<wire x1="370.84" y1="25.4" x2="370.84" y2="27.94" width="0.1524" layer="91"/>
<wire x1="370.84" y1="27.94" x2="373.38" y2="27.94" width="0.1524" layer="91"/>
<junction x="373.38" y="27.94"/>
<wire x1="375.92" y1="27.94" x2="378.46" y2="27.94" width="0.1524" layer="91"/>
<wire x1="378.46" y1="27.94" x2="378.46" y2="25.4" width="0.1524" layer="91"/>
<junction x="375.92" y="27.94"/>
<pinref part="GND7" gate="G$1" pin="GND"/>
<pinref part="U$2" gate="G$1" pin="GND"/>
<pinref part="U$2" gate="G$1" pin="GND@1"/>
<wire x1="373.38" y1="27.94" x2="373.38" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="292.1" y1="7.62" x2="292.1" y2="2.54" width="0.1524" layer="91"/>
<pinref part="GND9" gate="G$1" pin="GND"/>
<pinref part="TAC" gate="G$1" pin="4"/>
<wire x1="284.48" y1="7.62" x2="292.1" y2="7.62" width="0.1524" layer="91"/>
<pinref part="TAC" gate="G$1" pin="3"/>
<wire x1="284.48" y1="10.16" x2="284.48" y2="7.62" width="0.1524" layer="91"/>
<junction x="284.48" y="7.62"/>
</segment>
<segment>
<pinref part="GND11" gate="G$1" pin="GND"/>
<wire x1="261.62" y1="-7.62" x2="261.62" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="C"/>
</segment>
<segment>
<pinref part="U1" gate="U$1" pin="GND@2"/>
<wire x1="355.6" y1="-45.72" x2="353.06" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="353.06" y1="-45.72" x2="353.06" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="U1" gate="U$1" pin="GND@7"/>
<wire x1="353.06" y1="-48.26" x2="353.06" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="355.6" y1="-48.26" x2="353.06" y2="-48.26" width="0.1524" layer="91"/>
<junction x="353.06" y="-48.26"/>
<pinref part="GND5" gate="G$1" pin="GND"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="345.44" y1="-50.8" x2="345.44" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="345.44" y1="-55.88" x2="353.06" y2="-55.88" width="0.1524" layer="91"/>
<junction x="353.06" y="-55.88"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="1"/>
<wire x1="190.5" y1="-22.86" x2="195.58" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-22.86" x2="195.58" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="GND8" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND2" gate="G$1" pin="GND"/>
<pinref part="JP3" gate="A" pin="2"/>
<wire x1="289.56" y1="-50.8" x2="274.32" y2="-50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="G$1" pin="GND"/>
<wire x1="276.86" y1="-99.06" x2="276.86" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="276.86" y1="-96.52" x2="284.48" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="284.48" y1="-96.52" x2="284.48" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<junction x="276.86" y="-96.52"/>
</segment>
<segment>
<pinref part="GND10" gate="G$1" pin="GND"/>
<wire x1="254" y1="-104.14" x2="254" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="THCONN" gate="G$1" pin="4"/>
<wire x1="241.3" y1="-27.94" x2="228.6" y2="-27.94" width="0.1524" layer="91"/>
<label x="226.06" y="-27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="IO1" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PB3"/>
<wire x1="243.84" y1="25.4" x2="259.08" y2="25.4" width="0.1524" layer="91"/>
<label x="246.38" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="264.16" y1="-91.44" x2="266.7" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-91.44" x2="269.24" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="269.24" y1="-91.44" x2="271.78" y2="-91.44" width="0.1524" layer="91"/>
<label x="269.24" y="-91.44" size="1.778" layer="95"/>
<pinref part="R14" gate="G$1" pin="1"/>
<junction x="266.7" y="-91.44"/>
<pinref part="R11" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="5"/>
<wire x1="266.7" y1="-55.88" x2="251.46" y2="-55.88" width="0.1524" layer="91"/>
<label x="251.46" y="-55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="IO2" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PB2"/>
<wire x1="243.84" y1="22.86" x2="259.08" y2="22.86" width="0.1524" layer="91"/>
<label x="246.38" y="22.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="3"/>
<wire x1="266.7" y1="-53.34" x2="251.46" y2="-53.34" width="0.1524" layer="91"/>
<label x="251.46" y="-53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWIM" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PA0"/>
<wire x1="203.2" y1="38.1" x2="193.04" y2="38.1" width="0.1524" layer="91"/>
<label x="193.04" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J100" gate="G$1" pin="2"/>
<wire x1="220.98" y1="-7.62" x2="220.98" y2="7.62" width="0.1524" layer="91"/>
<label x="220.98" y="2.54" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="NRST" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PA1/NRST"/>
<wire x1="203.2" y1="35.56" x2="193.04" y2="35.56" width="0.1524" layer="91"/>
<label x="193.04" y="35.56" size="1.778" layer="95"/>
<pinref part="R2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="J100" gate="G$1" pin="4"/>
<wire x1="215.9" y1="-7.62" x2="215.9" y2="7.62" width="0.1524" layer="91"/>
<label x="215.9" y="2.54" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="VDD/VREF+/VDDA"/>
<wire x1="203.2" y1="27.94" x2="180.34" y2="27.94" width="0.1524" layer="91"/>
<wire x1="180.34" y1="27.94" x2="180.34" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="VDD"/>
<wire x1="180.34" y1="30.48" x2="180.34" y2="35.56" width="0.1524" layer="91"/>
<wire x1="180.34" y1="35.56" x2="180.34" y2="38.1" width="0.1524" layer="91"/>
<junction x="180.34" y="30.48"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="182.88" y1="35.56" x2="180.34" y2="35.56" width="0.1524" layer="91"/>
<junction x="180.34" y="35.56"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="180.34" y1="30.48" x2="170.18" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J100" gate="G$1" pin="1"/>
<wire x1="223.52" y1="-7.62" x2="223.52" y2="7.62" width="0.1524" layer="91"/>
<label x="223.52" y="2.54" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="U$14" gate="G$1" pin="VDD"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="264.16" y1="20.32" x2="264.16" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="180.34" y1="-83.82" x2="180.34" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-88.9" x2="187.96" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="187.96" y1="-88.9" x2="195.58" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VDD"/>
<pinref part="VBAT2" gate="G$1" pin="+"/>
<junction x="180.34" y="-88.9"/>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="C3" gate="G$1" pin="1"/>
<junction x="187.96" y="-88.9"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="S"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="342.9" y1="48.26" x2="342.9" y2="40.64" width="0.1524" layer="91"/>
<wire x1="332.74" y1="48.26" x2="342.9" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="VDD"/>
<junction x="342.9" y="48.26"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="VDD"/>
<wire x1="261.62" y1="-66.04" x2="261.62" y2="-71.12" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PB7"/>
<wire x1="243.84" y1="35.56" x2="259.08" y2="35.56" width="0.1524" layer="91"/>
<label x="246.38" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="347.98" y1="12.7" x2="337.82" y2="12.7" width="0.1524" layer="91"/>
<label x="337.82" y="12.7" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="MISO"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PB6"/>
<wire x1="243.84" y1="33.02" x2="259.08" y2="33.02" width="0.1524" layer="91"/>
<label x="246.38" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="345.44" y1="10.16" x2="337.82" y2="10.16" width="0.1524" layer="91"/>
<label x="337.82" y="10.16" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="MOSI"/>
<wire x1="345.44" y1="10.16" x2="347.98" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PB5"/>
<wire x1="243.84" y1="30.48" x2="259.08" y2="30.48" width="0.1524" layer="91"/>
<label x="246.38" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="347.98" y1="7.62" x2="337.82" y2="7.62" width="0.1524" layer="91"/>
<label x="337.82" y="7.62" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="SCK"/>
</segment>
</net>
<net name="SS" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PB4"/>
<wire x1="243.84" y1="27.94" x2="259.08" y2="27.94" width="0.1524" layer="91"/>
<label x="246.38" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="347.98" y1="0" x2="337.82" y2="0" width="0.1524" layer="91"/>
<label x="337.82" y="0" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="NSS"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="347.98" y1="17.78" x2="342.9" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="RESET"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PA2"/>
<wire x1="203.2" y1="33.02" x2="193.04" y2="33.02" width="0.1524" layer="91"/>
<label x="193.04" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="3"/>
<wire x1="185.42" y1="-22.86" x2="185.42" y2="-10.16" width="0.1524" layer="91"/>
<label x="185.42" y="-17.78" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PA3"/>
<wire x1="203.2" y1="30.48" x2="193.04" y2="30.48" width="0.1524" layer="91"/>
<label x="193.04" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="2"/>
<wire x1="187.96" y1="-22.86" x2="187.96" y2="-10.16" width="0.1524" layer="91"/>
<label x="187.96" y="-17.78" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="281.94" y1="-5.08" x2="289.56" y2="-5.08" width="0.1524" layer="91"/>
<label x="281.94" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="203.2" y1="22.86" x2="190.5" y2="22.86" width="0.1524" layer="91"/>
<label x="193.04" y="22.86" size="1.778" layer="95"/>
<pinref part="STM8L" gate="G$1" pin="PD0"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="241.3" y1="-5.08" x2="233.68" y2="-5.08" width="0.1524" layer="91"/>
<label x="233.68" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="203.2" y1="20.32" x2="190.5" y2="20.32" width="0.1524" layer="91"/>
<label x="193.04" y="20.32" size="1.778" layer="95"/>
<pinref part="STM8L" gate="G$1" pin="PB0"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="271.78" y1="-5.08" x2="269.24" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="AY"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="254" y1="-5.08" x2="251.46" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="AG"/>
</segment>
</net>
<net name="BTN" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="264.16" y1="7.62" x2="269.24" y2="7.62" width="0.1524" layer="91"/>
<pinref part="TAC" gate="G$1" pin="2"/>
<wire x1="264.16" y1="7.62" x2="254" y2="7.62" width="0.1524" layer="91"/>
<junction x="264.16" y="7.62"/>
<label x="254" y="7.62" size="1.778" layer="95"/>
<pinref part="TAC" gate="G$1" pin="1"/>
<wire x1="269.24" y1="10.16" x2="269.24" y2="7.62" width="0.1524" layer="91"/>
<junction x="269.24" y="7.62"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PC4"/>
<wire x1="259.08" y1="43.18" x2="243.84" y2="43.18" width="0.1524" layer="91"/>
<label x="246.38" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="DATA"/>
<wire x1="375.92" y1="-43.18" x2="383.54" y2="-43.18" width="0.1524" layer="91"/>
<label x="375.92" y="-43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PC0"/>
<wire x1="243.84" y1="38.1" x2="271.78" y2="38.1" width="0.1524" layer="91"/>
<label x="246.38" y="38.1" size="1.778" layer="95"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="271.78" y1="38.1" x2="271.78" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="4"/>
<wire x1="284.48" y1="-53.34" x2="274.32" y2="-53.34" width="0.1524" layer="91"/>
<label x="279.4" y="-53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="THCONN" gate="G$1" pin="3"/>
<wire x1="241.3" y1="-25.4" x2="228.6" y2="-25.4" width="0.1524" layer="91"/>
<label x="226.06" y="-25.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U1" gate="U$1" pin="SCK"/>
<wire x1="375.92" y1="-45.72" x2="383.54" y2="-45.72" width="0.1524" layer="91"/>
<label x="375.92" y="-45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PC1"/>
<wire x1="243.84" y1="40.64" x2="264.16" y2="40.64" width="0.1524" layer="91"/>
<label x="246.38" y="40.64" size="1.778" layer="95"/>
<pinref part="R9" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="JP3" gate="A" pin="6"/>
<wire x1="274.32" y1="-55.88" x2="284.48" y2="-55.88" width="0.1524" layer="91"/>
<label x="279.4" y="-55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="THCONN" gate="G$1" pin="2"/>
<wire x1="241.3" y1="-22.86" x2="228.6" y2="-22.86" width="0.1524" layer="91"/>
<label x="226.06" y="-22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="ANT" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="ANA"/>
<wire x1="368.3" y1="-15.24" x2="360.68" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="1"/>
<wire x1="360.68" y1="-17.78" x2="360.68" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DIO2" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="DIO2"/>
<wire x1="388.62" y1="10.16" x2="396.24" y2="10.16" width="0.1524" layer="91"/>
<label x="393.7" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PC6"/>
<wire x1="203.2" y1="40.64" x2="193.04" y2="40.64" width="0.1524" layer="91"/>
<label x="193.04" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIO1" class="0">
<segment>
<wire x1="393.7" y1="17.78" x2="396.24" y2="17.78" width="0.1524" layer="91"/>
<label x="393.7" y="17.78" size="1.778" layer="95"/>
<pinref part="U$2" gate="G$1" pin="DIO0"/>
<wire x1="393.7" y1="17.78" x2="388.62" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="STM8L" gate="G$1" pin="PC5"/>
<wire x1="203.2" y1="43.18" x2="193.04" y2="43.18" width="0.1524" layer="91"/>
<label x="193.04" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="SENSOR_PWR" class="0">
<segment>
<pinref part="STM8L" gate="G$1" pin="PB1"/>
<wire x1="243.84" y1="20.32" x2="259.08" y2="20.32" width="0.1524" layer="91"/>
<label x="246.38" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="332.74" y1="38.1" x2="317.5" y2="38.1" width="0.1524" layer="91"/>
<label x="317.5" y="38.1" size="1.778" layer="95"/>
<pinref part="Q1" gate="G$1" pin="G"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="332.74" y1="38.1" x2="335.28" y2="38.1" width="0.1524" layer="91"/>
<junction x="332.74" y="38.1"/>
</segment>
</net>
<net name="SENSOR_VDD" class="0">
<segment>
<wire x1="251.46" y1="-50.8" x2="256.54" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="256.54" y1="-50.8" x2="266.7" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="JP3" gate="A" pin="1"/>
<label x="243.84" y="-50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="264.16" y1="50.8" x2="271.78" y2="50.8" width="0.1524" layer="91"/>
<wire x1="271.78" y1="50.8" x2="276.86" y2="50.8" width="0.1524" layer="91"/>
<junction x="271.78" y="50.8"/>
<label x="266.7" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="D"/>
<wire x1="342.9" y1="25.4" x2="347.98" y2="25.4" width="0.1524" layer="91"/>
<wire x1="365.76" y1="25.4" x2="365.76" y2="35.56" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="365.76" y1="35.56" x2="375.92" y2="35.56" width="0.1524" layer="91"/>
<wire x1="358.14" y1="25.4" x2="365.76" y2="25.4" width="0.1524" layer="91"/>
<junction x="365.76" y="25.4"/>
<pinref part="U$2" gate="G$1" pin="3.3V"/>
<wire x1="347.98" y1="25.4" x2="358.14" y2="25.4" width="0.1524" layer="91"/>
<label x="347.98" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="276.86" y1="-71.12" x2="287.02" y2="-71.12" width="0.1524" layer="91"/>
<label x="281.94" y="-71.12" size="1.778" layer="95"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U1" gate="U$1" pin="VDD"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="355.6" y1="-43.18" x2="345.44" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="345.44" y1="-43.18" x2="335.28" y2="-43.18" width="0.1524" layer="91"/>
<junction x="345.44" y="-43.18"/>
<label x="335.28" y="-43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="THCONN" gate="G$1" pin="1"/>
<wire x1="241.3" y1="-20.32" x2="228.6" y2="-20.32" width="0.1524" layer="91"/>
<label x="226.06" y="-20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="IN" gate="G$1" pin="2"/>
<wire x1="254" y1="-91.44" x2="248.92" y2="-91.44" width="0.1524" layer="91"/>
<junction x="254" y="-91.44"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="259.08" y1="-88.9" x2="259.08" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-86.36" x2="259.08" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-81.28" x2="261.62" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="261.62" y1="-81.28" x2="266.7" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="276.86" y1="-81.28" x2="276.86" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="266.7" y1="-81.28" x2="276.86" y2="-81.28" width="0.1524" layer="91"/>
<junction x="276.86" y="-81.28"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="284.48" y1="-86.36" x2="284.48" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="284.48" y1="-81.28" x2="281.94" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="281.94" y1="-81.28" x2="276.86" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="R10" gate="G$1" pin="1"/>
<junction x="261.62" y="-81.28"/>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="R14" gate="G$1" pin="2"/>
<junction x="266.7" y="-81.28"/>
<pinref part="IN" gate="G$1" pin="1"/>
<wire x1="259.08" y1="-88.9" x2="248.92" y2="-88.9" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
