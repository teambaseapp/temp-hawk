<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.01" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="120" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="allan">
<packages>
<package name="SOIC127P600X175-16N">
<wire x1="-2.0066" y1="4.191" x2="-2.0066" y2="4.699" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="4.699" x2="-3.0988" y2="4.699" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="4.699" x2="-3.0988" y2="4.191" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="4.191" x2="-2.0066" y2="4.191" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.921" x2="-2.0066" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="3.429" x2="-3.0988" y2="3.429" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="3.429" x2="-3.0988" y2="2.921" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.921" x2="-2.0066" y2="2.921" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="1.651" x2="-2.0066" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="2.159" x2="-3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="2.159" x2="-3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="1.651" x2="-2.0066" y2="1.651" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.381" x2="-2.0066" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="0.889" x2="-3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.889" x2="-3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="0.381" x2="-2.0066" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.889" x2="-2.0066" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-0.381" x2="-3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.381" x2="-3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-0.889" x2="-2.0066" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.159" x2="-2.0066" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-1.651" x2="-3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-1.651" x2="-3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.159" x2="-2.0066" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-3.429" x2="-2.0066" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-2.921" x2="-3.0988" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-2.921" x2="-3.0988" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-3.429" x2="-2.0066" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-4.699" x2="-2.0066" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-4.191" x2="-3.0988" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-4.191" x2="-3.0988" y2="-4.699" width="0.1524" layer="51"/>
<wire x1="-3.0988" y1="-4.699" x2="-2.0066" y2="-4.699" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-4.191" x2="2.0066" y2="-4.699" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-4.699" x2="3.0988" y2="-4.699" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-4.699" x2="3.0988" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-4.191" x2="2.0066" y2="-4.191" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.921" x2="2.0066" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-3.429" x2="3.0988" y2="-3.429" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-3.429" x2="3.0988" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.921" x2="2.0066" y2="-2.921" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-1.651" x2="2.0066" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-2.159" x2="3.0988" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="2.0066" y2="-1.651" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.381" x2="2.0066" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.889" x2="3.0988" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.889" x2="3.0988" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="-0.381" x2="2.0066" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.889" x2="2.0066" y2="0.381" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.381" x2="3.0988" y2="0.381" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.381" x2="3.0988" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="0.889" x2="2.0066" y2="0.889" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.159" x2="2.0066" y2="1.651" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="1.651" x2="3.0988" y2="1.651" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="1.651" x2="3.0988" y2="2.159" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.159" x2="2.0066" y2="2.159" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="3.429" x2="2.0066" y2="2.921" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="2.921" x2="3.0988" y2="2.921" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="2.921" x2="3.0988" y2="3.429" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="3.429" x2="2.0066" y2="3.429" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="4.699" x2="2.0066" y2="4.191" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="4.191" x2="3.0988" y2="4.191" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="4.191" x2="3.0988" y2="4.699" width="0.1524" layer="51"/>
<wire x1="3.0988" y1="4.699" x2="2.0066" y2="4.699" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="-5.0038" x2="2.0066" y2="-5.0038" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-5.0038" x2="2.0066" y2="5.0038" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="5.0038" x2="0.3048" y2="5.0038" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="5.0038" x2="-0.3048" y2="5.0038" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="5.0038" x2="-2.0066" y2="5.0038" width="0.1524" layer="51"/>
<wire x1="-2.0066" y1="5.0038" x2="-2.0066" y2="-5.0038" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="5.0038" x2="-0.3048" y2="5.0038" width="0.1524" layer="51" curve="-180"/>
<text x="-3.2004" y="4.8768" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<wire x1="-1.0922" y1="-5.0038" x2="1.0922" y2="-5.0038" width="0.1524" layer="21"/>
<wire x1="1.0922" y1="5.0038" x2="0.3048" y2="5.0038" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="5.0038" x2="-0.3048" y2="5.0038" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="5.0038" x2="-1.0922" y2="5.0038" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="5.0038" x2="-0.3048" y2="5.0038" width="0.1524" layer="21" curve="-180"/>
<wire x1="3.9624" y1="-2.9972" x2="3.9624" y2="-3.3528" width="0.1524" layer="49"/>
<wire x1="3.9624" y1="-3.3528" x2="3.7084" y2="-3.3528" width="0.1524" layer="49"/>
<wire x1="3.7084" y1="-3.3528" x2="3.7084" y2="-2.9972" width="0.1524" layer="49"/>
<text x="-3.2004" y="4.8768" size="1.27" layer="49" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="6.985" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-8.255" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<smd name="1" x="-2.3622" y="4.445" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="2" x="-2.3622" y="3.175" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="3" x="-2.3622" y="1.905" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="4" x="-2.3622" y="0.635" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="5" x="-2.3622" y="-0.635" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="6" x="-2.3622" y="-1.905" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="7" x="-2.3622" y="-3.175" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="8" x="-2.3622" y="-4.445" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="9" x="2.3622" y="-4.445" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="10" x="2.3622" y="-3.175" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="11" x="2.3622" y="-1.905" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="12" x="2.3622" y="-0.635" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="13" x="2.3622" y="0.635" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="14" x="2.3622" y="1.905" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="15" x="2.3622" y="3.175" dx="2.1844" dy="0.5588" layer="1"/>
<smd name="16" x="2.3622" y="4.445" dx="2.1844" dy="0.5588" layer="1"/>
</package>
<package name="R0603">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-2.032" y="0.635" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.032" y="-2.159" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.63" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.63" width="0.127" layer="21"/>
</package>
<package name="R-0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-1.3" y1="0.473" x2="-1.3" y2="-0.473" width="0.127" layer="21"/>
<wire x1="-1.3" y1="-0.473" x2="1.2" y2="-0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.473" x2="1.2" y2="0.473" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.473" x2="-1.3" y2="0.473" width="0.127" layer="21"/>
</package>
<package name="10555-METRIC">
<smd name="P$1" x="-4.85" y="0" dx="3.3" dy="2" layer="1"/>
<smd name="P$2" x="4.35" y="0" dx="3.3" dy="2" layer="1"/>
<wire x1="-6.75" y1="3" x2="-6.75" y2="-3" width="0.127" layer="21"/>
<wire x1="-6.75" y1="-3" x2="6.25" y2="-3" width="0.127" layer="21"/>
<wire x1="6.25" y1="-3" x2="6.25" y2="3" width="0.127" layer="21"/>
<wire x1="6.25" y1="3" x2="-6.75" y2="3" width="0.127" layer="21"/>
<text x="-2.75" y="3" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.75" y="-2.75" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0805-METRIC">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="C0603">
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1.905" y="0.835" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.105" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<wire x1="-1.7" y1="0.63" x2="-1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="0.66" x2="1.7" y2="0.66" width="0.127" layer="21"/>
<wire x1="1.7" y1="0.63" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.66" x2="1.7" y2="-0.66" width="0.127" layer="21"/>
</package>
<package name="R0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="-0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="-2" y2="0.75" width="0.127" layer="21"/>
</package>
<package name="R2512">
<smd name="P$1" x="0" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<smd name="P$2" x="6.5024" y="0" dx="1.778" dy="3.2512" layer="1" rot="R180"/>
<wire x1="-1.27" y1="1.9304" x2="-1.27" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.9304" x2="7.7724" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="-1.9304" x2="7.7724" y2="1.9304" width="0.127" layer="21"/>
<wire x1="7.7724" y1="1.9304" x2="-1.27" y2="1.9304" width="0.127" layer="21"/>
<text x="1.016" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="0.508" y="-3.556" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="5.334" y1="1.524" x2="1.016" y2="1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="1.524" x2="1.016" y2="-1.524" width="0.127" layer="51"/>
<wire x1="1.016" y1="-1.524" x2="5.334" y2="-1.524" width="0.127" layer="51"/>
<wire x1="5.334" y1="-1.524" x2="5.334" y2="1.524" width="0.127" layer="51"/>
</package>
<package name="MELF0204">
<smd name="P$1" x="-0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<smd name="P$2" x="0.8" y="0" dx="1.27" dy="0.635" layer="1" rot="R90"/>
<wire x1="-1.2" y1="0.8" x2="1.2" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="0.8" x2="1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.2" y1="-0.8" x2="-1.2" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-0.8" x2="-1.2" y2="0.8" width="0.127" layer="21"/>
<text x="-0.8" y="1.2" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.8" y="-2.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R2616">
<smd name="P$1" x="0" y="0" dx="3" dy="3" layer="1" rot="R90"/>
<smd name="P$2" x="5.85" y="0" dx="3" dy="3" layer="1" rot="R90"/>
<wire x1="-1.5" y1="2" x2="7.5" y2="2" width="0.127" layer="21"/>
<wire x1="7.5" y1="2" x2="7.5" y2="-2" width="0.127" layer="21"/>
<wire x1="7.5" y1="-2" x2="-1.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="2" width="0.127" layer="21"/>
</package>
<package name="R-1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="-2.4" y1="1" x2="2.4" y2="1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1" x2="-2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-2.4" y2="1" width="0.127" layer="21"/>
</package>
<package name="R1210">
<smd name="P$1" x="-1.35" y="0" dx="1" dy="3" layer="1"/>
<smd name="P$2" x="1.35" y="0" dx="1" dy="3" layer="1"/>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.127" layer="21"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<text x="-1.5" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0612">
<wire x1="-2.8448" y1="2.9718" x2="2.8448" y2="2.9718" width="0.127" layer="21"/>
<wire x1="-2.8448" y1="-2.9718" x2="2.8448" y2="-2.9718" width="0.127" layer="21"/>
<wire x1="2.8448" y1="2.9718" x2="2.8448" y2="-2.9718" width="0.127" layer="21"/>
<wire x1="-2.8448" y1="-2.9718" x2="-2.8448" y2="2.9718" width="0.127" layer="21"/>
<text x="-0.1651" y="3.4036" size="0.6096" layer="25" align="center">&gt;NAME</text>
<smd name="1" x="-2.032" y="0" dx="1.27" dy="5.588" layer="1"/>
<smd name="2" x="2.032" y="0" dx="1.27" dy="5.588" layer="1"/>
</package>
<package name="R1020">
<wire x1="-2.8448" y1="2.9718" x2="2.8448" y2="2.9718" width="0.127" layer="21"/>
<wire x1="-2.8448" y1="-2.9718" x2="2.8448" y2="-2.9718" width="0.127" layer="21"/>
<wire x1="2.8448" y1="2.9718" x2="2.8448" y2="-2.9718" width="0.127" layer="21"/>
<wire x1="-2.8448" y1="-2.9718" x2="-2.8448" y2="2.9718" width="0.127" layer="21"/>
<text x="-0.1651" y="3.4036" size="0.6096" layer="25" align="center">&gt;NAME</text>
<smd name="1" x="-2.032" y="0" dx="1.27" dy="5.588" layer="1"/>
<smd name="2" x="2.032" y="0" dx="1.27" dy="5.588" layer="1"/>
</package>
<package name="C0402">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-0.508" x2="1.27" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="-0.508" x2="1.27" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.27" y1="0.508" x2="-1.27" y2="0.508" width="0.127" layer="21"/>
</package>
<package name="FILM-CAPACITOR-0.47U/400V">
<wire x1="-9.25" y1="3.9" x2="9.25" y2="3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="3.9" x2="9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="9.25" y1="-3.9" x2="-9.25" y2="-3.9" width="0.127" layer="21"/>
<wire x1="-9.25" y1="-3.9" x2="-9.25" y2="3.9" width="0.127" layer="21"/>
<pad name="P$1" x="-7.5" y="0" drill="0.9"/>
<pad name="P$2" x="7.5" y="0" drill="0.9"/>
<text x="-2.54" y="4.17" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-4.17" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="C0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="51"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="51"/>
<smd name="1" x="-1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2" y1="0.75" x2="-2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="2" y1="0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="0.75" x2="2" y2="0.75" width="0.127" layer="21"/>
<wire x1="-2" y1="-0.75" x2="2" y2="-0.75" width="0.127" layer="21"/>
</package>
<package name="C1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<wire x1="-2.4" y1="1" x2="2.4" y2="1" width="0.127" layer="21"/>
<wire x1="2.4" y1="1" x2="2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1" x2="-2.4" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1" x2="-2.4" y2="1" width="0.127" layer="21"/>
</package>
<package name="C1210">
<smd name="P$1" x="-1.35" y="0" dx="1" dy="3" layer="1"/>
<smd name="P$2" x="1.35" y="0" dx="1" dy="3" layer="1"/>
<wire x1="-2" y1="1.7" x2="2" y2="1.7" width="0.127" layer="21"/>
<wire x1="2" y1="1.7" x2="2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="2" y1="-1.7" x2="-2" y2="-1.7" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.7" x2="-2" y2="1.7" width="0.127" layer="21"/>
<text x="-1.5" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-3" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="DIO-214AA(SMB)">
<wire x1="-2.4" y1="2" x2="2.4" y2="2" width="0.2" layer="21"/>
<wire x1="-2.4" y1="-2" x2="2.4" y2="-2" width="0.2" layer="21"/>
<circle x="-4.191" y="0" radius="0.14141875" width="0.2" layer="21"/>
<wire x1="-3.81" y1="2.286" x2="3.81" y2="2.286" width="0.05" layer="39"/>
<wire x1="3.81" y1="2.286" x2="3.81" y2="-2.286" width="0.05" layer="39"/>
<wire x1="3.81" y1="-2.286" x2="-3.81" y2="-2.286" width="0.05" layer="39"/>
<wire x1="-3.81" y1="-2.286" x2="-3.81" y2="2.286" width="0.05" layer="39"/>
<wire x1="-2.4" y1="2" x2="-2.4" y2="1.6" width="0.2" layer="21"/>
<wire x1="2.4" y1="2" x2="2.4" y2="1.6" width="0.2" layer="21"/>
<wire x1="-2.4" y1="-2" x2="-2.4" y2="-1.65" width="0.2" layer="21"/>
<wire x1="2.4" y1="-2" x2="2.4" y2="-1.65" width="0.2" layer="21"/>
<text x="-2.85" y="2.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.05" y="-3.65" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="-2.413" y="0" dx="2.286" dy="2.54" layer="1"/>
<smd name="A" x="2.413" y="0" dx="2.286" dy="2.54" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="BQ24450">
<description>&lt;b&gt;Integrated charge controller for lead acid batteries&lt;/b&gt;&lt;br&gt;</description>
<wire x1="-16.51" y1="13.97" x2="-16.51" y2="-13.97" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-13.97" x2="17.78" y2="-13.97" width="0.254" layer="94"/>
<wire x1="17.78" y1="-13.97" x2="17.78" y2="13.97" width="0.254" layer="94"/>
<wire x1="17.78" y1="13.97" x2="-16.51" y2="13.97" width="0.254" layer="94"/>
<pin name="ISNS" x="-21.59" y="3.81" length="middle"/>
<pin name="ISNSM" x="-15.24" y="19.05" length="middle" rot="R270"/>
<pin name="ISNSP" x="-2.54" y="19.05" length="middle" rot="R270"/>
<pin name="IFB" x="1.27" y="19.05" length="middle" rot="R270"/>
<pin name="IN" x="-21.59" y="10.16" length="middle"/>
<pin name="GND" x="-3.81" y="-19.05" length="middle" rot="R90"/>
<pin name="PGOOD" x="22.86" y="-12.7" length="middle" rot="R180"/>
<pin name="BSTOP" x="-21.59" y="-2.54" length="middle"/>
<pin name="STAT2" x="-21.59" y="-8.89" length="middle"/>
<pin name="STAT1" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PRE-CHG" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="CE" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="VFB" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="COMP" x="3.81" y="-19.05" length="middle" rot="R90"/>
<pin name="DRVE" x="16.51" y="19.05" length="middle" rot="R270"/>
<pin name="DRCE" x="8.89" y="19.05" length="middle" rot="R270"/>
<text x="-12.7" y="15.24" size="1.27" layer="94" align="center">&gt;NAME</text>
<text x="0" y="-12.7" size="1.27" layer="94" align="center">&gt;VALUE</text>
</symbol>
<symbol name="RESISTOR">
<text x="-2.54" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-2.54" y="-3.81" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="CAPACITOR">
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
</symbol>
<symbol name="DIODE">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="0" y="2.032" size="1.27" layer="95" align="center">&gt;NAME</text>
<text x="0" y="-2.032" size="1.27" layer="96" align="center">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BQ24450" prefix="IC">
<gates>
<gate name="G$1" symbol="BQ24450" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-16N">
<connects>
<connect gate="G$1" pin="BSTOP" pad="8"/>
<connect gate="G$1" pin="CE" pad="12"/>
<connect gate="G$1" pin="COMP" pad="14"/>
<connect gate="G$1" pin="DRCE" pad="16"/>
<connect gate="G$1" pin="DRVE" pad="15"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="IFB" pad="4"/>
<connect gate="G$1" pin="IN" pad="5"/>
<connect gate="G$1" pin="ISNS" pad="1"/>
<connect gate="G$1" pin="ISNSM" pad="2"/>
<connect gate="G$1" pin="ISNSP" pad="3"/>
<connect gate="G$1" pin="PGOOD" pad="7"/>
<connect gate="G$1" pin="PRE-CHG" pad="11"/>
<connect gate="G$1" pin="STAT1" pad="10"/>
<connect gate="G$1" pin="STAT2" pad="9"/>
<connect gate="G$1" pin="VFB" pad="13"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="296-24367-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="6.07" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt; &lt;p&gt; 
Mount :-Surface mount</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-50R_1/8W_RF" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-49.9HRCT-ND  " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-50R_1/20W_RF" package="R-0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="FC0402-50BWCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="2.52"/>
</technology>
</technologies>
</device>
<device name="-POWER_RESISTOR" package="10555-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-100K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-100K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-1M_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00M-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-12K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-12.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-330R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF0603JT330RCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-30R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P30.0HCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0R" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P0.0GCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-4.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-24K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM24KCFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-560R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-560GRDKR-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.7K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-2.70K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-33R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM33.0CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-6.2K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM6.2KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-10.0K-CFCT-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.08"/>
</technology>
</technologies>
</device>
<device name="-10R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P10.0HCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5K_1/8W" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="71-CRCW0805-5K" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.14" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300K_/1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM300KCFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-33K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-33.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-110K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-110KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-20K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-20.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-140K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-140KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-84.5K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-84.5KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4823-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15K_1/8W" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-15.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-47.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10K_0.125W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-10KARCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/4W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM1.0KDCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-698R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-698HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.5K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-1.50K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-3.00K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.08" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1.58K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RC0603FR-071K58L" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-51.1R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-51.1HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.37K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.37KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-150GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-82GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-130R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-130GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12.1K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-12.1KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-22KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-270KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-100HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R" package="0805-METRIC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM470AZCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.13" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.0003R/3W-CURRENTSENSE" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="HCS2512FTL300CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.9625" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6.8R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-6.80ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.102" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-470ACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-270K/0.4W" package="MELF0204">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MMA-270KACT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.068" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-680GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0048" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="267K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-267KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-191K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-4862-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9.53K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-9.53KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-49.9K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-49.9KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-470GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.55K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.55KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.2K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.2KGRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-348K/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-348KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0057" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="27R/0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-27GRCT-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0044" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8.2R_2W" package="R2616">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="A103603CT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.56" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2.05K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-2.05KHRCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-13K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-13KGRCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3.3K" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-3.3KGRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-82K/1W" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF2512JT82K0CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.29" constant="no"/>
</technology>
</technologies>
</device>
<device name="-300R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-300GRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value=".01" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/4W" package="R-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM22CJCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-120R_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-120HRCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-120R_1/4W" package="R-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RMCF1206JT120RCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-68R_1/2W" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="541-68VCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-6.8K_1/2W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="P6.8KADCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.12" constant="no"/>
</technology>
</technologies>
</device>
<device name="-39K_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM39KKCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-330K_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM330KKCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="470K_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM470KKCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-3.3M_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM3.5MKCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-680K_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM680KKCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1K_1/2W" package="R-1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RNCP1206FTD1K00CT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM22KCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5.1R_1/2.5W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="RHM5.1KCT-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.5R_1/4W" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="A109810CT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.2" constant="no"/>
</technology>
</technologies>
</device>
<device name="-52.3K_1/10W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-52.3KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22R_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-22.0-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22K_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-22.0K-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.17" constant="no"/>
</technology>
</technologies>
</device>
<device name="-470R_1/8W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="MCT0603-470-CFCT-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.17" constant="no"/>
</technology>
</technologies>
</device>
<device name="-187K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-187KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100_UNITS" value="0.0052" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-26.1K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-26.1KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100_UNITS" value="0.0052" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-46.4K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-46.4KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100_UNITS" value="0.052" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-48.7K_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-48.7KHRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100_UNIT" value="0.0052" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-909R_0.1W" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-909HRCT-ND" constant="no"/>
<attribute name="PRICE_PER_100_UNIT" value="0.0052" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="0805-METRIC">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0.332R_2W" package="R1020">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="541-2518-1-ND" constant="no"/>
<attribute name="PRICE_PER_100_UNIT" value="0.3361" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.6" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.332R_1W" package="R0612">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="541-2503-1-ND" constant="no"/>
<attribute name="PRICE_PER_100_UNIT" value="0.3231" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.58" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" prefix="C" uservalue="yes">
<description>CAPACITOR</description>
<gates>
<gate name="G$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0.1UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1095-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-FILM_AC-CAP" package="FILM-CAPACITOR-0.47U/400V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1275-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1079-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.23"/>
</technology>
</technologies>
</device>
<device name="-33PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1055-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-18PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1052-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-DNP/NI" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="No Part" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2087-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3022-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-3386-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.11"/>
</technology>
</technologies>
</device>
<device name="-2.2UF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1821-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.12"/>
</technology>
</technologies>
</device>
<device name="-0.01UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1132-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-4700PF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2062-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.1"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/25V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1524-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3012-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.16" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7UF/25V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3335-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.19" constant="no"/>
</technology>
</technologies>
</device>
<device name="-4.7NF/10V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9092-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-18PF/10V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MOUSER" value="77-VJ0805A180GXQCBC" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.06" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/10V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1356-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.24" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-9730-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/6.3V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1024-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/25V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="587-1353-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.21" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-3303-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.26" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.1UF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1532-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22PF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-9031-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-8PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-5043-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10NF/16V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1926-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/6.3V" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1993-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.47" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V." package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-4112-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.18" constant="no"/>
</technology>
</technologies>
</device>
<device name="-10UF/6.3V-0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-3340-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="490-1494-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-2200PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1085-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-100PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1281-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.022UF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-150PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-1063-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-15PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1296-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-22NF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="445-1312-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/50V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="311-1909-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNITS" value="0.0367" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.11" constant="no"/>
</technology>
</technologies>
</device>
<device name="-1UF/10V-0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1863-1-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.031" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-47UF/16V" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-5514-2-ND " constant="no"/>
<attribute name="MANUFACTURER" value="C1210C476M4PACTU" constant="no"/>
<attribute name="MOUSER" value="80-C1210C476M4P" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.54" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="1.01" constant="no"/>
</technology>
</technologies>
</device>
<device name="-9PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2143-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.017" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-5.6PF/50V" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="399-7944-1-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.0193" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-12PF" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-1254-1-ND  " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.47UF" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-2377-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.8PF/100V" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="478-9758-1-ND" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.85" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0.22UF/50V" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1276-6478-1-ND" constant="no"/>
<attribute name="PRICE_PER_100_UNIT" value="0.0177" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="-SS24" package="DIO-214AA(SMB)">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="SS24FSCT-ND" constant="no"/>
<attribute name="PRICE_PER_100_UNIT" value="0.2262" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.48" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="transistor-power">
<description>&lt;b&gt;Power Transistors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SOT93">
<description>SOT-93&lt;p&gt;
grid 5.45 mm</description>
<wire x1="-7.62" y1="-6.35" x2="7.62" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="7.62" y1="6.223" x2="7.62" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="7.62" y1="6.223" x2="6.985" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-6.35" x2="-7.62" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="6.223" x2="-6.985" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="6.223" x2="-7.62" y2="6.223" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="6.985" x2="-7.62" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="6.985" x2="-7.62" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="9.525" x2="-3.1242" y2="13.3096" width="0.1524" layer="21"/>
<wire x1="6.985" y1="6.223" x2="6.985" y2="6.985" width="0.1524" layer="21"/>
<wire x1="6.985" y1="6.223" x2="-6.985" y2="6.223" width="0.1524" layer="21"/>
<wire x1="6.985" y1="6.985" x2="7.62" y2="6.985" width="0.1524" layer="21"/>
<wire x1="7.62" y1="6.985" x2="7.62" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-5.715" x2="6.985" y2="-5.715" width="0.0508" layer="21"/>
<wire x1="6.985" y1="5.715" x2="6.985" y2="-5.715" width="0.0508" layer="21"/>
<wire x1="6.985" y1="5.715" x2="-6.985" y2="5.715" width="0.0508" layer="21"/>
<wire x1="-6.985" y1="-5.715" x2="-6.985" y2="5.715" width="0.0508" layer="21"/>
<wire x1="0" y1="14.605" x2="3.1431" y2="13.3031" width="0.1524" layer="21" curve="-45.0001"/>
<wire x1="-3.1431" y1="13.3031" x2="0" y2="14.605" width="0.1524" layer="21" curve="-45.0001"/>
<wire x1="7.62" y1="9.525" x2="3.1242" y2="13.335" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-5.715" x2="7.62" y2="-6.35" width="0.0508" layer="51"/>
<wire x1="6.985" y1="5.715" x2="7.62" y2="6.35" width="0.0508" layer="51"/>
<wire x1="-6.985" y1="5.715" x2="-7.62" y2="6.35" width="0.0508" layer="51"/>
<wire x1="-6.985" y1="-5.715" x2="-7.62" y2="-6.35" width="0.0508" layer="51"/>
<circle x="0" y="10.16" radius="2.159" width="0.1524" layer="21"/>
<circle x="0" y="10.16" radius="5.08" width="0" layer="42"/>
<circle x="0" y="10.16" radius="5.08" width="0" layer="43"/>
<pad name="B" x="-5.461" y="-10.16" drill="1.397" shape="long" rot="R90"/>
<pad name="C" x="0" y="-10.16" drill="1.397" shape="long" rot="R90"/>
<pad name="E" x="5.461" y="-10.16" drill="1.397" shape="long" rot="R90"/>
<text x="-5.08" y="2.54" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="0" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.477" y="6.604" size="1.016" layer="21" ratio="10">A20,3mm</text>
<rectangle x1="-6.096" y1="-7.366" x2="-4.318" y2="-6.35" layer="21"/>
<rectangle x1="4.318" y1="-7.366" x2="6.096" y2="-6.35" layer="21"/>
<rectangle x1="-1.143" y1="-7.366" x2="1.143" y2="-6.35" layer="21"/>
<rectangle x1="-6.096" y1="-8.128" x2="-4.826" y2="-6.35" layer="21"/>
<rectangle x1="-0.635" y1="-8.128" x2="0.635" y2="-6.35" layer="21"/>
<rectangle x1="4.826" y1="-8.128" x2="6.096" y2="-6.35" layer="21"/>
<rectangle x1="-6.096" y1="-10.287" x2="-4.826" y2="-8.128" layer="51"/>
<rectangle x1="-0.635" y1="-10.287" x2="0.635" y2="-8.128" layer="51"/>
<rectangle x1="4.826" y1="-10.287" x2="6.096" y2="-8.128" layer="51"/>
<hole x="0" y="10.16" drill="4.1148"/>
</package>
</packages>
<symbols>
<symbol name="P-DAR">
<wire x1="2.54" y1="-2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.016" y2="1.016" width="0.1524" layer="94"/>
<wire x1="2.413" y1="1.651" x2="1.016" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="1.016" x2="1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.905" y1="1.905" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.016" y1="1.016" x2="1.651" y2="2.413" width="0.1524" layer="94"/>
<wire x1="1.651" y1="2.413" x2="2.413" y2="1.651" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="2.159" y2="1.651" width="0.254" layer="94"/>
<wire x1="2.159" y1="1.651" x2="1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.905" x2="1.651" y2="2.159" width="0.254" layer="94"/>
<wire x1="1.651" y1="2.159" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.651" y1="1.905" x2="1.905" y2="1.651" width="0.254" layer="94"/>
<text x="5.08" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-0.381" y1="-2.54" x2="0.381" y2="2.54" layer="94"/>
<pin name="B" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="E" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="C" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TIP147" prefix="Q">
<description>&lt;b&gt;PNP DARLINGTON TRANSISTOR&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="P-DAR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT93">
<connects>
<connect gate="1" pin="B" pad="B"/>
<connect gate="1" pin="C" pad="C"/>
<connect gate="1" pin="E" pad="E"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="symbols,holes &amp; pads">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<pin name="GND" x="0" y="0" visible="pad" length="short" direction="sup" rot="R270"/>
<text x="-2.54" y="-5.08" size="1.778" layer="95">GND</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="active">
<packages>
<package name="LS_A67K">
<smd name="C" x="-1.27" y="0" dx="3.5" dy="1.5" layer="1" rot="R90"/>
<smd name="A" x="1.27" y="0" dx="3.5" dy="1.5" layer="1" rot="R90"/>
<wire x1="-2.5" y1="2" x2="-2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2" x2="2.5" y2="-2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2" x2="2.5" y2="2" width="0.127" layer="21"/>
<wire x1="2.5" y1="2" x2="-2.5" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<circle x="0" y="1.524" radius="0.254" width="0.127" layer="21"/>
</package>
<package name="LS_Y876">
<smd name="A" x="1.5" y="0" dx="1" dy="0.7" layer="1" rot="R270"/>
<smd name="C" x="-1.5" y="0" dx="1" dy="0.7" layer="1" rot="R270"/>
<wire x1="-1.016" y1="-0.762" x2="1.016" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.762" x2="1.016" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.762" x2="-1.016" y2="0.762" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.762" x2="-1.016" y2="-0.762" width="0.127" layer="21"/>
<circle x="0" y="-0.254" radius="0.254" width="0.127" layer="21"/>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="LED0603">
<wire x1="0.45" y1="0.4" x2="-0.45" y2="0.4" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-0.4" x2="-0.45" y2="-0.4" width="0.1016" layer="51"/>
<smd name="C" x="0.85" y="0" dx="1" dy="1" layer="1" rot="R270"/>
<smd name="A" x="-0.85" y="0" dx="1" dy="1" layer="1" rot="R270"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.2" y1="-0.2" x2="1.1" y2="0.2" layer="51" rot="R270"/>
<rectangle x1="-1.1" y1="-0.2" x2="-0.2" y2="0.2" layer="51" rot="R270"/>
<rectangle x1="0.075" y1="0.225" x2="0.225" y2="0.525" layer="21" rot="R270"/>
<rectangle x1="0.075" y1="-0.525" x2="0.225" y2="-0.225" layer="21" rot="R270"/>
<rectangle x1="0" y1="-0.15" x2="0.3" y2="0.15" layer="21" rot="R270"/>
<wire x1="1.5" y1="0.7" x2="-1.6" y2="0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.7" x2="1.5" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.7" x2="1.5" y2="0.7" width="0.127" layer="21"/>
</package>
<package name="LED-5MM">
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.8796"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.8796"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IR-LED">
<circle x="0" y="0" radius="1.5" width="0.127" layer="51"/>
<pad name="+" x="-1.27" y="0" drill="0.5"/>
<pad name="-" x="1.27" y="0" drill="0.5"/>
<wire x1="1.5" y1="1.57" x2="1.5" y2="-1.57" width="0.127" layer="21"/>
<wire x1="-2.2" y1="0" x2="0" y2="2.2" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.2" y1="0" x2="0" y2="-2.2" width="0.127" layer="21" curve="90"/>
<wire x1="0" y1="2.2" x2="1.5" y2="1.6" width="0.127" layer="21" curve="-43.602819"/>
<wire x1="0" y1="-2.2" x2="1.5" y2="-1.6" width="0.127" layer="21" curve="43.602819"/>
</package>
<package name="LED-3MM">
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="-2.5908" y1="1.7272" x2="-1.8542" y2="1.7272" width="0.127" layer="21"/>
<wire x1="-2.2352" y1="1.3208" x2="-2.2352" y2="2.1082" width="0.127" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED">
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LS_A67K">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-" package="LS_Y876">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-RED" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-2512-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.09"/>
</technology>
</technologies>
</device>
<device name="-GREEN" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-3118-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17" constant="no"/>
</technology>
</technologies>
</device>
<device name="-YELLOW" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-1196-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17"/>
</technology>
</technologies>
</device>
<device name="-BLUE" package="LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="475-2816-1-ND"/>
<attribute name="PRICE_PER_UNIT" value="0.17"/>
</technology>
</technologies>
</device>
<device name="-5MM" package="LED-5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="C503B-RAN-CY0B0AA1-ND " constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.15" constant="no"/>
</technology>
</technologies>
</device>
<device name="-IR" package="IR-LED">
<connects>
<connect gate="G$1" pin="A" pad="+"/>
<connect gate="G$1" pin="C" pad="-"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1080-1076-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.1628" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.39" constant="no"/>
</technology>
</technologies>
</device>
<device name="RED" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="732-5005-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.224" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
<device name="-GRN" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="1125-1186-ND " constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.125" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.33" constant="no"/>
</technology>
</technologies>
</device>
<device name="-YLW" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="732-5010-ND" constant="no"/>
<attribute name="PRICE_PER_100UNIT" value="0.224" constant="no"/>
<attribute name="PRICE_PER_UNIT" value="0.28" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" deviceset="A4L-LOC" device="" value="FRAME"/>
<part name="IC1" library="allan" deviceset="BQ24450" device=""/>
<part name="R4" library="allan" deviceset="RESISTOR" device="-187K_0.1W" value="187K"/>
<part name="R5" library="allan" deviceset="RESISTOR" device="-26.1K_0.1W" value="26.1K"/>
<part name="R7" library="allan" deviceset="RESISTOR" device="-46.4K_0.1W" value="46.4K"/>
<part name="R6" library="allan" deviceset="RESISTOR" device="-48.7K_0.1W" value="48.7K"/>
<part name="R3" library="allan" deviceset="RESISTOR" device="-909R_0.1W" value="909R"/>
<part name="Q1" library="transistor-power" deviceset="TIP147" device=""/>
<part name="GND1" library="symbols,holes &amp; pads" deviceset="GND" device=""/>
<part name="R2" library="allan" deviceset="RESISTOR" device="-0.332R_1W" value="0.332R"/>
<part name="C1" library="allan" deviceset="CAPACITOR" device="-0.22UF/50V" value="0.22uF"/>
<part name="R8" library="allan" deviceset="RESISTOR" device="-470R/0.1W" value="470R"/>
<part name="R1" library="allan" deviceset="RESISTOR" device="-0.332R_1W" value="0.332R"/>
<part name="D1" library="allan" deviceset="DIODE" device="-SS24"/>
<part name="R9" library="allan" deviceset="RESISTOR" device="-1K_1/8W" value="1K"/>
<part name="R10" library="allan" deviceset="RESISTOR" device="-1K_1/8W" value="1K"/>
<part name="LED1" library="active" deviceset="LED" device="-RED"/>
<part name="LED2" library="active" deviceset="LED" device="-GREEN"/>
<part name="GND2" library="symbols,holes &amp; pads" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="-129.032" y="-91.186"/>
<instance part="IC1" gate="G$1" x="-69.088" y="30.988"/>
<instance part="R4" gate="G$1" x="-30.226" y="48.768" rot="R90"/>
<instance part="R5" gate="G$1" x="-30.226" y="35.814" rot="R90"/>
<instance part="R7" gate="G$1" x="-30.226" y="20.32" rot="R90"/>
<instance part="R6" gate="G$1" x="-39.878" y="25.908" rot="R180"/>
<instance part="R3" gate="G$1" x="-41.148" y="48.768" rot="R90"/>
<instance part="Q1" gate="1" x="-60.198" y="52.832" rot="R90"/>
<instance part="GND1" gate="G$1" x="-64.516" y="1.27"/>
<instance part="R2" gate="G$1" x="-77.978" y="55.372"/>
<instance part="C1" gate="G$1" x="-53.594" y="6.096"/>
<instance part="R8" gate="G$1" x="-59.436" y="11.43"/>
<instance part="R1" gate="G$1" x="-78.232" y="62.738"/>
<instance part="D1" gate="G$1" x="-35.56" y="55.372"/>
<instance part="R9" gate="G$1" x="12.7" y="38.1" rot="R90"/>
<instance part="R10" gate="G$1" x="25.4" y="38.1" rot="R90"/>
<instance part="LED1" gate="G$1" x="12.7" y="25.4"/>
<instance part="LED2" gate="G$1" x="25.4" y="25.4"/>
<instance part="GND2" gate="G$1" x="19.05" y="19.05"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="IC1" gate="G$1" pin="STAT1"/>
<wire x1="-44.958" y1="25.908" x2="-46.228" y2="25.908" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="Q1" gate="1" pin="B"/>
<pinref part="IC1" gate="G$1" pin="DRCE"/>
<wire x1="-60.198" y1="50.292" x2="-60.198" y2="50.038" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PRE-CHG"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-46.228" y1="43.688" x2="-41.148" y2="43.688" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="CE"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-46.228" y1="41.148" x2="-30.226" y2="41.148" width="0.1524" layer="91"/>
<wire x1="-30.226" y1="41.148" x2="-30.226" y2="43.688" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-30.226" y1="41.148" x2="-30.226" y2="40.894" width="0.1524" layer="91"/>
<junction x="-30.226" y="41.148"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-30.226" y1="30.734" x2="-30.226" y2="30.226" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-30.226" y1="30.226" x2="-30.226" y2="25.908" width="0.1524" layer="91"/>
<wire x1="-30.226" y1="25.908" x2="-30.226" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-34.798" y1="25.908" x2="-30.226" y2="25.908" width="0.1524" layer="91"/>
<junction x="-30.226" y="25.908"/>
<pinref part="IC1" gate="G$1" pin="VFB"/>
<wire x1="-46.228" y1="33.528" x2="-39.878" y2="33.528" width="0.1524" layer="91"/>
<wire x1="-39.878" y1="33.528" x2="-39.878" y2="30.226" width="0.1524" layer="91"/>
<wire x1="-39.878" y1="30.226" x2="-30.226" y2="30.226" width="0.1524" layer="91"/>
<junction x="-30.226" y="30.226"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PGOOD"/>
<wire x1="-46.228" y1="18.288" x2="-39.878" y2="18.288" width="0.1524" layer="91"/>
<wire x1="-39.878" y1="18.288" x2="-39.878" y2="15.24" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-39.878" y1="15.24" x2="-30.226" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCHARGE" class="0">
<segment>
<pinref part="Q1" gate="1" pin="C"/>
<wire x1="-55.118" y1="55.372" x2="-52.578" y2="55.372" width="0.1524" layer="91"/>
<wire x1="-52.578" y1="55.372" x2="-46.99" y2="55.372" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-46.99" y1="55.372" x2="-41.148" y2="55.372" width="0.1524" layer="91"/>
<wire x1="-41.148" y1="53.848" x2="-41.148" y2="55.372" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="DRVE"/>
<wire x1="-52.578" y1="50.038" x2="-52.578" y2="55.372" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="-41.148" y1="55.372" x2="-38.1" y2="55.372" width="0.1524" layer="91"/>
<wire x1="-46.99" y1="55.372" x2="-46.99" y2="58.674" width="0.1524" layer="91"/>
<junction x="-46.99" y="55.372"/>
<junction x="-52.578" y="55.372"/>
<junction x="-41.148" y="55.372"/>
<label x="-52.07" y="58.928" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="25.4" y1="50.8" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
<label x="25.4" y="51.054" size="1.778" layer="95"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="Q1" gate="1" pin="E"/>
<wire x1="-72.898" y1="55.372" x2="-71.628" y2="55.372" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="ISNSP"/>
<wire x1="-71.628" y1="55.372" x2="-67.818" y2="55.372" width="0.1524" layer="91"/>
<wire x1="-67.818" y1="55.372" x2="-65.278" y2="55.372" width="0.1524" layer="91"/>
<wire x1="-71.628" y1="50.038" x2="-71.628" y2="55.372" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="IFB"/>
<wire x1="-67.818" y1="50.038" x2="-67.818" y2="55.372" width="0.1524" layer="91"/>
<junction x="-71.628" y="55.372"/>
<junction x="-67.818" y="55.372"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="-73.152" y1="62.738" x2="-71.628" y2="62.738" width="0.1524" layer="91"/>
<wire x1="-71.628" y1="62.738" x2="-71.628" y2="55.372" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VIN" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="ISNSM"/>
<wire x1="-84.328" y1="50.038" x2="-84.328" y2="55.372" width="0.1524" layer="91"/>
<wire x1="-84.328" y1="55.372" x2="-83.058" y2="55.372" width="0.1524" layer="91"/>
<wire x1="-84.328" y1="55.372" x2="-93.98" y2="55.372" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="IN"/>
<wire x1="-93.98" y1="55.372" x2="-101.346" y2="55.372" width="0.1524" layer="91"/>
<wire x1="-90.678" y1="41.148" x2="-93.98" y2="41.148" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="41.148" x2="-93.98" y2="55.372" width="0.1524" layer="91"/>
<junction x="-93.98" y="55.372"/>
<junction x="-84.328" y="55.372"/>
<pinref part="R2" gate="G$1" pin="1"/>
<label x="-101.346" y="55.372" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-84.328" y1="55.372" x2="-84.328" y2="62.738" width="0.1524" layer="91"/>
<wire x1="-84.328" y1="62.738" x2="-83.312" y2="62.738" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="12.7" y1="43.18" x2="12.7" y2="50.8" width="0.1524" layer="91"/>
<label x="12.7" y="51.054" size="1.778" layer="95"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="ISNS"/>
<wire x1="-90.678" y1="34.798" x2="-93.98" y2="34.798" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="34.798" x2="-93.98" y2="28.448" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="BSTOP"/>
<wire x1="-93.98" y1="28.448" x2="-90.678" y2="28.448" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="COMP"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="-65.278" y1="11.938" x2="-65.278" y2="11.43" width="0.1524" layer="91"/>
<wire x1="-65.278" y1="11.43" x2="-64.516" y2="11.43" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-54.356" y1="11.43" x2="-53.594" y2="11.43" width="0.1524" layer="91"/>
<wire x1="-53.594" y1="11.43" x2="-53.594" y2="11.176" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="G$1" pin="GND"/>
<wire x1="-64.516" y1="1.27" x2="-64.516" y2="3.302" width="0.1524" layer="91"/>
<wire x1="-64.516" y1="3.302" x2="-53.594" y2="3.302" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-53.594" y1="3.302" x2="-53.594" y2="3.556" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="-64.516" y1="3.302" x2="-72.898" y2="3.302" width="0.1524" layer="91"/>
<wire x1="-72.898" y1="3.302" x2="-72.898" y2="11.938" width="0.1524" layer="91"/>
<junction x="-64.516" y="3.302"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<wire x1="12.7" y1="22.86" x2="12.7" y2="20.828" width="0.1524" layer="91"/>
<wire x1="12.7" y1="20.828" x2="19.05" y2="20.828" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="19.05" y1="20.828" x2="25.4" y2="20.828" width="0.1524" layer="91"/>
<wire x1="25.4" y1="20.828" x2="25.4" y2="22.86" width="0.1524" layer="91"/>
<pinref part="GND2" gate="G$1" pin="GND"/>
<wire x1="19.05" y1="20.828" x2="19.05" y2="19.05" width="0.1524" layer="91"/>
<junction x="19.05" y="20.828"/>
</segment>
</net>
<net name="VOUT" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-30.226" y1="55.372" x2="-30.226" y2="53.848" width="0.1524" layer="91"/>
<wire x1="-30.226" y1="55.372" x2="-20.32" y2="55.372" width="0.1524" layer="91"/>
<label x="-20.32" y="55.372" size="1.778" layer="95" xref="yes"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="-33.02" y1="55.372" x2="-30.226" y2="55.372" width="0.1524" layer="91"/>
<junction x="-30.226" y="55.372"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="12.7" y1="30.48" x2="12.7" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="25.4" y1="30.48" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<pinref part="LED2" gate="G$1" pin="A"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
